import React, { Component } from "react";
import { Image, View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import LocalStorageService from "../src/soul/services/LocalStorageService";
import { GB3 } from "../src/soul/constants/Variables";
import { minutesDiff } from "../src/soul/assets/reusableFunctions";
import { setUrlMaps } from "../src/soul/actions/storageActions";

class ImageComponent extends Component {
  constructor(props) {
    super(props);
    let uri = this.props.source.uri;
    if (uri) {
      if (!uri.startsWith("file://") && this.props.source.name) {
        let localFile = this.props.urlMaps[this.props.source.name];
        if (localFile && localFile.completed) {
          uri = localFile.localUri;
        } else if (!localFile || minutesDiff(localFile.downloadUpdatedAt) > 5) {
          this.startDowload();
        }
      }
    }
    this.state = {
      uri: uri,
    };
  }

  startDowload = async () => {
    let space = await LocalStorageService.freeCapacity();
    if (this.props.connectionStatus && space >= GB3) {
      let localUri = LocalStorageService.images(this.props.source.name);
      let urlMap = {
        localUri: localUri,
        remoteUri: this.props.source.uri,
        downloadStartedAt: new Date(),
        downloadUpdatedAt: new Date(),
        completed: false,
        name: this.props.source.name,
        downloadPersent: 0,
        dowloading: true,
      };
      LocalStorageService.downloadFile(
        this.props.source.uri,
        localUri,
        this.callback,
        urlMap
      );
      this.props.setUrlMaps({
        ...this.props.urlMaps,
        ...{ [this.props.source.name]: urlMap },
      });
    }
  };

  callback = (downloadProgress, urlMap) => {
    urlMap.downloadPersent =
      (downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite) *
      100;
    urlMap.downloadUpdatedAt = new Date();
    urlMap.completed = urlMap.downloadPersent == 100;
    urlMap.dowloading = !urlMap.completed;
    if (urlMap.completed) {
      this.props.setUrlMaps({
        ...this.props.urlMaps,
        [urlMap.name]: urlMap,
      });
    }
  };

  onError = (error) => {
    console.log("imagecomponent", error);
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.style[1].height != nextProps.style[1].height) {
      return true;
    }
    if (this.props.source.uri != nextProps.source.uri) {
      return true;
    }
    if (this.state.uri != nextState.uri) {
      return true;
    }
    return false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.source.uri != prevProps.source.uri) {
      let uri = this.props.source.uri || "";
      if (!uri.startsWith("file://") && this.props.source.name) {
        let localFile = this.props.urlMaps[this.props.source.name];
        if (localFile && localFile.completed) {
          uri = localFile.localUri;
        } else if (!localFile || minutesDiff(localFile.downloadUpdatedAt) > 5) {
          this.startDowload();
        }
      }
      this.setState({
        uri: uri,
      });
    }
  }

  render() {
    let props = {
      cache: "only-if-cached",
      onError: this.onError,
      resizeMode: this.props.resizeMode,
      source: { uri: this.state.uri },
      style: this.props.style,
    };
    return (
      <View style={styles.container}>
        <Image {...props} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E0E0E0",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  urlMaps: state.storageData.urlMaps,
});

export default connect(mapStateToProps, { setUrlMaps })(ImageComponent);
