import React, { Component, Fragment } from "react";
import { TouchableOpacity, View, StyleSheet, Text } from "react-native";
import PropTypes from "prop-types";
import { Button } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import TabBarIcon from "../src/soul/assets/TabBarIcon";
import { connect } from "react-redux";
import {
  setClassesInfo,
  getClassesInfo,
} from "../src/soul/actions/commonActions";
import { isEmptyArray } from "../src/soul/assets/reusableFunctions";
import FireBase from "../src/soul/assets/FireBase";
import FireBaseCollectionService from "../src/soul/services/FireBaseCollectionService";
import { alertValue, LoadingPage } from "./ReUsableComponents";
import ModalView from "./ModalView";
import ClassesModel from "./ClassesModel";

class ClassesDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classId: this.props.classId,
      loading: false,
      modalVisible: false,
    };
  }

  async componentDidMount() {
    await this.props.getClassesInfo();
    this.getClasses();
  }

  componentDidUpdate() {
    if (isEmptyArray(this.props.classes)) this.getClasses();
  }

  updateLoadingState = (v, func = null) => {
    this.setState(
      {
        loading: v,
      },
      func
    );
  };

  getClasses = () => {
    if (
      isEmptyArray(this.props.classes) &&
      this.props.connectionStatus &&
      !this.state.loading
    ) {
      this.updateLoadingState(true, this.classesCall);
    }
  };

  refreshClasses = () => {
    if (!this.state.loading) {
      this.updateLoadingState(true, this.classesCall);
    }
  };

  classesCall = () => {
    let classesCollection = FireBaseCollectionService.classes();
    FireBase.getCollection(classesCollection).then(async (classes) => {
      if (classes) {
        await this.props.setClassesInfo(classes);
      } else {
        alertValue("Something went wrong");
      }
      this.updateLoadingState(false);
    });
  };

  parseClasses = () => {
    return this.props.classes.map((c) => ({
      label: `${c.name} - ${c.batch.batchName}`,
      value: c.uniqueId,
    }));
  };

  setClasses = (classIds) => {
    if (this.props.multiSelect) {
      this.setState(
        {
          modalVisible: false,
        },
        () => this.props.setClassIds(classIds)
      );
    }
  };

  setClassId = (classId) => {
    if (!this.props.multiSelect && classId != this.state.classId) {
      this.setState(
        {
          classId: classId,
          modalVisible: false,
        },
        () => this.props.setClassId(classId)
      );
    }
  };

  openModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  getHeader = (title, iconName) => {
    return (
      <Fragment>
        <Button
          buttonStyle={{
            backgroundColor: "#20368F",
            height: 30,
          }}
          containerStyle={{
            alignItems: "flex-end",
            paddingHorizontal: 5,
          }}
          titleStyle={{
            fontSize: 10,
            color: "#FFFFFF",
          }}
          icon={
            <TabBarIcon
              name={iconName}
              type={"Ionicons"}
              size={15}
              color="#FFFFFF"
            />
          }
          iconContainerStyle={{
            padding: 5,
          }}
          title={title}
          onPress={this.openModal}
        />
        <TouchableOpacity onPress={this.refreshClasses}>
          <TabBarIcon
            name="refresh"
            type="FontAwesome"
            size={20}
            color="#C1BEBB"
          />
        </TouchableOpacity>
      </Fragment>
    );
  };

  getComponent = () => {
    if (this.props.multiSelect) {
      return (
        <View style={styles.filtersTop}>
          {this.getHeader("Add classes", "ios-add-circle-outline")}
        </View>
      );
    } else {
      return (
        <Fragment>
          <View style={styles.filtersTop}>
            {this.getHeader("Search Classes", "ios-search")}
          </View>
          <View style={styles.filtersItem}>
            <Text style={styles.filterText}>Filters</Text>
          </View>
          <View style={styles.filtersItem}>
            <Dropdown
              label={"Select Class"}
              data={this.parseClasses()}
              value={this.state.classId}
              containerStyle={{ width: this.props.width * 0.8 }}
              onChangeText={this.setClassId}
            />
          </View>
        </Fragment>
      );
    }
  };

  render() {
    return (
      <View style={styles.filters}>
        {this.getComponent()}
        {this.state.loading ? <LoadingPage style={styles.loading} /> : null}
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={styles.modalStyle}
        >
          <ClassesModel
            setClassId={this.setClassId}
            setClasses={this.setClasses}
            multiSelect={this.props.multiSelect}
            selectedClassIds={this.props.selectedClassIds}
          />
        </ModalView>
      </View>
    );
  }
}

ClassesDropDown.propTypes = {
  classId: PropTypes.string,
  setClassId: PropTypes.func,
  classes: PropTypes.array,
  setClassIds: PropTypes.func,
  selectedClassIds: PropTypes.array,
};

ClassesDropDown.defaultProps = {
  classId: "",
  setClassId: () => null,
  setClassIds: () => null,
  classes: [],
  selectedClassIds: [],
};

const styles = StyleSheet.create({
  filters: {
    backgroundColor: "#F4F2F1",
    minHeight: 50,
    margin: 6,
    padding: 5,
    borderRadius: 4,
    justifyContent: "center",
  },
  filtersItem: { alignItems: "center" },
  filtersTop: { flexDirection: "row", justifyContent: "space-between" },
  filterText: { fontWeight: "bold", color: "#20368F" },
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
  modalStyle: {
    alignItems: "center",
    margin: 0,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  classes: state.commonInfo.classes,
});

export default connect(mapStateToProps, {
  setClassesInfo,
  getClassesInfo,
})(ClassesDropDown);
