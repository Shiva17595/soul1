import * as Permissions from "expo-permissions";
import * as MediaLibrary from "expo-media-library";
import { GRANTED } from "../src/soul/constants/Variables";

export default class AppPermissions {
  static checkPermission(permissionType) {
    return new Promise(async (resolve, reject) => {
      try {
        let obj = await Permissions.getAsync(permissionType);
        if (obj.status === GRANTED) {
          resolve();
        } else {
          let obj = await Permissions.askAsync(permissionType);
          if (obj.status === GRANTED) {
            resolve();
          } else {
            reject();
          }
        }
      } catch (error) {
        reject();
      }
    });
  }

  static checkMediaPermission() {
    return new Promise(async (resolve, reject) => {
      try {
        let obj = await MediaLibrary.getPermissionsAsync();
        if (obj.status === GRANTED) {
          resolve();
        } else {
          let obj = await MediaLibrary.requestPermissionsAsync();
          if (obj.status === GRANTED) {
            resolve();
          } else {
            reject();
          }
        }
      } catch (error) {
        reject();
      }
    });
  }
}
