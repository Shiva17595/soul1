import React, { Component } from "react";
import { TouchableOpacity, View, StyleSheet } from "react-native";
import PropTypes from "prop-types";

export default class RadioButton extends Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={StyleSheet.flatten([
          styles.radioButton,
          {
            flexDirection: this.props.flexDirection
          }
        ])}
      >
        <View
          style={StyleSheet.flatten([
            styles.radioCircle,
            {
              borderColor: this.props.color,
              borderRadius: this.props.size * 2
            }
          ])}
        >
          <View
            style={{
              backgroundColor: this.props.checked
                ? this.props.color
                : "#FFFFFF",
              borderRadius: this.props.size,
              padding: this.props.size,
              margin: this.props.size * 0.2
            }}
          />
        </View>
        {this.props.children}
      </TouchableOpacity>
    );
  }
}

RadioButton.propTypes = {
  checked: PropTypes.bool,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  flexDirection: PropTypes.string,
  onPress: PropTypes.func,
  size: PropTypes.number
};

RadioButton.defaultProps = {
  checked: false,
  color: "#20368F",
  flexDirection: "row",
  onPress: () => null,
  size: 10
};

const styles = StyleSheet.create({
  radioButton: {
    alignItems: "center",
    borderBottomWidth: 0.5,
    borderBottomColor: "#C9C9C9",
    margin: 6
  },
  radioCircle: {
    borderWidth: 2,
    opacity: 1,
    marginHorizontal: 4,
    marginBottom: 5
  }
});
