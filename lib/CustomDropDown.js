import React, { PureComponent } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import ModalView from "./ModalView";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class CustomDropDown extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
  }

  getOptions = () => {
    return this.props.data.map(item => (
      <View key={item.value}>
        <Text>{item.lable}</Text>
      </View>
    ));
  };
  openModal = () => {
    this.setState({
      modalVisible: true
    });
  };

  closeModal = () => {
    this.setState({
      modalVisible: false
    });
  };

  render() {
    return (
      <View style={this.props.style}>
        <TouchableOpacity
          onPress={this.openModal}
          activeOpacity={1}
          style={styles.dropDownStyles}
        >
          <View style={styles.dropDownTextStyles}>
            <Text>{this.props.label}</Text>
          </View>
        </TouchableOpacity>
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={StyleSheet.flatten([
            {
              margin: this.props.width * 0.15
            },
            styles.modalStyle
          ])}
          animationType="fade"
        >
          <KeyboardAwareScrollView
            enableOnAndroid
            keyboardShouldPersistTaps="always"
            extraScrollHeight={50}
          >
            <View
              style={StyleSheet.flatten([
                {
                  height:
                    this.props.height * 0.6 < this.props.data.length * 30
                      ? this.props.height * 0.6
                      : this.props.data.length * 30
                },
                styles.options
              ])}
            >
              {this.getOptions()}
            </View>
          </KeyboardAwareScrollView>
        </ModalView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalStyle: {},
  dropDownTextStyles: {},
  dropDownStyles: {
    flexDirection: "row"
  },
  options: {
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center"
  }
});

const mapStateToProps = state => {
  return {
    width: state.commonInfo.width,
    height: state.commonInfo.height
  };
};

export default connect(
  mapStateToProps,
  null
)(CustomDropDown);
