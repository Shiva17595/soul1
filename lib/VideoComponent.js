import React, { Component } from "react";
import VideoPlayer from "expo-video-player";
import { connect } from "react-redux";
import { minutesDiff } from "../src/soul/assets/reusableFunctions";
import { setUrlMaps } from "../src/soul/actions/storageActions";
import { GB3, VIDEO } from "../src/soul/constants/Variables";
import LocalStorageService from "../src/soul/services/LocalStorageService";
import { setVideoId } from "../src/soul/actions/commonActions";

class VideoComponent extends Component {
  constructor(props) {
    super(props);
    let uri = this.props.source.uri;
    if (!uri.startsWith("file://") && this.props.source.name) {
      let localFile = this.props.urlMaps[this.props.source.name];
      if (localFile && localFile.completed) {
        uri = localFile.localUri;
      } else if (
        this.props.downloadFile &&
        (!localFile || minutesDiff(localFile.downloadUpdatedAt) > 5)
      ) {
        this.startDowload();
      }
    }
    this.state = {
      uri: uri,
    };
  }

  startDowload = () => {
    return new Promise(async (resolve, reject) => {
      let space = await LocalStorageService.freeCapacity();
      if (space >= GB3 && this.props.connectionStatus) {
        let localUri = LocalStorageService.videos(this.props.source.name);
        let urlMap = {
          localUri: localUri,
          remoteUri: this.props.source.uri,
          downloadStartedAt: new Date(),
          downloadUpdatedAt: new Date(),
          completed: false,
          name: this.props.source.name,
          downloadPersent: 0,
          dowloading: true,
        };
        LocalStorageService.downloadFile(
          this.props.source.uri,
          localUri,
          this.callback,
          urlMap
        );
        this.props.setUrlMaps({
          ...this.props.urlMaps,
          ...{ [this.props.source.name]: urlMap },
        });
      }
      resolve();
    });
  };

  callback = (downloadProgress, urlMap) => {
    urlMap.downloadPersent =
      (downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite) *
      100;
    urlMap.downloadUpdatedAt = new Date();
    urlMap.completed = urlMap.downloadPersent == 100;
    urlMap.dowloading = !urlMap.completed;
    if (urlMap.completed) {
      this.props.setUrlMaps({
        ...this.props.urlMaps,
        [urlMap.name]: urlMap,
      });
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.shouldPlay != nextState.shouldPlay) {
      return true;
    }
    if (this.props.style.height != nextProps.style.height) {
      return true;
    }
    if (
      this.props.videoId ==
        `${this.props.viewName}-${this.props.source.name}` &&
      nextProps.videoId != `${nextProps.viewName}-${nextProps.source.name}`
    ) {
      return true;
    }
    if (
      this.props.videoId !=
        `${this.props.viewName}-${this.props.source.name}` &&
      nextProps.videoId == `${nextProps.viewName}-${nextProps.source.name}`
    ) {
      return true;
    }
    return false;
  }

  switchToLandscape = () => {
    if (this.props.onPressFullScreen) {
      let file = {
        uri: this.state.uri,
        name: this.props.source.name,
        type: VIDEO,
      };
      this.props.onPressFullScreen(file);
    } else if (this.props.switchToLandscape) {
      this.props.switchToLandscape();
    }
  };

  switchToPortrait = () => {
    if (this.props.switchToPortrait) this.props.switchToPortrait();
  };

  error = (error) => {
    console.log("videocomponent", error);
  };

  playbackCallback = ({ shouldPlay }) => {
    let videoId = `${this.props.viewName}-${this.props.source.name}`;
    if (shouldPlay && this.props.videoId != videoId) {
      console.log("setting the videoId", videoId);
      this.props.setVideoId(videoId);
    }
  };

  shouldPlay = () => {
    return (
      this.props.videoId == `${this.props.viewName}-${this.props.source.name}`
    );
  };

  render() {
    return (
      <VideoPlayer
        videoProps={{
          shouldPlay: this.shouldPlay(),
          resizeMode: this.props.resizeMode,
          source: {
            uri: this.state.uri,
          },
        }}
        width={this.props.style.width}
        height={this.props.style.height}
        inFullscreen={this.props.inFullscreen}
        showControlsOnLoad={true}
        switchToPortrait={this.switchToPortrait}
        switchToLandscape={this.switchToLandscape}
        showFullscreenButton={this.props.showFullscreenButton}
        fadeOutDuration={200}
        errorCallback={this.error}
        playbackCallback={this.playbackCallback}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  urlMaps: state.storageData.urlMaps,
  videoId: state.commonInfo.videoId,
  downloadFileName: state.storageData.downloadFileName,
});

export default connect(mapStateToProps, { setUrlMaps, setVideoId })(
  VideoComponent
);
