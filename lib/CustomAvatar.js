import React, { Component } from "react";
import { connect } from "react-redux";
import { Avatar } from "react-native-elements";
import LocalStorageService from "../src/soul/services/LocalStorageService";
import { setUrlMaps } from "../src/soul/actions/storageActions";
import {
  minutesDiff,
  findExtention,
} from "../src/soul/assets/reusableFunctions";
import { GB3 } from "../src/soul/constants/Variables";

class CustomAvatar extends Component {
  constructor(props) {
    super(props);
    let uri = this.props.source.uri;
    if (uri) {
      if (!uri.startsWith("file://") && this.props.uid) {
        let fileName = `${this.props.uid}.${findExtention(
          this.props.source.uri
        )}`;
        let localFile = this.props.urlMaps[fileName];
        if (localFile && localFile.completed) {
          uri = localFile.localUri;
        } else if (
          this.props.dowload &&
          (!localFile || minutesDiff(localFile.downloadUpdatedAt) > 5)
        ) {
          this.startDowload();
        }
      }
    }
    this.state = {
      uri: uri,
    };
  }

  startDowload = async () => {
    let space = await LocalStorageService.freeCapacity();
    if (this.props.connectionStatus && space >= GB3) {
      let fileName = `${this.props.uid}.${findExtention(
        this.props.source.uri
      )}`;
      let localUri = LocalStorageService.avatar(fileName);
      let urlMap = {
        localUri: localUri,
        remoteUri: this.props.source.uri,
        downloadStartedAt: new Date(),
        downloadUpdatedAt: new Date(),
        completed: false,
        name: fileName,
        downloadPersent: 0,
        dowloading: true,
      };
      LocalStorageService.downloadFile(
        this.props.source.uri,
        localUri,
        this.callback,
        urlMap
      );
      this.props.setUrlMaps({
        ...this.props.urlMaps,
        ...{ [fileName]: urlMap },
      });
    }
  };

  callback = async (downloadProgress, urlMap) => {
    urlMap.downloadPersent =
      (downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite) *
      100;
    urlMap.downloadUpdatedAt = new Date();
    urlMap.completed = urlMap.downloadPersent == 100;
    urlMap.dowloading = !urlMap.completed;
    if (urlMap.completed) {
      this.props.setUrlMaps({
        ...this.props.urlMaps,
        [urlMap.name]: urlMap,
      });
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.source.uri != nextProps.source.uri) {
      return true;
    }
    if (this.props.connectionStatus != nextProps.connectionStatus) {
      return true;
    }
    if (this.state.uri != nextState.uri) {
      return true;
    }
    return false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.source.uri != prevProps.source.uri) {
      this.setState({
        uri: this.props.source.uri,
      });
    }
  }

  render() {
    let props;
    if (this.state.uri) {
      props = { ...{ ...this.props, source: { uri: this.state.uri } } };
    } else {
      props = { ...{ ...this.props, source: require("./group-icon.png") } };
    }
    return <Avatar {...props} />;
  }
}

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  urlMaps: state.storageData.urlMaps,
});
export default connect(mapStateToProps, { setUrlMaps })(CustomAvatar);
