import React, { Component } from "react";
import { TouchableOpacity, View, StyleSheet, FlatList } from "react-native";
import PropTypes from "prop-types";
import { SearchBar, Button } from "react-native-elements";
import { connect } from "react-redux";
import TabBarIcon from "../src/soul/assets/TabBarIcon";
import { filterArray, getKey } from "../src/soul/assets/reusableFunctions";
import ClassItem from "./ClassItem";
import { EmptyComponent } from "./ReUsableComponents";

class ClassesModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      selectedClassIds: this.props.selectedClassIds,
    };
  }

  updateSearch = (search = "") => {
    if (search != this.state.search) {
      this.setState({
        search: search ? search.toLowerCase() : "",
      });
    }
  };

  clearSearch = () => {
    this.updateSearch();
  };

  addOrRemoveClass = (classId, remove) => {
    if (this.props.multiSelect) {
      if (remove) {
        this.setState((prev) => ({
          selectedClassIds: prev.selectedClassIds.filter(
            (cId) => cId != classId
          ),
        }));
      } else {
        this.setState((prev) => {
          prev.selectedClassIds.push(classId);
          return { selectedClassIds: prev.selectedClassIds };
        });
      }
    } else {
      this.props.setClassId(classId);
    }
  };

  renderItem = ({ item }) => {
    return (
      <ClassItem
        {...item}
        onClick={this.addOrRemoveClass}
        multiSelect={this.props.multiSelect}
        selected={this.props.selectedClassIds.includes(item.uniqueId)}
      />
    );
  };

  setClasses = () => {
    this.props.setClasses(this.state.selectedClassIds);
  };

  addClasses = () => {
    return (
      <Button
        buttonStyle={{
          backgroundColor: "#20368F",
          height: 30,
          margin: 5,
        }}
        containerStyle={{
          alignItems: "flex-end",
          paddingHorizontal: 5,
        }}
        titleStyle={{
          fontSize: 10,
          color: "#FFFFFF",
        }}
        icon={
          <TabBarIcon
            name={"add-to-list"}
            type={"Entypo"}
            size={15}
            color="#FFFFFF"
          />
        }
        iconContainerStyle={{
          padding: 5,
        }}
        title="ADD"
        onPress={this.setClasses}
      />
    );
  };

  render() {
    let searchString = this.state.search.replace(/\s+/g, " ").trim();
    return (
      <View
        style={styles.modal(this.props.width * 0.8, this.props.height * 0.8)}
      >
        <SearchBar
          placeholder="Search"
          onChangeText={this.updateSearch}
          value={this.state.search}
          platform="default"
          clearIcon={
            this.state.search ? (
              <TouchableOpacity onPress={this.clearSearch}>
                <TabBarIcon
                  name="cancel"
                  type="MaterialIcons"
                  size={20}
                  color={"#A6A6A6"}
                />
              </TouchableOpacity>
            ) : null
          }
          containerStyle={styles.searchBar(this.props.width * 0.8, 35)}
          inputContainerStyle={styles.seacrhBarInput}
          placeholderTextColor={"#AFAAAA"}
          round
        />
        <View style={{ height: 20 }} />
        {this.props.multiSelect && this.addClasses()}
        <FlatList
          data={filterArray(this.props.classes, searchString, [
            "name",
            ["batch", "batchName"],
          ])}
          renderItem={this.renderItem}
          keyExtractor={getKey}
          ListEmptyComponent={
            <EmptyComponent style={styles.emptyPage} message={"No Results"} />
          }
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }
}

ClassesModel.propTypes = {
  setClasses: PropTypes.func,
  setClassId: PropTypes.func,
  classes: PropTypes.array,
  selectedClassIds: PropTypes.array,
};

ClassesModel.defaultProps = {
  setClassId: () => null,
  setClasses: () => null,
  classes: [],
  selectedClassIds: [],
};

const styles = StyleSheet.create({
  modal: (width, height) => ({
    height: height,
    width: width,
    backgroundColor: "#FFFFFF",
  }),
  emptyPage: {
    height: 100,
    justifyContent: "center",
    alignItems: "center",
  },
  searchBar: (width, height) => ({
    width: width,
    height: height,
    backgroundColor: "#FFFFFF",
    borderWidth: 0,
    shadowColor: "white",
    borderBottomColor: "#FFFFFF",
    borderTopColor: "#FFFFFF",
  }),
  seacrhBarInput: { backgroundColor: "#F4F2F1" },
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  classes: state.commonInfo.classes,
});

export default connect(mapStateToProps, null)(ClassesModel);
