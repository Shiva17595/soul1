import React, { Component } from "react";
import { FlatList, Text, View, StyleSheet } from "react-native";
import PropTypes from "prop-types";

import RadioButton from "./RadioButton";
import TabBarIcon from "../src/soul/assets/TabBarIcon";

class RadioGroup extends Component {
  generateRadioButtons = () => {
    const radios = this.props.radioButtons.map(radioButton => {
      return (
        <RadioButton
          checked={radioButton["checked"]}
          onPress={() => this.props.onPress(radioButton["id"])}
          key={radioButton["lable"]}
        >
          <View style={styles.radioButton}>
            <TabBarIcon
              name={radioButton["iconName"]}
              type={radioButton["iconType"]}
              size={30}
              color="#969696"
            />
            <View style={{ flexDirection: "column" }}>
              <Text style={styles.textStyling}>{radioButton["lable"]}</Text>
              <Text style={styles.bottomTextStyling}>
                {radioButton["lableBottomText"]}
              </Text>
            </View>
          </View>
        </RadioButton>
      );
    });
    return radios;
  };
  render() {
    return <View style={styles.container}>{this.generateRadioButtons()}</View>;
  }
}

RadioGroup.propTypes = {
  labelStyle: PropTypes.object,
  onPress: PropTypes.func,
  radioButtons: PropTypes.array
};

RadioGroup.defaultProps = {
  labelStyle: {},
  onPress: () => null,
  radioButtons: []
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    marginVertical: 10
  },
  radioButton: {
    flexDirection: "row",
    paddingHorizontal: 4,
    paddingBottom: 5
  },
  textStyling: {
    color: "#252525"
  },
  bottomTextStyling: {
    color: "#BCBCBC"
  }
});

export default RadioGroup;
