import React, { Component } from "react";
import Modal from "react-native-modal";

export default class ModalView extends Component {
  render() {
    return <Modal {...this.props}>{this.props.children}</Modal>;
  }
}
