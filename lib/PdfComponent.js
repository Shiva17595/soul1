import React, { Component } from "react";
import { View } from "react-native";
import { alertValue } from "./ReUsableComponents";
import PDFReader from "rn-pdf-reader-js";
import { connect } from "react-redux";
import LocalStorageService from "../src/soul/services/LocalStorageService";
import { GB3, MB1 } from "../src/soul/constants/Variables";
import { minutesDiff } from "../src/soul/assets/reusableFunctions";
import { setUrlMaps } from "../src/soul/actions/storageActions";

class PdfComponent extends Component {
  constructor(props) {
    super(props);
    let uri = this.props.source.uri;
    if (!uri.startsWith("file://") && this.props.source.fileName) {
      let localFile = this.props.urlMaps[this.props.source.fileName];
      if (localFile && localFile.completed) {
        uri = localFile.localUri;
      } else if (!localFile || minutesDiff(localFile.downloadUpdatedAt) > 5) {
        this.startDowload();
      }
    }
    this.state = {
      uri: uri,
    };
  }

  startDowload = async () => {
    let space = await LocalStorageService.freeCapacity();
    if (
      this.props.connectionStatus &&
      space >= GB3 &&
      this.props.source.size < MB1 * 5
    ) {
      let localUri = LocalStorageService.documents(this.props.source.fileName);
      let urlMap = {
        localUri: localUri,
        remoteUri: this.props.source.uri,
        downloadStartedAt: new Date(),
        downloadUpdatedAt: new Date(),
        completed: false,
        name: this.props.source.fileName,
        downloadPersent: 0,
        dowloading: true,
      };
      LocalStorageService.downloadFile(
        this.props.source.uri,
        localUri,
        this.callback,
        urlMap
      );
      this.props.setUrlMaps({
        ...this.props.urlMaps,
        ...{ [this.props.source.fileName]: urlMap },
      });
    }
  };

  callback = (downloadProgress, urlMap) => {
    urlMap.downloadPersent =
      (downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite) *
      100;
    urlMap.downloadUpdatedAt = new Date();
    urlMap.completed = urlMap.downloadPersent == 100;
    urlMap.dowloading = !urlMap.completed;
    if (urlMap.completed) {
      this.props.setUrlMaps({
        ...this.props.urlMaps,
        [urlMap.name]: urlMap,
      });
    }
  };

  error = () => {
    alertValue(
      "Somethig went wrong. Selected document might be not a pdf or unsupported"
    );
  };

  // shouldComponentUpdate() {
  //   return false;
  // }

  render() {
    return (
      <View style={this.props.style}>
        <PDFReader
          source={{
            uri: this.state.uri,
          }}
          onError={this.error}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  urlMaps: state.storageData.urlMaps,
});

export default connect(mapStateToProps, { setUrlMaps })(PdfComponent);
