import React, { Component } from "react";
import { connect } from "react-redux";
import ImageComponent from "./ImageComponent";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import VideoComponent from "./VideoComponent";
import { VIDEO, IMAGE } from "../src/soul/constants/Variables";
import TabBarIcon from "../src/soul/assets/TabBarIcon";

class ImageOrVideo extends Component {
  removeAttachment = () => {
    this.props.removeAttachment(this.props.file.uri, this.props.file.type);
  };

  getAttachmentComponent = () => {
    let props = this.props;
    if (props.file.type == IMAGE) {
      return (
        <View
          style={styles.file(
            props.style.width,
            props.style.height,
            props.style.borderRadius,
            props.style.backgroundColor
          )}
        >
          {props.showOptionsIcon ? (
            <View
              style={{
                alignSelf: "flex-start",
                paddingTop: props.removeButtonPaddingTop || 0,
                padding: props.removeButtonPadding || 10,
                position: "absolute",
                zIndex: 1,
              }}
            >
              <TouchableOpacity onPress={props.onClickOptions}>
                <TabBarIcon
                  styles={{
                    shadowColor: "#000",
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.8,
                  }}
                  size={props.closeIconSize || 20}
                  type="Entypo"
                  name="dots-three-horizontal"
                  color="#FFFFFF"
                />
              </TouchableOpacity>
            </View>
          ) : null}
          {props.needRemoveButton ? (
            <View
              style={{
                alignSelf: "flex-end",
                paddingTop: props.removeButtonPaddingTop || 0,
                padding: props.removeButtonPadding || 10,
                position: "absolute",
                zIndex: 1,
              }}
            >
              <TouchableOpacity onPress={this.removeAttachment}>
                <TabBarIcon
                  styles={{
                    shadowColor: "#000",
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.8,
                  }}
                  size={props.closeIconSize || 20}
                  name="closecircle"
                  type="AntDesign"
                  color="#FFFFFF"
                />
              </TouchableOpacity>
            </View>
          ) : null}
          <ImageComponent
            style={[{ width: props.width }, props.style]}
            source={{
              uri: props.file.uri,
              name: props.file.name,
            }}
            resizeMode={props.resizeMode || "cover"}
          />
        </View>
      );
    } else if (props.file.type == VIDEO) {
      return (
        <View
          style={styles.file(
            props.style.width,
            props.style.height,
            props.style.borderRadius,
            props.style.backgroundColor
          )}
        >
          {props.showOptionsIcon ? (
            <View
              style={{
                alignSelf: "flex-start",
                paddingTop: props.removeButtonPaddingTop || 0,
                padding: props.removeButtonPadding || 10,
                position: "absolute",
                zIndex: 1,
              }}
            >
              <TouchableOpacity onPress={props.onClickOptions}>
                <TabBarIcon
                  styles={{
                    shadowColor: "#000",
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.8,
                  }}
                  size={props.closeIconSize || 20}
                  type="Entypo"
                  name="dots-three-horizontal"
                  color="#FFFFFF"
                />
              </TouchableOpacity>
            </View>
          ) : null}
          {props.needRemoveButton ? (
            <View
              style={{
                alignSelf: "flex-end",
                paddingTop: props.removeButtonPaddingTop || 0,
                padding: props.removeButtonPadding || 10,
                position: "absolute",
                zIndex: 1,
              }}
            >
              <TouchableOpacity onPress={this.removeAttachment}>
                <TabBarIcon
                  styles={{
                    shadowColor: "#000",
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.8,
                  }}
                  size={props.closeIconSize || 20}
                  name="closecircle"
                  type="AntDesign"
                  color="#FFFFFF"
                />
              </TouchableOpacity>
            </View>
          ) : null}
          <VideoComponent
            source={{
              uri: props.file.uri,
              name: props.file.name,
            }}
            resizeMode={props.resizeMode || "cover"}
            style={props.style}
            viewName={this.props.viewName}
            showFullscreenButton={
              props.showFullscreenButton === false ? false : true
            }
            onPressFullScreen={props.onPressFullScreen}
            inFullscreen={props.inFullscreen || false}
            switchToLandscape={props.switchToLandscape}
            switchToPortrait={props.switchToPortrait}
            downloadFile={props.downloadFile || false}
          />
        </View>
      );
    }
    return null;
  };

  render() {
    return this.getAttachmentComponent();
  }
}

const styles = StyleSheet.create({
  attachmentTop: (width, height) => ({
    flex: 1,
    position: "absolute",
    width: width,
    height: height,
  }),
  file: (width, height, borderRadius, backgroundColor) => ({
    width: width,
    height: height,
    overflow: "hidden",
    borderRadius: borderRadius || 0,
    backgroundColor: backgroundColor || "#FFFFFF",
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  width: state.commonInfo.width,
});

export default connect(mapStateToProps, null)(ImageOrVideo);
