import React from "react";
import {
  View,
  ActivityIndicator,
  DatePickerIOS,
  Text,
  TouchableOpacity,
  Alert,
} from "react-native";
import Constants from "expo-constants";
import TabBarIcon from "../src/soul/assets/TabBarIcon";

export const ProgressBar = (props) => {
  return (
    <View>
      {props.showDownloadPersent && (
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text>{props.downloadPersent}%</Text>
        </View>
      )}
      <View
        style={{
          height: 10,
          borderColor: "#000",
          borderWidth: 2,
          borderRadius: 5,
          flexDirection: "row",
          width: props.width,
        }}
      >
        <View
          style={{
            width: (props.width * props.downloadPersent) / 100 - 4,
            backgroundColor: "#3EA4ED",
          }}
        />
        <View
          style={{
            width: (props.width * (100 - props.downloadPersent)) / 100 - 4,
            backgroundColor: "#FFFFFF",
          }}
        />
      </View>
    </View>
  );
};

export const StatusBar = () => {
  return (
    <View
      style={{
        backgroundColor: "#20368F",
        height: Constants.statusBarHeight,
      }}
    />
  );
};

export const EmptyComponent = (props) => {
  return (
    <View style={props.style}>
      <Text
        style={{
          color: "#AFAAAA",
          fontSize: 25,
        }}
      >
        {props.message}
      </Text>
    </View>
  );
};

export const LoadingPage = (props) => {
  return (
    <View style={props.style}>
      <ActivityIndicator
        size={props.loadSpinnerSize ? props.loadSpinnerSize : "large"}
      />
    </View>
  );
};

export function seperator() {
  return <View style={{ height: 8, backgroundColor: "#E0E0E0" }} />;
}

export const DatePicker = (props) => {
  return (
    <View style={props.style}>
      {props.header}
      <DatePickerIOS {...props} />
    </View>
  );
};

export const CustomizeCheckBox = (props) => {
  return (
    <TouchableOpacity onPress={props.onUpdate} disabled={props.disabled}>
      <TabBarIcon
        type="MaterialCommunityIcons"
        name={
          props.selected ? "checkbox-intermediate" : "checkbox-blank-outline"
        }
        size={props.iconSize ? props.iconSize : 20}
        color={props.color ? "#7bb956" : ""}
      />
    </TouchableOpacity>
  );
};

export const alertValue = (value) => {
  Alert.alert(value);
};
