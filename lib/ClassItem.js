import React, { Component } from "react";
import { TouchableOpacity, View, StyleSheet, Text } from "react-native";
import PropTypes from "prop-types";
import { CustomizeCheckBox } from "./ReUsableComponents";
import { connect } from "react-redux";

class ClassItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selected,
    };
  }

  shouldComponentUpdate(prevProps, prevState) {
    return this.state.selected != prevState.selected;
  }

  onPress = () => {
    this.props.onClick(this.props.uniqueId, this.state.selected);
    if (this.props.multiSelect) {
      this.setState((prev) => ({
        selected: !prev.selected,
      }));
    }
  };

  render() {
    return (
      <View style={styles.item(this.props.width * 0.8)}>
        <TouchableOpacity onPress={this.onPress}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {this.props.multiSelect ? (
              <CustomizeCheckBox
                onUpdate={this.onPress}
                selected={this.state.selected}
                iconSize={25}
                color={this.state.selected ? "#7bb956" : ""}
              />
            ) : null}
            <Text
              style={{
                padding: 10,
                fontSize: 18,
                flexWrap: "wrap",
                maxWidth: this.props.width * 0.7,
                fontSize: 12,
              }}
            >
              {`${this.props.name} - ${this.props.batch.batchName}`}
            </Text>
          </View>
          <View style={styles.borderDown} />
        </TouchableOpacity>
      </View>
    );
  }
}

ClassItem.propTypes = {
  setClassId: PropTypes.func,
  multiSelect: PropTypes.bool,
  width: PropTypes.number,
};

ClassItem.defaultProps = {
  onClick: () => null,
  multiSelect: false,
  width: "80%",
};

const styles = StyleSheet.create({
  item: (width) => ({
    width: width,
    height: 50,
  }),
  borderDown: { borderWidth: 0.5, borderColor: "#CFD1CE" },
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(ClassItem);
