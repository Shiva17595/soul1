import React, { Component } from "react";
import { Text, View, TouchableOpacity, Alert } from "react-native";
import { Camera, Permissions } from "expo";
import AppPermissions from "./AppPermissions";

export default class CameraView extends Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back
  };

  _onOpenVideoCamera = () => {
    AppPermissions.checkPermission(Permissions.AUDIO_RECORDING)
      .then(() => {
        if (this.camera) {
          this.camera
            .recordAsync({
              quality: "4:3",
              maxDuration: "3000",
              maxFileSize: 10240
            })
            .then(attactment => {
              this.props.returnAttachment(attactment);
            })
            .catch(() => {
              Alert.alert("Something Went Wrong");
            });
        }
      })
      .catch(() => {
        Alert.alert("Permission needed for recording");
      });
  };

  componentWillMount() {
    AppPermissions.checkPermission(Permissions.CAMERA)
      .then(() => {
        this.setState({ hasCameraPermission: true });
      })
      .catch(() => {
        this.setState({ hasCameraPermission: false });
      });
  }

  closeButton = () => {
    return (
      <View style={{ justifyContent: "flex-start" }}>
        <TouchableOpacity onPress={this.props.closeCamera}>
          <TabBarIcon
            size={50}
            name="ios-close"
            type="Ionicons"
            color="#0F98D1"
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return (
        <View>
          {this.closeButton()}
          <Text>No access to camera</Text>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera
            style={{ flex: 1 }}
            type={this.state.type}
            ref={ref => {
              this.camera = ref;
            }}
          >
            {this.closeButton()}
            <View
              style={{
                flex: 1,
                backgroundColor: "transparent",
                flexDirection: "row"
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 0.1,
                  alignSelf: "flex-end",
                  alignItems: "center"
                }}
                onPress={() => {
                  this.setState({
                    type:
                      this.state.type === Camera.Constants.Type.back
                        ? Camera.Constants.Type.front
                        : Camera.Constants.Type.back
                  });
                }}
              >
                <Text
                  style={{ fontSize: 18, marginBottom: 10, color: "white" }}
                >
                  {" "}
                  Flip{" "}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 0.1,
                  alignSelf: "flex-end",
                  alignItems: "center"
                }}
                onPress={this._onOpenVideoCamera}
              >
                <Text
                  style={{ fontSize: 18, marginBottom: 10, color: "white" }}
                >
                  Video
                </Text>
              </TouchableOpacity>
            </View>
          </Camera>
        </View>
      );
    }
  }
}
