import React, { PureComponent } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import ImageOrVideo from "./ImageOrVideo";
import NavigationService from "../src/soul/actions/NavigationService";
import { connect } from "react-redux";

class MultipleImagesAndVideos extends PureComponent {
  onPress = () => {
    this.props.onPress(this.props.files, this.props.name);
  };

  onPressFullScreen = (file) => {
    NavigationService.navigate("View", { file });
  };

  component = (file, width, height) => {
    return (
      <View style={{ margin: 1 }}>
        <ImageOrVideo
          style={{
            width: width,
            height: height,
            borderRadius: this.props.borderRadius || 0,
          }}
          file={file}
          onPressFullScreen={this.onPressFullScreen}
          viewName={this.props.viewName}
        />
      </View>
    );
  };

  countOne = () => {
    return (
      <View>
        {this.component(
          this.props.files[0],
          this.props.width,
          this.props.height
        )}
      </View>
    );
  };

  countTwo = () => {
    return (
      <View
        style={{
          flexDirection: "row",
        }}
      >
        {this.component(
          this.props.files[0],
          this.props.width / 2,
          this.props.height
        )}
        {this.component(
          this.props.files[1],
          this.props.width / 2,
          this.props.height
        )}
      </View>
    );
  };

  countThree = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
          }}
        >
          {this.component(
            this.props.files[0],
            this.props.width / 2,
            this.props.height / 2
          )}
          {this.component(
            this.props.files[1],
            this.props.width / 2,
            this.props.height / 2
          )}
        </View>
        {this.component(
          this.props.files[2],
          this.props.width,
          this.props.height / 2
        )}
      </View>
    );
  };

  countGreater = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
          }}
        >
          {this.component(
            this.props.files[0],
            this.props.width / 2,
            this.props.height / 2
          )}
          {this.component(
            this.props.files[1],
            this.props.width / 2,
            this.props.height / 2
          )}
        </View>
        <View
          style={{
            flexDirection: "row",
          }}
        >
          {this.component(
            this.props.files[2],
            this.props.width / 2,
            this.props.height / 2
          )}
          <View>
            {this.component(
              this.props.files[3],
              this.props.width / 2,
              this.props.height / 2
            )}
            <View
              style={{
                ...StyleSheet.absoluteFill,
                alignItems: "center",
                justifyContent: "center",
                width: this.props.width / 2,
                height: this.props.height / 2,
              }}
            >
              <Text
                style={{
                  fontSize: 25,
                  color: "#FFFFFF",
                }}
              >
                +{this.props.files.length - 3}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    let itemCount = this.props.files.length;
    let content;
    if (itemCount == 1) {
      content = this.countOne();
      return (
        <View>
          <TouchableOpacity
            onPress={this.onPress}
            style={{
              backgroundColor: "transparent",
            }}
            activeOpacity={1}
          >
            {content}
          </TouchableOpacity>
        </View>
      );
    } else if (itemCount == 2) content = this.countTwo();
    else if (itemCount == 3) content = this.countThree();
    else if (itemCount >= 4) content = this.countGreater();
    return (
      <View>
        {content}
        <TouchableOpacity
          onPress={this.onPress}
          style={{
            ...StyleSheet.absoluteFill,
            width: this.props.width,
            height: this.props.height,
            backgroundColor: "transparent",
          }}
        />
      </View>
    );
  }
}

export default MultipleImagesAndVideos;
