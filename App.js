import React, { Component } from "react";
import { YellowBox, Platform } from "react-native";
import { Provider } from "react-redux";
import { Audio } from "expo-av";
import store from "./src/soul/components/Store.js";
import AppRoot from "./src/soul/components/Root.js";
import FireBase from "./src/soul/assets/FireBase.js";
import _ from "lodash";
import LocalStoragePath from "./src/soul/services/LocalStorageService.js";
import AppPermissions from "./lib/AppPermissions.js";
import * as Permissions from "expo-permissions";
import { Notifications } from "expo";

export default class App extends Component {
  constructor(props) {
    super(props);
    LocalStoragePath.createBaseFolders();
    Audio.setAudioModeAsync({
      playsInSilentModeIOS: true,
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DUCK_OTHERS,
      shouldDuckAndroid: true,
      playThroughEarpieceAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DUCK_OTHERS,
      staysActiveInBackground: true,
    });
    YellowBox.ignoreWarnings(["Setting a timer"]);
    const _console = _.clone(console);
    console.warn = (message) => {
      if (message.indexOf("Setting a timer") <= -1) {
        _console.warn(message);
      }
    };
    try {
      if (!FireBase._appInitialised()) {
        FireBase.init();
      }
    } catch (error) {
      console.log(error);
    }
  }

  initNotifications = () => {
    AppPermissions.checkPermission(Permissions.NOTIFICATIONS).then(async () => {
      const token = await Notifications.getExpoPushTokenAsync();
      console.log(token);
    });
  };

  componentDidMount() {
    this.initNotifications();
  }

  render() {
    return (
      <Provider store={store}>
        <AppRoot />
      </Provider>
    );
  }
}
