import CurrentUser from "./CurrentUserDataService";
import {
  PROFILE_PICTURES_FILES,
  COMMENTS_FILES,
  NEWS_FEED_FILES,
  CHANNELS_FILES,
  EVENTS_FILES,
  ASSIGNMENT_FILES,
} from "../assets/FireBaseStorageName";

class FireBaseStorageService {
  channels = (channelId, fileName) => {
    let basePath = `${this.collegePath()}/${CHANNELS_FILES}/${channelId}`;
    return fileName ? `${basePath}/${fileName}` : basePath;
  };

  events = (fileName) => {
    let basePath = `${this.collegePath()}/${EVENTS_FILES}`;
    return fileName ? `${basePath}/${fileName}` : basePath;
  };

  assignments = (fileName) => {
    let basePath = `${this.collegePath()}/${ASSIGNMENT_FILES}`;
    return fileName ? `${basePath}/${fileName}` : basePath;
  };

  comments = (fileName) => {
    let basePath = `${this.newsFeed()}/${COMMENTS_FILES}`;
    return fileName ? `${basePath}/${fileName}` : basePath;
  };

  newsFeed = (fileName) => {
    let basePath = `${this.collegePath()}/${NEWS_FEED_FILES}`;
    return fileName ? `${basePath}/${fileName}` : basePath;
  };

  profilePictures = (fileName) => {
    let basePath = `${this.collegePath()}/${PROFILE_PICTURES_FILES}`;
    return fileName ? `${basePath}/${fileName}` : basePath;
  };

  collegePath = (fileName) => {
    let collegeCode = CurrentUser.getProp("collegeCode");
    return fileName ? `${collegeCode}/${fileName}` : collegeCode;
  };
}

// create instance
const StoragePaths = new FireBaseStorageService();

// lock instance
Object.freeze(StoragePaths);

export default StoragePaths;
