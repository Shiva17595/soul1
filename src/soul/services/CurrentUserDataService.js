import CommonAsyncStorageService from "./CommonAsyncStorageService";
import { USER_DATA_KEY } from "../constants/Variables";

class CurrentUserData {
  constructor(user = {}) {
    this._key = USER_DATA_KEY;
    this.user = user;
  }

  set = async user => {
    try {
      Object.assign(this.user, user);
      await CommonAsyncStorageService.set(USER_DATA_KEY, user);
    } catch (error) {
      return error;
    }
  };

  get = async () => {
    try {
      const user = await CommonAsyncStorageService.get(USER_DATA_KEY);
      Object.assign(this.user, user ? user : {});
      return user;
    } catch (error) {
      return false;
    }
  };

  getProp = prop => (this.user.hasOwnProperty(prop) && this.user[prop]) || null;
}

// create instance
const CurrentUser = new CurrentUserData();

// lock instance
Object.freeze(CurrentUser);

export default CurrentUser;
