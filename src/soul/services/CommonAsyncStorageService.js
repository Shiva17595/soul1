import { AsyncStorage } from "react-native";

class CommonAsyncStorageService {
  set = async (key, data) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (error) {
      console.log("-------------------------------------------------------");
      console.log(data);
      console.log(error);
      console.log("-------------------------------------------------------");
      return error;
    }
  };

  get = async key => {
    try {
      const data = await AsyncStorage.getItem(key);
      return data && JSON.parse(data);
    } catch (error) {
      console.log("-------------------------------------------------------");
      console.log(key);
      console.log(error);
      console.log("-------------------------------------------------------");
      return false;
    }
  };
}

// create instance
const CommonStorageObject = new CommonAsyncStorageService();

// lock instance
Object.freeze(CommonStorageObject);

export default CommonStorageObject;
