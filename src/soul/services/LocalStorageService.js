import * as FileSystem from "expo-file-system";
import {
  APP_NAME,
  IMAGES,
  MEDIA,
  VIDEOS,
  VIDEO,
  DOCUMENTS,
  AVATAR,
} from "../constants/Variables";

class LocalStorageService {
  freeCapacity = () => {
    return new Promise(async (resolve, reject) => {
      FileSystem.getFreeDiskStorageAsync()
        .then((freeDiskStorage) => {
          resolve(freeDiskStorage);
        })
        .catch(() => {
          resolve(null);
        });
    });
  };

  getFiles = (fileUri) => {
    return new Promise(async (resolve, reject) => {
      FileSystem.readDirectoryAsync(fileUri)
        .then((filesArray) => {
          resolve(filesArray);
        })
        .catch(() => {
          resolve([]);
        });
    });
  };

  deleteFile = (fileUri, options = { idempotent: true }) => {
    return new Promise(async (resolve, reject) => {
      FileSystem.deleteAsync(fileUri, options)
        .then((filesArray) => {
          resolve();
        })
        .catch(() => {
          resolve();
        });
    });
  };

  checkExists = (uri) => {
    return new Promise(async (resolve, reject) => {
      try {
        let res = await FileSystem.getInfoAsync(uri);
        resolve(res.exists);
      } catch (error) {
        resolve(false);
      }
    });
  };

  checkAndCreateFolder = (folder_path) => {
    return new Promise(async (resolve, reject) => {
      this.checkExists(folder_path).then(async (exists) => {
        if (exists) {
          resolve(true);
        } else {
          try {
            await FileSystem.makeDirectoryAsync(folder_path, {
              intermediates: true,
            });
            resolve(true);
          } catch (error) {
            console.log(error);
            resolve(false);
          }
        }
      });
    });
  };

  localFileUri = (file, type) => {
    return new Promise(async (resolve, reject) => {
      if (file.uri.startsWith("file://")) {
        resolve(file.uri);
      } else {
        let path = type == VIDEO ? this.videos() : this.images();
        let uri = `${path}${file.name}`;
        this.checkExists(uri).then((exists) => {
          if (exists) {
            resolve(uri);
          } else {
            this.checkAndCreateFolder(path).then((res) => {
              if (res) {
                this.downloadFile(file.uri, uri);
              }
            });
            resolve(file.uri);
          }
        });
      }
    });
  };

  createBaseFolders = () => {
    let imagesPath = this.images();
    let videosPath = this.videos();
    let documentPath = this.documents();
    let avatar = this.avatar();
    this.checkAndCreateFolder(imagesPath);
    this.checkAndCreateFolder(videosPath);
    this.checkAndCreateFolder(documentPath);
    this.checkAndCreateFolder(avatar);
  };

  basePath = () => {
    return `${FileSystem.documentDirectory}${APP_NAME}/`;
  };

  images = (fileName) => {
    let f = encodeURIComponent(`${APP_NAME} ${IMAGES}`);
    let basePath = `${this.basePath()}${MEDIA}/${f}/`;
    return fileName ? `${basePath}${fileName}` : basePath;
  };

  videos = (fileName) => {
    let f = encodeURIComponent(`${APP_NAME} ${VIDEOS}`);
    let basePath = `${this.basePath()}${MEDIA}/${f}/`;
    return fileName ? `${basePath}${fileName}` : basePath;
  };

  avatar = (fileName) => {
    let f = encodeURIComponent(`${APP_NAME} ${AVATAR}`);
    let basePath = `${this.basePath()}${MEDIA}/${f}/`;
    return fileName ? `${basePath}${fileName}` : basePath;
  };

  documents = (fileName) => {
    let f = encodeURIComponent(`${APP_NAME} ${DOCUMENTS}`);
    let basePath = `${this.basePath()}${MEDIA}/${f}/`;
    return fileName ? `${basePath}${fileName}` : basePath;
  };

  deleteFile = (localUri) => {
    return new Promise(async (resolve, reject) => {
      try {
        FileSystem.deleteAsync(localUri, { idempotent: true });
      } catch {}
      resolve();
    });
  };

  downloadFile(uri, file, callback, options = null) {
    let downloadResumable = FileSystem.createDownloadResumable(
      uri,
      file,
      {},
      (downloadProgress) => {
        callback(downloadProgress, options);
      }
    );
    downloadResumable.downloadAsync();
    return downloadResumable;
  }

  downloadResumable(url, fileUri, options, callback, resumeData) {
    const downloadResumable = new FileSystem.DownloadResumable(
      url,
      fileUri,
      options,
      callback,
      resumeData
    );
    downloadResumable.resumeAsync();
  }
}

// create instance
const LocalStoragePath = new LocalStorageService();

// lock instance
Object.freeze(LocalStoragePath);

export default LocalStoragePath;
