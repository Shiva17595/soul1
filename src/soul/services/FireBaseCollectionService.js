import CurrentUser from "./CurrentUserDataService";
import {
  COLLEGE_COLLECTION,
  BRANCHES_COLLECTION,
  CLASSES_COLLECTION,
  MASTER_COLLECTION,
  STUDENTS_COLLECTION,
  SUBJECTS_COLLECTION,
  NEWS_FEED_COLLECTION,
  COMMENTS_COLLECTION,
  USERS_COLLECTIONS,
  ATTENDENCE_COLLECTION,
  CHANNEL_COLLECTION,
  MESSAGE_COLLECTION,
  DEGREE_CATEGORY_COLLECTION,
  DEGREE_COLLECTION,
  ACADEMICS_COLLECTION,
  EVENTS_COLLECTION,
  ASSIGNMENTS_COLLECTION,
} from "../assets/FireBaseCollections";

class FireBaseCollectionService {
  batches = (batchId) => {
    let basePath = `${this.collegePath()}/${BATCHES_COLLECTION}`;
    if (CurrentUser.getProp("isMaster"))
      return batchId ? `${basePath}/${batchId}` : basePath;
  };

  branches = (brancheId) => {
    let basePath = `${this.collegePath()}/${BRANCHES_COLLECTION}`;
    if (CurrentUser.getProp("isMaster"))
      return brancheId ? `${basePath}/${brancheId}` : basePath;
  };

  masters = (masterId) => {
    let basePath = `${this.collegePath()}/${MASTER_COLLECTION}`;
    return masterId ? `${basePath}/${masterId}` : basePath;
  };

  classes = (classId) => {
    let basePath = `${this.collegePath()}/${CLASSES_COLLECTION}`;
    return classId ? `${basePath}/${classId}` : basePath;
  };

  events = (eventId) => {
    let basePath = `${this.collegePath()}/${EVENTS_COLLECTION}`;
    return eventId ? `${basePath}/${eventId}` : basePath;
  };

  assignments = (assignmentId) => {
    let basePath = `${this.collegePath()}/${ASSIGNMENTS_COLLECTION}`;
    return assignmentId ? `${basePath}/${assignmentId}` : basePath;
  };

  attendence = (classId, attendenceId) => {
    let basePath = `${this.classes(classId)}/${ATTENDENCE_COLLECTION}`;
    return attendenceId ? `${basePath}/${attendenceId}` : basePath;
  };

  academics = (classId, academicId) => {
    let basePath = `${this.classes(classId)}/${ACADEMICS_COLLECTION}`;
    return academicId ? `${basePath}/${academicId}` : basePath;
  };

  studentAcademics = (classId, academicId, studentId) => {
    let basePath = `${this.academics(
      classId,
      academicId
    )}/${STUDENTS_COLLECTION}`;
    return studentId ? `${basePath}/${studentId}` : basePath;
  };

  newsFeed = (postId) => {
    let basePath = `${this.collegePath()}/${NEWS_FEED_COLLECTION}`;
    return postId ? `${basePath}/${postId}` : basePath;
  };

  students = (studentId) => {
    let basePath = `${this.collegePath()}/${STUDENTS_COLLECTION}`;
    return studentId ? `${basePath}/${studentId}` : basePath;
  };

  subjects = (subjectId) => {
    let basePath = `${this.collegePath()}/${SUBJECTS_COLLECTION}`;
    if (CurrentUser.getProp("isMaster"))
      return subjectId ? `${basePath}/${subjectId}` : basePath;
  };

  channels = (channelId) => {
    let basePath = `${this.collegePath()}/${CHANNEL_COLLECTION}`;
    return channelId ? `${basePath}/${channelId}` : basePath;
  };

  degreeCategory = () => {
    return `${this.collegePath()}/${DEGREE_CATEGORY_COLLECTION}`;
  };

  degree = () => {
    return `${this.collegePath()}/${DEGREE_COLLECTION}`;
  };

  messages = (channelId, messageId) => {
    let basePath = `${this.channels(channelId)}/${MESSAGE_COLLECTION}`;
    return messageId ? `${basePath}/${messageId}` : basePath;
  };

  collegePath = () => {
    let collegeCode = CurrentUser.getProp("collegeCode");
    return `${COLLEGE_COLLECTION}/${collegeCode}`;
  };

  comments = (postId, commentId) => {
    let basePath = `${this.newsFeed(postId)}/${COMMENTS_COLLECTION}`;
    return commentId ? `${basePath}/${commentId}` : basePath;
  };

  users = (userId) => {
    let basePath = USERS_COLLECTIONS;
    return userId ? `${basePath}/${userId}` : basePath;
  };
}

// create instance
const CollectionPaths = new FireBaseCollectionService();

// lock instance
Object.freeze(CollectionPaths);

export default CollectionPaths;
