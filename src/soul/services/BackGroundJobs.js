import * as BackgroundFetch from "expo-background-fetch";
import * as TaskManager from "expo-task-manager";
import { Platform } from "react-native";

let list = ["test_task", "new-task", "new1-task"];
const FETCH_TASKNAME = "nhey4-task";
const INTERVAL = 1;

function test() {
  console.log("function is running", Platform.OS);
}

export async function registerFetchTask() {
  TaskManager.defineTask(FETCH_TASKNAME, test);
  BackgroundFetch.unregisterTaskAsync("nhey3-task");
  const status = await BackgroundFetch.getStatusAsync();
  switch (status) {
    case BackgroundFetch.Status.Restricted:
    case BackgroundFetch.Status.Denied:
      console.log("Background execution is disabled");
      return;

    default: {
      console.debug("Background execution allowed", Platform.OS);

      let tasks = await TaskManager.getRegisteredTasksAsync();
      if (tasks.find(f => f.taskName === FETCH_TASKNAME) == null) {
        console.log("Registering task");
        BackgroundFetch.registerTaskAsync(FETCH_TASKNAME, {
          minimumInterval: INTERVAL,
          stopOnTerminate: false,
          startOnBoot: true
        }).then(() => BackgroundFetch.setMinimumIntervalAsync(INTERVAL));

        tasks = await TaskManager.getRegisteredTasksAsync();
        console.debug("Registered tasks", tasks);
      } else {
        console.log(`Task ${FETCH_TASKNAME} already registered, skipping`);
      }

      console.log("Setting interval to", INTERVAL);
      await BackgroundFetch.setMinimumIntervalAsync(INTERVAL);
    }
  }
}
