import { AsyncStorage } from "react-native";
import CurrentUser from "./CurrentUserDataService";
import FireBase from "../assets/FireBase";
import {
  COLLEGE_COLLECTION,
  STUDENTS_COLLECTION,
  MASTER_COLLECTION,
  STUDENT_FIELDS as studentFields,
  MASTER_FIELDS as masterFileds,
} from "../assets/FireBaseCollections";

class UserService {
  signin(params) {
    return new Promise((resolve, reject) => {
      try {
        let { email, password } = params;
        FireBase.authentication(email, password)
          .then((response) => {
            this.getuserInfo(response.user.uid)
              .then(async (userData) => {
                await CurrentUser.set(userData);
                resolve(userData);
              })
              .catch((error) => {
                reject();
              });
          })
          .catch((error) => {
            reject();
          });
      } catch (error) {
        reject();
      }
    });
  }

  loginStatus() {
    return new Promise((resolve, reject) => {
      try {
        FireBase.onAuthStateChanged()
          .then((user) => {
            if (user) {
              this.getuserInfo(user.uid)
                .then(async (userData) => {
                  await CurrentUser.set(userData);
                  resolve(userData);
                })
                .catch((error) => {
                  AsyncStorage.clear();
                  reject(false);
                });
            } else {
              reject(false);
            }
          })
          .catch((error) => {
            AsyncStorage.clear();
            reject(false);
          });
      } catch (error) {
        AsyncStorage.clear();
        reject(false);
      }
    });
  }

  getUserCollectionInfo() {}

  getuserInfo(doc, logOutIfError = true) {
    return new Promise(async (resolve, reject) => {
      try {
        let userData = await FireBase.getDocumentInfo("users", doc);
        let userPrivateInfo = await FireBase.getDocumentInfo(
          "users",
          `${doc}/userSecureData/userPrivateInfo`
        );

        const user = {};
        user.avatar = userData.avatar;
        user.biography = userData.biography;
        user.contactNumber = userData.contactNumber;
        user.profilePictures = userData.profilePictures;
        user.collegeCode = userPrivateInfo.collegeCode;
        user.email = userPrivateInfo.email;
        user.uid = userPrivateInfo.uid;
        user.userName = userData.userName;
        if (userPrivateInfo.isStudent) {
          let userCollegeInfoPath = `${userPrivateInfo.collegeCode}/${STUDENTS_COLLECTION}/${doc}`;
          let userCollegeInfo = await FireBase.getDocumentInfo(
            COLLEGE_COLLECTION,
            userCollegeInfoPath
          );
          user.isStudent = userPrivateInfo.isStudent;
          user.rollNo = userCollegeInfo[studentFields.rollNo];
          user.classInfo = userCollegeInfo[studentFields.classInfo];
          user.presentYear = userCollegeInfo[studentFields.presentYear];
          user.branchInfo = userCollegeInfo[studentFields.branchInfo];
          user.attendenceInfo = userCollegeInfo[studentFields.attendenceInfo];
          user.name = userCollegeInfo[studentFields.name];
        } else if (userPrivateInfo.isMaster) {
          let userCollegeInfoPath = `${userPrivateInfo.collegeCode}/${MASTER_COLLECTION}/${doc}`;

          user.isMaster = userPrivateInfo.isMaster;
          let userCollegeInfo = await FireBase.getDocumentInfo(
            COLLEGE_COLLECTION,
            userCollegeInfoPath
          );
          user.classes = userCollegeInfo[masterFileds.classes];
          user.classInfo = userCollegeInfo[masterFileds.classInfo];
          user.qualification = userCollegeInfo.qualification;
          user.schedule = userCollegeInfo.schedule;
          user.subjects = userCollegeInfo.subjects;
          user.name = userCollegeInfo[masterFileds.name];
        }
        resolve(user);
      } catch (error) {
        if (logOutIfError) this.logout();
        reject();
      }
    });
  }

  createUserIfNotExists(params) {
    return new Promise(async (resolve, reject) => {
      try {
        await FireBase._createUser(params)
          .then((response) => {
            resolve();
          })
          .catch((error) => {
            resolve();
          });
      } catch (error) {}
    });
  }

  resetPassword(email) {
    //need to implement
    return false;
  }

  logout() {
    return new Promise((resolve, reject) => {
      FireBase.logout().then(AsyncStorage.clear().then(resolve)).catch(resolve);
    });
  }
}

// create instance
const User = new UserService();

// lock instance
Object.freeze(User);

export default User;
