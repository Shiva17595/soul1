import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import CreatePostHeader from "./CreatePostHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import TabBarIcon from "../../../assets/TabBarIcon";
import BottomSheetPanel from "./BottomSheetPanel";
import ModalView from "../../../../../lib/ModalView";
import AttachmentsComponents from "./AttachmentsComponents";
import { setPendingPostItems } from "../../../actions/newsFeedActions";
import NavigationService from "../../../actions/NavigationService";
import { isEmptyHash } from "../../../assets/reusableFunctions";
import PdfComponent from "../../../../../lib/PdfComponent";
import { Button } from "react-native-elements";

class CreatePostScreen extends Component {
  bottomSheetRef = React.createRef();

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = params.headerTitle;
    let headerTitleStyle = {
      color: "white",
    };
    let headerStyle = {
      backgroundColor: "#20368F",
    };
    let headerRight = (
      <TouchableOpacity
        style={styles.headerPost}
        onPress={() => params.onPost()}
      >
        <Text
          style={{
            color: "#FFFFFF",
          }}
        >
          {params.option}
        </Text>
      </TouchableOpacity>
    );
    let headerLeft = (
      <TouchableOpacity
        style={styles.headerPost}
        onPress={() => params.goBack()}
      >
        <Text
          style={{
            color: "#FFFFFF",
          }}
        >
          Close
        </Text>
      </TouchableOpacity>
    );

    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerRight,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this._isMounted = true;
    let initialPost = {
      content: "",
      files: [],
      security: 0,
      document: {},
    };
    this.postToEdit = this.props.navigation.getParam("postToEdit", initialPost);
    this.state = {
      content: this.postToEdit.content,
      postTextFontSize: 22,
      modalVisible: false,
      localattachments: [],
      files: [...this.postToEdit.files],
      security: this.postToEdit.security,
      document: { ...this.postToEdit.document },
    };
  }

  componentDidMount() {
    if (!this.postToEdit.uniqueId) {
      this.props.navigation.setParams({
        onPost: this.onPost,
        goBack: this.goBack,
        creatingPost: false,
        headerTitle: "Create Post",
        option: "Post",
      });
    } else {
      this.props.navigation.setParams({
        onPost: this.onPost,
        goBack: this.goBack,
        creatingPost: false,
        headerTitle: "Update Post",
        option: "Save",
      });
    }
  }

  goBack = () => {
    this.props.navigation.navigate("Main");
  };

  postCondition = () => {
    return (
      (this.state.content && this.postToEdit.content != this.state.content) ||
      this.state.localattachments.length ||
      this.state.document.uri != this.postToEdit.document.uri ||
      (this.postToEdit.files.length != this.state.files.length &&
        this.state.files.length) ||
      this.postToEdit.security != this.state.security
    );
  };

  onPost = async () => {
    if (this.postCondition()) {
      let postDocument = {
        content: this.state.content,
        files: this.state.files,
        user: {
          uid: this.props.user.uid,
          name: this.props.user.name,
          avatar: this.props.user.avatar,
        },
        classInfo: this.props.user.classInfo,
        uploadedDate: new Date().toString(),
        uniqueId: `${
          this.postToEdit.uniqueId
        }-${new Date().getTime().toString()}`,
        localattachments: this.state.localattachments,
        userName: this.props.user.userName,
        security: this.state.security,
        avatar: this.props.user.avatar,
        edituniqueId: this.postToEdit.uniqueId,
        document: this.state.document,
      };

      let posts = [...this.props.pendingPosts];
      posts.push(postDocument);
      await this.props.setPendingPostItems(posts);
      NavigationService.navigate("NewsFeed");
    }
  };

  onClickSecurity = () => {
    this.props.navigation.push("PostSecurity", {
      security: this.state.security,
    });
  };

  onTextInputChange = (value) => {
    let postTextFontSize = 22;
    if (value.length > 30) {
      postTextFontSize = 15;
    }
    this.setState({
      content: value,
      postTextFontSize: postTextFontSize,
    });
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  addAttachment = (attachment) => {
    if (attachment) {
      this.setState((prev) => ({
        document: {},
        localattachments: prev.localattachments.concat(attachment),
        modalVisible: false,
      }));
    }
  };

  getAttachmentsComponent = () => {
    if (!isEmptyHash(this.state.document)) {
      return (
        <PdfComponent
          style={styles.document(this.props.width, this.props.height)}
          source={this.state.document}
        />
      );
    } else if (
      this.state.localattachments.length != 0 ||
      this.state.files.length != 0
    ) {
      return (
        <AttachmentsComponents
          attachments={this.state.localattachments.concat(this.state.files)}
          removeAttachment={this.removeAttachment}
        />
      );
    }
  };

  addDocument = (document) => {
    this.setState({
      document: document,
      localattachments: [],
      files: [],
      modalVisible: false,
    });
  };

  removeAttachment = (attactmentUri) => {
    this.setState((state, props) => ({
      localattachments: state.localattachments.filter(
        (value) => value.uri != attactmentUri
      ),
      files: state.files.filter((value) => value.uri != attactmentUri),
    }));
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate() {
    let security = this.props.navigation.getParam("security");
    if (security == undefined) security = null;
    if (security != null && security != this.state.security) {
      this._isMounted && this.setState({ security: security });
    }
  }

  openModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          enableOnAndroid
          keyboardShouldPersistTaps="always"
          extraScrollHeight={50}
        >
          <CreatePostHeader
            security={this.state.security}
            onClickSecurity={this.onClickSecurity}
          />
          <Button
            buttonStyle={{
              backgroundColor: "#20368F",
            }}
            containerStyle={{
              alignItems: "flex-end",
              height: 40,
            }}
            titleStyle={{
              fontSize: 10,
            }}
            icon={
              <TabBarIcon name="attachment" type="MaterialIcons" size={20} />
            }
            iconContainerStyle={{
              padding: 10,
            }}
            title="Add Attachments"
            onPress={this.openModal}
          />
          <TextInput
            style={styles.inputText(this.state.postTextFontSize)}
            onChangeText={this.onTextInputChange}
            value={this.state.content}
            multiline={true}
            maxLength={1000}
            spellCheck={true}
            placeholder="Want to share some knowledge?"
          />
          <View>{this.getAttachmentsComponent()}</View>
          <ModalView
            isVisible={this.state.modalVisible}
            onBackButtonPress={this.closeModal}
            onBackdropPress={this.closeModal}
            style={styles.modalStyle}
          >
            <BottomSheetPanel
              close={this.closeModal}
              addAttachment={this.addAttachment}
              addDocument={this.addDocument}
            />
          </ModalView>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  headerPost: {
    backgroundColor: "#20368F",
    margin: 5,
    padding: 10,
    borderRadius: 5,
    fontSize: 15,
  },
  inputText: (fontSize) => ({
    fontSize: fontSize,
  }),
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    backgroundColor: "#3E3E3E",
  },
  document: (width, height) => ({
    flex: 1,
    width: width,
    height: height,
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  pendingPosts: state.newsFeeds.pendingPosts,
});

export default connect(mapStateToProps, {
  setPendingPostItems,
})(CreatePostScreen);
