import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import {
  SECURITY_TEXT,
  SECURITY_ICON_NAME,
  SECURITY_ICON_TYPE,
} from "../../../constants/Variables";
import TabBarIcon from "../../../assets/TabBarIcon";
import { connect } from "react-redux";
import CustomAvatar from "../../../../../lib/CustomAvatar";

class CreatePostHeader extends Component {
  render() {
    return (
      <View style={styles.createPostHeader}>
        <View style={styles.profilePic}>
          <CustomAvatar
            rounded
            source={{ uri: this.props.user.avatar }}
            uid={this.props.user.uid}
          />
        </View>
        <View style={styles.createPostTitle}>
          <Text style={styles.userName}>{this.props.user.userName}</Text>
          <TouchableOpacity
            style={styles.privacyButtton}
            onPress={this.props.onClickSecurity}
          >
            <View style={styles.privacyButttonContent}>
              <View style={{ paddingHorizontal: 4 }}>
                <TabBarIcon
                  name={SECURITY_ICON_NAME[this.props.security]}
                  type={SECURITY_ICON_TYPE[this.props.security]}
                  size={10}
                />
              </View>
              <View>
                <Text
                  style={{
                    color: "#919090",
                    fontSize: 10,
                    paddingTop: 2,
                  }}
                >
                  {SECURITY_TEXT[this.props.security]}
                </Text>
              </View>
              <View style={{ paddingHorizontal: 4 }}>
                <TabBarIcon
                  name="md-arrow-dropdown"
                  type="Ionicons"
                  size={13}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  createPostHeader: {
    flexDirection: "row",
    padding: 15,
  },
  profilePic: {
    justifyContent: "center",
    alignItems: "center",
  },
  createPostTitle: {
    fontSize: 17,
    color: "#3c3c3c",
    paddingHorizontal: 12,
  },
  privacyButtton: {
    height: 20,
    backgroundColor: "#FFFFFF",
    borderWidth: 1,
    borderColor: "#919090",
    borderRadius: 5,
  },
  privacyButttonContent: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  userName: {
    color: "#252525",
    fontSize: 12,
    fontWeight: "bold",
    paddingBottom: 6,
  },
});

const mapStateToProps = (state) => ({
  user: state.commonInfo.user,
});

export default connect(mapStateToProps, null)(CreatePostHeader);
