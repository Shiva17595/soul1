import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ActivityIndicator
} from "react-native";

import {
  SECURITY_TEXT,
  SECURITY_ICON_NAME,
  SECURITY_ICON_TYPE,
  SECURITY_BOTTOM_TEXT
} from "../../../constants/Variables";
import RadioGroup from "../../../../../lib/RadioGroup";

export default class PostSecurityScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Select Privacy";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerRight = (
      <TouchableOpacity
        style={styles.headerPost}
        onPress={() => params.onDone()}
      >
        <Text style={{ color: "#FFFFFF" }}>Done</Text>
      </TouchableOpacity>
    );

    let headerLeft = (
      <TouchableOpacity
        style={styles.headerPost}
        onPress={() => params.goBack()}
      >
        <Text style={{ color: "#FFFFFF" }}>Close</Text>
      </TouchableOpacity>
    );

    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerRight,
      headerLeft
    };
  };

  constructor(props) {
    super(props);

    let radios = [];
    let security = this.props.navigation.getParam("security", "0");
    for (let i = 0; i < 3; i++) {
      let radioDetail = {};
      radioDetail["id"] = i;
      radioDetail["iconName"] = SECURITY_ICON_NAME[i];
      radioDetail["iconType"] = SECURITY_ICON_TYPE[i];
      radioDetail["lable"] = SECURITY_TEXT[i];
      radioDetail["lableBottomText"] = SECURITY_BOTTOM_TEXT[i];
      if (security == i) {
        radioDetail["checked"] = true;
      } else {
        radioDetail["checked"] = false;
      }
      radios.push(radioDetail);
    }

    this.state = {
      security: security,
      radioButtons: radios
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onDone: this.onDone,
      goBack: this.goBack
    });
  }

  goBack = () => {
    this.props.navigation.navigate("CreatePost", {
      security: this.props.navigation.getParam("security", "0")
    });
  };

  onDone = () => {
    this.props.navigation.navigate("CreatePost", {
      security: this.state.security
    });
  };

  securityStateSet = securityType => {
    const radioButtons = this.state.radioButtons;
    radioButtons.find(e => e.checked).checked = false;
    radioButtons.find(e => e.id === securityType).checked = true;
    this.setState({ radioButtons: radioButtons, security: securityType });
  };

  render() {
    if (this.state.radioButtons.length != 0) {
      return (
        <View style={styles.container}>
          <View style={{ marginVertical: 10, marginHorizontal: 5 }}>
            <Text style={styles.securityHeader}>
              With whom do you want to Share the post?
            </Text>
          </View>
          <View style={{ marginVertical: 10, marginHorizontal: 5 }}>
            <Text style={styles.securitySubTitle}>
              Your post will appear in News Feed of people Based on your choice
            </Text>
          </View>
          <RadioGroup
            onPress={this.securityStateSet}
            radioButtons={this.state.radioButtons}
            selectedRadio={this.state.security}
          />
        </View>
      );
    } else {
      return <ActivityIndicator size="large" />;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  headerPost: {
    color: "white",
    backgroundColor: "#20368F",
    margin: 5,
    padding: 10,
    borderRadius: 5,
    fontSize: 15
  },
  securityHeader: {
    color: "#1F1A1B",
    fontSize: 15,
    fontWeight: "bold"
  },
  securitySubTitle: {
    color: "#A9A2A3",
    fontSize: 15
  }
});
