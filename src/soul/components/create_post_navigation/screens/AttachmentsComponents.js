import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import ImageOrVideo from "../../../../../lib/ImageOrVideo";
import { findHeight } from "../../../assets/reusableFunctions";
import { VIDEO } from "../../../constants/Variables";

class AttachmentsComponents extends Component {
  render() {
    let attachmentComponents = this.props.attachments.map((attachment) => {
      return (
        <View style={styles.attachment} key={attachment.uri}>
          <ImageOrVideo
            style={styles.attachmentStyle(
              this.props.width - 10,
              attachment.type == VIDEO
                ? 500
                : findHeight(
                    attachment.width,
                    this.props.width - 10,
                    attachment.height
                  )
            )}
            file={attachment}
            removeAttachment={this.props.removeAttachment}
            needRemoveButton={true}
            closeIconSize={30}
            resizeMode={attachment.type == VIDEO ? "cover" : "contain"}
            showFullscreenButton={false}
          />
        </View>
      );
    });
    return <View>{attachmentComponents}</View>;
  }
}

const styles = StyleSheet.create({
  attachment: {
    marginBottom: 10,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
  },
  attachmentTop: {
    alignSelf: "flex-end",
  },
  attachmentStyle: (width, height) => ({
    flex: 1,
    width: width,
    height: height > 500 ? 500 : height,
  }),
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(AttachmentsComponents);
