import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Alert } from "react-native";
import TabBarIcon from "../../../assets/TabBarIcon";
import { connect } from "react-redux";
import {
  _onOpenCamera,
  _onChoosePic,
  _onChooseDocument,
} from "../../../assets/reusableFunctions";

class BottomSheetPanel extends Component {
  openCamera = async () => {
    let attachment = await _onOpenCamera();
    if (attachment) this.props.addAttachment(attachment);
  };

  choosePic = async () => {
    let attachment = await _onChoosePic();
    if (attachment) this.props.addAttachment(attachment);
  };

  chooseDocument = async () => {
    let attachment = await _onChooseDocument();
    if (attachment) this.props.addDocument(attachment);
  };

  render() {
    return (
      <View style={styles.bottomSheetPanelContainer(this.props.height)}>
        <View style={styles.headerComponent}>
          <View style={styles.panelHeader}>
            <TouchableOpacity onPress={this.props.close}>
              <View style={styles.panelHandle} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.panelComponent}>
          <TouchableOpacity onPress={this.choosePic}>
            <View style={styles.panelComponentItems}>
              <TabBarIcon
                name="ios-images"
                type="Ionicons"
                styles={styles.itemIcon}
                color="#6CD502"
              />
              <Text style={styles.itemText}>Photo/Video</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.panelComponent}>
          <TouchableOpacity onPress={this.chooseDocument}>
            <View style={styles.panelComponentItems}>
              <TabBarIcon
                name="ios-document"
                type="Ionicons"
                styles={styles.itemIcon}
                color="#335EE9"
              />
              <Text style={styles.itemText}>Document</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.panelComponent}>
          <TouchableOpacity onPress={this.openCamera}>
            <View style={styles.panelComponentItems}>
              <TabBarIcon
                name="camera"
                type="Entypo"
                styles={styles.itemIcon}
                color="#0F98D1"
              />
              <Text style={styles.itemText}>Camera</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomSheetPanelContainer: (height) => ({
    margin: 0,
    height: height / 2,
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  }),
  headerComponent: {
    paddingTop: 20,
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
  },
  panelComponent: {
    borderBottomWidth: 0.5,
    borderBottomColor: "#3E3E3E",
    padding: 10,
  },
  panelComponentItems: {
    flexDirection: "row",
    alignItems: "center",
  },
  itemIcon: {
    padding: 5,
  },
  itemText: {
    paddingLeft: 5,
  },
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(BottomSheetPanel);
