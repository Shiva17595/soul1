import React from "react";
import { createAppContainer, createStackNavigator } from "react-navigation";
import CreatePostScreen from "./screens/CreatePostScreen";
import PostSecurityScreen from "./screens/PostSecurityScreen";

export default createAppContainer(
  createStackNavigator(
    {
      CreatePost: CreatePostScreen,
      PostSecurity: PostSecurityScreen
    },
    {
      initialRouteName: "CreatePost"
    }
  )
);
