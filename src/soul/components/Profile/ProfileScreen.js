import React, { Component, Fragment } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList,
  Alert,
  Image,
} from "react-native";
import { connect } from "react-redux";
import ImageOrVideo from "../../../../lib/ImageOrVideo";
import FireBaseCollectionService from "../../services/FireBaseCollectionService";
import {
  EmptyComponent,
  alertValue,
  LoadingPage,
  seperator,
} from "../../../../lib/ReUsableComponents";
import FireBase from "../../assets/FireBase";
import {
  IMAGE,
  componentNames,
  LIMIT,
  ERROR_MESSAGE,
  CONNECTION_ERROR,
} from "../../constants/Variables";
import User from "../../services/UserService";
import { Button, Card, Avatar } from "react-native-elements";
import TabBarIcon from "../../assets/TabBarIcon";
import { isEmptyHash, getKey } from "../../assets/reusableFunctions";
import { setVideoId, userInfo } from "../../actions/commonActions";
import PostItem from "../bottom_navigation/screens/posts/PostItem";
import { NEWS_FEED_FIELDS } from "../../assets/FireBaseCollections";
import { setChannels } from "../../actions/chatActions";
import NavigationService from "../../actions/NavigationService";
import { NavigationActions } from "react-navigation";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import AppPermissions from "../../../../lib/AppPermissions";
import FireBaseStorageService from "../../services/FireBaseStorageService";
import { connectActionSheet } from "@expo/react-native-action-sheet";

class ProfileScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = params.headerTitle;
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.userInfoParams = this.props.navigation.getParam("userInfo", {});
    this.state = {
      loading: true,
      userInfo: {},
      userPosts: [],
      userClassPosts: [],
      loadedAllPosts: false,
      error: false,
      changingAvatar: false,
    };
    this.isUnmounted = false;
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  goBack = () => {
    this.props.navigation.dispatch(NavigationActions.back());
  };

  getFilters(userId, security) {
    let filters = {};
    if (security)
      filters[NEWS_FEED_FIELDS.security] = {
        type: "==",
        value: security,
      };
    if (userId) {
      filters[`${NEWS_FEED_FIELDS.user}.uid`] = {
        type: "==",
        value: userId,
      };
    }
    return filters;
  }

  getOrders() {
    let orders = {};
    orders[NEWS_FEED_FIELDS.uploadedDate] = "desc";
    return orders;
  }

  getUserPosts = () => {
    let newsFeedCollection = FireBaseCollectionService.newsFeed();
    let promises = [];
    if (this.state.userInfo.uid == this.props.user.uid) {
      promises.push(
        FireBase.getCollection(
          newsFeedCollection,
          this.getOrders(),
          null,
          this.getFilters(this.props.user.uid)
        )
      );
    } else {
      promises.push(
        FireBase.getCollection(
          newsFeedCollection,
          this.getOrders(),
          null,
          this.getFilters(this.state.userInfo.uid, 0)
        )
      );
      if (
        this.state.userInfo.classInfo.classId ==
        this.props.user.classInfo.classId
      )
        promises.push(
          FireBase.getCollection(
            newsFeedCollection,
            this.getOrders(),
            null,
            this.getFilters(this.state.userInfo.uid, 1)
          )
        );
    }
    Promise.all(promises)
      .then((results) => {
        let [userPosts = [], userClassPosts = []] = results;
        !this.isUnmounted &&
          this.setState({
            loading: false,
            userPosts: userPosts,
            userClassPosts: userClassPosts,
            loadedAllPosts:
              userPosts.length < LIMIT && userClassPosts.length < LIMIT,
          });
      })
      .catch(() => {
        !this.isUnmounted &&
          this.setState({
            loading: false,
            error: true,
          });
      });
  };

  getUserInfo = async () => {
    if (this.props.connectionStatus) {
      let userInfo;
      if (this.userInfoParams.uid != this.props.user.uid) {
        try {
          userInfo = await User.getuserInfo(this.userInfoParams.uid, false);
        } catch {
          alertValue(ERROR_MESSAGE);
          this.goBack();
        }
      } else {
        userInfo = { ...this.props.user };
      }
      !this.isUnmounted &&
        this.setState(
          {
            userInfo: userInfo,
          },
          this.getUserPosts
        );
    }
  };

  componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
      headerTitle: this.userInfoParams.userName
        ? `${this.userInfoParams.userName}'s Profile`
        : `${this.userInfoParams.name}'s Profile`,
    });
    this.getUserInfo();
  }

  getInfo = (title, value) => {
    return (
      <View
        style={{
          flexDirection: "row",
          padding: 5,
        }}
      >
        <Text
          style={{
            flex: 2,
            fontWeight: "bold",
            fontSize: 16,
            flexWrap: "wrap",
          }}
        >
          {title}
        </Text>
        <Text
          style={{
            flex: 4,
            fontSize: 16,
            flexWrap: "wrap",
          }}
        >
          {value}
        </Text>
      </View>
    );
  };

  editAvatar = async (avatar) => {
    if (
      this.props.connectionStatus &&
      this.state.userInfo.uid == this.props.user.uid
    ) {
      !this.isUnmounted &&
        this.setState((prev) => ({
          changingAvatar: true,
          userInfo: { ...prev.userInfo, avatar: avatar },
        }));
      try {
        let remoteUri = "";
        if (avatar) {
          remoteUri = await FireBase.uploadFile(
            avatar,
            FireBaseStorageService.profilePictures(
              `${this.props.user.uid}-${new Date().getTime().toString()}`
            )
          );
          if (!remoteUri) {
            throw "Cannot Upload Image";
          }
        }
        let collection = FireBaseCollectionService.users();
        let userData = {
          avatar: remoteUri,
        };
        FireBase.updateDocumentInfo(collection, this.props.user.uid, userData)
          .then(() => {
            this.props.userInfo({ ...this.props.user, ...userData });
            !this.isUnmounted &&
              this.setState({
                changingAvatar: false,
              });
          })
          .catch((error) => {
            alertValue("Something went wrong, Please try again.");
            !this.isUnmounted &&
              this.setState({
                changingAvatar: false,
              });
          });
      } catch (error) {
        alertValue(ERROR_MESSAGE);
      }
    } else {
      alertValue(CONNECTION_ERROR);
    }
  };

  changeProfilePic = (buttonIndex) => {
    if (buttonIndex == 1) {
      AppPermissions.checkPermission(Permissions.CAMERA_ROLL)
        .then(async () => {
          const attactment = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 0.1,
          });
          if (!attactment.cancelled) {
            if (attactment.type == IMAGE) {
              this.editAvatar(attactment.uri);
            } else {
              alertValue("File type is not supported");
            }
          }
        })
        .catch(() => {
          alertValue("Camera Permission is needed");
        });
    } else if (buttonIndex == 0) {
      AppPermissions.checkPermission(Permissions.CAMERA)
        .then(async () => {
          const attactment = await ImagePicker.launchCameraAsync();
          if (!attactment.cancelled) {
            if (attactment.type == IMAGE) {
              this.editAvatar(attactment.uri);
            } else {
              Alert.alert("File type is not supported");
            }
          }
        })
        .catch(() => {
          Alert.alert("Camera Permission is needed");
        });
    } else if (buttonIndex == 2) {
      Alert.alert(
        "Remove Photo",
        "Confirm",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "Remove",
            onPress: () => {
              this.editAvatar("");
            },
          },
        ],
        {
          cancelable: false,
        }
      );
    }
  };

  chooseProfilePic = () => {
    const options = ["Take Photo", "Choose Photo", "Remove Photo", "Cancel"];
    const cancelButtonIndex = 3;
    const destructiveButtonIndex = 2;
    this.props.showActionSheetWithOptions(
      {
        options,
        destructiveButtonIndex,
        cancelButtonIndex,
      },
      this.changeProfilePic
    );
  };

  viewFile = () => {
    NavigationService.navigate("View", {
      file: { uri: this.state.userInfo.avatar, type: IMAGE },
    });
  };

  profilePic = () => {
    return (
      <Fragment>
        {this.state.userInfo.avatar ? (
          <Fragment>
            <ImageOrVideo
              style={styles.imageStyle(this.props.width)}
              file={{ uri: this.state.userInfo.avatar, type: IMAGE }}
            />
            <TouchableOpacity
              onPress={this.viewFile}
              style={[
                {
                  ...StyleSheet.absoluteFill,
                },
                styles.imageStyle(this.props.width),
              ]}
            ></TouchableOpacity>
          </Fragment>
        ) : (
          <Image
            source={require("./group-large-icon.jpg")}
            style={styles.imageStyle(this.props.width)}
          />
        )}
        {this.state.userInfo.uid == this.props.user.uid ? (
          <View style={styles.image(this.props.width)}>
            <TouchableOpacity
              onPress={this.chooseProfilePic}
              disabled={this.state.changingAvatar}
            >
              <Avatar
                rounded
                icon={{ tyep: "Entypo", name: "camera" }}
                iconStyle={{ color: "#20368F" }}
                size={35}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        {this.state.changingAvatar ? (
          <LoadingPage style={styles.spinner(this.props.width)} />
        ) : null}
      </Fragment>
    );
  };

  studentProfileInfo = () => {
    return (
      <View style={{ paddingVertical: 6 }}>
        <Card containerStyle={styles.cardContainerStyle}>
          {this.getInfo("Full Name", this.state.userInfo.name)}
          {this.getInfo("Display Name", this.state.userInfo.userName)}
          {this.getInfo("Email", this.state.userInfo.email)}
          {this.getInfo("Roll No", this.state.userInfo.rollNo)}
          {this.getInfo("Role", "Student")}
          {this.getInfo("About", this.state.userInfo.biography)}
          {this.getInfo("Branch", this.state.userInfo.branchInfo.branchName)}
          {this.getInfo("Class", this.state.userInfo.classInfo.className)}
        </Card>
      </View>
    );
  };

  masterProfileInfo = () => {
    return (
      <View style={{ paddingVertical: 6 }}>
        <Card containerStyle={styles.cardContainerStyle}>
          {this.getInfo("Full Name", this.state.userInfo.name)}
          {this.getInfo("Display Name", this.state.userInfo.userName)}
          {this.getInfo("Email", this.state.userInfo.email)}
          {this.getInfo("Role", "Professor")}
          {this.getInfo("About", this.state.userInfo.biography)}
          {this.getInfo("Class", this.state.userInfo.classInfo.className)}
        </Card>
      </View>
    );
  };

  openChannel = () => {
    let channelId =
      this.props.user.uid > this.state.userInfo.uid
        ? `${this.state.userInfo.uid}${this.props.user.uid}`
        : `${this.props.user.uid}${this.state.userInfo.uid}`;
    let memberIds = [this.props.user.uid];
    if (this.state.userInfo.uid != this.props.user.uid)
      memberIds.push(this.state.userInfo.uid);
    let channelDoc = this.props.channels.find((c) => c.uniqueId == channelId);
    if (!channelDoc) {
      channelDoc = {
        active: false,
        isGroup: false,
        memberIds: memberIds,
        members: {
          [this.state.userInfo.uid]: {
            name: this.state.userInfo.name,
            avatar: this.state.userInfo.avatar,
            subTitle: this.state.userInfo.isMaster
              ? this.state.userInfo.classInfo.className
              : this.state.userInfo.rollNo,
          },
          [this.props.user.uid]: {
            name: this.props.user.name,
            avatar: this.props.user.avatar,
            subTitle: this.props.user.isMaster
              ? this.props.user.classInfo.className
              : this.props.user.rollNo,
          },
        },
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      let channels = [...this.props.channels];
      this.props.setChannels([
        ...channels,
        { ...channelDoc, ...{ uniqueId: channelId } },
      ]);
    }
    NavigationService.push("ChatScreen", channelId, { channelId });
  };

  editProfile = () => {
    if (this.props.user.uid == this.state.userInfo.uid) {
      NavigationService.navigate("EditUserProfile");
    }
  };

  profile = () => {
    return (
      <Fragment>
        {this.profilePic()}
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            height: 60,
            width: this.props.width,
          }}
        >
          <Button
            buttonStyle={{
              backgroundColor: "#20368F",
            }}
            containerStyle={{
              flex: 0.5,
              justifyContent: "center",
              padding: 5,
              height: 60,
            }}
            icon={
              <TabBarIcon
                type="Entypo"
                name="message"
                size={20}
                color="#FFFFFF"
              />
            }
            iconContainerStyle={{
              margin: 5,
            }}
            title="Message"
            onPress={this.openChannel}
          />
          {this.state.userInfo.uid == this.props.user.uid ? (
            <Button
              buttonStyle={{
                backgroundColor: "#20368F",
              }}
              containerStyle={{
                flex: 0.5,
                justifyContent: "center",
                padding: 5,
                height: 60,
              }}
              icon={
                <TabBarIcon
                  type="Feather"
                  name="edit"
                  size={20}
                  color="#FFFFFF"
                />
              }
              iconContainerStyle={{
                margin: 5,
              }}
              title="Edit Profile"
              onPress={this.editProfile}
            />
          ) : null}
        </View>
        {this.state.userInfo.isStudent
          ? this.studentProfileInfo()
          : this.masterProfileInfo()}
      </Fragment>
    );
  };

  renderFooter = () => {
    return this.state.loading ? <LoadingPage /> : null;
  };

  getPost = async (postId) => {
    if (postId) {
      try {
        let newsFeedCollection = FireBaseCollectionService.newsFeed();
        return await FireBase.getDocument(newsFeedCollection, postId);
      } catch {}
    }
    return;
  };

  getNewPosts = async () => {
    let newsFeedCollection = FireBaseCollectionService.newsFeed();
    let promises = [];
    if (this.state.userInfo.uid == this.props.user.uid) {
      let post = await this.getPost(
        this.state.userPosts[this.state.userPosts.length - 1].uniqueId
      );
      promises.push(
        FireBase.getCollection(
          newsFeedCollection,
          this.getOrders(),
          post,
          this.getFilters(this.props.user.uid)
        )
      );
    } else {
      let post = await this.getPost(
        this.state.userPosts[this.state.userPosts.length - 1].uniqueId
      );
      promises.push(
        FireBase.getCollection(
          newsFeedCollection,
          this.getOrders(),
          post,
          this.getFilters(this.state.userInfo.uid, 0)
        )
      );
      if (
        this.state.userInfo.classInfo.classId ==
        this.props.user.classInfo.classId
      ) {
        console.log(this.state.userClassPosts);
        let post = this.state.userClassPosts[
          this.state.userClassPosts.length - 1
        ];
        let classPost;
        if (post) {
          classPost = await this.getPost(post.uniqueId);
        }
        promises.push(
          FireBase.getCollection(
            newsFeedCollection,
            this.getOrders(),
            classPost,
            this.getFilters(this.state.userInfo.uid, 1)
          )
        );
      }
    }
    Promise.all(promises)
      .then((results) => {
        let [userPosts = [], userClassPosts = []] = results;
        !this.isUnmounted &&
          this.setState((prev) => ({
            loading: false,
            userPosts: prev.userPosts.concat(userPosts),
            userClassPosts: prev.userClassPosts.concat(userClassPosts),
            loadedAllPosts:
              userPosts.length < LIMIT && userClassPosts.length < LIMIT,
          }));
      })
      .catch(() => {
        !this.isUnmounted &&
          this.setState({
            loading: false,
            error: true,
          });
      });
  };

  loadMorePosts = () => {
    if (!this.state.loadedAllPosts && !this.state.loading) {
      !this.isUnmounted &&
        this.setState(
          {
            loading: true,
          },
          this.getNewPosts
        );
    }
  };

  renderPosts = (post) => {
    return (
      <PostItem
        post={post.item}
        navigation={this.props.navigation}
        removePost={this.removePost}
        viewProfileDisabled={true}
        viewName={componentNames.userProfile}
      />
    );
  };

  removePost = (postId) => {
    !this.isUnmounted &&
      this.setState((prev) => ({
        userPosts: prev.userPosts.filter((item) => item.uniqueId != postId),
      }));
  };

  getVisiableArray = ({ viewableItems }) => {
    if (
      this.props.viewName == componentNames.userProfile &&
      this.props.videoId
    ) {
      let videoItem = viewableItems[0] ? viewableItems[0].item.files[0] : null;
      if (
        !videoItem ||
        this.props.videoId != `${componentNames.userProfile}-${videoItem.name}`
      ) {
        console.log("test");
        this.props.setVideoId(null);
      }
    }
  };

  getPosts = () => {
    return this.state.userPosts
      .concat(this.state.userClassPosts)
      .sort((p1, p2) => (p1.uploadedDate > p2.uploadedDate ? -1 : 1));
  };

  render() {
    if (isEmptyHash(this.state.userInfo) && !this.props.connectionStatus) {
      return (
        <EmptyComponent
          style={styles.loading}
          message={"Please check you internet connection and try again"}
        />
      );
    }
    if (isEmptyHash(this.state.userInfo) && this.state.loading) {
      return <LoadingPage style={styles.loading} />;
    }
    return (
      <View style={styles.container}>
        <FlatList
          data={this.getPosts()}
          renderItem={this.renderPosts}
          keyExtractor={getKey}
          ItemSeparatorComponent={seperator}
          ListHeaderComponent={this.profile()}
          onEndReached={this.loadMorePosts}
          onEndReachedThreshold={0.1}
          ListFooterComponent={this.renderFooter}
          onViewableItemsChanged={this.getVisiableArray}
          viewabilityConfig={{
            minimumViewTime: 1000,
            itemVisiblePercentThreshold: 70,
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  imageStyle: (width) => ({
    height: 300,
    width: width,
  }),
  image: (width) => ({
    flex: 1,
    position: "absolute",
    padding: 10,
    alignItems: "flex-end",
    justifyContent: "flex-end",
    width: width,
    height: 300,
  }),
  spinner: (width) => ({
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    width: width,
    height: 300,
  }),
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  cardContainerStyle: {
    padding: 0,
    borderRadius: 10,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  videoId: state.commonInfo.videoId,
  channels: state.chatData.channels,
  viewName: state.commonInfo.viewName,
});

const ProfileScreenWithActionSheet = connectActionSheet(ProfileScreen);

export default connect(mapStateToProps, { setVideoId, setChannels, userInfo })(
  ProfileScreenWithActionSheet
);
