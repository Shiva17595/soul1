import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import FireBaseCollectionService from "../../services/FireBaseCollectionService";
import { alertValue, LoadingPage } from "../../../../lib/ReUsableComponents";
import FireBase from "../../assets/FireBase";
import { userInfo } from "../../actions/commonActions";

class EditProfileScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Edit Profile";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );

    let headerRight = (
      <TouchableOpacity
        onPress={params.onSubmit}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Submit</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
      headerRight,
    };
  };

  constructor(props) {
    super(props);
    this.isUnmounted = false;
    this.state = {
      displayName: this.props.user.userName,
      about: this.props.user.biography,
      submitting: false,
    };
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
      onSubmit: this.onSubmit,
    });
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  onDisplayNameChange = (value) => {
    !this.isUnmounted &&
      this.setState({
        displayName: value,
      });
  };

  onAboutChange = (value) => {
    !this.isUnmounted &&
      this.setState({
        about: value,
      });
  };

  validateFields = () => {
    console.log(this.props.user.userName);
    console.log(this.props.user.biography);
    console.log(this.state.displayName);
    console.log(this.state.about);
    console.log(this.props.user.userName != this.state.displayName.trim());
    console.log(this.props.user.biography != this.state.about.trim());

    if (!this.state.displayName.trim() || !this.state.about.trim()) {
      alertValue("All fields are mandatory");
      return false;
    } else if (
      this.props.user.userName != this.state.displayName.trim() ||
      this.props.user.biography != this.state.about.trim()
    ) {
      console.log("pppppppppppppppp");
      return true;
    }
    return false;
  };

  onSubmit = () => {
    if (this.validateFields() && this.props.connectionStatus) {
      !this.isUnmounted &&
        this.setState(
          {
            submitting: true,
          },
          this.updateProfile
        );
    } else if (!this.props.connectionStatus) {
      alertValue("Please check you insert connection and try again");
    }
  };

  updateProfile = () => {
    let collection = FireBaseCollectionService.users();
    let userData = {
      userName: this.state.displayName,
      biography: this.state.about,
    };
    FireBase.updateDocumentInfo(collection, this.props.user.uid, userData)
      .then(() => {
        this.props.userInfo({ ...this.props.user, ...userData });
        !this.isUnmounted &&
          this.setState(
            {
              submitting: false,
            },
            this.goBack
          );
      })
      .catch(() => {
        alertValue("Something went wrong, Please try again.");
        !this.isUnmounted &&
          this.setState({
            submitting: false,
          });
      });
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View
          style={{
            padding: 5,
          }}
        >
          <View style={{ paddingVertical: 5 }}>
            <Text style={{ fontWeight: "bold", paddingVertical: 8 }}>
              Display Name
            </Text>
            <View style={styles.inputBox(this.props.width - 10)}>
              <TextInput
                multiline={false}
                onChangeText={this.onDisplayNameChange}
                style={{ fontSize: 18, padding: 10 }}
                value={this.state.displayName}
                maxLength={25}
                spellCheck={true}
                placeholder="Display Name"
              />
            </View>
          </View>
          <View style={{ paddingVertical: 5 }}>
            <Text style={{ fontWeight: "bold", paddingVertical: 8 }}>
              About
            </Text>
            <View
              style={styles.inputBox(
                this.props.width - 10,
                this.props.height * 0.3
              )}
            >
              <TextInput
                multiline={true}
                onChangeText={this.onAboutChange}
                style={{ fontSize: 18, padding: 10 }}
                value={this.state.about}
                maxLength={1000}
                spellCheck={true}
                placeholder="Tell us something about you."
              />
            </View>
          </View>
        </View>
        {this.state.submitting ? <LoadingPage style={styles.loading} /> : null}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
  inputBox: (width, height) => ({
    borderWidth: 1.5,
    borderColor: "#E8E7E7",
    borderRadius: 10,
    width: width,
    height: height ? height : 40,
  }),
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, { userInfo })(EditProfileScreen);
