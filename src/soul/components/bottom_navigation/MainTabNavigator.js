import React from "react";
import {
  createStackNavigator,
  createBottomTabNavigator,
} from "react-navigation";
import NewsFeedScreen from "./screens/NewsFeed/NewsFeedScreen";
import MenuScreen from "./screens/Menu/MenuScreen";
import TabBarIcon from "../../assets/TabBarIcon";
import PostScreen from "./screens/NewsFeed/PostScreen";
import ScheduleTopTabView from "./screens/ScheduleAndAttendence/ScheduleTopTabView";
import { StatusBar } from "../../../../lib/ReUsableComponents";
import ChatTopTabView from "./screens/Chat/ChatTopTabView";
import ChatScreen from "./screens/Chat/Channel/ChatScreen";
import PdfView from "./screens/Chat/Channel/PdfView";
import ChannelInfo from "./screens/Chat/Channel/ChannelInfo";
import CreateGroupScreen from "./screens/Chat/List/CreateGroupScreen";
import CustomAvatar from "../../../../lib/CustomAvatar";
import CurrentUser from "../../services/CurrentUserDataService";
import ModifyGroupScreen from "./screens/Chat/List/ModifyGroupScreen";
import NotificationScreen from "./screens/Notification/NotificationScreen";
import StudentsScreen from "./screens/Menu/StudentsScreen";
import MastersScreen from "./screens/Menu/MastersScreen";
import CoursesScreen from "./screens/Menu/CoursesScreen";
import StudentAcademicsScreen from "./screens/Menu/Academics/StudentAcademicsScreen";
import AcademicsScreen from "./screens/Menu/Academics/AcademicsScreen";
import StudentResultScreen from "./screens/Menu/Academics/StudentResultScreen";
import DownloadsScreen from "./screens/Menu/Downloads/DownloadsScreen";
import EventsScreen from "./screens/Menu/Events/EventsScreen";
import CreateEventScreen from "./screens/Menu/Events/CreateEventScreen";
import MyEventsScreen from "./screens/Menu/Events/MyEventsScreen";
import ViewEventScreen from "./screens/Menu/Events/ViewEventScreen";
import CreateAssignmentScreen from "./screens/Menu/Assignments/CreateAssignmentScreen";
import StudentAssignmentsScreen from "./screens/Menu/Assignments/StudentAssignmentsScreen";
import MasterAssignmentsScreen from "./screens/Menu/Assignments/MasterAssignmentsScreen";

const ChatStack = createStackNavigator({
  Chat: {
    screen: ChatTopTabView,
    navigationOptions: ({ navigation }) => {
      header = <StatusBar />;
      return { header };
    },
  },
  ChatScreen: ChatScreen,
  ChannelInfo: ChannelInfo,
  CreateGroup: CreateGroupScreen,
  ModifyGroup: ModifyGroupScreen,
  PdfView: PdfView,
});

ChatStack.navigationOptions = ({ navigation }) => {
  const { params } = navigation.state;
  let tabBarLabel = "Chat";
  let tabBarIcon = (
    <TabBarIcon focused={navigation.isFocused()} name="chat" type="Entypo" />
  );
  const { routeName } = navigation.state.routes[navigation.state.index];
  let tabBarVisible = true;
  if (routeName !== "Chat") {
    tabBarVisible = false;
  }
  let tabBarOptions = {
    keyboardHidesTabBar: true,
    activeTintColor: "#20368F",
  };
  if (params.onTabFocus) params.onTabFocus(null);
  return {
    tabBarVisible,
    tabBarLabel,
    tabBarIcon,
    tabBarOptions,
  };
};

const NewsFeedStack = createStackNavigator({
  NewsFeed: NewsFeedScreen,
  Post: PostScreen,
});

NewsFeedStack.navigationOptions = ({ navigation }) => {
  const { params } = navigation.state;
  tabBarLabel = "NewsFeed";
  tabBarIcon = ({ focused }) => (
    <TabBarIcon focused={focused} name="news" type="Entypo" />
  );
  let tabBarOptions = {
    keyboardHidesTabBar: true,
    activeTintColor: "#20368F",
  };
  if (params.onTabFocus) params.onTabFocus(null);
  return {
    tabBarLabel,
    tabBarIcon,
    tabBarOptions,
  };
};

const ScheduleStack = createStackNavigator({
  Schedule: {
    screen: ScheduleTopTabView,
    navigationOptions: ({ navigation }) => {
      header = <StatusBar />;
      return { header };
    },
  },
});

ScheduleStack.navigationOptions = ({ navigation }) => {
  const { params } = navigation.state;
  tabBarLabel = "Schedule";
  tabBarIcon = ({ focused }) => (
    <TabBarIcon focused={focused} name="calendar" type="AntDesign" />
  );
  let tabBarOptions = {
    keyboardHidesTabBar: true,
    activeTintColor: "#20368F",
  };
  if (params.onTabFocus) params.onTabFocus(null);
  return {
    tabBarLabel,
    tabBarIcon,
    tabBarOptions,
  };
};

const NotificationStack = createStackNavigator({
  Notification: NotificationScreen,
});

NotificationStack.navigationOptions = ({ navigation }) => {
  const { params } = navigation.state;
  tabBarLabel = "Notification";
  tabBarIcon = ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name="notifications-none"
      type="MaterialIcons"
    />
  );
  let tabBarOptions = {
    keyboardHidesTabBar: true,
    activeTintColor: "#20368F",
  };
  if (params.onTabFocus) params.onTabFocus(null);
  return {
    tabBarLabel,
    tabBarIcon,
    tabBarOptions,
  };
};

const MenuStack = createStackNavigator({
  Menu: MenuScreen,
  Students: StudentsScreen,
  Masters: MastersScreen,
  Events: EventsScreen,
  CreateEvent: CreateEventScreen,
  MyEvents: MyEventsScreen,
  MasterAssignments: MasterAssignmentsScreen,
  StudentAssignments: StudentAssignmentsScreen,
  CreateAssignment: CreateAssignmentScreen,
  ViewEvent: ViewEventScreen,
  PrivacySettings: MenuScreen,
  StudentAcademics: StudentAcademicsScreen,
  MasterAcademics: AcademicsScreen,
  StudentResult: StudentResultScreen,
  Downloads: DownloadsScreen,
  Courses: CoursesScreen,
});

MenuStack.navigationOptions = ({ navigation }) => {
  const { params } = navigation.state;
  tabBarLabel = "Menu";
  tabBarIcon = ({ focused }) => (
    <CustomAvatar
      rounded
      uid={CurrentUser.getProp("uid")}
      source={{ uri: CurrentUser.getProp("avatar") }}
      size={22}
      containerStyle={focused ? { borderColor: "#20368F", borderWidth: 2 } : {}}
    />
  );
  let tabBarOptions = {
    keyboardHidesTabBar: true,
    activeTintColor: "#20368F",
  };
  if (params.onTabFocus) params.onTabFocus(null);
  return {
    tabBarLabel,
    tabBarIcon,
    tabBarOptions,
  };
};

export default createBottomTabNavigator(
  {
    NewsFeedStack,
    ScheduleStack,
    ChatStack: {
      screen: ChatStack,
      navigationOptions: ({ navigation }) => {},
    },
    NotificationStack,
    MenuStack,
  },
  {
    initialRouteName: "MenuStack",
  }
);
