import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Card } from "react-native-elements";
import { connect } from "react-redux";
import TabBarIcon from "../../../../assets/TabBarIcon";

class MenuCard extends Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        activeOpacity={0.6}
        onPress={this.props.onPress}
      >
        <Card
          containerStyle={styles.cardContainerStyle}
          wrapperStyle={styles.wrapperStyle}
        >
          <View style={styles.wrapperContent}>
            <View style={styles.icon}>
              <TabBarIcon
                type={this.props.iconType}
                name={this.props.iconName}
                size={30}
                color={this.props.iconColor || "#A6A2A1"}
              />
            </View>
            <View style={styles.textWrap}>
              <Text style={styles.text}>{this.props.optionText}</Text>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffff"
  },
  cardContainerStyle: {
    height: 50,
    padding: 0,
    borderRadius: 10
  },
  wrapperStyle: {
    height: 50
  },
  wrapperContent: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center"
  },
  icon: {
    flex: 1,
    paddingHorizontal: 8,
    justifyContent: "center"
  },
  textWrap: {
    flex: 9,
    justifyContent: "flex-start"
  },
  text: {
    fontWeight: "bold"
  }
});

const mapStateToProps = state => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height
});

export default connect(mapStateToProps, null)(MenuCard);
