import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import { EmptyComponent } from "../../../../../../../lib/ReUsableComponents";
import { isEmptyHash, getDate } from "../../../../../assets/reusableFunctions";
import DownloadComponent from "./DownloadComponent";

class DownloadsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Downloads";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.isUnmounted = false;
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
    });
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  renderComponent = (value) => {
    return <DownloadComponent {...value.item} />;
  };

  getDownloads = () => {
    return Object.values(this.props.downloadUrlMaps).sort((a, b) =>
      getDate(a.downloadStartedAt) > getDate(b.downloadStartedAt) ? -1 : 1
    );
  };

  render() {
    if (isEmptyHash(this.props.downloadUrlMaps)) {
      return (
        <EmptyComponent
          style={styles.empty}
          message="Downloads list is empty"
        />
      );
    } else {
      return (
        <View style={styles.container}>
          <FlatList
            data={this.getDownloads()}
            renderItem={this.renderComponent}
            keyExtractor={(item) => item.name}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  downloadUrlMaps: state.storageData.downloadUrlMaps,
});

export default connect(mapStateToProps, null)(DownloadsScreen);
