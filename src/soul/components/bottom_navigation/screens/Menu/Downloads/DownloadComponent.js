import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, Text, View, Alert } from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import { setDownloadUrlMaps } from "../../../../../actions/storageActions";
import {
  minutesDiff,
  dateConvertion,
  shareFile,
} from "../../../../../assets/reusableFunctions";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import LocalStorageService from "../../../../../services/LocalStorageService";
import * as MediaLibrary from "expo-media-library";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import { CONNECTION_ERROR, DOCUMENT } from "../../../../../constants/Variables";
import NavigationService from "../../../../../actions/NavigationService";

class DownloadComponent extends Component {
  shouldComponentUpdate(nextProps) {
    if (this.props.downloadPersent != nextProps.downloadPersent) return true;
    return false;
  }

  callback = async (downloadProgress, urlMap) => {
    urlMap.downloadPersent = Math.round(
      (downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite) *
        100
    );
    urlMap.downloadUpdatedAt = new Date();
    urlMap.completed = urlMap.downloadPersent == 100;
    urlMap.dowloading = !urlMap.completed;
    if (urlMap.completed) {
      try {
        await MediaLibrary.createAssetAsync(urlMap.localUri);
      } catch (error) {
        urlMap.failed = true;
      }
    }
    this.props.setDownloadUrlMaps({
      ...this.props.downloadUrlMaps,
      ...{ [urlMap.name]: urlMap },
    });
  };

  reStartDownload = () => {
    if (this.props.connectionStatus) {
      let urlMap = {
        localUri: this.props.localUri,
        remoteUri: this.props.remoteUri,
        downloadStartedAt: new Date(),
        downloadUpdatedAt: new Date(),
        completed: false,
        name: this.props.name,
        downloadPersent: 0,
        dowloading: true,
      };
      LocalStorageService.downloadFile(
        this.props.remoteUri,
        this.props.localUri,
        this.callback,
        urlMap
      );
      this.props.setDownloadUrlMaps({
        ...this.props.downloadUrlMaps,
        ...{ [this.props.name]: urlMap },
      });
    } else {
      alertValue(CONNECTION_ERROR);
    }
  };

  showFile = () => {
    let params = {
      document: {
        uri: this.props.localUri,
        type: this.props.type,
      },
      saveFileEnabled: false,
    };
    if (this.props.type == DOCUMENT) {
      NavigationService.navigate("ViewPdf", params);
    } else {
      NavigationService.navigate("View", params);
    }
  };

  share = () => {
    shareFile(this.props.localUri);
  };

  removeDownload = () => {
    Alert.alert(
      "Remove Download",
      "Confirm",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Remove",
          onPress: this.remove,
        },
      ],
      {
        cancelable: false,
      }
    );
  };

  remove = () => {
    LocalStorageService.deleteFile(this.props.localUri);
    let newDownloadUrls = {};
    Object.keys(this.props.downloadUrlMaps).forEach((name) => {
      if (name != this.props.name)
        newDownloadUrls[name] = { ...this.props.downloadUrlMaps[name] };
    });
    this.props.setDownloadUrlMaps(newDownloadUrls);
  };

  getProgressBar = () => {
    if (this.props.completed) {
      return (
        <View
          style={{
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={this.showFile}
            activeOpacity={0.5}
          >
            <Text style={styles.buttonText}>View</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={this.share}
            activeOpacity={0.5}
          >
            <Text style={styles.buttonText}>Share</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={this.removeDownload}
            activeOpacity={0.5}
          >
            <Text style={styles.buttonText}>Remove</Text>
          </TouchableOpacity>
        </View>
      );
    } else if (minutesDiff(this.props.downloadUpdatedAt) > 5) {
      return (
        <TouchableOpacity onPress={this.reStartDownload}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ padding: 5 }}>
              <TabBarIcon
                size={20}
                name="refresh"
                type="MaterialIcons"
                color="#d6524d"
              />
            </View>
            <Text style={{ color: "#d6524d", padding: 5 }}>retry download</Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      let widthPersent = this.props.width * 0.8;
      return (
        <View>
          <View style={styles.persent}>
            <Text>{this.props.downloadPersent}%</Text>
          </View>
          <View style={styles.progressBar(widthPersent)}>
            <View
              style={{
                width: (widthPersent * this.props.downloadPersent) / 100 - 4,
                backgroundColor: "#3EA4ED",
              }}
            />
            <View
              style={{
                width:
                  (widthPersent * (100 - this.props.downloadPersent)) / 100 - 4,
                backgroundColor: "#FFFFFF",
              }}
            />
          </View>
        </View>
      );
    }
  };

  render() {
    return (
      <Card containerStyle={{ borderRadius: 5 }}>
        <View style={styles.card}>
          <View style={styles.title}>
            <Text style={{ fontWeight: "bold" }}>File Name : </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{
                maxWidth: this.props.width * 0.6,
              }}
            >
              {this.props.name}
            </Text>
          </View>
          <View style={styles.title}>
            <Text style={{ fontWeight: "bold" }}>Download Time : </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{
                maxWidth: this.props.width * 0.6,
              }}
            >
              {dateConvertion(this.props.downloadStartedAt)}
            </Text>
          </View>
          {this.getProgressBar()}
        </View>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    height: 80,
    justifyContent: "space-between",
  },
  title: {
    flexDirection: "row",
    alignItems: "flex-start",
  },
  persent: {
    alignItems: "center",
    justifyContent: "center",
  },
  progressBar: (width) => ({
    height: 10,
    borderColor: "#000",
    borderWidth: 2,
    borderRadius: 5,
    flexDirection: "row",
    width: width,
  }),
  buttonStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#E0E0E0",
    margin: 5,
    borderRadius: 5,
  },
  buttonText: { fontSize: 18 },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  downloadUrlMaps: state.storageData.downloadUrlMaps,
  width: state.commonInfo.width,
});

export default connect(mapStateToProps, { setDownloadUrlMaps })(
  DownloadComponent
);
