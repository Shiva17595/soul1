import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import { SearchBar } from "react-native-elements";
import TabBarIcon from "../../../../assets/TabBarIcon";
import Masters from "../Chat/List/Masters";

class MastersScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Professors";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      search: ""
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack
    });
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  openUserProfile = params => {
    this.props.navigation.push("Profile", { userInfo: params });
  };

  clearSearch = () => {
    this.updateSearch();
  };

  updateSearch = (search = "") => {
    if (search != this.state.search) {
      this.setState({
        search: search ? search.toLowerCase() : ""
      });
    }
  };

  getSearchBar = () => {
    return (
      <View>
        <SearchBar
          placeholder="Search"
          onChangeText={this.updateSearch}
          value={this.state.search}
          platform="default"
          clearIcon={
            this.state.search ? (
              <TouchableOpacity onPress={this.clearSearch}>
                <TabBarIcon
                  name="cancel"
                  type="MaterialIcons"
                  size={20}
                  color={"#A6A6A6"}
                />
              </TouchableOpacity>
            ) : null
          }
          containerStyle={styles.searchBar}
          inputContainerStyle={styles.seacrhBarInput}
          placeholderTextColor={"#AFAAAA"}
          round
        />
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.getSearchBar()}
        <Masters
          onPress={this.openUserProfile}
          search={this.state.search}
          selectedList={[]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff"
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center"
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF"
  },
  searchBar: {
    backgroundColor: "#FFFFFF",
    borderWidth: 0,
    shadowColor: "white",
    borderBottomColor: "#FFFFFF",
    borderTopColor: "#FFFFFF"
  },
  seacrhBarInput: { backgroundColor: "#F4F2F1" }
});

const mapStateToProps = state => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height
});

export default connect(mapStateToProps, null)(MastersScreen);
