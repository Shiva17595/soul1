import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
} from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import { DATE_FORMAT, COLORS } from "../../../../../constants/Variables";
import {
  getDate,
  isEmptyHash,
  findHeight,
} from "../../../../../assets/reusableFunctions";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import NavigationService from "../../../../../actions/NavigationService";

class ViewEventScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Event";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    let eventId = this.props.navigation.getParam("eventId");
    this.state = {
      event: this.props.events.find((e) => e.uniqueId == eventId),
      showAbout: false,
      showConditions: false,
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
    });
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  viewFile = () => {
    NavigationService.navigate("View", { file: this.state.event.poster });
  };

  getPoster = () => {
    if (!isEmptyHash(this.state.event.poster)) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          style={{
            alignItems: "center",
            justifyContent: "center",
          }}
          onPress={this.viewFile}
        >
          <ImageOrVideo
            style={styles.attachmentStyle(
              this.props.width - 20,
              findHeight(
                this.state.event.poster.width,
                this.props.width - 20,
                this.state.event.poster.height
              )
            )}
            file={this.state.event.poster}
          />
        </TouchableOpacity>
      );
    } else {
      let color = COLORS[this.state.event.name.charCodeAt() % 10];
      return (
        <View
          style={{
            width: this.props.width - 20,
            height: 230,
            borderRadius: 5,
            backgroundColor: color,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text
            style={{
              flexWrap: "wrap",
              maxWidth: this.props.width - 20,
              fontSize: 20,
              color: "#FFFFFF",
            }}
          >
            {this.state.event.name}
          </Text>
        </View>
      );
    }
  };

  changeShowAbout = () => {
    this.setState((prev) => ({
      showAbout: !prev.showAbout,
    }));
  };

  changeShowConditions = () => {
    this.setState((prev) => ({
      showConditions: !prev.showConditions,
    }));
  };

  getAbout = () => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.expandItem}
          onPress={this.changeShowAbout}
        >
          <Text style={{ fontWeight: "bold" }}>About</Text>
          <TabBarIcon
            type="AntDesign"
            name={this.state.showAbout ? "up" : "down"}
          />
        </TouchableOpacity>
        <View style={{ paddingVertical: 10 }}>
          {this.state.showAbout ? (
            <Text style={{ paddingVertical: 5 }}>{this.state.event.about}</Text>
          ) : null}
        </View>
      </View>
    );
  };

  getConditions = () => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.expandItem}
          onPress={this.changeShowConditions}
        >
          <Text style={{ fontWeight: "bold" }}>Terms & conditions</Text>
          <TabBarIcon
            type="AntDesign"
            name={this.state.showConditions ? "up" : "down"}
          />
        </TouchableOpacity>
        <View style={{ paddingVertical: 10 }}>
          {this.state.showConditions ? (
            <Text style={{ paddingVertical: 5 }}>
              {this.state.event.conditions}
            </Text>
          ) : null}
        </View>
      </View>
    );
  };

  getInfo = (title, value) => {
    return (
      <View
        style={{
          flexDirection: "row",
          padding: 5,
        }}
      >
        <Text
          style={{
            flex: 2,
            fontWeight: "bold",
            fontSize: 16,
            flexWrap: "wrap",
          }}
        >
          {title}
        </Text>
        <Text
          style={{
            flex: 4,
            fontSize: 16,
            flexWrap: "wrap",
          }}
        >
          {value}
        </Text>
      </View>
    );
  };

  eventInfo = () => {
    return (
      <View style={{ paddingVertical: 6 }}>
        <Card containerStyle={styles.cardContainerStyle}>
          {this.getInfo("Event Name", this.state.event.name)}
          {this.getInfo(
            "Event Date",
            getDate(this.state.event.date).toDateString(DATE_FORMAT)
          )}
          {this.getInfo("Location", this.state.event.location)}
        </Card>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.getPoster()}
          {this.eventInfo()}
          {this.getAbout()}
          {this.getConditions()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
    padding: 10,
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  cardContainerStyle: {
    padding: 0,
    margin: 0,
    borderRadius: 10,
  },
  attachmentStyle: (width, height) => ({
    flex: 1,
    width: width,
    height: height > 500 ? 500 : height,
    borderRadius: 5,
  }),
  expandItem: {
    height: 30,
    flexDirection: "row",
    padding: 5,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#E0E0E0",
    borderRadius: 10,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  events: state.eventsData.events,
});

export default connect(mapStateToProps, null)(ViewEventScreen);
