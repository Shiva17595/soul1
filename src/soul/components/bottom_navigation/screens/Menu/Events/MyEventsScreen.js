import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList,
  Alert,
} from "react-native";
import { connect } from "react-redux";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import {
  EmptyComponent,
  LoadingPage,
  alertValue,
} from "../../../../../../../lib/ReUsableComponents";
import {
  getKey,
  nDaysBackDate,
  isEmptyArray,
} from "../../../../../assets/reusableFunctions";
import { connectActionSheet } from "@expo/react-native-action-sheet";
import NavigationService from "../../../../../actions/NavigationService";
import EventItem from "./EventItem";
import { getMyEvents, setMyEvents } from "../../../../../actions/eventActions";
import { EVENTS_FIELDS } from "../../../../../assets/FireBaseCollections";
import { ERROR_MESSAGE } from "../../../../../constants/Variables";

class MyEventsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Events";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.isUnmounted = false;
    this.state = {
      events: [],
      loadingEvents: false,
      refreshing: false,
      loadedAll: false,
      deletingEvent: false,
    };
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
    });
    await this.props.getMyEvents();
    this.loadEvents();
  }

  loadEvents = (eventId) => {
    if (this.props.connectionStatus && !this.state.loadingEvents) {
      !this.isUnmounted &&
        this.setState(
          {
            loadingEvents: true,
          },
          () => {
            this.eventsCall(eventId);
          }
        );
    }
  };

  getOrders() {
    let orders = {};
    orders[EVENTS_FIELDS.date] = "desc";
    return orders;
  }

  getFilters() {
    let filters = {
      [EVENTS_FIELDS.date]: {
        type: ">=",
        value: nDaysBackDate(90),
      },
      [`${EVENTS_FIELDS.user}.uid`]: {
        type: "==",
        value: this.props.user.uid,
      },
    };
    return filters;
  }

  eventsCall = async (eventId) => {
    let collectionName = FireBaseCollectionService.events();
    let event = await FireBase.getOffsetDoc(collectionName, eventId);
    FireBase.getCollection(
      collectionName,
      this.getOrders(),
      event,
      this.getFilters()
    )
      .then((events) => {
        let allEvents = !event ? events : this.state.events.concat(events);
        this.props.setMyEvents(allEvents);
        !this.isUnmounted &&
          this.setState({
            events: allEvents,
            loadingEvents: false,
            refreshing: false,
            loadedAll: isEmptyArray(events),
          });
      })
      .catch(() => {
        !this.isUnmounted &&
          this.setState({
            loadingEvents: false,
            refreshing: false,
          });
      });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  deleteEvent = (eventId) => {
    let collection = FireBaseCollectionService.events(eventId);
    FireBase.getDocumentReference(collection)
      .delete()
      .then(() => {
        let events = this.state.events.filter((e) => e.uniqueId != eventId);
        this.props.setMyEvents(events);
        !this.isUnmounted &&
          this.setState((prev) => ({
            deletingEvent: false,
            events: events,
          }));
      })
      .catch(() => {
        alertValue(ERROR_MESSAGE);
        !this.isUnmounted &&
          this.setState((prev) => ({
            deletingEvent: false,
          }));
      });
  };

  performAction = (buttonIndex, eventId) => {
    if (buttonIndex == 0) {
      this.editEvent(eventId);
    } else if (buttonIndex == 1) {
      let self = this;
      setTimeout(() => {
        Alert.alert(
          "Delete Event",
          "This event will be deleted permanently or You can also edit this event if you just want to change something.",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            {
              text: "Delete",
              onPress: () => {
                self.setState(
                  {
                    deletingEvent: true,
                  },
                  () => {
                    self.deleteEvent(eventId);
                  }
                );
              },
            },
          ],
          {
            cancelable: false,
          }
        );
      }, 1000);
    }
  };

  eventOptions = (eventId) => {
    const options = ["Edit Event", "Delete Event", "Cancel"];
    const cancelButtonIndex = 2;
    const destructiveButtonIndex = 1;
    this.props.showActionSheetWithOptions(
      {
        options,
        destructiveButtonIndex,
        cancelButtonIndex,
      },
      (index) => this.performAction(index, eventId)
    );
  };

  editEvent = (eventId) => {
    NavigationService.push("CreateEvent", eventId, { eventId });
  };

  renderEvent = ({ item }) => {
    return <EventItem {...item} openEvent={this.eventOptions} />;
  };

  renderFooter = () => {
    return this.state.loadingEvents && !this.state.refreshing ? (
      <LoadingPage
        style={{ marginTop: 10, alignItems: "center", height: 30 }}
        loadSpinnerSize="large"
      />
    ) : null;
  };

  refreshEvents = () => {
    if (this.props.connectionStatus && !this.state.refreshing) {
      !this.isUnmounted &&
        this.setState(
          {
            refreshing: true,
          },
          this.eventsCall
        );
    }
  };

  loadMoreEvents = () => {
    if (!this.state.loadedAll) {
      let eventOffset = this.state.events[this.state.events.length - 1];
      this.loadEvents(eventOffset ? eventOffset.uniqueId : null);
    }
  };

  getEventsToRender = () => {
    return isEmptyArray(this.state.events)
      ? this.props.events
      : this.state.events;
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.getEventsToRender()}
          renderItem={this.renderEvent}
          keyExtractor={getKey}
          ListEmptyComponent={
            <EmptyComponent
              style={styles.emptyPage(this.props.width, this.props.height)}
              message={
                this.state.loadingEvents || this.state.refreshing
                  ? "loading.."
                  : "No Events"
              }
            />
          }
          onEndReached={this.loadMoreEvents}
          onEndReachedThreshold={0.1}
          refreshing={this.state.refreshing}
          onRefresh={this.refreshEvents}
          ListFooterComponent={this.renderFooter}
          horizontal={false}
          numColumns={2}
        />
        {this.state.deletingEvent && <LoadingPage style={styles.loading} />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  cardContainerStyle: (width) => ({
    padding: 0,
    margin: 5,
    borderRadius: 5,
    width: width,
  }),
  emptyPage: (width, height) => ({
    flex: 1,
    width: width,
    height: height,
    alignItems: "center",
    justifyContent: "center",
  }),
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  events: state.eventsData.myEvents,
});

const MyEventsScreenWithActionSheet = connectActionSheet(MyEventsScreen);

export default connect(mapStateToProps, { getMyEvents, setMyEvents })(
  MyEventsScreenWithActionSheet
);
