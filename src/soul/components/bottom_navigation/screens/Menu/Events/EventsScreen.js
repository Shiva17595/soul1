import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import { Button } from "react-native-elements";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import {
  EmptyComponent,
  LoadingPage,
} from "../../../../../../../lib/ReUsableComponents";
import {
  getKey,
  nDaysBackDate,
  isEmptyArray,
} from "../../../../../assets/reusableFunctions";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import NavigationService from "../../../../../actions/NavigationService";
import EventItem from "./EventItem";
import { getEvents, setEvents } from "../../../../../actions/eventActions";
import { EVENTS_FIELDS } from "../../../../../assets/FireBaseCollections";

class EventsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Events";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.isUnmounted = false;
    this.state = {
      events: [],
      loadingEvents: false,
      refreshing: false,
      loadedAll: false,
    };
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
    });
    await this.props.getEvents();
    this.loadEvents();
  }

  loadEvents = (eventId) => {
    if (this.props.connectionStatus && !this.state.loadingEvents) {
      !this.isUnmounted &&
        this.setState(
          {
            loadingEvents: true,
          },
          () => {
            this.eventsCall(eventId);
          }
        );
    }
  };

  getOrders() {
    let orders = {};
    orders[EVENTS_FIELDS.date] = "desc";
    return orders;
  }

  getFilters() {
    let filters = {
      [EVENTS_FIELDS.date]: {
        type: ">=",
        value: nDaysBackDate(90),
      },
    };
    if (!this.props.user.isMaster) {
      filters[EVENTS_FIELDS.classIds] = {
        type: "array-contains",
        value: this.props.user.classInfo.classId,
      };
    }
    return filters;
  }

  eventsCall = async (eventId) => {
    let collectionName = FireBaseCollectionService.events();
    let event = await FireBase.getOffsetDoc(collectionName, eventId);
    FireBase.getCollection(
      collectionName,
      this.getOrders(),
      event,
      this.getFilters()
    )
      .then((events) => {
        let allEvents = !event ? events : this.state.events.concat(events);
        this.props.setEvents(allEvents);
        !this.isUnmounted &&
          this.setState({
            events: allEvents,
            loadingEvents: false,
            refreshing: false,
            loadedAll: isEmptyArray(events),
          });
      })
      .catch(() => {
        !this.isUnmounted &&
          this.setState({
            loadingEvents: false,
            refreshing: false,
          });
      });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  openCreateScreen = () => {
    NavigationService.push("CreateEvent", new Date().getTime().toString());
  };

  openMyEvents = () => {
    NavigationService.navigate("MyEvents");
  };

  getMasterOptions = () => {
    if (this.props.user.isMaster) {
      return (
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            padding: 10,
          }}
        >
          <Button
            buttonStyle={{
              backgroundColor: "#20368F",
              height: 40,
            }}
            containerStyle={{
              paddingHorizontal: 5,
            }}
            titleStyle={{
              fontSize: 12,
              color: "#FFFFFF",
            }}
            icon={
              <TabBarIcon
                name={"event"}
                type={"MaterialIcons"}
                size={15}
                color="#FFFFFF"
              />
            }
            iconContainerStyle={{
              padding: 5,
            }}
            title={"My Events"}
            onPress={this.openMyEvents}
          />
          <Button
            buttonStyle={{
              backgroundColor: "#20368F",
              height: 40,
            }}
            containerStyle={{
              paddingHorizontal: 5,
            }}
            titleStyle={{
              fontSize: 12,
              color: "#FFFFFF",
            }}
            icon={
              <TabBarIcon
                name={"ios-create"}
                type={"Ionicons"}
                size={15}
                color="#FFFFFF"
              />
            }
            iconContainerStyle={{
              padding: 5,
            }}
            title={"Create Event"}
            onPress={this.openCreateScreen}
          />
        </View>
      );
    }
    return null;
  };

  openEvent = (eventId) => {
    NavigationService.push("ViewEvent", eventId, { eventId });
  };

  renderEvent = ({ item }) => {
    return <EventItem {...item} openEvent={this.openEvent} />;
  };

  renderFooter = () => {
    return this.state.loadingEvents && !this.state.refreshing ? (
      <LoadingPage
        style={{ marginTop: 10, alignItems: "center", height: 30 }}
        loadSpinnerSize="large"
      />
    ) : null;
  };

  refreshEvents = () => {
    if (this.props.connectionStatus && !this.state.refreshing) {
      !this.isUnmounted &&
        this.setState(
          {
            refreshing: true,
          },
          this.eventsCall
        );
    }
  };

  loadMoreEvents = () => {
    if (!this.state.loadedAll) {
      let eventOffset = this.state.events[this.state.events.length - 1];
      this.loadEvents(eventOffset ? eventOffset.uniqueId : null);
    }
  };

  getEventsToRender = () => {
    return isEmptyArray(this.state.events)
      ? this.props.events
      : this.state.events;
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.getEventsToRender()}
          renderItem={this.renderEvent}
          keyExtractor={getKey}
          ListHeaderComponent={this.getMasterOptions()}
          ListEmptyComponent={
            <EmptyComponent
              style={styles.emptyPage(this.props.width, this.props.height)}
              message={
                this.state.loadingEvents || this.state.refreshing
                  ? "loading.."
                  : "No Events"
              }
            />
          }
          onEndReached={this.loadMoreEvents}
          onEndReachedThreshold={0.1}
          refreshing={this.state.refreshing}
          onRefresh={this.refreshEvents}
          ListFooterComponent={this.renderFooter}
          horizontal={false}
          numColumns={2}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  cardContainerStyle: (width) => ({
    padding: 0,
    margin: 5,
    borderRadius: 5,
    width: width,
  }),
  emptyPage: (width, height) => ({
    flex: 1,
    width: width,
    height: height,
    alignItems: "center",
    justifyContent: "center",
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  events: state.eventsData.events,
});

export default connect(mapStateToProps, { getEvents, setEvents })(EventsScreen);
