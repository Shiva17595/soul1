import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  TextInput,
  Platform,
  DatePickerAndroid,
} from "react-native";
import { connect } from "react-redux";
import { NavigationActions, StackActions } from "react-navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import {
  LoadingPage,
  DatePicker,
  alertValue,
} from "../../../../../../../lib/ReUsableComponents";
import {
  isEmptyArray,
  findHeight,
  isEmptyHash,
  _onChoosePic,
  getDate,
} from "../../../../../assets/reusableFunctions";
import ClassesDropDown from "../../../../../../../lib/ClassesDropDown";
import User from "../../../../../services/UserService";
import { logout } from "../../../../../actions/authenticateUser";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import { Button } from "react-native-elements";
import { DATE_FORMAT, ERROR_MESSAGE } from "../../../../../constants/Variables";
import ModalView from "../../../../../../../lib/ModalView";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import NavigationService from "../../../../../actions/NavigationService";
import FireBaseStorageService from "../../../../../services/FireBaseStorageService";

class CreateEventScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Event";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    let headerRight = (
      <TouchableOpacity
        onPress={params.create ? params.create : params.update}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>
          {params.create ? "Create" : "Update"}
        </Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
      headerRight,
    };
  };

  constructor(props) {
    super(props);
    this.isUnmounted = false;
    this.securityCheck();
    let eventId = this.props.navigation.getParam("eventId");
    let event = this.findEvent(eventId);
    this.state = {
      classIds: event.classIds || [],
      poster: event.poster || {},
      eventName: event.name || "",
      about: event.about || "",
      conditions: event.conditions || "",
      eventDate: event.date || new Date(),
      location: event.location || "",
      modalVisible: false,
      creatingEvent: false,
      duration: event.duration || "",
      event: event,
    };
  }

  findEvent = (eventId) => {
    let event = this.props.events.find((e) => e.uniqueId == eventId);
    return event || {};
  };

  securityCheck = () => {
    if (!this.props.user.isMaster) {
      User.logout();
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Login" })],
      });
      this.props.navigation.dispatch(resetAction);
      this.props.logout();
    }
  };

  setClassIds = (classIds) => {
    !this.isUnmounted &&
      this.setState({
        classIds: classIds,
      });
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  async componentDidMount() {
    if (isEmptyHash(this.state.event)) {
      this.props.navigation.setParams({
        goBack: this.goBack,
        create: this.onCreate,
      });
    } else {
      this.props.navigation.setParams({
        goBack: this.goBack,
        update: this.onUpdate,
      });
    }
  }

  onUpdate = () => {
    let doc = {};
    if (!this.validateFields()) {
      return;
    }
    if (this.state.eventName.trim() != this.state.event.name) {
      doc["name"] = this.state.eventName.trim();
    }
    if (this.state.about.trim() != this.state.event.about) {
      doc["about"] = this.state.about.trim();
    }
    if (this.state.conditions.trim() != this.state.event.conditions) {
      doc["conditions"] = this.state.conditions.trim();
    }
    if (this.state.eventDate != this.state.event.date) {
      doc["date"] = this.state.eventDate;
    }
    if (this.state.location.trim() != this.state.event.location) {
      doc["location"] = this.state.location.trim();
    }
    if (this.state.duration.trim() != this.state.event.duration) {
      doc["duration"] = this.state.duration.trim();
    }
    if (this.state.poster.uri != this.state.event.poster.uri) {
      doc["poster"] = this.state.poster;
    }
    if (this.state.classIds.join(",") != this.state.event.classIds.join(",")) {
      let classIds = isEmptyArray(this.state.classIds)
        ? this.props.classes.map((c) => c.uniqueId)
        : this.state.classIds;
      doc["classIds"] = classIds;
    }
    if (isEmptyHash(doc)) return;
    if (!this.state.creatingEvent) {
      this.setState(
        {
          creatingEvent: true,
        },
        () => {
          this.updateEvent(doc);
        }
      );
    }
  };

  updateEvent = async (doc) => {
    let collection = FireBaseCollectionService.events();
    if (this.state.poster.uri != this.state.event.poster.uri) {
      let fileName = `${
        this.state.event.uniqueId
      }-${new Date().getTime().toString()}.${this.state.poster.uri
        .split(/[\s.]+/)
        .pop()}`;
      doc["poster"] = await this.uploadFile(this.state.poster, fileName);
    }
    FireBase.updateDocumentInfo(collection, this.state.event.uniqueId, doc)
      .then(() => {
        NavigationService.navigate("MyEvents");
        !this.isUnmounted &&
          this.setState({
            creatingEvent: false,
          });
      })
      .catch((error) => {
        console.log(error);
        alertValue(ERROR_MESSAGE);
        !this.isUnmounted &&
          this.setState({
            creatingEvent: false,
          });
      });
  };

  validateFields = () => {
    if (this.state.eventName.trim() && this.state.about.trim()) {
      return true;
    } else {
      alertValue("Event name & about fields are mandatory");
      return false;
    }
  };

  onCreate = () => {
    if (!this.state.creatingEvent && this.validateFields()) {
      this.setState(
        {
          creatingEvent: true,
        },
        this.createEvent
      );
    }
  };

  uploadFile = (file, fileName) => {
    return new Promise(async (resolve, reject) => {
      let result = {};
      if (isEmptyHash(file)) resolve(result);
      let path = FireBaseStorageService.events(fileName);
      let uri = await FireBase.uploadFile(file.uri, path);
      if (uri) {
        result = {
          type: file.type,
          width: file.width,
          height: file.height,
          uri: uri,
          name: fileName,
        };
      }
      resolve(result);
    });
  };

  createEvent = async () => {
    let collection = FireBaseCollectionService.events();
    let eventId = FireBase.generateDocumentId(collection);
    let fileName = `${eventId}-${new Date()
      .getTime()
      .toString()}.${this.state.poster.uri.split(/[\s.]+/).pop()}`;
    let poster = await this.uploadFile(this.state.poster, fileName);
    let classIds = isEmptyArray(this.state.classIds)
      ? this.props.classes.map((c) => c.uniqueId)
      : this.state.classIds;
    let doc = {
      name: this.state.eventName.trim(),
      about: this.state.about.trim(),
      conditions: this.state.conditions.trim(),
      poster: poster,
      date: this.state.eventDate,
      location: this.state.location.trim(),
      classIds: classIds,
      duration: this.state.duration.trim(),
      user: {
        uid: this.props.user.uid,
        name: this.props.user.name,
        avatar: this.props.user.avatar,
      },
    };
    FireBase.createDocument(collection, doc, eventId)
      .then(() => {
        NavigationService.navigate("Events");
        !this.isUnmounted &&
          this.setState({
            creatingEvent: false,
          });
      })
      .catch(() => {
        alertValue(ERROR_MESSAGE);
        !this.isUnmounted &&
          this.setState({
            creatingEvent: false,
          });
      });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  choosePoster = async () => {
    let attachment = await _onChoosePic("Images");
    if (attachment) {
      this.setState({
        poster: attachment,
      });
    }
  };

  removePoster = () => {
    this.setState({
      poster: {},
    });
  };

  getPoster = () => {
    if (isEmptyHash(this.state.poster)) {
      return (
        <TouchableOpacity style={styles.poster} onPress={this.choosePoster}>
          <Text style={{ fontWeight: "bold" }}>Add Poster</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            paddingVertical: 10,
          }}
        >
          <ImageOrVideo
            style={styles.attachmentStyle(
              this.props.width - 10,
              findHeight(
                this.state.poster.width,
                this.props.width - 10,
                this.state.poster.height
              )
            )}
            file={this.state.poster}
            removeAttachment={this.removePoster}
            needRemoveButton={true}
            closeIconSize={30}
            resizeMode={"contain"}
          />
        </View>
      );
    }
  };

  onEventNameChange = (value) => {
    this.setState({
      eventName: value,
    });
  };

  onEventChange = (value) => {
    this.setState({
      about: value,
    });
  };

  onLocationChange = (value) => {
    this.setState({
      location: value,
    });
  };

  onConditionsChange = (value) => {
    this.setState({
      conditions: value,
    });
  };

  onDurationChange = (value) => {
    this.setState({
      duration: value,
    });
  };

  getFields = () => {
    return (
      <View style={{ alignItems: "center" }}>
        <View style={{ paddingVertical: 5 }}>
          <Text style={{ fontWeight: "bold", paddingVertical: 8 }}>
            Event Name
          </Text>
          <View style={styles.inputBox(this.props.width - 20)}>
            <TextInput
              multiline={false}
              onChangeText={this.onEventNameChange}
              style={{ fontSize: 18, padding: 10 }}
              value={this.state.eventName}
              maxLength={25}
              spellCheck={true}
              placeholder="Enter Event Name"
            />
          </View>
        </View>
        <View style={{ paddingVertical: 5 }}>
          <Text style={{ fontWeight: "bold", paddingVertical: 8 }}>About</Text>
          <View style={styles.inputBox(this.props.width - 20, 100)}>
            <TextInput
              multiline={true}
              onChangeText={this.onEventChange}
              style={{ fontSize: 18, padding: 10 }}
              value={this.state.about}
              maxLength={1000}
              spellCheck={true}
              placeholder="Enter Description"
            />
          </View>
        </View>
        <View style={{ paddingVertical: 5 }}>
          <Text style={{ fontWeight: "bold", paddingVertical: 8 }}>
            Location
          </Text>
          <View style={styles.inputBox(this.props.width - 20)}>
            <TextInput
              multiline={false}
              onChangeText={this.onLocationChange}
              style={{ fontSize: 18, padding: 10 }}
              value={this.state.location}
              maxLength={50}
              spellCheck={true}
              placeholder="Enter Location"
            />
          </View>
        </View>
        <View style={{ paddingVertical: 5 }}>
          <Text style={{ fontWeight: "bold", paddingVertical: 8 }}>
            Terms & Conditions
          </Text>
          <View style={styles.inputBox(this.props.width - 20, 100)}>
            <TextInput
              multiline={true}
              onChangeText={this.onConditionsChange}
              style={{ fontSize: 18, padding: 10 }}
              value={this.state.conditions}
              maxLength={1000}
              spellCheck={true}
              placeholder="Enter Conditions"
            />
          </View>
        </View>
        <View style={{ paddingVertical: 5 }}>
          <Text style={{ fontWeight: "bold", paddingVertical: 8 }}>
            Duration (in minutes)
          </Text>
          <View style={styles.inputBox(this.props.width - 20)}>
            <TextInput
              multiline={false}
              keyboardType={"number-pad"}
              onChangeText={this.onDurationChange}
              style={{ fontSize: 18, padding: 10 }}
              value={this.state.duration}
              maxLength={20}
              spellCheck={true}
              placeholder="Enter Duration"
            />
          </View>
        </View>
      </View>
    );
  };

  openDatePicker = async () => {
    if (Platform.OS === "ios") {
      this.setState({
        modalVisible: true,
      });
    } else {
      try {
        const { action, year, month, day } = await DatePickerAndroid.open({
          date: this.state.eventDate,
          minDate: new Date(),
        });
        if (action !== DatePickerAndroid.dismissedAction) {
          this.setState({
            eventDate: new Date(year, month, day),
          });
        }
      } catch ({ code, message }) {
        console.log("Cannot open date picker", message);
      }
    }
  };

  getDataFields = () => {
    return (
      <View
        style={{
          flexDirection: "row",
          height: 50,
          backgroundColor: "#F4F2F1",
          justifyContent: "space-between",
          padding: 5,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text style={{ fontWeight: "bold", padding: 3 }}>Event Date : </Text>
          <Text>{getDate(this.state.eventDate).toDateString(DATE_FORMAT)}</Text>
        </View>
        <Button
          buttonStyle={{
            backgroundColor: "#20368F",
            height: 30,
          }}
          containerStyle={{
            alignItems: "flex-end",
            paddingHorizontal: 5,
            justifyContent: "center",
          }}
          titleStyle={{
            fontSize: 10,
            color: "#FFFFFF",
          }}
          icon={<TabBarIcon name="clock" type="EvilIcons" size={20} />}
          iconContainerStyle={{
            padding: 5,
          }}
          title={"Select Date"}
          onPress={this.openDatePicker}
        />
      </View>
    );
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  setEventDate = (date) => {
    this.setState({
      eventDate: date,
    });
  };

  getClassesList = () => {
    return (
      <View>
        <ClassesDropDown
          setClassIds={this.setClassIds}
          selectedClassIds={this.state.classIds}
          multiSelect={true}
        />
        <View style={styles.poster}>
          <Text style={{ fontWeight: "bold" }}>Selected Classes</Text>
          {isEmptyArray(this.state.classIds) ? (
            <Text>All Classes</Text>
          ) : (
            this.props.classes.map((c) => {
              return this.state.classIds.includes(c.uniqueId) ? (
                <Text
                  key={c.uniqueId}
                >{`${c.name} - ${c.batch.batchName}`}</Text>
              ) : null;
            })
          )}
        </View>
      </View>
    );
  };

  render() {
    let content = (
      <KeyboardAwareScrollView
        enableOnAndroid
        keyboardShouldPersistTaps="always"
        extraScrollHeight={50}
        enableAutomaticScroll={!this.state.creatingEvent}
      >
        <View style={styles.container}>
          {this.getPoster()}
          {this.getFields()}
          {this.getDataFields()}
          {this.getClassesList()}
        </View>
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={styles.modalStyle}
        >
          <DatePicker
            date={this.state.eventDate}
            mode="date"
            onDateChange={this.setEventDate}
            style={styles.bottomSheetPanelContainer(this.props.height / 2)}
            minimumDate={new Date()}
            header={
              <View style={{ alignItems: "center", paddingBottom: 10 }}>
                <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                  Select Date :
                </Text>
              </View>
            }
          />
        </ModalView>
        {this.state.creatingEvent ? (
          <LoadingPage
            style={styles.loading}
            spinnerPlace={{ padding: this.props.height / 2 }}
          />
        ) : null}
      </KeyboardAwareScrollView>
    );
    return content;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  poster: {
    backgroundColor: "#F4F2F1",
    minHeight: 60,
    margin: 6,
    padding: 5,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  attachmentStyle: (width, height) => ({
    flex: 1,
    width: width,
    height: height > 500 ? 500 : height,
  }),
  inputBox: (width, height) => ({
    borderWidth: 1.5,
    borderColor: "#E8E7E7",
    borderRadius: 10,
    width: width,
    height: height ? height : 40,
  }),
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
  bottomSheetPanelContainer: (heigth) => ({
    margin: 0,
    height: "50%",
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexDirection: "column",
    justifyContent: "center",
    heigth: heigth,
  }),
  loading: {
    ...StyleSheet.absoluteFill,
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  classes: state.commonInfo.classes,
  events: state.eventsData.myEvents,
});

export default connect(mapStateToProps, { logout })(CreateEventScreen);
