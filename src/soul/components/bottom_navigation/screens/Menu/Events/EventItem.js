import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import { DATE_FORMAT, COLORS } from "../../../../../constants/Variables";
import { getDate, isEmptyHash } from "../../../../../assets/reusableFunctions";

class EventsScreen extends Component {
  openEvent = () => {
    this.props.openEvent(this.props.uniqueId);
  };

  getPoster = () => {
    if (!isEmptyHash(this.props.poster)) {
      return (
        <ImageOrVideo
          style={{
            width: (this.props.width - 20) / 2 - 6,
            height: 230,
            borderRadius: 5,
          }}
          file={this.props.poster}
        />
      );
    } else {
      let color = COLORS[this.props.name.charCodeAt() % 10];
      return (
        <View
          style={{
            width: (this.props.width - 20) / 2 - 6,
            height: 230,
            borderRadius: 5,
            backgroundColor: color,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text
            style={{
              flexWrap: "wrap",
              maxWidth: (this.props.width - 20) / 2 - 10,
              fontSize: 20,
              color: "#FFFFFF",
            }}
          >
            {this.props.name}
          </Text>
        </View>
      );
    }
  };

  render() {
    return (
      <TouchableOpacity onPress={this.openEvent} activeOpacity={1}>
        <Card
          containerStyle={styles.cardContainerStyle(
            (this.props.width - 20) / 2
          )}
        >
          <View
            style={{
              width: (this.props.width - 20) / 2,
              height: 350,
              padding: 3,
            }}
          >
            {this.getPoster()}
            <View style={{ padding: 5 }}>
              <Text style={{ fontWeight: "bold" }}>Event Name</Text>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  maxWidth: (this.props.width - 20) / 2 - 16,
                }}
              >
                {this.props.name}
              </Text>
              <Text style={{ fontWeight: "bold" }}>Event Date</Text>
              <Text>{getDate(this.props.date).toDateString(DATE_FORMAT)}</Text>
              <Text style={{ fontWeight: "bold" }}>Location</Text>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  maxWidth: (this.props.width - 20) / 2 - 16,
                }}
              >
                {this.props.location}
              </Text>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  cardContainerStyle: (width) => ({
    padding: 0,
    margin: 5,
    borderRadius: 5,
    width: width,
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(EventsScreen);
