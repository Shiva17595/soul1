import React, { Component } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TouchableOpacity,
} from "react-native";
import User from "../../../../services/UserService";
import { connect } from "react-redux";
import { StackActions, NavigationActions } from "react-navigation";
import { logout } from "../../../../actions/authenticateUser";
import MenuCard from "./MenuCard";
import CustomAvatar from "../../../../../../lib/CustomAvatar";
import { getKey } from "../../../../assets/reusableFunctions";
import {
  LoadingPage,
  alertValue,
} from "../../../../../../lib/ReUsableComponents";
import NavigationService from "../../../../actions/NavigationService";

class MenuScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    let headerTitle = "Menu";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
    };
  };

  constructor(props) {
    super(props);
    this.menuItems = [
      {
        optionText: "Events",
        iconType: "MaterialIcons",
        iconName: "event",
        iconColor: "#A6A2A1",
        onPress: this.showEvents,
        all: true,
      },
      {
        optionText: "Academic Performance",
        iconType: "Entypo",
        iconName: "graduation-cap",
        iconColor: "#A6A2A1",
        onPress: this.showMasterAcademics,
        master: true,
      },
      {
        optionText: "Academic Performance",
        iconType: "Entypo",
        iconName: "graduation-cap",
        iconColor: "#A6A2A1",
        onPress: this.showStudentAcademics,
        student: true,
      },
      {
        optionText: "Assignments",
        iconType: "MaterialIcons",
        iconName: "assignment",
        iconColor: "#A6A2A1",
        onPress: this.onShowMasterAssignments,
        master: true,
      },
      {
        optionText: "Assignments",
        iconType: "MaterialIcons",
        iconName: "assignment",
        iconColor: "#A6A2A1",
        onPress: this.onShowStudentAssignments,
        student: true,
      },
      {
        optionText: "Students",
        iconType: "Entypo",
        iconName: "users",
        iconColor: "#A6A2A1",
        onPress: this.showStudentsList,
        all: true,
      },
      {
        optionText: "Professors",
        iconType: "Entypo",
        iconName: "users",
        iconColor: "#A6A2A1",
        onPress: this.showMastersList,
        all: true,
      },
      {
        optionText: "Courses",
        iconType: "Entypo",
        iconName: "book",
        iconColor: "#A6A2A1",
        onPress: this.showCoursesList,
        all: true,
      },
      {
        optionText: "Privacy Settings",
        iconType: "Entypo",
        iconName: "lock",
        iconColor: "#A6A2A1",
        onPress: this.openPrivacySettings,
        all: true,
      },
      {
        optionText: "Downloads",
        iconType: "Feather",
        iconName: "download",
        iconColor: "#A6A2A1",
        onPress: this.openDownloads,
        all: true,
      },
      {
        optionText: "Downloads",
        iconType: "Feather",
        iconName: "download",
        iconColor: "#A6A2A1",
        onPress: this.openUploads,
        master: true,
      },
      {
        optionText: "Logout",
        iconType: "MaterialCommunityIcons",
        iconName: "logout",
        iconColor: "#A6A2A1",
        onPress: this.onPressSignOut,
        all: true,
      },
    ];
    this.state = {
      loggingOut: false,
    };
  }

  showCoursesList = () => {
    NavigationService.navigate("Courses");
  };

  openPrivacySettings = () => {
    NavigationService.navigate("PrivacySettings");
  };

  openDownloads = () => {
    NavigationService.navigate("Downloads");
  };

  openUploads = () => {
    NavigationService.navigate("Downloads");
  };

  showEvents = () => {
    NavigationService.navigate("Events");
  };

  onShowMasterAssignments = () => {
    NavigationService.navigate("MasterAssignments");
  };

  onShowStudentAssignments = () => {
    NavigationService.navigate("StudentAssignments");
  };

  showMasterAcademics = () => {
    NavigationService.navigate("MasterAcademics");
  };

  showStudentAcademics = () => {
    NavigationService.navigate("StudentAcademics", {
      userInfo: {
        name: this.props.user.userName,
        uid: this.props.user.uid,
        classId: this.props.user.classInfo.classId,
      },
    });
  };

  showStudentsList = () => {
    NavigationService.navigate("Students");
  };

  showMastersList = () => {
    NavigationService.navigate("Masters");
  };

  onPressSignOut = () => {
    if (!this.loggingOut) {
      this.setState(
        {
          loggingOut: true,
        },
        () => {
          User.logout()
            .then(() => {
              this.setState(
                {
                  loggingOut: false,
                },
                () => {
                  alertValue("Logged Out");
                  const resetAction = StackActions.reset({
                    index: 0,
                    actions: [
                      NavigationActions.navigate({ routeName: "Login" }),
                    ],
                  });
                  this.props.navigation.dispatch(resetAction);
                  this.props.logout();
                }
              );
            })
            .catch(() => {
              alertValue("Something Went Wrong, Please try again");
              this.setState({
                loggingOut: false,
              });
            });
        }
      );
    }
  };

  openProfile = () => {
    let userInfo = {
      avatar: this.props.user.avatar,
      uid: this.props.user.uid,
      userName: this.props.user.userName,
    };
    this.props.navigation.push("Profile", { userInfo });
  };

  listHeader = () => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.headerStyle(this.props.width)}
        onPress={this.openProfile}
      >
        <View style={styles.avatar}>
          <CustomAvatar
            source={{ uri: this.props.user.avatar }}
            rounded
            size={50}
            uid={this.props.user.uid}
          />
        </View>
        <View style={styles.headerText}>
          <Text ellipsizeMode="tail" numberOfLines={1} style={styles.userName}>
            {this.props.user.userName}
          </Text>
          <Text style={styles.headerSubtitle}>see your profile</Text>
        </View>
        <View style={styles.headerBorder} />
      </TouchableOpacity>
    );
  };

  renderOption = ({ item }) => {
    return <MenuCard {...item} />;
  };

  getMenuItems = () => {
    if (this.props.user.isMaster) {
      return this.menuItems.filter((m) => m.all || m.master);
    } else if (this.props.user.isStudent) {
      return this.menuItems.filter((m) => m.all || m.student);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.getMenuItems()}
          renderItem={this.renderOption}
          keyExtractor={getKey}
          ListHeaderComponent={this.listHeader()}
        />
        {this.state.loggingOut ? <LoadingPage style={styles.loading} /> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
  headerStyle: (width) => ({
    flexDirection: "row",
    height: 70,
    width: width,
    alignItems: "center",
  }),
  avatar: { flex: 1.4, paddingHorizontal: 8, justifyContent: "center" },
  headerText: {
    flex: 8.6,
    justifyContent: "flex-start",
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
  },
  headerSubtitle: {
    color: "#AFAAAA",
    fontSize: 12,
  },
  headerBorder: { height: 1, backgroundColor: "#E0E0E0", marginHorizontal: 5 },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, { logout })(MenuScreen);
