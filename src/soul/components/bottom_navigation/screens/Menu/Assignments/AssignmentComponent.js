import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { Card } from "react-native-elements";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";

class AssignmentComponent extends Component {
  onClick = () => {
    this.props.onClick(this.props.uniqueId, this.props.document);
  };

  render() {
    return (
      <TouchableOpacity onPress={this.onClick} activeOpacity={1}>
        <Card style={styles.component} containerStyle={{ padding: 5 }}>
          <View style={{ flexDirection: "row", paddingVertical: 5 }}>
            <Text style={{ flex: 1, fontWeight: "bold" }}>Name </Text>
            <Text style={{ flex: 4, flexWrap: "wrap" }}>{this.props.name}</Text>
          </View>
          <View style={{ flexDirection: "row", paddingVertical: 5 }}>
            <Text style={{ flex: 1, fontWeight: "bold" }}>Subject </Text>
            <Text style={{ flex: 4, flexWrap: "wrap" }}>
              {this.props.subject.subjectName}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View style={{ padding: 10 }}>
              <CustomAvatar
                size={40}
                source={{
                  uri: this.props.user.avatar,
                }}
                rounded
              />
            </View>
            <Text style={{ flex: 4, flexWrap: "wrap" }}>
              {this.props.user.name}
            </Text>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  component: {
    height: 80,
    justifyContent: "space-between",
  },
});

export default AssignmentComponent;
