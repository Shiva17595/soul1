import React, { Component } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
} from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import { DATE_FORMAT, COLORS } from "../../../../../constants/Variables";
import {
  getDate,
  isEmptyHash,
  getKey,
} from "../../../../../assets/reusableFunctions";
import {
  EmptyComponent,
  LoadingPage,
} from "../../../../../../../lib/ReUsableComponents";
import AssignmentComponent from "./AssignmentComponent";

class Assignments extends Component {
  renderAssignment = ({ item }) => {
    return <AssignmentComponent {...item} onClick={this.props.onClick} />;
  };

  renderFooter = () => {
    return this.props.loading && !this.props.refreshing ? (
      <LoadingPage
        style={{ marginTop: 10, alignItems: "center", height: 30 }}
        loadSpinnerSize="large"
      />
    ) : null;
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.assignments}
          renderItem={this.renderAssignment}
          keyExtractor={getKey}
          ListEmptyComponent={
            <EmptyComponent
              style={styles.emptyPage(this.props.width, this.props.height)}
              message={
                this.props.loading || this.props.refreshing
                  ? "loading..."
                  : "No Assignments"
              }
            />
          }
          onEndReached={this.props.loadMoreAssignments}
          onEndReachedThreshold={0.1}
          refreshing={this.props.refreshing}
          onRefresh={this.props.refreshAssignments}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  emptyPage: (width, height) => ({
    flex: 1,
    width: width,
    height: height,
    alignItems: "center",
    justifyContent: "center",
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(Assignments);
