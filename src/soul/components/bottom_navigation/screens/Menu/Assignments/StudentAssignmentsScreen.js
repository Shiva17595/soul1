import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import {
  getAssignments,
  setAssignments,
} from "../../../../../actions/assignmentActions";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import { ASSIGNMENTS_FIELDS } from "../../../../../assets/FireBaseCollections";
import {
  LoadingPage,
  alertValue,
} from "../../../../../../../lib/ReUsableComponents";
import { isEmptyArray } from "../../../../../assets/reusableFunctions";
import Assignments from "./Assignments";
import { setDownloadUrlMaps } from "../../../../../actions/storageActions";
import AppPermissions from "../../../../../../../lib/AppPermissions";
import LocalStorageService from "../../../../../services/LocalStorageService";
import {
  DOCUMENT,
  CONNECTION_ERROR,
  MB1,
} from "../../../../../constants/Variables";
import * as MediaLibrary from "expo-media-library";
import { connectActionSheet } from "@expo/react-native-action-sheet";

class StudentAssignmentsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Assignments";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.isUnmounted = false;
    this.state = {
      loading: false,
      assignments: [],
      refreshing: false,
      loadedAll: false,
    };
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
    });
    await this.props.getAssignments();
    this.getAssignments();
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  getAssignments = (assignmentId) => {
    if (this.props.connectionStatus && !this.state.loading) {
      !this.isUnmounted &&
        this.setState(
          {
            loading: true,
          },
          () => {
            this.assignmentsCall(assignmentId);
          }
        );
    }
  };

  getOrders() {
    let orders = {};
    orders[ASSIGNMENTS_FIELDS.createdAt] = "desc";
    return orders;
  }

  getFilters() {
    let filters = {
      [ASSIGNMENTS_FIELDS.classIds]: {
        type: "array-contains",
        value: this.props.user.classInfo.classId,
      },
    };
    return filters;
  }

  assignmentsCall = async (assignmentId) => {
    let collectionName = FireBaseCollectionService.assignments();
    let assignment = await FireBase.getOffsetDoc(collectionName, assignmentId);
    FireBase.getCollection(
      collectionName,
      this.getOrders(),
      assignment,
      this.getFilters()
    )
      .then((assignments) => {
        let allAssignments = !assignment
          ? assignments
          : this.state.assignments.concat(assignments);
        this.props.setAssignments(allAssignments);
        !this.isUnmounted &&
          this.setState({
            assignments: allAssignments,
            loading: false,
            refreshing: false,
            loadedAll: isEmptyArray(assignments),
          });
      })
      .catch(() => {
        !this.isUnmounted &&
          this.setState({
            loading: false,
            refreshing: false,
          });
      });
  };

  renderFooter = () => {
    return this.state.loading && !this.state.refreshing ? (
      <LoadingPage
        style={{ marginTop: 10, alignItems: "center", height: 30 }}
        loadSpinnerSize="large"
      />
    ) : null;
  };

  refreshAssignments = () => {
    if (this.props.connectionStatus && !this.state.refreshing) {
      !this.isUnmounted &&
        this.setState(
          {
            refreshing: true,
          },
          this.assignmentsCall
        );
    }
  };

  loadMoreAssignments = () => {
    if (!this.state.loadedAll) {
      let assignment = this.state.assignments[
        this.state.assignments.length - 1
      ];
      this.getAssignments(assignment ? assignment.uniqueId : null);
    }
  };

  getAssignmentsToRender = () => {
    if (isEmptyArray(this.state.assignments)) {
      return this.props.assignments;
    }
    return this.state.assignments;
  };

  callback = async (downloadProgress, urlMap) => {
    urlMap.downloadPersent = Math.round(
      (downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite) *
        100
    );
    urlMap.downloadUpdatedAt = new Date();
    urlMap.completed = urlMap.downloadPersent == 100;
    urlMap.dowloading = !urlMap.completed;
    if (urlMap.completed) {
      urlMap.failed = await this.copyDocument(urlMap.localUri);
    }
    this.props.setDownloadUrlMaps({
      ...this.props.downloadUrlMaps,
      ...{ [urlMap.name]: urlMap },
    });
  };

  copyDocument = (uri) => {
    return new Promise(async (resolve, reject) => {
      try {
        await MediaLibrary.createAssetAsync(uri);
        resolve(false);
      } catch (error) {
        resolve(true);
      }
    });
  };

  download = (document) => {
    AppPermissions.checkMediaPermission()
      .then(async () => {
        if (this.props.connectionStatus) {
          let fileName = `document-${new Date().getTime().toString()}.pdf`;
          let localUri = LocalStorageService.documents(fileName);
          let urlMap = {
            localUri: localUri,
            remoteUri: document.uri,
            downloadStartedAt: new Date(),
            downloadUpdatedAt: new Date(),
            completed: false,
            name: fileName,
            downloadPersent: 0,
            dowloading: true,
            type: DOCUMENT,
          };
          alertValue("Added to download list");
          LocalStorageService.downloadFile(
            document.uri,
            localUri,
            this.callback,
            urlMap
          );
          this.props.setDownloadUrlMaps({
            ...this.props.downloadUrlMaps,
            ...{ [fileName]: urlMap },
          });
        } else {
          alertValue(CONNECTION_ERROR);
        }
      })
      .catch((error) => {
        alertValue("Permission is needed to save file");
      });
  };

  performAction = (option, assignmentId, document) => {
    if (option == 0) {
      if (document.size > MB1 * 10) {
        alertValue(LARGE_FILE);
      } else {
        this.props.navigation.push("ViewPdf", { document });
      }
    } else if (option == 1) {
      this.download(document);
    }
  };

  onClickAssinment = (assignmentId, document) => {
    const options = ["View", "Download", "Cancel"];
    const cancelButtonIndex = 2;
    this.props.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
      },
      (option) => {
        this.performAction(option, assignmentId, document);
      }
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Assignments
          assignments={this.getAssignmentsToRender()}
          onClick={this.onClickAssinment}
          loadMoreAssignments={this.loadMoreAssignments}
          refreshing={this.state.refreshing}
          refreshAssignments={this.refreshAssignments}
          loading={this.state.loading}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  assignments: state.assignmentsData.assignments,
  downloadUrlMaps: state.storageData.downloadUrlMaps,
});

const StudentAssignmentsWithActionSheet = connectActionSheet(
  StudentAssignmentsScreen
);

export default connect(mapStateToProps, {
  getAssignments,
  setAssignments,
  setDownloadUrlMaps,
})(StudentAssignmentsWithActionSheet);
