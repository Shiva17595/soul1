import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import {
  isEmptyHash,
  _onChooseDocument,
  isEmptyArray,
  jsonDig,
} from "../../../../../assets/reusableFunctions";
import ClassesDropDown from "../../../../../../../lib/ClassesDropDown";
import {
  alertValue,
  LoadingPage,
} from "../../../../../../../lib/ReUsableComponents";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import FireBaseStorageService from "../../../../../services/FireBaseStorageService";
import { ERROR_MESSAGE } from "../../../../../constants/Variables";
import NavigationService from "../../../../../actions/NavigationService";
import { Dropdown } from "react-native-material-dropdown";

class CreateAssignment extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Assignment";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    let headerRight = (
      <TouchableOpacity
        onPress={params.create ? params.create : params.update}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>
          {params.create ? "Create" : "Update"}
        </Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
      headerRight,
    };
  };

  constructor(props) {
    super(props);
    let assignmentId = this.props.navigation.getParam("assignmentId");
    let assignment = this.findAssignment(assignmentId);
    this.state = {
      name: assignment.name || "",
      document: assignment.document || {},
      classIds: assignment.classIds || [],
      assignment: assignment,
      loading: false,
      subjectId: jsonDig(assignment, ["subject", "subjectId"]) || "",
      assignment: assignment,
    };
    this.isUnmounted = false;
  }

  findAssignment = (assignmentId) => {
    let assignment = this.props.assignments.find(
      (e) => e.uniqueId == assignmentId
    );
    return assignment || {};
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  onCreate = () => {
    if (this.validateFields()) {
      this.setState(
        {
          loading: true,
        },
        this.createAssignment
      );
    }
  };

  uploadFile = (file, fileName) => {
    return new Promise(async (resolve, reject) => {
      let result = {};
      let path = FireBaseStorageService.assignments(fileName);
      let uri = await FireBase.uploadFile(file.uri, path);
      if (uri) {
        result = {
          type: file.type,
          uri: uri,
          name: file.name,
          fileName: fileName,
          size: file.size,
        };
        resolve(result);
      } else {
        reject();
      }
    });
  };

  createAssignment = async () => {
    try {
      let collection = FireBaseCollectionService.assignments();
      let assignmentId = FireBase.generateDocumentId(collection);
      let fileName = `${assignmentId}-${new Date()
        .getTime()
        .toString()}.${this.state.document.uri.split(/[\s.]+/).pop()}`;
      let document = await this.uploadFile(this.state.document, fileName);
      let doc = {
        name: this.state.name.trim(),
        classIds: this.state.classIds,
        document: document,
        user: {
          uid: this.props.user.uid,
          name: this.props.user.name,
          avatar: this.props.user.avatar,
        },
        subject: this.props.user.subjects.find(
          (s) => s.subjectId == this.state.subjectId
        ),
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      await FireBase.createDocument(collection, doc, assignmentId);
      NavigationService.navigate("MasterAssignments");
    } catch {
      alertValue(ERROR_MESSAGE);
    }
    !this.isUnmounted &&
      this.setState({
        loading: false,
      });
  };

  validateFields = () => {
    if (
      this.state.name.trim() &&
      !isEmptyHash(this.state.document) &&
      !isEmptyArray(this.state.classIds) &&
      this.state.subjectId
    ) {
      return true;
    } else {
      alertValue("All fields are mandatory");
      return false;
    }
  };

  onUpdate = () => {
    let doc = {};
    if (!this.validateFields()) {
      return;
    }
    if (this.state.assignment.name.trim() != this.state.name.trim()) {
      doc["name"] = this.state.name;
    }
    if (this.state.document.uri != this.state.assignment.uri) {
      doc["document"] = this.state.document;
    }
    if (
      this.state.classIds.join(",") != this.state.assignment.classIds.join(",")
    ) {
      doc["classIds"] = this.state.classIds;
    }
    if (this.state.assignment.subject.subjectId != this.state.subjectId) {
      doc["subject"] = this.props.user.subjects.find(
        (s) => s.subjectId == this.state.subjectId
      );
    }
    if (isEmptyHash(doc)) return;
    if (!this.state.loading) {
      this.setState(
        {
          loading: true,
        },
        () => {
          this.updateAssignment(doc);
        }
      );
    }
  };

  updateAssignment = async (doc) => {
    let collection = FireBaseCollectionService.assignments();
    if (this.state.document.uri != this.state.assignment.uri) {
      let fileName = `${
        this.state.assignment.uniqueId
      }-${new Date().getTime().toString()}.${this.state.document.uri
        .split(/[\s.]+/)
        .pop()}`;
      doc["document"] = await this.uploadFile(this.state.document, fileName);
    }
    FireBase.updateDocumentInfo(collection, this.state.assignment.uniqueId, doc)
      .then(() => {
        NavigationService.navigate("MasterAssignments");
        !this.isUnmounted &&
          this.setState({
            loading: false,
          });
      })
      .catch(() => {
        alertValue(ERROR_MESSAGE);
        !this.isUnmounted &&
          this.setState({
            loading: false,
          });
      });
  };

  chooseDocument = async () => {
    let document = await _onChooseDocument();
    if (document) {
      this.setState({
        document: document,
      });
    }
  };

  getDocument = () => {
    return (
      <TouchableOpacity style={styles.document} onPress={this.chooseDocument}>
        <Text style={{ fontWeight: "bold" }}>
          {isEmptyHash(this.state.document)
            ? "Select Document"
            : this.state.document.name || this.state.document.fileName}
        </Text>
      </TouchableOpacity>
    );
  };

  async componentDidMount() {
    if (isEmptyHash(this.state.assignment)) {
      this.props.navigation.setParams({
        goBack: this.goBack,
        create: this.onCreate,
      });
    } else {
      this.props.navigation.setParams({
        goBack: this.goBack,
        update: this.onUpdate,
      });
    }
  }

  setClassIds = (classIds) => {
    !this.isUnmounted &&
      this.setState({
        classIds: classIds,
      });
  };

  getClassesList = () => {
    return (
      <View>
        <ClassesDropDown
          setClassIds={this.setClassIds}
          selectedClassIds={this.state.classIds}
          multiSelect={true}
        />
        <View style={styles.document}>
          <Text style={{ fontWeight: "bold" }}>Selected Classes</Text>
          {isEmptyArray(this.state.classIds) ? (
            <Text>No Classes Selected</Text>
          ) : (
            this.props.classes.map((c) => {
              return this.state.classIds.includes(c.uniqueId) ? (
                <Text
                  key={c.uniqueId}
                >{`${c.name} - ${c.batch.batchName}`}</Text>
              ) : null;
            })
          )}
        </View>
      </View>
    );
  };

  parseSubjects = () => {
    return this.props.user.subjects.map((s) => ({
      label: s.subjectName,
      value: s.subjectId,
    }));
  };

  setSubjectId = (subjectId) => {
    if (this.state.subjectId != subjectId) {
      !this.isUnmounted &&
        this.setState({
          subjectId: subjectId,
        });
    }
  };

  getSubjects = () => {
    return (
      <View style={styles.subjectFilter}>
        <View style={styles.filtersItem}>
          <Dropdown
            label={"Select Subject"}
            data={this.parseSubjects()}
            value={this.state.subjectId}
            containerStyle={{ width: this.props.width * 0.8 }}
            onChangeText={this.setSubjectId}
          />
        </View>
      </View>
    );
  };

  onAssignmentNameChange = (value) => {
    this.setState({
      name: value,
    });
  };

  getFields = () => {
    return (
      <View style={{ alignItems: "center" }}>
        <View style={{ paddingVertical: 5 }}>
          <Text style={{ fontWeight: "bold", paddingVertical: 8 }}>
            Assignment Name
          </Text>
          <View style={styles.inputBox(this.props.width - 20)}>
            <TextInput
              multiline={false}
              onChangeText={this.onAssignmentNameChange}
              style={{ fontSize: 18, padding: 10 }}
              value={this.state.name}
              maxLength={100}
              spellCheck={true}
              placeholder="Enter Assignment Name"
            />
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.getDocument()}
        {this.getFields()}
        {this.getClassesList()}
        {this.getSubjects()}
        {this.state.loading ? <LoadingPage style={styles.loading} /> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  document: {
    backgroundColor: "#F4F2F1",
    minHeight: 60,
    margin: 6,
    padding: 5,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  inputBox: (width, height) => ({
    borderWidth: 1.5,
    borderColor: "#E8E7E7",
    borderRadius: 10,
    width: width,
    height: height ? height : 40,
  }),
  loading: {
    flex: 1,
    ...StyleSheet.absoluteFill,
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
  subjectFilter: {
    backgroundColor: "#F4F2F1",
    minHeight: 50,
    margin: 6,
    padding: 5,
    borderRadius: 4,
    justifyContent: "center",
  },
  filtersItem: { alignItems: "center" },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  classes: state.commonInfo.classes,
  assignments: state.assignmentsData.myAssignments,
});

export default connect(mapStateToProps, null)(CreateAssignment);
