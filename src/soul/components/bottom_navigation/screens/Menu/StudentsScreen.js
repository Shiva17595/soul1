import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import Students from "../Chat/List/Students";
import { SearchBar } from "react-native-elements";
import TabBarIcon from "../../../../assets/TabBarIcon";
import ClassesDropDown from "../../../../../../lib/ClassesDropDown";

class StudentsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Students";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      search: "",
      classId: this.props.user.classInfo.classId,
    };
    this.isUnmounted = false;
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
    });
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  openUserProfile = (params) => {
    this.props.navigation.push("Profile", { userInfo: params });
  };

  clearSearch = () => {
    this.updateSearch();
  };

  updateSearch = (search = "") => {
    if (search != this.state.search) {
      !this.isUnmounted &&
        this.setState({
          search: search ? search.toLowerCase() : "",
        });
    }
  };

  setClassId = (classId) => {
    if (classId != this.state.classId) {
      !this.isUnmounted &&
        this.setState({
          classId: classId,
        });
    }
  };

  filterComponent = () => {
    return <ClassesDropDown setClassId={this.setClassId} />;
  };

  getSearchBar = () => {
    return (
      <View>
        <SearchBar
          placeholder="Search"
          onChangeText={this.updateSearch}
          value={this.state.search}
          platform="default"
          clearIcon={
            this.state.search ? (
              <TouchableOpacity onPress={this.clearSearch}>
                <TabBarIcon
                  name="cancel"
                  type="MaterialIcons"
                  size={20}
                  color={"#A6A6A6"}
                />
              </TouchableOpacity>
            ) : null
          }
          containerStyle={styles.searchBar}
          inputContainerStyle={styles.seacrhBarInput}
          placeholderTextColor={"#AFAAAA"}
          round
        />
        {this.filterComponent()}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.getSearchBar()}
        <Students
          classId={this.state.classId}
          onPress={this.openUserProfile}
          search={this.state.search}
          selectedList={[]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  searchBar: {
    backgroundColor: "#FFFFFF",
    borderWidth: 0,
    shadowColor: "white",
    borderBottomColor: "#FFFFFF",
    borderTopColor: "#FFFFFF",
  },
  seacrhBarInput: { backgroundColor: "#F4F2F1" },
  filters: {
    backgroundColor: "#F4F2F1",
    minHeight: 50,
    margin: 6,
    padding: 5,
    borderRadius: 4,
  },
  filtersItem: { alignItems: "center" },
  filterText: { fontWeight: "bold", color: "#20368F" },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(StudentsScreen);
