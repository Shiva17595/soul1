import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import {
  EmptyComponent,
  LoadingPage,
  alertValue,
} from "../../../../../../../lib/ReUsableComponents";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import { isEmptyHash, getKey } from "../../../../../assets/reusableFunctions";
import { Card } from "react-native-elements";
import StudentResultComponent from "./StudentResultComponent";

class StudentResultScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Examination Results";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      resultData: {},
      userInfo: this.props.navigation.getParam("userInfo"),
      subjects: this.props.navigation.getParam("subjects"),
    };
    this.isUnmounted = false;
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
    });
    if (this.props.connectionStatus) this.getStudentResult();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      isEmptyHash(this.state.resultData) &&
      prevProps.connectionStatus != this.props.connectionStatus
    ) {
      !this.isUnmounted &&
        this.setState(
          {
            loading: true,
          },
          this.getStudentResult
        );
    }
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  getStudentResult = () => {
    let userInfo = this.state.userInfo;
    let collection = FireBaseCollectionService.studentAcademics(
      userInfo.classId,
      userInfo.examinationId
    );
    FireBase.getDocumentInfo(collection, userInfo.uid)
      .then((resultData) => {
        !this.isUnmounted &&
          this.setState({
            loading: false,
            resultData: resultData,
          });
      })
      .catch(() => {
        !this.isUnmounted &&
          this.setState({
            loading: false,
          });
      });
  };

  getInfo = (title, value) => {
    return (
      <View
        style={{
          flexDirection: "row",
          padding: 5,
        }}
      >
        <Text
          style={{
            flex: 2,
            fontWeight: "bold",
            fontSize: 16,
            flexWrap: "wrap",
          }}
        >
          {title}
        </Text>
        <Text
          style={{
            flex: 4,
            fontSize: 16,
            flexWrap: "wrap",
          }}
        >
          {value}
        </Text>
      </View>
    );
  };

  getPassedCount = () => {
    let count = 0;
    this.state.resultData.results.forEach((result) => {
      let subject = this.state.subjects.find(
        (s) => s.subjectId == result.subjectId
      );
      if (subject.minimumMarks <= result.marksScored) {
        count++;
      }
    });
    return count;
  };

  getFailedCount = () => {
    let count = 0;
    this.state.resultData.results.forEach((result) => {
      let subject = this.state.subjects.find(
        (s) => s.subjectId == result.subjectId
      );
      if (subject.minimumMarks > result.marksScored) {
        count++;
      }
    });
    return count;
  };

  getClassName = () => {
    let klass = this.props.classes.find(
      (c) => c.uniqueId == this.state.userInfo.classId
    );
    return klass ? klass.name : this.props.user.classInfo.className;
  };

  getHeader = () => {
    return (
      <View style={{ paddingVertical: 6 }}>
        <Card containerStyle={styles.cardContainerStyle}>
          {this.getInfo("Full Name", this.state.resultData.name)}
          {this.getInfo(
            "Examination Name",
            this.state.userInfo.examinationName
          )}
          {this.getInfo("Roll Number", this.state.resultData.rollNo)}
          {this.getInfo("Class Name", this.getClassName())}
          {this.getInfo("Total Passed", this.getPassedCount())}
          {this.getInfo("Total Failed", this.getFailedCount())}
        </Card>
      </View>
    );
  };

  renderComponent = (result) => {
    return (
      <StudentResultComponent {...result.item} subjects={this.state.subjects} />
    );
  };

  render() {
    let content = <EmptyComponent />;
    if (this.state.loading) {
      if (!this.props.connectionStatus) {
        content = (
          <EmptyComponent
            style={styles.empty}
            message="Please check your internet connection or try again later"
          />
        );
      } else if (this.state.loading) {
        content = <LoadingPage style={styles.empty} />;
      }
    } else if (!isEmptyHash(this.state.resultData)) {
      content = (
        <View style={styles.container}>
          <FlatList
            data={this.state.resultData.results}
            renderItem={this.renderComponent}
            keyExtractor={getKey}
            ListHeaderComponent={this.getHeader()}
          />
        </View>
      );
    } else {
      content = (
        <View style={styles.container}>
          <EmptyComponent style={styles.empty} message="Result Not Found" />
        </View>
      );
    }
    return content;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  classes: state.commonInfo.classes,
});

export default connect(mapStateToProps, null)(StudentResultScreen);
