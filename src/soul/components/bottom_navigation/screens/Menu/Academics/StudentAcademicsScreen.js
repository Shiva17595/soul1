import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import {
  EmptyComponent,
  LoadingPage,
} from "../../../../../../../lib/ReUsableComponents";
import { getKey, isEmptyArray } from "../../../../../assets/reusableFunctions";
import { ACADEMICS_FIELDS } from "../../../../../assets/FireBaseCollections";
import ExaminationResult from "./ExaminationResult";

class StudentAcademicsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Examination Results";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      academics: [],
      userInfo: this.props.navigation.getParam("userInfo"),
    };
    this.isUnmounted = false;
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
    });
    if (this.props.connectionStatus) this.getAcademics();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      isEmptyArray(this.state.academics) &&
      prevProps.connectionStatus != this.props.connectionStatus
    ) {
      !this.isUnmounted &&
        this.setState(
          {
            loading: true,
          },
          this.getAcademics
        );
    }
  }

  getAcademics = () => {
    let collection = FireBaseCollectionService.academics(
      this.state.userInfo.classId
    );
    FireBase.getCollection(collection, {
      [ACADEMICS_FIELDS.createdAt]: "desc",
    }).then((academics) => {
      !this.isUnmounted &&
        !this.isUnmounted &&
        this.setState({
          academics: academics,
          loading: false,
        });
    });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  openStudentResult = (examinationId, examinationName, subjects) => {
    this.props.navigation.push("StudentResult", {
      userInfo: { ...this.state.userInfo, examinationId, examinationName },
      subjects: subjects,
    });
  };

  renderComponent = (exam) => {
    return (
      <ExaminationResult {...exam.item} onPress={this.openStudentResult} />
    );
  };

  render() {
    let content = <EmptyComponent />;
    if (this.state.loading) {
      if (this.props.connectionStatus) {
        content = <LoadingPage style={styles.empty} />;
      } else if (!this.props.connectionStatus) {
        content = (
          <EmptyComponent
            style={styles.empty}
            message="Please check your internet connection or try again later"
          />
        );
      }
    } else {
      content = (
        <View style={styles.container}>
          <FlatList
            data={this.state.academics}
            renderItem={this.renderComponent}
            keyExtractor={getKey}
          />
        </View>
      );
    }
    return content;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(StudentAcademicsScreen);
