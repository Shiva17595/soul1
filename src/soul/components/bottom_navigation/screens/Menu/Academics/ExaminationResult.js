import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import { daysDiff } from "../../../../../assets/reusableFunctions";

class ExaminationResult extends Component {
  onClick = () => {
    this.props.onPress(
      this.props.uniqueId,
      this.props.name,
      this.props.subjects
    );
  };

  render() {
    return (
      <TouchableOpacity
        style={styles.card}
        activeOpacity={0.6}
        onPress={this.onClick}
      >
        <Card
          containerStyle={styles.cardContainerStyle}
          wrapperStyle={styles.wrapperStyle(
            daysDiff(this.props.createdAt) > 3 ? "#FFFFFF" : "#7bb956"
          )}
        >
          <View>
            <Text style={styles.text}>{this.props.name}</Text>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#ffff",
  },
  cardContainerStyle: {
    padding: 0,
    borderRadius: 10,
  },
  wrapperStyle: (color) => ({
    backgroundColor: color,
    borderRadius: 10,
  }),
  text: {
    fontWeight: "bold",
    padding: 10,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(ExaminationResult);
