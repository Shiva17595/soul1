import React, { PureComponent } from "react";
import { StyleSheet, Text, View, Alert } from "react-native";
import { Card } from "react-native-elements";
import { connect } from "react-redux";

class StudentResultComponent extends PureComponent {
  constructor(props) {
    super(props);
    let subject = this.props.subjects.find(
      (s) => s.subjectId == this.props.subjectId
    );
    this.maximumMarks = subject.maximumMarks;
    this.minimumMarks = subject.minimumMarks;
    this.persent = (this.props.marksScored / subject.maximumMarks) * 100;
    this.widthPersent = (this.props.width * 80) / 100;
  }

  render() {
    return (
      <Card containerStyle={{ borderRadius: 5 }}>
        <View style={styles.card}>
          <View style={styles.title}>
            <Text style={{ fontWeight: "bold" }}>Subject Name : </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              onPress={() => Alert.alert(this.props.subjectName)}
            >
              {this.props.subjectName}
            </Text>
          </View>
          <View style={styles.title}>
            <Text style={{ fontWeight: "bold" }}>Marks :</Text>
            <Text>{this.props.marksScored}</Text>
          </View>
          <View style={styles.title}>
            <Text style={{ fontWeight: "bold" }}>Total Marks :</Text>
            <Text>{this.maximumMarks}</Text>
          </View>
          <View>
            <View style={styles.persent}>
              <Text>{this.persent}%</Text>
            </View>
            <View style={styles.progressBar(this.widthPersent)}>
              <View
                style={{
                  width: (this.widthPersent * this.persent) / 100 - 4,
                  backgroundColor:
                    this.props.marksScored >= this.minimumMarks
                      ? "#7bb956"
                      : "#d6524d",
                }}
              />
              <View
                style={{
                  width: (this.widthPersent * (100 - this.persent)) / 100 - 4,
                  backgroundColor: "#FFFFFF",
                }}
              />
            </View>
          </View>
        </View>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    height: 100,
    justifyContent: "space-between",
  },
  title: {
    flexDirection: "row",
    alignItems: "flex-start",
  },
  persent: {
    alignItems: "center",
    justifyContent: "center",
  },
  progressBar: (width) => ({
    height: 10,
    borderColor: "#000",
    borderWidth: 2,
    borderRadius: 5,
    flexDirection: "row",
    width: width,
  }),
});

const mapStateToProps = (state) => {
  return {
    width: state.commonInfo.width,
    height: state.commonInfo.height,
    classInfo: state.userData.classInfo,
  };
};

export default connect(mapStateToProps, null)(StudentResultComponent);
