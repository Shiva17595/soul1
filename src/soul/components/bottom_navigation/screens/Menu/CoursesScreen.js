import React, { Component, Fragment } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList
} from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import { getCourses, setCourses } from "../../../../actions/commonActions";
import FireBaseCollectionService from "../../../../services/FireBaseCollectionService";
import { isEmptyArray, getKey } from "../../../../assets/reusableFunctions";
import {
  LoadingPage,
  EmptyComponent
} from "../../../../../../lib/ReUsableComponents";
import FireBase from "../../../../assets/FireBase";

class CoursesScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Courses";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack
    });
    await this.props.getCourses();
    if (isEmptyArray(this.props.courses) && this.props.connectionStatus) {
      this.setState(
        {
          loading: true
        },
        this.getCourses
      );
    }
  }

  getCourses = () => {
    let degreeCategoryCollection = FireBaseCollectionService.degreeCategory();
    let degreeCollection = FireBaseCollectionService.degree();
    let promises = [];
    promises.push(
      FireBase.getCollection(degreeCategoryCollection, null, null, null, 1000)
    );
    promises.push(
      FireBase.getCollection(degreeCollection, null, null, null, 1000)
    );
    Promise.all(promises).then(results => {
      let [degreeCatogeries = [], degrees = []] = results;
      degrees.forEach(d => {
        let catogery = degreeCatogeries.find(
          dc => dc.uniqueId == d.degreeCategory.degreeCategoryId
        );
        catogery.degrees = catogery.degrees || [];
        catogery.degrees.push(d);
      });
      this.props.setCourses(degreeCatogeries);
      this.setState({
        loading: false
      });
    });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  getDisplayCard = course => {
    return (
      <Card title={course.item.name} containerStyle={styles.cardContainerStyle}>
        <View>
          <View>
            <Text>{course.item.description}</Text>
          </View>
          {course.item.degrees.map(d => (
            <View key={d.name}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  paddingVertical: 6
                }}
              >
                <Text style={{ fontWeight: "bold", flex: 8 }}>{d.name}</Text>
                <Text
                  style={{
                    fontWeight: "bold",
                    flex: 2,
                    alignSelf: "center",
                    justifyContent: "flex-end"
                  }}
                >
                  {d.duriationInYears} Years
                </Text>
              </View>
              <Text>{d.description}</Text>
            </View>
          ))}
        </View>
      </Card>
    );
  };

  render() {
    let content = <EmptyComponent />;
    if (isEmptyArray(this.props.courses)) {
      if (this.props.connectionStatus && this.state.loading) {
        content = <LoadingPage style={styles.emptyPage} />;
      } else if (!this.props.connectionStatus) {
        content = (
          <EmptyComponent
            style={styles.emptyPage}
            message="Please check your internet connection or try again later"
          />
        );
      }
    } else {
      content = (
        <View style={styles.container}>
          <FlatList
            data={this.props.courses}
            renderItem={this.getDisplayCard}
            keyExtractor={getKey}
          />
        </View>
      );
    }
    return content;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff"
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center"
  },
  backButtonText: {
    paddingHorizontal: 6,
    color: "#FFFFFF"
  },
  emptyPage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  cardContainerStyle: {
    borderRadius: 10
  }
});

const mapStateToProps = state => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  courses: state.commonInfo.courses
});

export default connect(mapStateToProps, { getCourses, setCourses })(
  CoursesScreen
);
