import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import { findHeight } from "../../../../../assets/reusableFunctions";
import { componentNames } from "../../../../../constants/Variables";
import { setVideoId } from "../../../../../actions/commonActions";

class FilesViewScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = params.title;
    let headerTitleStyle = {
      color: "white",
    };
    let headerStyle = {
      backgroundColor: "#20368F",
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text
          style={{
            paddingHorizontal: 6,
            color: "#FFFFFF",
          }}
        >
          Back
        </Text>
      </TouchableOpacity>
    );

    // let headerRight = (
    //   <TouchableOpacity
    //     onPress={params.done}
    //     style={styles.headerButton}
    //     activeOpacity={1}
    //   >
    //     <Text
    //       style={{
    //         paddingHorizontal: 6,
    //         color: "#FFFFFF"
    //       }}
    //     >
    //       Done
    //     </Text>
    //   </TouchableOpacity>
    // );

    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
      // headerRight
    };
  };

  constructor(props) {
    super(props);
    this.files = this.props.navigation.getParam("files", []);
    this.title = this.props.navigation.getParam("title", "");
    this.props.navigation.setParams({
      goBack: this.goBack,
      title: this.title,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  getRenderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={this.viewFile.bind(this, item)}
        activeOpacity={1}
      >
        <ImageOrVideo
          style={{
            width: this.props.width,
            height: findHeight(item.width, this.props.width, item.height),
          }}
          file={item}
          viewName={componentNames.filesView}
        />
      </TouchableOpacity>
    );
  };

  viewFile = (file) => {
    this.props.navigation.push("View", { file });
  };

  getVisiableArray = ({ viewableItems }) => {
    console.log(viewableItems);
    if (this.props.viewName == componentNames.filesView && this.props.videoId) {
      let videoItem = viewableItems[0] ? viewableItems[0].item : null;
      console.log(videoItem);
      if (
        !videoItem ||
        this.props.videoId != `${componentNames.filesView}-${videoItem.name}`
      ) {
        this.props.setVideoId(null);
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.files}
          renderItem={this.getRenderItem}
          keyExtractor={(item, index) => `${index}`}
          showsVerticalScrollIndicator={false}
          ItemSeparatorComponent={() => (
            <View style={{ height: 8, backgroundColor: "#E0E0E0" }} />
          )}
          onViewableItemsChanged={this.getVisiableArray}
          viewabilityConfig={{
            minimumViewTime: 1000,
            itemVisiblePercentThreshold: 70,
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  videoId: state.commonInfo.videoId,
  viewName: state.commonInfo.viewName,
});

export default connect(mapStateToProps, { setVideoId })(FilesViewScreen);
