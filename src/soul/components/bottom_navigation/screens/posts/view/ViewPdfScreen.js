import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import LocalStorageService from "../../../../../services/LocalStorageService";
import PdfComponent from "../../../../../../../lib/PdfComponent";
import AppPermissions from "../../../../../../../lib/AppPermissions";
import { setDownloadUrlMaps } from "../../../../../actions/storageActions";
import { CONNECTION_ERROR, DOCUMENT } from "../../../../../constants/Variables";
import * as MediaLibrary from "expo-media-library";
import { shareFile } from "../../../../../assets/reusableFunctions";

class ViewPdfScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = params.title;
    let headerTitleStyle = {
      color: "white",
    };
    let headerStyle = {
      backgroundColor: "#20368F",
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.headerTextStyle}>Back</Text>
      </TouchableOpacity>
    );
    let headerRight;
    if (params.saveFileEnabled) {
      headerRight = (
        <TouchableOpacity
          onPress={params.download}
          style={styles.headerButton}
          activeOpacity={1}
        >
          <Text style={styles.headerTextStyle}>Download</Text>
        </TouchableOpacity>
      );
    } else {
      headerRight = null;
    }

    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
      headerRight,
    };
  };

  constructor(props) {
    super(props);
    let file = this.props.navigation.getParam("document", {});
    let saveFileEnabled = this.props.navigation.getParam(
      "saveFileEnabled",
      true
    );
    this.state = {
      file: file,
      saveFileEnabled: saveFileEnabled,
    };
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  copyDocument = (uri, share = true) => {
    return new Promise(async (resolve, reject) => {
      try {
        await MediaLibrary.createAssetAsync(uri);
        if (share) alertValue("Successfully saved");
        resolve(false);
      } catch (error) {
        console.log(error);
        if (share) shareFile(uri);
        resolve(true);
      }
    });
  };

  download = () => {
    AppPermissions.checkMediaPermission()
      .then(async () => {
        let localFile = this.props.urlMaps[this.state.file.fileName];
        if (this.state.file.uri.startsWith("file://")) {
          this.copyDocument(this.state.file.uri);
        } else if (localFile && localFile.completed) {
          this.copyDocument(localFile.localUri);
        } else if (this.props.connectionStatus) {
          let fileName = `document-${new Date().getTime().toString()}.pdf`;
          let localUri = LocalStorageService.documents(fileName);
          let urlMap = {
            localUri: localUri,
            remoteUri: this.state.file.uri,
            downloadStartedAt: new Date(),
            downloadUpdatedAt: new Date(),
            completed: false,
            name: fileName,
            downloadPersent: 0,
            dowloading: true,
            type: DOCUMENT,
          };
          alertValue("Added to download list");
          LocalStorageService.downloadFile(
            this.state.file.uri,
            localUri,
            this.callback,
            urlMap
          );
          this.props.setDownloadUrlMaps({
            ...this.props.downloadUrlMaps,
            ...{ [fileName]: urlMap },
          });
        } else {
          alertValue(CONNECTION_ERROR);
        }
      })
      .catch((error) => {
        alertValue("Permission is needed to save file");
      });
  };

  callback = async (downloadProgress, urlMap) => {
    urlMap.downloadPersent = Math.round(
      (downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite) *
        100
    );
    urlMap.downloadUpdatedAt = new Date();
    urlMap.completed = urlMap.downloadPersent == 100;
    urlMap.dowloading = !urlMap.completed;
    if (urlMap.completed) {
      urlMap.failed = await this.copyDocument(urlMap.localUri, false);
    }
    this.props.setDownloadUrlMaps({
      ...this.props.downloadUrlMaps,
      ...{ [urlMap.name]: urlMap },
    });
  };

  componentDidMount() {
    this.props.navigation.setParams({
      goBack: this.goBack,
      download: this.download,
      title: this.state.file.name,
      saveFileEnabled: this.state.saveFileEnabled,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <PdfComponent style={styles.container} source={this.state.file} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  headerTextStyle: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  urlMaps: state.storageData.urlMaps,
  downloadUrlMaps: state.storageData.downloadUrlMaps,
});

export default connect(mapStateToProps, { setDownloadUrlMaps })(ViewPdfScreen);
