import React, { Component } from "react";
import { connect } from "react-redux";
import { StyleSheet, View, Dimensions } from "react-native";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import {
  setDownloadFileName,
  setDownloadUrlMaps,
} from "../../../../../actions/storageActions";
import Constants from "expo-constants";
import { connectActionSheet } from "@expo/react-native-action-sheet";
import AppPermissions from "../../../../../../../lib/AppPermissions";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import * as MediaLibrary from "expo-media-library";
import LocalStorageService from "../../../../../services/LocalStorageService";
import { CONNECTION_ERROR } from "../../../../../constants/Variables";
import {
  shareFile,
  switchToLandscape,
  switchToPortrait,
  unlockScreenOrientation,
  findExtention,
} from "../../../../../assets/reusableFunctions";

class ViewScreen extends Component {
  constructor(props) {
    super(props);
    this.file = this.props.navigation.getParam("file", {});
    this.saveFileEnabled = this.props.navigation.getParam(
      "saveFileEnabled",
      true
    );
    this.state = {
      width: this.props.width,
      height: this.props.height,
      inFullscreen: false,
    };
    this.isUnmounted = false;
  }

  async componentDidMount() {
    await unlockScreenOrientation();
    Dimensions.addEventListener("change", this.onChangeDimensions);
  }

  componentWillUnmount() {
    switchToPortrait();
    Dimensions.removeEventListener("change", this.onChangeDimensions);
  }

  onChangeDimensions = ({ window: { width, height } }) => {
    !this.isUnmounted &&
      this.setState({
        width: width,
        height: height,
        inFullscreen: height < width,
      });
  };

  switchToLandscape = async () => {
    await switchToLandscape();
    const { width, height } = await Dimensions.get("window");
    !this.isUnmounted &&
      this.setState({
        width: width,
        height: height,
        inFullscreen: true,
      });
    await unlockScreenOrientation();
  };

  switchToPortrait = async () => {
    await switchToPortrait();
    const { width, height } = await Dimensions.get("window");
    !this.isUnmounted &&
      this.setState({
        width: width,
        height: height,
        inFullscreen: true,
      });
    await unlockScreenOrientation();
  };

  navigateToBack = () => {
    this.props.navigation.goBack();
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.width != nextState.width) {
      return true;
    }
    return false;
  }

  callback = async (downloadProgress, urlMap) => {
    urlMap.downloadPersent = Math.round(
      (downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite) *
        100
    );
    urlMap.downloadUpdatedAt = new Date();
    urlMap.completed = urlMap.downloadPersent == 100;
    urlMap.dowloading = !urlMap.completed;
    if (urlMap.completed) {
      urlMap.failed = await this.copyImage(urlMap.localUri, false);
    }
    this.props.setDownloadUrlMaps({
      ...this.props.downloadUrlMaps,
      ...{ [urlMap.name]: urlMap },
    });
  };

  copyImage = (uri, share = true) => {
    return new Promise(async (resolve, reject) => {
      try {
        await MediaLibrary.createAssetAsync(uri);
        if (share) alertValue("Successfully saved");
        resolve(false);
      } catch (error) {
        console.log(error);
        if (share) shareFile(uri);
        resolve(true);
      }
    });
  };

  saveImage = (index) => {
    if (index == 0) {
      AppPermissions.checkMediaPermission()
        .then(async () => {
          let localFile = this.props.urlMaps[this.file.name];
          if (this.file.uri.startsWith("file://")) {
            this.copyImage(this.file.uri);
          } else if (localFile && localFile.completed) {
            this.copyImage(localFile.localUri);
          } else if (this.props.connectionStatus) {
            let extention = findExtention(this.file.uri);
            let fileName = `${
              this.file.type
            }-${new Date().getTime().toString()}.${extention}`;
            let localUri = LocalStorageService.images(fileName);
            let urlMap = {
              localUri: localUri,
              remoteUri: this.file.uri,
              downloadStartedAt: new Date(),
              downloadUpdatedAt: new Date(),
              completed: false,
              name: fileName,
              downloadPersent: 0,
              dowloading: true,
              type: this.file.type,
            };
            alertValue("Added to download list");
            LocalStorageService.downloadFile(
              this.file.uri,
              localUri,
              this.callback,
              urlMap
            );
            this.props.setDownloadUrlMaps({
              ...this.props.downloadUrlMaps,
              ...{ [fileName]: urlMap },
            });
          } else {
            alertValue(CONNECTION_ERROR);
          }
        })
        .catch((error) => {
          alertValue("Camera Permission is needed");
        });
    }
  };

  onClickOptions = () => {
    const options = ["Save", "Cancel"];
    const cancelButtonIndex = 1;
    this.props.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
      },
      this.saveImage
    );
  };

  render() {
    return (
      <View style={styles.container(this.state.width, this.state.height)}>
        <View style={{ alignItems: "center" }}>
          <ImageOrVideo
            style={{
              width: this.state.width,
              height: this.state.height,
            }}
            file={this.file}
            resizeMode="contain"
            needRemoveButton
            removeAttachment={this.navigateToBack}
            removeButtonPaddingTop={Constants.statusBarHeight}
            closeIconSize={30}
            showOptionsIcon={this.saveFileEnabled}
            onClickOptions={this.onClickOptions}
            inFullscreen={this.state.inFullscreen}
            switchToLandscape={this.switchToLandscape}
            switchToPortrait={this.switchToPortrait}
            downloadFile={true}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: (width, height) => ({
    flex: 1,
    backgroundColor: "#000000",
    width: width,
    height: height,
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  urlMaps: state.storageData.urlMaps,
  downloadUrlMaps: state.storageData.downloadUrlMaps,
});

const ViewFile = connectActionSheet(ViewScreen);

export default connect(mapStateToProps, {
  setDownloadFileName,
  setDownloadUrlMaps,
})(ViewFile);
