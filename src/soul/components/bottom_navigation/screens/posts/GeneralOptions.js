import React, { Component } from "react";
import PostOption from "./PostOption";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";

class GeneralOptions extends Component {
  callbackFunction = index => {
    this.props.callbackFunction(index);
  };

  render() {
    const options = this.props.optionsArray.map((option, index) => {
      return (
        <PostOption
          key={index}
          iconName={option[3]}
          iconType={option[2]}
          option={option[0]}
          optionDescription={option[1]}
          index={index + 1}
          callbackFunction={this.callbackFunction}
        />
      );
    });
    return (
      <View
        style={StyleSheet.flatten([
          styles.bottomSheetPanelContainer,
          {
            height: 50 * (this.props.optionsArray.length + 2) - 10
          }
        ])}
      >
        {this.props.title}
        {options}
        <View style={styles.cancelTop}>
          <PostOption
            iconName={this.props.cancel[3]}
            iconType={this.props.cancel[2]}
            option={this.props.cancel[0]}
            optionDescription={this.props.cancel[1]}
            index={0}
            callbackFunction={this.callbackFunction}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height
});

export default connect(
  mapStateToProps,
  null
)(GeneralOptions);

const styles = StyleSheet.create({
  cancelTop: {
    borderTopWidth: 3,
    borderTopColor: "#D8D8D8",
    width: "100%",
    height: 40
  },
  bottomSheetPanelContainer: {
    backgroundColor: "#FFFFFF"
  }
});
