import React, { Component } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import TabBarIcon from "../../../../assets/TabBarIcon";
import { connect } from "react-redux";
import FireBase from "../../../../assets/FireBase";
import { NEWS_FEED_FIELDS } from "../../../../assets/FireBaseCollections";
import CollectionPaths from "../../../../services/FireBaseCollectionService";
import SoundService from "../../../../assets/AudioSound";

class PostFooter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      likedUsers: this.props.likedUsers,
      liked: this.props.likedUsers.includes(this.props.user.uid),
      updatingLikeStatus: false,
    };
  }

  changeLikeStatus = async () => {
    if (!this.state.updatingLikeStatus) {
      this.setState({
        updatingLikeStatus: true,
      });
      let likedUsers = [...this.state.likedUsers];
      let valueToUpdate = {};
      let index = this.state.likedUsers.indexOf(this.props.user.uid);
      if (index != -1) {
        valueToUpdate[
          NEWS_FEED_FIELDS.noOfLikes
        ] = FireBase.filedValueIncrement(-1);
        valueToUpdate[
          NEWS_FEED_FIELDS.likedUsers
        ] = FireBase.filedValueArrayRemove(this.props.user.uid);
        likedUsers.splice(index, 1);
      } else {
        valueToUpdate[
          NEWS_FEED_FIELDS.noOfLikes
        ] = FireBase.filedValueIncrement(1);
        valueToUpdate[
          NEWS_FEED_FIELDS.likedUsers
        ] = FireBase.filedValueArrayUnion(this.props.user.uid);
        likedUsers.push(this.props.user.uid);
      }
      SoundService.playLikeSound();
      if (this.props.connectionStatus) {
        await FireBase.getDocumentReference(
          CollectionPaths.newsFeed(this.props.uniqueId)
        ).update(valueToUpdate);

        this.setState({
          likedUsers: likedUsers,
          liked: likedUsers.includes(this.props.user.uid),
          updatingLikeStatus: false,
        });
      } else {
        this.setState({
          likedUsers: likedUsers,
          liked: !this.state.liked,
          updatingLikeStatus: false,
        });
      }
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.likedUsers.length != nextState.likedUsers.length) {
      return true;
    }
    if (this.state.liked != nextState.liked) {
      return true;
    }
    if (this.props.noOfComments != nextState.noOfComments) {
      return true;
    }
    return this.props.connectionStatus != nextProps.connectionStatus;
  }

  render() {
    return (
      <View style={styles.postFooter}>
        <View style={styles.footerTop}>
          <Text style={styles.footerLikeCommentText}>
            {this.state.likedUsers.length} Likes
          </Text>
          <Text style={styles.footerLikeCommentText}>
            {this.props.noOfComments} Comments
          </Text>
        </View>
        <View style={styles.footerBottom}>
          <View>
            <TouchableOpacity onPress={this.changeLikeStatus}>
              <View style={styles.button}>
                <View style={{ paddingHorizontal: 6 }}>
                  <TabBarIcon
                    name="like1"
                    type="AntDesign"
                    size={20}
                    color={this.state.liked ? "#455FF6" : "#9D9A9A"}
                  />
                </View>
                <View>
                  <Text
                    style={styles.like(
                      this.state.liked ? "#455FF6" : "#9D9A9A"
                    )}
                  >
                    Like
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={this.props.navigateToPostScreen}>
              <View style={styles.button}>
                <View style={{ paddingHorizontal: 6 }}>
                  <TabBarIcon
                    name="comment"
                    type="MaterialCommunityIcons"
                    size={20}
                    color="#9D9A9A"
                  />
                </View>
                <View>
                  <Text style={{ color: "#9D9A9A", fontWeight: "bold" }}>
                    Comment
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  postFooter: {
    flexDirection: "column",
  },
  footerTop: {
    flexDirection: "row",
    borderBottomWidth: 0.5,
    borderBottomColor: "#9D9A9A",
    width: "94%",
    marginHorizontal: "3%",
  },
  footerBottom: {
    flexDirection: "row",
    width: "94%",
    marginHorizontal: "3%",
  },
  footerLikeCommentText: {
    color: "#9D9A9A",
    paddingBottom: 5,
    paddingHorizontal: 3,
  },
  button: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    margin: 10,
    justifyContent: "space-between",
  },
  like: (color) => ({
    fontWeight: "bold",
    paddingTop: 2,
    color: color,
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
});

export default connect(mapStateToProps, null)(PostFooter);
