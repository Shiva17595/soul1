import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import TabBarIcon from "../../../../assets/TabBarIcon";
import { connect } from "react-redux";

class PostOption extends Component {
  callbackFunction = () => {
    this.props.callbackFunction(this.props.index);
  };

  render() {
    return (
      <TouchableOpacity onPress={this.callbackFunction} activeOpacity={0.7}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "flex-start",
            width: this.props.width,
            height: 50
          }}
        >
          <View style={{ padding: 7 }}>
            <TabBarIcon
              name={this.props.iconName}
              type={this.props.iconType}
              size={25}
              color="#3E3C3C"
            />
          </View>
          <View style={{ padding: 5 }}>
            <View style={{ padding: 2 }}>
              <Text style={{ color: "#3E3C3C", fontSize: 18 }}>
                {this.props.option}
              </Text>
            </View>
            <View style={{ padding: this.props.optionDescription ? 2 : 0 }}>
              <Text style={{ color: "#A4A4A4" }}>
                {this.props.optionDescription}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height
});

export default connect(
  mapStateToProps,
  null
)(PostOption);
