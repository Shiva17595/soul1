import React, { Component, Fragment } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Platform,
} from "react-native";
import { connect } from "react-redux";
import MultipleImagesAndVideos from "../../../../../../lib/MultipleImagesAndVideos";
import { findHeight, isEmptyHash } from "../../../../assets/reusableFunctions";
import { MB1 } from "../../../../constants/Variables";
import PdfComponent from "../../../../../../lib/PdfComponent";
import TabBarIcon from "../../../../assets/TabBarIcon";

class PostBody extends Component {
  shouldComponentUpdate(nextProps) {
    let uri =
      this.props.files[this.props.files.length - 1] &&
      this.props.files[this.props.files.length - 1].uri;
    let nextUri =
      nextProps.files[nextProps.files.length - 1] &&
      nextProps.files[nextProps.files.length - 1].uri;
    if (uri != nextUri) return true;
    if (this.props.files.length != nextProps.files.length) return true;
    if (this.props.document.uri != nextProps.document.uri) return true;
    return false;
  }

  navigateToShowFiles = (files, name) => {
    this.props.navigation.push("FilesView", { files: files, title: name });
  };

  navigateToShowDocument = () => {
    this.props.navigation.push("ViewPdf", { document: this.props.document });
  };

  calculateHeight = () => {
    let height;
    if (this.props.files.length == 1) {
      height = findHeight(
        this.props.files[0].width,
        this.props.width,
        this.props.files[0].height
      );
    }
    return height || this.props.height * 0.3;
  };

  getDocument = () => {
    if (this.props.document.size > MB1 * 5 || Platform.OS == "android") {
      return (
        <TouchableOpacity
          style={styles.documentStyle}
          onPress={this.navigateToShowDocument}
          activeOpacity={1}
        >
          <View style={styles.pdfIcon}>
            <TabBarIcon type="Ionicons" name="ios-document" color="#335EE9" />
          </View>
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={styles.pdfText(this.props.width - 35)}
          >
            {this.props.document.name}
          </Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.bodyContainer(
            this.props.width,
            this.props.height * 0.4
          )}
          onLongPress={this.navigateToShowDocument}
          activeOpacity={1}
        >
          <PdfComponent
            style={styles.bodyContainer(
              this.props.width,
              this.props.height * 0.4
            )}
            source={this.props.document}
          />
        </TouchableOpacity>
      );
    }
  };

  render() {
    return (
      <Fragment>
        {isEmptyHash(this.props.document) ? (
          <MultipleImagesAndVideos
            onPress={this.navigateToShowFiles}
            name={`${this.props.name}'s Post`}
            files={this.props.files}
            width={this.props.width}
            height={this.calculateHeight()}
            viewName={this.props.viewName}
          />
        ) : (
          this.getDocument()
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  bodyContainer: (width, height) => ({
    width: width,
    height: height,
  }),
  documentStyle: {
    flexDirection: "row",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
    height: 50,
    alignItems: "center",
    borderRadius: 10,
    padding: 5,
  },
  pdfIcon: {
    width: 25,
    padding: 5,
  },
  pdfText: (width) => ({
    color: "#000000",
    maxWidth: width,
  }),
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(PostBody);
