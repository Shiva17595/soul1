import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import {
  SECURITY_ICON_NAME,
  SECURITY_ICON_TYPE,
  CONNECTION_ERROR,
} from "../../../../constants/Variables";
import TabBarIcon from "../../../../assets/TabBarIcon";
import FireBase from "../../../../assets/FireBase";
import NavigationService from "../../../../actions/NavigationService";
import GeneralOptions from "./GeneralOptions";
import ModalView from "../../../../../../lib/ModalView";
import { setPostsAndClassPosts } from "../../../../actions/newsFeedActions";
import FeedBackScreen from "../../../feedback/FeedBackScreen";
import { NEWS_FEED_COLLECTION } from "../../../../assets/FireBaseCollections";
import CustomAvatar from "../../../../../../lib/CustomAvatar";
import CollectionPaths from "../../../../services/FireBaseCollectionService";
import { alertValue } from "../../../../../../lib/ReUsableComponents";

const optionsArray = [
  ["Give feedback on this post", "", "MaterialIcons", "feedback"],
  ["Edit Post", "", "MaterialIcons", "edit"],
  ["Delete Post", "", "MaterialCommunityIcons", "delete"],
];

const cancel = ["Cancel", "", "EvilIcons", "close"];

class PostHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deleting: false,
      modalVisible: false,
      feedbackModal: false,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.uniqueId != nextProps.uniqueId) {
      return true;
    }
    if (this.state.deleting != nextState.deleting) {
      return true;
    }
    if (this.state.modalVisible != nextState.modalVisible) {
      return true;
    }
    if (this.state.feedbackModal != nextState.feedbackModal) {
      return true;
    }
    return this.props.currentTime != nextProps.currentTime;
  }

  deletePost = () => {
    if (this.props.connectionStatus) {
      FireBase.getDocumentReference(
        CollectionPaths.newsFeed(this.props.uniqueId)
      )
        .delete()
        .then(() => {
          this.props.setPostsAndClassPosts(
            this.props.newsFeedData.filter(
              (item) => item.uniqueId != this.props.uniqueId
            ),
            this.props.classNewsFeedData.filter(
              (item) => item.uniqueId != this.props.uniqueId
            )
          );
          if (this.props.removePost) {
            this.props.removePost(this.props.uniqueId);
          } else {
            NavigationService.navigate("NewsFeed", {
              removePostId: this.props.uniqueId,
            });
          }
        })
        .catch((error) => {
          alertValue("Something Went Wrong");
          this.setState({
            deleting: false,
          });
        });
    } else {
      alertValue(CONNECTION_ERROR);
    }
  };

  performSelectedAction = (index) => {
    this.closeModal();
    switch (index) {
      case 1:
        setTimeout(() => {
          this.openFeedBackModal();
        }, 500);
        break;
      case 2:
        this.props.navigateToEditPost();
        break;
      case 3:
        let self = this;
        setTimeout(() => {
          Alert.alert(
            "Delete Post",
            "This post will be deleted permanently or You can also edit this post if you just want to change something.",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
              },
              {
                text: "Delete",
                onPress: () => {
                  self.setState(
                    {
                      deleting: true,
                    },
                    () => {
                      self.deletePost();
                    }
                  );
                },
              },
            ],
            {
              cancelable: false,
            }
          );
        }, 1000);
        break;
      default:
        break;
    }
  };

  getDeletingComponent = () => {
    if (this.props.connectionStatus && this.state.deleting) {
      return (
        <View style={styles.deletingIndicator}>
          <View
            style={{
              padding: 4,
            }}
          >
            <ActivityIndicator size="small" />
          </View>
          <Text
            style={{
              padding: 4,
              color: "red",
            }}
          >
            deleting....
          </Text>
        </View>
      );
    }
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  openModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  closeFeedBackModal = () => {
    this.setState({
      feedbackModal: false,
    });
  };

  openFeedBackModal = () => {
    this.setState({
      feedbackModal: true,
    });
  };

  openUserProfile = () => {
    let userInfo = {
      avatar: this.props.avatar,
      uid: this.props.uid,
      userName: this.props.userName,
    };
    NavigationService.push("Profile", this.props.uid, { userInfo });
  };

  render() {
    return (
      <View>
        {this.getDeletingComponent()}
        <View style={styles.postHeader}>
          <View
            style={{
              flexDirection: "row",
            }}
          >
            <CustomAvatar
              rounded
              source={{
                uri: this.props.avatar,
              }}
              uid={this.props.uid}
              onPress={
                this.props.viewProfileDisabled ? null : this.openUserProfile
              }
            />
            <View style={styles.postTitle}>
              <Text
                style={styles.postUserName}
                onPress={
                  this.props.viewProfileDisabled ? null : this.openUserProfile
                }
              >
                {" "}
                {this.props.userName}{" "}
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 6,
                }}
              >
                <TabBarIcon
                  name={SECURITY_ICON_NAME[this.props.security]}
                  type={SECURITY_ICON_TYPE[this.props.security]}
                  size={15}
                  color="#7E7B7B"
                />
                <Text style={styles.postTime}> {this.props.uploadedDate} </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              justifyContent: "center",
            }}
          >
            <TouchableOpacity
              style={{
                width: (this.props.width * 8) / 100,
              }}
              onPress={this.openModal}
              disabled={this.props.disabled}
            >
              <TabBarIcon
                type="Entypo"
                name="dots-three-horizontal"
                size={20}
                color="#898787"
              />
            </TouchableOpacity>
          </View>
        </View>
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={styles.modalStyle}
        >
          <GeneralOptions
            title={
              <View style={styles.headerComponent}>
                <View style={styles.panelHeader}>
                  <View style={styles.panelHandle} />
                </View>
              </View>
            }
            optionsArray={
              this.props.user.uid == this.props.uid
                ? optionsArray
                : optionsArray.slice(0, 1)
            }
            cancel={cancel}
            callbackFunction={this.performSelectedAction}
          />
        </ModalView>
        <ModalView
          isVisible={this.state.feedbackModal}
          onBackButtonPress={this.closeFeedBackModal}
          onBackdropPress={this.closeFeedBackModal}
          style={styles.modalStyle}
        >
          <FeedBackScreen
            title={
              <View style={styles.headerComponent}>
                <View style={styles.panelHeader}>
                  <Text style={styles.panelHeaderText}>
                    Please specify the problem you are facing
                  </Text>
                </View>
              </View>
            }
            id={this.props.uniqueId}
            type={NEWS_FEED_COLLECTION}
            close={this.closeFeedBackModal}
          />
        </ModalView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  postHeader: {
    flexDirection: "row",
    padding: 15,
    justifyContent: "space-between",
  },
  postTitle: {
    flex: 1,
  },
  postTitle: {
    fontSize: 17,
    color: "#3c3c3c",
    paddingHorizontal: 10,
  },
  postTime: {
    fontSize: 12,
    color: "#8C8989",
    paddingHorizontal: 3,
  },
  postUserName: {
    paddingBottom: 4,
    fontWeight: "bold",
  },
  headerComponent: {
    paddingTop: 20,
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
  },
  deletingIndicator: {
    flexDirection: "row",
    height: 25,
    backgroundColor: "#FFFFFF",
  },
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
  panelHeaderText: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#393535",
    borderBottomWidth: 0.5,
    borderBottomColor: "#000000",
  },
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  newsFeedData: state.newsFeeds.newsFeedData,
  classNewsFeedData: state.newsFeeds.classNewsFeedData,
});

export default connect(mapStateToProps, {
  setPostsAndClassPosts,
})(PostHeader);
