import React, { Component } from "react";
import { StyleSheet, Text, View, Alert, TextInput } from "react-native";
import { Button } from "react-native-elements";
import { connect } from "react-redux";
import CustomAvatar from "../../../../../../lib/CustomAvatar";

class NewPost extends Component {
  render() {
    return (
      <View>
        <View style={styles.newPostBox}>
          <View style={styles.profilePic}>
            <CustomAvatar
              rounded
              source={{ uri: this.props.user.avatar }}
              uid={this.props.user.uid}
              download={false}
            />
          </View>
          <View style={styles.inputBox}>
            <Button
              title="Add a post"
              type="clear"
              containerStyle={{ backgroundColor: "#F4F2F1", borderRadius: 8 }}
              titleStyle={{ color: "#000000" }}
              onPress={this.props.navigateToCreatePost}
            />
          </View>
        </View>
        <View style={{ height: 8, backgroundColor: "#E0E0E0" }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  newPostBox: {
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    padding: 10,
  },
  profilePic: {
    paddingTop: 3,
  },
  inputBox: {
    flex: 1,
    width: 100,
    paddingHorizontal: 8,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
});

export default connect(mapStateToProps, null)(NewPost);
