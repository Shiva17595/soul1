import React, { Component } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import PostHeader from "./PostHeader";
import PostFooter from "./PostFooter";
import PostBody from "./PostBody";
import NavigationService from "../../../../actions/NavigationService";
import { dateConvertion } from "../../../../assets/reusableFunctions";

class PostItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTotalContent: false,
    };
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  navigateToPostScreen = () => {
    NavigationService.navigate("Post", { post: this.props.post });
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (
      parseInt(this.props.post.noOfComments) + this.props.newComments !=
      parseInt(nextProps.post.noOfComments) + nextProps.newComments
    ) {
      return true;
    }
    if (this.props.post.noOfLikes != nextProps.post.noOfLikes) {
      return true;
    }
    return this.state.showTotalContent != nextState.showTotalContent;
  }

  showTotal = () => {
    this._isMounted &&
      this.setState({
        showTotalContent: !this.state.showTotalContent,
      });
  };

  navigateToEditPost = () => {
    if (this.props.user.uid == this.props.post.user.uid) {
      NavigationService.navigate("NewPost", {
        postToEdit: this.props.post,
      });
    }
  };

  render() {
    let post = this.props.post;
    return (
      <View style={styles.container}>
        <TouchableOpacity activeOpacity={1} onPress={this.navigateToPostScreen}>
          <View style={styles.postHeader}>
            <PostHeader
              uniqueId={post.uniqueId}
              uid={post.user.uid}
              userName={post.user.name}
              security={post.security}
              avatar={post.user.avatar}
              uploadedDate={dateConvertion(post.uploadedDate)}
              currentTime={new Date().toString()}
              disabled={false}
              navigateToEditPost={this.navigateToEditPost}
              removePost={this.props.removePost}
              viewProfileDisabled={this.props.viewProfileDisabled || false}
            />
          </View>
          <View style={{ margin: post.content ? 10 : 0 }}>
            <Text
              style={{ flexWrap: "wrap" }}
              ellipsizeMode="tail"
              selectable
              numberOfLines={this.state.showTotalContent ? 200 : 2}
              onPress={this.showTotal}
            >
              {post.content}
            </Text>
            {!this.state.showTotalContent && post.content.length > 20 ? (
              <Text onPress={this.showTotal} style={{ color: "#9D9A9A" }}>
                See more
              </Text>
            ) : null}
          </View>
          <View style={styles.imageStyling}>
            <PostBody
              files={post.files}
              name={post.user.name}
              uniqueId={post.uniqueId}
              document={post.document}
              navigation={this.props.navigation}
              viewName={this.props.viewName}
            />
          </View>
          <View style={styles.postFooter}>
            <PostFooter
              likedUsers={post.likedUsers}
              uniqueId={post.uniqueId}
              noOfLikes={post.noOfLikes}
              noOfComments={
                post.noOfComments +
                (this.props.newComments ? this.props.newComments : 0)
              }
              navigateToPostScreen={this.navigateToPostScreen}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 2,
  },
  postHeader: {},
  postFooter: {},
  imageStyling: {
    paddingVertical: 5,
    marginVertical: 5,
    width: "100%",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
});

export default connect(mapStateToProps, null)(PostItem);
