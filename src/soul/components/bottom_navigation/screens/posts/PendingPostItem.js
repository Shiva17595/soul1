import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import PostHeader from "./PostHeader";
import PostFooter from "./PostFooter";
import { setPendingPostItems } from "../../../../actions/newsFeedActions";
import FireBase from "../../../../assets/FireBase";
import { connect } from "react-redux";
import PostBody from "./PostBody";
import {
  dateConvertion,
  isEmptyArray,
  isEmptyHash,
} from "../../../../assets/reusableFunctions";
import NavigationService from "../../../../actions/NavigationService";
import CollectionPaths from "../../../../services/FireBaseCollectionService";
import FireBaseStorageService from "../../../../services/FireBaseStorageService";
import { VIDEO } from "../../../../constants/Variables";
import SoundService from "../../../../assets/AudioSound";

class PendingPostItem extends Component {
  constructor(props) {
    super(props);
    this._isMounted = true;
    this.state = {
      post: { ...this.props.post },
      status: false,
      posting: false,
      showTotalContent: false,
    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  createPost = async () => {
    if (this.state.posting) {
      let post = { ...this.state.post };
      post.files = [...post.files];
      let localattachments = [...post.localattachments];
      if (this.props.connectionStatus) {
        if (!isEmptyArray(localattachments)) {
          let promises = localattachments.map((attachment) => {
            attachment.fileName = `${attachment.type}-${
              this.props.user.uid
            }-${new Date().getTime()}.${attachment.uri.split(/[\s.]+/).pop()}`;
            return FireBase.uploadFile(
              attachment.uri,
              FireBaseStorageService.newsFeed(attachment.fileName)
            );
          });
          post.localattachments = [];
          let uris = await Promise.all(promises);
          uris.forEach((uri, i) => {
            if (uri) {
              let file = {
                type: localattachments[i].type,
                width: localattachments[i].width,
                height: localattachments[i].height,
                uri: uri,
                name: localattachments[i].fileName,
              };
              if (localattachments[i].type == VIDEO)
                file.duration = localattachments[i].duration;
              post.files.push(file);
            } else {
              post.localattachments.push(localattachments[i]);
            }
          });
          this.updatePostItems(post, isEmptyArray(post.localattachments));
        } else if (!isEmptyHash(post.document)) {
          let fileName = `document-${
            this.props.user.uid
          }-${new Date().getTime()}.pdf`;
          let path = FireBaseStorageService.newsFeed(fileName);
          let uri = await FireBase.uploadFile(post.document.uri, path);
          if (uri) {
            post.document = {
              uri: uri,
              name: post.document.name,
              fileName: fileName,
              size: post.document.size,
              success: true,
            };
          }
          this.updatePostItems(post, post.document.success || false);
        } else {
          this.updatePostItems(post, true);
        }
      } else {
        this.updatePostItems(post, false);
      }
    }
  };

  updatePostItems = (post, status) => {
    let pendingPosts = [...this.props.pendingPosts];
    let collectionName = CollectionPaths.newsFeed();
    for (let i = 0; i < pendingPosts.length; i++) {
      if (pendingPosts[i].uniqueId == post.uniqueId) {
        if (status) {
          if (!post.edituniqueId) {
            let newsFeedDocument = {
              content: post.content,
              files: post.files,
              likedUsers: [],
              noOfComments: 0,
              security: parseInt(post.security),
              noOfLikes: 0,
              user: post.user,
              classInfo: post.classInfo,
              updatedAt: new Date(),
              uploadedDate: new Date(),
              document: {},
            };
            if (!isEmptyHash(post.document)) {
              newsFeedDocument.document = {
                uri: post.document.uri,
                name: post.document.name,
                fileName: post.document.fileName,
                size: post.document.size,
              };
            }
            FireBase.createDocument(collectionName, newsFeedDocument)
              .then((response) => {
                pendingPosts.splice(i, 1);
                newsFeedDocument.uniqueId = response.id;
                this._isMounted &&
                  this.setState({
                    post: newsFeedDocument,
                    status: true,
                    posting: false,
                  });
                SoundService.playLikeSound();
                this.props.setPendingPostItems(pendingPosts);
              })
              .catch((error) => {
                this.props.setPendingPostItems(pendingPosts);
                this._isMounted &&
                  this.setState({
                    post: post,
                    status: false,
                    posting: false,
                  });
              });
          } else {
            let newsFeedDocumentToUpdate = {
              content: post.content,
              files: post.files,
              security: post.security,
              updatedAt: new Date(),
              document: {},
            };
            if (!isEmptyHash(post.document)) {
              newsFeedDocumentToUpdate.document = {
                uri: post.document.uri,
                name: post.document.name,
                fileName: post.document.fileName,
                size: post.document.size,
              };
            }
            FireBase.updateDocumentInfo(
              collectionName,
              post.edituniqueId,
              newsFeedDocumentToUpdate
            )
              .then(async () => {
                pendingPosts.splice(i, 1);
                this._isMounted &&
                  this.setState({
                    status: true,
                    posting: false,
                  });
                SoundService.playLikeSound();
                this.props.setPendingPostItems(pendingPosts);
              })
              .catch((error) => {
                this.props.setPendingPostItems(pendingPosts);
                this._isMounted &&
                  this.setState({
                    post: post,
                    status: false,
                    posting: false,
                  });
              });
          }
        } else {
          this._isMounted &&
            this.setState({
              post: post,
              status: false,
              posting: false,
            });
          pendingPosts[i] = post;
          this.props.setPendingPostItems(pendingPosts);
        }
      }
    }
  };

  getRetryComponent = () => {
    if (!this.state.status && this.props.connectionStatus) {
      return (
        <View style={styles.retryComponent}>
          <View style={{ padding: 4 }}>
            <ActivityIndicator size="small" />
          </View>
          <Text style={{ padding: 4 }}>posting....</Text>
        </View>
      );
    } else if (!this.state.status && !this.props.connectionStatus) {
      return (
        <View style={styles.retryComponent}>
          <Text style={{ padding: 4, color: "red" }}>
            No Internet Connection
          </Text>
        </View>
      );
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.posting != nextState.posting) {
      return true;
    }
    if (this.state.status != nextState.status) {
      return true;
    }
    if (this.state.showTotalContent != nextState.showTotalContent) {
      return true;
    }
    return this.props.connectionStatus != nextProps.connectionStatus;
  }

  componentDidUpdate() {
    if (
      this.props.connectionStatus &&
      !this.state.status &&
      !this.state.posting
    ) {
      this._isMounted && this.setState({ posting: true }, this.createPost);
    }
  }

  componentDidMount() {
    if (this.props.connectionStatus) {
      this._isMounted && this.setState({ posting: true }, this.createPost);
    }
  }

  navigateToPostScreen = () => {
    NavigationService.navigate("Post", { post: this.state.post });
  };

  showTotal = () => {
    this._isMounted &&
      this.setState({
        showTotalContent: !this.state.showTotalContent,
      });
  };

  render() {
    let post = this.state.post;
    return (
      <View style={styles.container}>
        {this.getRetryComponent()}
        <TouchableOpacity
          activeOpacity={1}
          onPress={this.navigateToPostScreen}
          disabled={!this.state.status}
        >
          <View style={styles.postHeader}>
            <PostHeader
              uniqueId={post.uniqueId}
              userName={post.user.name}
              security={post.security}
              avatar={post.user.avatar}
              uploadedDate={dateConvertion(post.uploadedDate)}
              disabled={!this.state.status}
            />
          </View>
          <View style={styles.postContent}>
            <Text
              style={{ flexWrap: "wrap" }}
              ellipsizeMode="tail"
              selectable
              numberOfLines={this.state.showTotalContent ? 200 : 2}
              onPress={this.showTotal}
            >
              {post.content}
            </Text>
          </View>
          <View style={styles.imageStyling}>
            <PostBody
              files={
                post.localattachments
                  ? post.files.concat(post.localattachments)
                  : post.files
              }
              document={post.document}
              name={this.props.post.user.name}
              navigation={this.props.navigation}
            />
          </View>
          {this.state.status ? (
            <View style={styles.postFooter}>
              <PostFooter
                likedUsers={[]}
                uniqueId={post.uniqueId}
                noOfLikes={post.noOfLikes}
                noOfComments={post.noOfComments}
                navigateToPostScreen={this.navigateToPostScreen}
              />
            </View>
          ) : (
            <View />
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 2,
    backgroundColor: "#FFFFFF",
  },
  postHeader: {},
  postFooter: {},
  imageStyling: {
    paddingVertical: 5,
    marginVertical: 5,
    width: "100%",
  },
  retryComponent: {
    flexDirection: "row",
    height: 25,
    backgroundColor: "#FFFFFF",
  },
  postContent: {
    margin: 10,
    flexDirection: "row",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  pendingPosts: state.newsFeeds.pendingPosts,
});

export default connect(mapStateToProps, {
  setPendingPostItems,
})(PendingPostItem);
