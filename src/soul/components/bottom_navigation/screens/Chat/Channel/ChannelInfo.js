import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Alert,
  Image,
} from "react-native";
import { connect } from "react-redux";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import { IMAGE, ERROR_MESSAGE } from "../../../../../constants/Variables";
import UserComponent from "../List/UserComponent";
import {
  EmptyComponent,
  LoadingPage,
  alertValue,
} from "../../../../../../../lib/ReUsableComponents";
import NavigationService from "../../../../../actions/NavigationService";
import { setChannels } from "../../../../../actions/chatActions";
import { Avatar } from "react-native-elements";
import { connectActionSheet } from "@expo/react-native-action-sheet";
import FireBase from "../../../../../assets/FireBase";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import ModalView from "../../../../../../../lib/ModalView";
import EditChannelNameModel from "./EditChannelNameModel";
import FireBaseStorageService from "../../../../../services/FireBaseStorageService";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import AppPermissions from "../../../../../../../lib/AppPermissions";

class ChannelInfo extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Channel Info";
    let headerTitleStyle = {
      color: "white",
    };
    let headerStyle = {
      backgroundColor: "#20368F",
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.backButton}
        activeOpacity={1}
      >
        <TabBarIcon
          name="ios-arrow-back"
          type="Ionicons"
          size={30}
          color="#FFFFFF"
        />
        <Text
          style={{
            paddingHorizontal: 6,
            color: "#FFFFFF",
          }}
        >
          Back
        </Text>
      </TouchableOpacity>
    );

    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
    };
  };

  constructor(props) {
    super(props);
    this.props.navigation.setParams({
      goBack: this.goBack,
    });

    let channelId = this.props.navigation.getParam("channelId");
    let channel = { ...this.findChannel(channelId) };
    this.state = {
      removingUser: false,
      channel: channel,
      editChannelNameModel: false,
      channelName: this.getChannelName(channel),
      channelImage: this.getImageUri(channel),
      changingAvatar: false,
    };
    this.isUnmounted = false;
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  findChannel = (channelId) => {
    return this.props.channels.find((c) => c.uniqueId == channelId);
  };

  goBack = () => {
    NavigationService.navigate("ChatScreen", {
      channelId: this.state.channel.uniqueId,
    });
  };

  getImageUri = (channel) => {
    if (channel.isGroup) return channel.avatar;
    if (channel.memberIds.length == 1) {
      return this.props.user.avatar;
    }
    let mId = channel.memberIds.find((mId) => mId != this.props.user.uid);
    return channel.members[mId].avatar;
  };

  getChannelName = (channel) => {
    if (channel.isGroup) return channel.name;
    if (channel.memberIds.length == 1) {
      return this.props.user.name;
    }
    let mId = channel.memberIds.find((mId) => mId != this.props.user.uid);
    return channel.members[mId].name;
  };

  openChannel = (params) => {
    let channelId =
      this.props.user.uid > params.uid
        ? `${params.uid}${this.props.user.uid}`
        : `${this.props.user.uid}${params.uid}`;
    let memberIds = [this.props.user.uid];
    if (params.uid != this.props.user.uid) memberIds.push(params.uid);
    let channelDoc = this.findChannel(channelId);
    if (!channelDoc) {
      channelDoc = {
        active: false,
        isGroup: false,
        memberIds: memberIds,
        members: {
          [params.uid]: {
            name: params.name,
            avatar: params.avatar,
            subTitle: params.subTitle,
          },
          [this.props.user.uid]: {
            name: this.props.user.name,
            avatar: this.props.user.avatar,
            subTitle: this.props.user.isMaster
              ? this.props.user.classInfo.className
              : this.props.user.rollNo,
          },
        },
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      let channels = [...this.props.channels];
      this.props.setChannels([
        ...channels,
        { ...channelDoc, ...{ uniqueId: channelId } },
      ]);
    }
    this.props.navigation.push("ChatScreen", { channelId });
  };

  addChannelMembers = () => {
    this.props.navigation.push("ModifyGroup", {
      channelId: this.state.channel.uniqueId,
    });
  };

  removeUser = (uid) => {
    if (!this.state.removingUser) {
      !this.isUnmounted && this.setState({ removingUser: true });
      let channel = { members: {} };
      Object.keys(this.state.channel.members).forEach((k) => {
        if (k != uid) {
          channel.members[k] = this.state.channel.members[k];
        }
      });
      channel.memberIds = this.state.channel.memberIds.filter(
        (mId) => mId != uid
      );
      let collection = FireBaseCollectionService.channels();
      FireBase.updateDocumentInfo(
        collection,
        this.state.channel.uniqueId,
        channel
      )
        .then(() => {
          if (this.props.user.uid == uid) {
            this.props.setChannels(
              this.props.channels.filter(
                (c) => c.uniqueId != this.state.channel.uniqueId
              )
            );
            NavigationService.navigate("ChatList");
          } else {
            !this.isUnmounted &&
              this.setState((prev) => ({
                channel: { ...prev.channel, ...channel },
                removingUser: false,
              }));
            let channels = [...this.props.channels];
            let editChannel = channels.find(
              (c) => c.uniqueId != this.state.channel.uniqueId
            );
            editChannel.memberIds = channel.memberIds;
            editChannel.members = channel.members;
            this.props.setChannels(channels);
          }
        })
        .catch(() => {
          !this.isUnmounted &&
            this.setState({
              removingUser: false,
            });
          alertValue("Cannot remove the user from group. Please try again");
        });
    }
  };

  performAction = (buttonIndex, uid) => {
    if (buttonIndex == 1) {
      setTimeout(() => {
        Alert.alert(
          this.props.user.uid == uid
            ? "Exit from the Group?"
            : "Remove User from the Group?",
          "Confirm",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            {
              text: "Remove",
              onPress: () => {
                this.removeUser(uid);
              },
            },
          ],
          {
            cancelable: false,
          }
        );
      }, 200);
    } else if (buttonIndex == 0) {
    }
  };

  componentDidUpdate() {
    let channel = this.findChannel(this.state.channel.uniqueId);
    if (channel.name && channel.name != this.state.channelName) {
      !this.isUnmounted &&
        this.setState({
          channelName: channel.name,
        });
    }
  }

  showOptions = (uid) => {
    const options = [
      "Info",
      this.props.user.uid == uid ? "Exit Group" : "Remove from Group",
      "Cancel",
    ];
    const cancelButtonIndex = 2;
    const destructiveButtonIndex = 1;
    this.props.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
        destructiveButtonIndex,
      },
      (index) => {
        this.performAction(index, uid);
      }
    );
  };

  closeEditChannelNameModel = () => {
    !this.isUnmounted &&
      this.setState({
        editChannelNameModel: false,
      });
  };

  openEditChannelNameModel = () => {
    !this.isUnmounted &&
      this.setState({
        editChannelNameModel: true,
      });
  };

  editChannelAvatar = async (avatar = "") => {
    if (this.props.connectionStatus) {
      !this.isUnmounted &&
        this.setState({
          changingAvatar: true,
          channelImage: avatar,
        });
      try {
        let remoteUri = "";
        if (avatar) {
          remoteUri = await FireBase.uploadFile(
            avatar,
            FireBaseStorageService.profilePictures(this.state.channel.uniqueId)
          );
          if (!remoteUri) {
            throw "Cannot Upload Image";
          }
        }
        let channel = {
          avatar: remoteUri,
          updatedAt: new Date(),
        };
        await FireBase.updateDocumentInfo(
          FireBaseCollectionService.channels(),
          this.state.channel.uniqueId,
          channel
        );
        let channels = [...this.props.channels];
        let editChannel = channels.find(
          (c) => c.uniqueId == this.state.channel.uniqueId
        );
        editChannel.avatar = channel.avatar;
        this.props.setChannels(channels);
      } catch (error) {
        alertValue(ERROR_MESSAGE);
      }
      !this.isUnmounted &&
        this.setState({
          changingAvatar: false,
        });
    } else {
      alertValue("Please check you insert connection and try again");
    }
  };

  changeGroupImage = (buttonIndex) => {
    if (buttonIndex == 1) {
      AppPermissions.checkPermission(Permissions.CAMERA_ROLL)
        .then(async () => {
          const attactment = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 0.1,
          });
          if (!attactment.cancelled) {
            if (attactment.type == IMAGE) {
              this.editChannelAvatar(attactment.uri);
            } else {
              alertValue("File type is not supported");
            }
          }
        })
        .catch(() => {
          alertValue("Camera Permission is needed");
        });
    } else if (buttonIndex == 0) {
      AppPermissions.checkPermission(Permissions.CAMERA)
        .then(async () => {
          const attactment = await ImagePicker.launchCameraAsync();
          if (!attactment.cancelled) {
            if (attactment.type == IMAGE) {
              this.editChannelAvatar(attactment.uri);
            } else {
              Alert.alert("File type is not supported");
            }
          }
        })
        .catch(() => {
          Alert.alert("Camera Permission is needed");
        });
    } else if (buttonIndex == 2) {
      Alert.alert(
        "Remove Icon",
        "Confirm",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "Remove",
            onPress: () => {
              this.editChannelAvatar("");
            },
          },
        ],
        {
          cancelable: false,
        }
      );
    }
  };

  chooseGroupImage = () => {
    const options = ["Take Photo", "Choose Photo", "Remove Icon", "Cancel"];
    const cancelButtonIndex = 3;
    const destructiveButtonIndex = 2;
    this.props.showActionSheetWithOptions(
      {
        options,
        destructiveButtonIndex,
        cancelButtonIndex,
      },
      this.changeGroupImage
    );
  };

  render() {
    return (
      <View style={styles.container(this.props.width, this.props.height)}>
        <ScrollView>
          <View>
            {this.state.channelImage ? (
              <ImageOrVideo
                style={styles.imageStyle(this.props.width)}
                file={{ uri: this.state.channelImage, type: IMAGE }}
              />
            ) : (
              <Image
                source={require("./group-large-icon.jpg")}
                style={styles.imageStyle(this.props.width)}
              />
            )}

            {this.state.channel.isGroup ? (
              <View style={styles.image(this.props.width)}>
                <TouchableOpacity
                  onPress={this.chooseGroupImage}
                  disabled={this.state.changingAvatar}
                >
                  <Avatar
                    rounded
                    icon={{ tyep: "Entypo", name: "camera" }}
                    iconStyle={{ color: "#20368F" }}
                    size={35}
                  />
                </TouchableOpacity>
              </View>
            ) : null}
            {this.state.changingAvatar ? (
              <LoadingPage style={styles.spinner(this.props.width)} />
            ) : null}
          </View>
          <View style={{ padding: 6 }}>
            <View style={styles.header}>
              <Text style={styles.heading}>Name</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Text
                style={{
                  padding: 10,
                  fontSize: 18,
                  flexWrap: "wrap",
                  maxWidth: this.props.width * 0.7,
                }}
              >
                {this.state.channelName}
              </Text>
              {this.state.channel.isGroup ? (
                <View style={{ flexDirection: "row" }}>
                  <TouchableOpacity
                    style={{
                      paddingHorizontal: 10,
                      justifyContent: "center",
                      paddingVertical: 5,
                      paddingHorizontal: 2,
                    }}
                    onPress={this.openEditChannelNameModel}
                  >
                    <Avatar
                      rounded
                      icon={{ tyep: "MaterialIcons", name: "edit" }}
                      iconStyle={{ color: "#20368F" }}
                      size={35}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      paddingHorizontal: 10,
                      justifyContent: "center",
                      paddingVertical: 5,
                      paddingHorizontal: 2,
                    }}
                    onPress={this.addChannelMembers}
                  >
                    <Avatar
                      rounded
                      icon={{ name: "add-circle" }}
                      iconStyle={{ color: "#20368F" }}
                      size={35}
                    />
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
            <View style={styles.header}>
              <Text style={styles.heading}>Members</Text>
            </View>
            <FlatList
              data={this.state.channel.memberIds}
              renderItem={({ item, index }) => (
                <UserComponent
                  avatar={this.state.channel.members[item].avatar}
                  name={this.state.channel.members[item].name}
                  subTitle={this.state.channel.members[item].subTitle}
                  uid={item}
                  onPress={this.openChannel}
                  onLongPress={
                    this.state.channel.isGroup ? this.showOptions : null
                  }
                />
              )}
              keyExtractor={(item, index) => item}
              ListEmptyComponent={
                <EmptyComponent
                  style={styles.emptyPage}
                  message={"No Results"}
                />
              }
              showsVerticalScrollIndicator={false}
            />
          </View>
        </ScrollView>
        {this.state.removingUser ? (
          <LoadingPage style={styles.loading} />
        ) : null}
        <ModalView
          isVisible={this.state.editChannelNameModel}
          onBackButtonPress={this.closeEditChannelNameModel}
          onBackdropPress={this.closeEditChannelNameModel}
          style={styles.modalStyle}
        >
          <EditChannelNameModel
            channelName={this.state.channel.name}
            channelId={this.state.channel.uniqueId}
            closeEditChannelNameModel={this.closeEditChannelNameModel}
          />
        </ModalView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: (width, height) => ({
    flex: 1,
    height: height,
    width: width,
  }),
  emptyPage: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  backButton: {
    height: 40,
    flexDirection: "row",
    alignSelf: "flex-start",
    paddingHorizontal: 8,
    alignItems: "center",
  },
  imageStyle: (width) => ({
    height: 300,
    width: width,
  }),
  header: {
    marginTop: 3,
    backgroundColor: "#F4F2F1",
    borderRadius: 3,
    alignItems: "center",
    paddingHorizontal: 6,
    flexDirection: "row",
    height: 35,
  },
  heading: {
    fontWeight: "bold",
  },
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
  image: (width) => ({
    flex: 1,
    position: "absolute",
    padding: 10,
    alignItems: "flex-end",
    justifyContent: "flex-end",
    width: width,
    height: 300,
  }),
  spinner: (width) => ({
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    width: width,
    height: 300,
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  channels: state.chatData.channels,
});

const ChannelInfoWithActionSheet = connectActionSheet(ChannelInfo);

export default connect(mapStateToProps, { setChannels })(
  ChannelInfoWithActionSheet
);
