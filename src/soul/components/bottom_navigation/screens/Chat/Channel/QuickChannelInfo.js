import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import { IMAGE } from "../../../../../constants/Variables";
import TabBarIcon from "../../../../../assets/TabBarIcon";

class QuickChannelInfo extends Component {
  openChannelInfo = () => {
    this.props.closeModal();
    this.props.openChannelInfo({ channelId: this.props.channelId });
  };

  openChannel = () => {
    this.props.closeModal();
    this.props.openChannel({ channelId: this.props.channelId });
  };

  render() {
    return (
      <View style={styles.component(this.props.width, this.props.height)}>
        <ImageOrVideo
          style={styles.imageStyle(this.props.width, this.props.height)}
          file={{ uri: this.props.avatar, type: IMAGE }}
          resizeMode="cover"
        />
        <View style={styles.layout(this.props.width, this.props.height)}>
          <View style={styles.name}>
            <Text style={styles.textStyle}>{this.props.name}</Text>
          </View>
          <View style={styles.layoutButtons}>
            <TouchableOpacity onPress={this.openChannel}>
              <TabBarIcon
                name="message"
                type="MaterialIcons"
                size={30}
                color="#20368F"
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.openChannelInfo}>
              <TabBarIcon
                name="info"
                type="Feather"
                size={30}
                color="#20368F"
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  component: (width, heigth) => ({
    backgroundColor: "#FFFFFF",
    justifyContent: "center",
    width: width * 0.8,
    height: heigth * 0.5,
  }),
  imageStyle: (width, height) => ({
    height: height * 0.5,
    width: width * 0.8,
  }),
  layout: (width, height) => ({
    height: height * 0.5,
    width: width * 0.8,
    justifyContent: "space-between",
    ...StyleSheet.absoluteFill,
  }),
  layoutButtons: {
    flexDirection: "row",
    backgroundColor: "#FFFFFF",
    height: 50,
    justifyContent: "space-around",
    alignItems: "center",
  },
  name: {
    height: 40,
    paddingHorizontal: 5,
    backgroundColor: "rgba(52, 52, 52, 0.2)",
    justifyContent: "center",
  },
  textStyle: {
    color: "#FFFFFF",
    fontSize: 20,
  },
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(QuickChannelInfo);
