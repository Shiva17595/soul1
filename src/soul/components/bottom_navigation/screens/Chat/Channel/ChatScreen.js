import React, { Component } from "react";
import { StyleSheet, View, Platform } from "react-native";
import {
  StatusBar,
  alertValue,
} from "../../../../../../../lib/ReUsableComponents";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import { connect } from "react-redux";
import CustomizeGiftedChat from "./CustomizeGiftedChat";
import FireBase from "../../../../../assets/FireBase";
import { setChannels } from "../../../../../actions/chatActions";
import NavigationService from "../../../../../actions/NavigationService";
import { ERROR_MESSAGE } from "../../../../../constants/Variables";

class ChatScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    let header = <StatusBar />;
    return {
      header,
    };
  };

  constructor(props) {
    super(props);
    this.channelId = this.props.navigation.getParam("channelId");
    this.channel = {
      ...this.findChannel(this.channelId),
    };
    delete this.channel["uniqueId"];
    this.state = {
      loading: true,
      channelName: this.getChannelName(),
      channelImage: this.getChannelAvatar(),
    };
    this.isUnmounted = false;
  }

  findChannel = (channelId) => {
    return this.props.channels.find((c) => c.uniqueId == channelId);
  };

  async componentDidMount() {
    this.createChannel();
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  createChannel = () => {
    return new Promise((resolve, reject) => {
      let collection = FireBaseCollectionService.channels();
      FireBase.getDocument(collection, this.channelId)
        .then((doc) => {
          if (!doc || !doc.exists) {
            FireBase.createDocument(collection, this.channel, this.channelId)
              .then(() => {
                resolve();
                !this.isUnmounted &&
                  this.setState({
                    loading: false,
                  });
              })
              .catch((error) => {
                alertValue(ERROR_MESSAGE);
                this.goBack();
              });
          } else {
            !this.isUnmounted &&
              this.setState({
                loading: false,
              });
          }
        })
        .catch((error) => {
          !this.isUnmounted &&
            this.setState({
              loading: false,
            });
        });
    });
  };

  goBack = () => {
    NavigationService.navigate("ChatList");
  };

  getChannelName = () => {
    let c = this.channel;
    if (c.isGroup) return c.name;
    if (c.memberIds.length == 1) {
      return this.props.user.name;
    }
    let mId = c.memberIds.find((mId) => mId != this.props.user.uid);
    return c.members[mId].name;
  };

  getChannelAvatar = () => {
    let c = this.channel;

    if (c.isGroup) return c.avatar;
    if (c.memberIds.length == 1) {
      return this.props.user.avatar;
    }
    let mId = c.memberIds.find((mId) => mId != this.props.user.uid);
    return c.members[mId].avatar;
  };

  openChannelInfo = () => {
    this.props.navigation.push("ChannelInfo", {
      channelId: this.channelId,
    });
  };

  componentDidUpdate() {
    let channel = this.findChannel(this.channelId);
    if (
      channel &&
      channel.isGroup &&
      (channel.name != this.state.channelName ||
        channel.avatar != this.state.channelImage)
    ) {
      !this.isUnmounted &&
        this.setState({
          channelName: channel.name,
          channelImage: channel.avatar,
        });
    }
  }

  render() {
    let content, giftedChat;
    giftedChat = (
      <CustomizeGiftedChat
        channel={this.channel}
        channelName={this.state.channelName}
        channelAvatar={this.state.channelImage}
        channelId={this.channelId}
        otherUsers={this.channel.members}
        isGroup={this.channel.isGroup}
        loading={this.state.loading}
        goBack={this.goBack}
        openChannelInfo={this.openChannelInfo}
        navigation={this.props.navigation}
      />
    );

    if (Platform.OS === "ios") {
      content = giftedChat;
    } else {
      content = giftedChat;
    }
    return (
      <View style={styles.container(this.props.height, this.props.width)}>
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: (height, width) => ({
    flex: 1,
    backgroundColor: "#FFFFFF",
    height: height,
    width: width,
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  channels: state.chatData.channels,
});

export default connect(mapStateToProps, { setChannels })(ChatScreen);
