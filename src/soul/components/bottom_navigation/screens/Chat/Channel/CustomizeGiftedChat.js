import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
  Text,
  Clipboard,
  Alert,
  SafeAreaView,
  KeyboardAvoidingView,
} from "react-native";
import {
  StatusBar,
  LoadingPage,
  alertValue,
} from "../../../../../../../lib/ReUsableComponents";
import { connect } from "react-redux";
import { GiftedChat, Send, Bubble } from "react-native-gifted-chat";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import FireBase from "../../../../../assets/FireBase";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import {
  isEmptyArray,
  getDate,
  findWidth,
  minutesDiff,
} from "../../../../../assets/reusableFunctions";
import {
  getChatMessages,
  setChatMessages,
  getPendingChatMessages,
  setPendingChatMessages,
} from "../../../../../actions/chatActions";
import { CHAT_FIELDS } from "../../../../../assets/FireBaseCollections";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import {
  DOCUMENT,
  FILE,
  VIDEO,
  CHAT_LIMIT,
  CONNECTION_ERROR,
} from "../../../../../constants/Variables";
import ModalView from "../../../../../../../lib/ModalView";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";
import ChatAdditionalOptions from "./ChatAdditionalOptions";
import Constants from "expo-constants";
import FireBaseStorageService from "../../../../../services/FireBaseStorageService";
import MultipleImagesAndVideos from "../../../../../../../lib/MultipleImagesAndVideos";
import { connectActionSheet } from "@expo/react-native-action-sheet";

class CustomizeGiftedChat extends Component {
  static navigationOptions = ({ navigation }) => {
    let header = <StatusBar />;
    return {
      header,
    };
  };

  constructor(props) {
    super(props);
    this.loginUser = {
      _id: this.props.user.uid,
      name: this.props.user.name,
      avatar: this.props.user.avatar,
    };
    this.state = {
      messages: [],
      pendingMessages: [],
      loadedAllMessages: false,
      modalVisible: false,
      loadingEarlierMessages: false,
      files: [],
    };
    this.isUnmounted = false;
  }

  onSend = (messages = []) => {
    let databaseFormat = {};
    if (this.state.files.length > 0) {
      let newM = {
        createdAt: new Date(),
        text: "",
        type: FILE,
        user: this.loginUser,
        _id: FireBase.generateDocumentId(
          FireBaseCollectionService.messages(this.props.channelId)
        ),
      };
      newM.files = this.state.files;
      messages.push(newM);
    }
    messages.forEach((m) => {
      databaseFormat[m["_id"]] = this.convertToDatabaseFormat(m);
      m.pending = true;
    });
    !this.isUnmounted &&
      this.setState((previousState) => ({
        pendingMessages: GiftedChat.append(
          previousState.pendingMessages,
          messages
        ),
        files: [],
      }));
    this.createMessages(databaseFormat);
  };

  uploadFiles = (messageId, message) => {
    return new Promise((resolve, reject) => {
      let promises = [];
      let files = [];
      message.files.forEach((a, index) => {
        let fileName = `${a.type}-${messageId}-${index}`;
        let path = FireBaseStorageService.channels(
          this.props.channelId,
          fileName
        );
        let file = {
          type: a.type,
          name: `${fileName}.${a.uri.split(/[\s.]+/).pop()}`,
          width: a.width,
          height: a.height,
        };
        if (a.type == VIDEO) file.duration = a.duration;
        files.push(file);
        promises.push(FireBase.uploadFile(a.uri, path));
      });
      message.files = [];
      Promise.all(promises).then((uris) => {
        uris.forEach((v, index) => {
          if (v != undefined) {
            let file = {
              uri: v,
              type: files[index].type,
              name: files[index].name,
              width: files[index].width,
              height: files[index].height,
            };
            if (files[index].type == VIDEO)
              file.duration = files[index].duration;
            message.files.push(file);
          }
        });
        resolve();
      });
    });
  };

  createMessages = (messages) => {
    let collectionName = FireBaseCollectionService.messages(
      this.props.channelId
    );
    Object.keys(messages).forEach(async (messageId) => {
      let message = messages[messageId];
      try {
        if (message.files) {
          await this.uploadFiles(messageId, message);
        }
        FireBase.createDocument(collectionName, message, messageId);
      } catch (error) {}
    });
  };

  convertToDatabaseFormat = (message) => {
    let doc = {
      message: message.text,
      messageType: message.type || "text",
      uid: this.props.user.uid,
      createdAt: message.createdAt,
      updatedAt: message.updatedAt ? message.updatedAt : message.createdAt,
    };
    if (message.files) doc.files = message.files;
    return doc;
  };

  getEarlierMessages = async () => {
    let document;
    let collection = FireBaseCollectionService.messages(this.props.channelId);
    let docId = this.state.messages[this.state.messages.length - 1]._id;
    if (docId) {
      document = await FireBase.getDocument(collection, docId);
    }
    let messages = [];
    let earlierMessages = await FireBase.getCollection(
      collection,
      { [CHAT_FIELDS.createdAt]: "desc" },
      document,
      null,
      CHAT_LIMIT
    );
    earlierMessages.forEach((m) => {
      messages.push(this.convertMessage(m.uniqueId, m));
    });
    !this.isUnmounted &&
      this.setState((prev) => ({
        messages: GiftedChat.append(messages, prev.messages),
        loadedAllMessages: messages.length < CHAT_LIMIT ? true : false,
        loadingEarlierMessages: false,
      }));
  };

  loadEarlierMessages = () => {
    if (
      !this.state.loadingEarlierMessages &&
      !this.state.loadedAllMessages &&
      !isEmptyArray(this.state.messages)
    ) {
      !this.isUnmounted &&
        this.setState(
          {
            loadingEarlierMessages: true,
          },
          this.getEarlierMessages
        );
    }
  };

  loadChats = () => {
    return (
      <LoadingPage
        style={styles.loading(this.props.height - Constants.statusBarHeight)}
      />
    );
  };

  renderSend = (props) => {
    return (
      <Send
        {...props}
        containerStyle={{
          justifyContent: "center",
        }}
      >
        <View>
          <TabBarIcon
            type="MaterialIcons"
            name="send"
            size={30}
            color="#1B7DE2"
          />
        </View>
      </Send>
    );
  };

  componentWillUnmount() {
    this.props.setChatMessages(
      this.props.channelId,
      this.state.messages.slice(
        this.state.messages.length - 100,
        this.state.messages.length
      )
    );
    this.props.setPendingChatMessages(
      this.props.channelId,
      this.state.pendingMessages
    );
    if (this.inactivateListener) this.inactivateListener();
    this.isUnmounted = true;
  }

  printArray = (array) => {
    array.forEach((a) => console.log(a._id, a.text));
  };

  convertMessage = (id, data) => {
    let m = {
      _id: id,
      text: data.message,
      createdAt: getDate(data.createdAt),
      updatedAt: getDate(data.updatedAt),
      user:
        data.uid == this.props.user.uid
          ? this.loginUser
          : { ...this.props.otherUsers[data.uid], ...{ _id: data.uid } },
      sent: true,
    };
    if (data.files) {
      m.type = FILE;
      m.files = data.files;
    } else if (data.document) {
      m.type = DOCUMENT;
      m.document = data.document;
    }
    return m;
  };

  messageUpdates = (messagesRef) => {
    let messages = [...this.state.messages];
    let pendingMessages = [...this.state.pendingMessages];
    let stateChanged = false;
    let newPendingMessages = [];
    pendingMessages.forEach((pM) => {
      let doc;
      messagesRef.forEach((m) => {
        if (m.id == pM._id) doc = m;
      });
      if (doc && !doc.metadata.hasPendingWrites) {
        pM.pending = false;
        pM.sent = true;
        if (doc.data().files) pM.files = doc.data().files;
        stateChanged = true;
        messages = GiftedChat.append(messages, pM);
      } else {
        newPendingMessages.push(pM);
      }
    });
    pendingMessages = newPendingMessages;
    let messagesDiff = messagesRef.docChanges();
    messagesDiff.forEach((message) => {
      if (!message.doc.metadata.hasPendingWrites) {
        stateChanged = true;
        if (message.type === "added") {
          if (!messages.find((m) => m._id == message.doc.id)) {
            messages = GiftedChat.append(
              messages,
              this.convertMessage(message.doc.id, message.doc.data())
            );
          }
        } else if (message.type === "modified") {
          messages.find((m) => {
            if (m._id == message.doc.id) {
              m.text = message.doc.data().message;
              return true;
            }
          });
        } else if (message.type === "removed") {
          for (let i = 0; i < messages.length; i++) {
            if (messages[i]._id == message.doc.id) {
              messages.splice(i, 1);
              break;
            }
          }
        }
      }
    });
    if (stateChanged) {
      this.setMessages(messages, pendingMessages);
      this.props.setChatMessages(
        this.props.channelId,
        messages.slice(messages.length - 100, messages.length)
      );
      this.props.setPendingChatMessages(this.props.channelId, pendingMessages);
    }
  };

  initialMessages = (collection, docId) => {
    let stateMessages = [...this.state.messages];
    return new Promise(async (resolve, reject) => {
      let document,
        documentId = docId;
      for (let i = 0; i < 3 && documentId; i++) {
        if (documentId) {
          try {
            document = await FireBase.getDocument(collection, documentId);
            if (!document || !document.exists) {
              break;
            }
          } catch (error) {
            document = null;
          }
        }
        let messages = [];
        let newMessages = await FireBase.getCollection(
          collection,
          { [CHAT_FIELDS.createdAt]: "asc" },
          document,
          null,
          CHAT_LIMIT
        );
        if (!isEmptyArray(newMessages)) {
          for (let j = newMessages.length - 1; j >= 0; j = j - 1) {
            messages.push(
              this.convertMessage(newMessages[j].uniqueId, newMessages[j])
            );
          }
          stateMessages = GiftedChat.append(stateMessages, messages);
          this.setMessages(stateMessages);
        }
        if (newMessages.length < CHAT_LIMIT) {
          this.props.setChatMessages(
            this.props.channelId,
            stateMessages.slice(
              stateMessages.length - 100,
              stateMessages.length
            )
          );
          resolve(!isEmptyArray(messages) ? messages[0]._id : null);
          return;
        }
        documentId = messages[0]._id;
      }
      let newMessages = await FireBase.getCollection(
        collection,
        { [CHAT_FIELDS.createdAt]: "desc" },
        null,
        null,
        50
      );
      let messages = [];
      if (!isEmptyArray(newMessages)) {
        newMessages.forEach((m) => {
          messages.push(this.convertMessage(m.uniqueId, m));
        });
      }
      this.props.setChatMessages(
        this.props.channelId,
        messages.slice(messages.length - 100, messages.length)
      );
      this.setMessages(messages);
      resolve(!isEmptyArray(messages) ? messages[0]._id : null);
    });
  };

  async componentDidMount() {
    await this.props.getChatMessages(this.props.channelId);
    await this.props.getPendingChatMessages(this.props.channelId);
    let messages =
      [...this.props.messagesByChannel[this.props.channelId]] || [];
    if (!isEmptyArray(this.props.channel.deletedMessages)) {
      messages = messages.filter(
        (m) => !this.props.channel.deletedMessages.includes(m._id)
      );
    }
    let pendingMessages =
      [...this.props.pendingMessagesByChannel[this.props.channelId]] || [];
    this.setMessages(messages, pendingMessages);
    if (this.props.connectionStatus) {
      let collection = FireBaseCollectionService.messages(this.props.channelId);
      let docId, document, startDoc;
      if (!isEmptyArray(messages)) {
        docId = messages[0]._id;
      }
      startDoc = await this.initialMessages(collection, docId);
      startDoc = startDoc || docId;
      if (startDoc) {
        try {
          document = await FireBase.getDocument(collection, startDoc);
        } catch (error) {
          document = null;
        }
      }
      this.inactivateListener = FireBase.getListener(
        collection,
        { [CHAT_FIELDS.createdAt]: "asc" },
        document,
        null,
        { includeMetadataChanges: true },
        this.messageUpdates
      );
    }
  }

  setMessages = (messages = [], pendingMessages = []) => {
    (!isEmptyArray(messages) || !isEmptyArray(pendingMessages)) &&
      !this.isUnmounted &&
      this.setState({
        messages: messages,
        pendingMessages: pendingMessages,
      });
  };

  chatHeader = () => {
    return (
      <View
        style={{
          flex: 1,
          position: "absolute",
          zIndex: 1,
          justifyContent: "flex-start",
          height: 50,
          width: this.props.width,
          backgroundColor: "#FFFFFF",
          elevation: 3,
          borderColor: "#000000",
          borderBottomWidth: 0,
          shadowColor: "#000000",
          shadowOffset: { width: 0.5, height: 0.5 },
          shadowOpacity: 0.2,
          shadowRadius: 0,
        }}
      >
        <View
          style={{
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            onPress={this.props.goBack}
            style={{
              width: 30,
              alignItems: "center",
              justifyContent: "center",
              height: 50,
            }}
          >
            <TabBarIcon
              name="ios-arrow-back"
              type="Ionicons"
              size={30}
              color="#20368F"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: "row",
              width: this.props.width - 60,
              height: 50,
              alignItems: "center",
            }}
            onPress={this.props.openChannelInfo}
            activeOpacity={0.6}
          >
            <View style={{ paddingHorizontal: 10 }}>
              <CustomAvatar
                size={40}
                source={{
                  uri: this.props.channelAvatar,
                }}
                rounded
              />
            </View>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 16,
                maxWidth: this.props.width - 130,
              }}
              ellipsizeMode="tail"
              numberOfLines={1}
            >
              {this.props.channelName}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.openModel}
            style={{
              width: 30,
              alignItems: "center",
              justifyContent: "center",
              height: 50,
            }}
          >
            <TabBarIcon type="Feather" name="plus" size={30} color="#20368F" />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  viewPdf = (document) => {
    this.props.navigation.push("ViewPdf", { document });
  };

  renderCustomView = ({ currentMessage = {} }) => {
    if (currentMessage.type == DOCUMENT) {
      return (
        <TouchableOpacity
          onPress={this.viewPdf.bind(this, currentMessage.document)}
          activeOpacity={0.5}
          style={{
            flexDirection: "row",
            backgroundColor: "rgba(52, 52, 52, 0.2)",
            height: 40,
            borderRadius: 15,
            alignItems: "center",
            margin: 5,
          }}
        >
          <View
            style={{
              width: 25,
              padding: 5,
            }}
          >
            <TabBarIcon type="Ionicons" name="ios-document" color="#FFFFFF" />
          </View>
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={{ color: "#FFFFFF", maxWidth: this.props.width * 0.7 - 35 }}
          >
            {currentMessage.document.name}
          </Text>
        </TouchableOpacity>
      );
    } else if (currentMessage.type == FILE) {
      return (
        <View
          style={{
            width: this.props.width * 0.7,
            height: this.props.height * 0.3,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <MultipleImagesAndVideos
            onPress={this.navigateToShowFiles}
            name={currentMessage.user.name}
            files={currentMessage.files}
            width={this.props.width * 0.7 - 10}
            height={this.props.height * 0.3 - 10}
          />
        </View>
      );
    }
    return null;
  };

  renderBubble = (props) => {
    let documentStyle = {};
    if (props.currentMessage.type == DOCUMENT)
      documentStyle = { height: 65, width: this.props.width * 0.7 };
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: { backgroundColor: "#20368F", ...documentStyle },
          left: documentStyle,
        }}
      />
    );
  };

  removeAttachment = (uri) => {
    let attachments = [...this.state.files];
    attachments.find((a, index) => {
      if (a.uri == uri) {
        attachments.splice(index, 1);
        return true;
      }
    });
    !this.isUnmounted &&
      this.setState({
        files: attachments,
      });
  };

  renderAttachment = (attachment) => {
    return (
      <View style={{ padding: 5 }}>
        <ImageOrVideo
          needRemoveButton={true}
          removeAttachment={this.removeAttachment}
          closeIconSize={15}
          playButtonSize={25}
          style={{
            width: findWidth(
              attachment.height,
              this.props.height * 0.1 - 10,
              attachment.width
            ),
            height: this.props.height * 0.1 - 10,
            borderRadius: 5,
          }}
          file={attachment}
        />
      </View>
    );
  };

  sendImages = () => {
    this.onSend();
  };

  renderChatFooter = () => {
    return this.state.files.length > 0 ? (
      <View
        style={{
          width: this.props.width,
          height: this.props.height * 0.1,
          backgroundColor: "#E0E0E0",
          flexDirection: "row",
        }}
      >
        <View style={{ width: this.props.width - 30 }}>
          <FlatList
            data={this.state.files}
            renderItem={(a) => this.renderAttachment(a.item)}
            keyExtractor={(item, index) => `${index}`}
            horizontal={true}
          />
        </View>
        <TouchableOpacity
          style={{ width: 30, alignItems: "center", justifyContent: "center" }}
          onPress={this.sendImages}
        >
          <TabBarIcon
            type="MaterialIcons"
            name="send"
            size={30}
            color="#1B7DE2"
          />
        </TouchableOpacity>
      </View>
    ) : null;
  };

  navigateToShowFiles = (files, name) => {
    this.props.navigation.push("FilesView", { files: files, title: name });
  };

  sendDocument = (attachment) => {
    this.closeModal();
    if (this.inactivateListener) this.inactivateListener();
    this.props.navigation.push("PdfView", {
      document: attachment,
      channelId: this.props.channelId,
    });
  };

  setAttachment = (attachment) => {
    !this.isUnmounted &&
      this.setState((oldState) => ({
        files: [...oldState.files, attachment],
        modalVisible: false,
      }));
  };

  closeModal = () => {
    !this.isUnmounted &&
      this.setState({
        modalVisible: false,
      });
  };

  openModel = () => {
    !this.isUnmounted &&
      this.setState({
        modalVisible: true,
      });
  };

  deleteMessage = (messageId) => {
    if (this.props.connectionStatus) {
      FireBase.getDocumentReference(
        FireBaseCollectionService.messages(this.props.channelId, messageId)
      )
        .delete()
        .then(() => {
          let messages = [...this.state.messages];
          for (let i = 0; i < messages.length; i++) {
            if (messages[i]._id == messageId) {
              messages.splice(i, 1);
              break;
            }
          }
          this.props.setChatMessages(
            this.props.channelId,
            messages.slice(
              this.state.messages.length - 100,
              this.state.messages.length
            )
          );
          !this.isUnmounted && this.setState({ messages });
        })
        .catch((error) => {
          alertValue("Something Went Wrong");
        });
    } else {
      alertValue(CONNECTION_ERROR);
    }
  };

  makeAction = async (option, message) => {
    if (!message.text) {
      option = option + 1;
    }
    switch (option) {
      case 0:
        await Clipboard.setString(message.text);
        break;
      // case 1:
      //   console.log("fffffffdddddd");
      //   break;
      case 1:
        if (message.user._id == this.props.user.uid) {
          let self = this;
          Alert.alert(
            "Delete Message",
            "This message will be deleted permanently.",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
              },
              {
                text: "Delete",
                onPress: () => {
                  self.setState(
                    {
                      deleting: true,
                    },
                    () => {
                      self.deleteMessage(message._id);
                    }
                  );
                },
              },
            ],
            {
              cancelable: false,
            }
          );
        }
        break;
    }
  };

  removePendingMessage = (message) => {
    let pendingMessages = this.state.pendingMessages.filter(
      (m) => m._id != message._id
    );
    this.props.setPendingChatMessages(this.props.channelId, pendingMessages);
    !this.isUnmounted &&
      this.setState({
        pendingMessages: pendingMessages,
      });
  };

  makeActionForPending = (option, message) => {
    switch (option) {
      case 0:
        this.createMessages({
          [message._id]: this.convertToDatabaseFormat(message),
        });
        break;
      case 1:
        Alert.alert(
          "Remove Message",
          "This message will be Removed permanently.",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            {
              text: "Delete",
              onPress: () => {
                this.removePendingMessage(message);
              },
            },
          ],
          {
            cancelable: false,
          }
        );
        break;
    }
  };

  showActions = (c, m) => {
    if (m.pending && minutesDiff(m.createdAt) > 3) {
      const options = ["Retry", "Delete Message", "Cancel"];
      let cancelButtonIndex = 2;
      let destructiveButtonIndex = 1;
      this.props.showActionSheetWithOptions(
        {
          options,
          cancelButtonIndex,
          destructiveButtonIndex,
        },
        (index) => {
          this.makeActionForPending(index, m);
        }
      );
    } else if (!m.pending) {
      const options = ["Cancel"];
      let cancelButtonIndex = 1;
      let destructiveButtonIndex = null;
      if (m.user._id == this.props.user.uid) {
        options.unshift("Delete Message");
        // options.unshift("Edit Message");
        cancelButtonIndex = 2;
        destructiveButtonIndex = 1;
      }
      if (m.text) {
        options.unshift("Copy");
      }
      this.props.showActionSheetWithOptions(
        {
          options,
          cancelButtonIndex,
          destructiveButtonIndex,
        },
        (index) => {
          this.makeAction(index, m);
        }
      );
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.chatHeader()}
        {this.props.loading ? (
          this.loadChats()
        ) : (
          <View
            style={styles.giftedChatStyle(
              this.props.height - Constants.statusBarHeight
            )}
          >
            <GiftedChat
              messages={GiftedChat.append(
                this.state.messages,
                this.state.pendingMessages
              )}
              onSend={this.onSend}
              placeholder={`Message ${this.props.channelName}`}
              user={this.loginUser}
              listViewProps={{
                onEndReached: this.loadEarlierMessages,
                onEndReachedThreshold: 0.5,
              }}
              renderLoading={this.loadChats}
              textInputProps={{ maxLength: 10000, spellCheck: true }}
              renderSend={this.renderSend}
              scrollToBottom={true}
              renderAvatar={null}
              renderUsernameOnMessage={this.props.isGroup}
              renderChatFooter={this.renderChatFooter}
              renderCustomView={this.renderCustomView}
              renderBubble={this.renderBubble}
              onLongPress={this.showActions}
            />
          </View>
        )}
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={styles.modalStyle}
        >
          <ChatAdditionalOptions
            close={this.closeModal}
            setAttachment={this.setAttachment}
            sendDocument={this.sendDocument}
          />
        </ModalView>
        {Platform.OS == "android" ? (
          <KeyboardAvoidingView
            style={{ flex: 1 }}
            behavior={"padding"}
            // keyboardVerticalOffset={50 + Constants.statusBarHeight}
            keyboardShouldPersistTaps="always"
          />
        ) : null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loading: (height) => ({
    flex: height / 50 - 1,
    alignItems: "center",
    justifyContent: "center",
  }),
  giftedChatStyle: (height) => ({
    flex: height / 50 - 1,
    heigth: height,
    justifyContent: "center",
  }),
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
  nothing: {
    opacity: 0.8,
    position: "absolute",
    right: 10,
    bottom: 30,
    zIndex: 999,
    height: 40,
    width: 40,
    borderRadius: 20,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000000",
    shadowOpacity: 0.5,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 1,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  messagesByChannel: state.chatData.messagesByChannel,
  pendingMessagesByChannel: state.chatData.pendingMessagesByChannel,
});

const ChatComponent = connectActionSheet(CustomizeGiftedChat);

export default connect(mapStateToProps, {
  getChatMessages,
  setChatMessages,
  getPendingChatMessages,
  setPendingChatMessages,
})(ChatComponent);
