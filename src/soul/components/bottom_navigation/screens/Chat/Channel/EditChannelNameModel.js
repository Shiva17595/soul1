import React, { Component, Fragment } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Text,
} from "react-native";
import { connect } from "react-redux";
import CollectionPaths from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import { setChannels } from "../../../../../actions/chatActions";
import {
  alertValue,
  StatusBar,
  LoadingPage,
} from "../../../../../../../lib/ReUsableComponents";
import Constants from "expo-constants";
import { ERROR_MESSAGE } from "../../../../../constants/Variables";

class EditChannelNameModel extends Component {
  constructor(props) {
    super(props);
    this._isMounted = true;
    this.state = {
      channelName: this.props.channelName,
      submitting: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.channelName != prevProps.channelName) {
      this.setState({
        channelName: this.props.channelName,
      });
    }
  }

  onTextInputChange = (value) => {
    this._isMounted &&
      this.setState({
        channelName: value,
      });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  header = () => {
    return (
      <View
        style={{
          width: this.props.width,
          height: Constants.statusBarHeight + 50,
        }}
      >
        <StatusBar />
        <View
          style={{
            justifyContent: "flex-start",
            height: 50,
            width: this.props.width,
            backgroundColor: "#20368F",
            elevation: 3,
            borderColor: "#000000",
            borderBottomWidth: 0,
            shadowColor: "#000000",
            shadowOffset: { width: 0.5, height: 0.5 },
            shadowOpacity: 0.2,
            shadowRadius: 0,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              width: this.props.width,
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              onPress={this.props.closeEditChannelNameModel}
              style={{
                width: 70,
                padding: 5,
                alignItems: "flex-start",
                justifyContent: "center",
                height: 50,
              }}
            >
              <Text style={{ color: "#FFFFFF", fontWeight: "bold" }}>Back</Text>
            </TouchableOpacity>
            <Text style={{ color: "#FFFFFF", fontWeight: "bold" }}>
              Edit Channel Name
            </Text>
            <TouchableOpacity
              onPress={this.editChannelName}
              style={{
                padding: 5,
                width: 70,
                alignItems: "flex-start",
                justifyContent: "center",
                height: 50,
              }}
              disabled={
                !this.state.channelName.trim() ||
                this.state.channelName == this.props.channelName ||
                this.state.submitting
              }
            >
              <Text style={{ color: "#FFFFFF", fontWeight: "bold" }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  editChannelName = async () => {
    if (this.props.connectionStatus) {
      this.setState({
        submitting: true,
      });
      let channel = {
        name: this.state.channelName.trim(),
        updatedAt: new Date(),
      };
      try {
        await FireBase.updateDocumentInfo(
          CollectionPaths.channels(),
          this.props.channelId,
          channel
        );
        let channels = [...this.props.channels];
        let editChannel = channels.find(
          (c) => c.uniqueId == this.props.channelId
        );
        editChannel.name = this.state.channelName;
        this.props.setChannels(channels);
        this.props.closeEditChannelNameModel();
      } catch {
        alertValue(ERROR_MESSAGE);
      }
      this._isMounted &&
        this.setState({
          submitting: false,
        });
    } else {
      alertValue("Please check you insert connection and try again");
    }
  };

  render() {
    return (
      <View style={styles.container(this.props.height, this.props.width)}>
        {this.header()}
        <View style={styles.inputBox(this.props.width)}>
          <TextInput
            onChangeText={this.onTextInputChange}
            style={{ fontSize: 18, padding: 5 }}
            value={this.state.channelName}
            maxLength={30}
            spellCheck={true}
            autoFocus
          />
        </View>
        {this.state.submitting ? <LoadingPage style={styles.loading} /> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: (height, width) => ({
    flex: 1,
    backgroundColor: "#FFFFFF",
    margin: 0,
    height: height,
    width: width,
  }),
  inputBox: (width) => ({
    borderWidth: 1.5,
    borderColor: "#E8E7E7",
    borderRadius: 10,
    width: width,
    padding: 10,
    marginTop: 10,
  }),
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  channels: state.chatData.channels,
});

export default connect(mapStateToProps, { setChannels })(EditChannelNameModel);
