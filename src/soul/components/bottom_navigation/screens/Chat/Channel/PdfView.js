import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import FireBase from "../../../../../assets/FireBase";
import FireBaseStorageService from "../../../../../services/FireBaseStorageService";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import { setPendingChatMessages } from "../../../../../actions/chatActions";
import { DOCUMENT, ERROR_MESSAGE } from "../../../../../constants/Variables";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import PdfComponent from "../../../../../../../lib/PdfComponent";

class PdfView extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Document";
    let headerTitleStyle = {
      color: "white",
    };
    let headerStyle = {
      backgroundColor: "#20368F",
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.headerTextStyle}>Back</Text>
      </TouchableOpacity>
    );

    let headerRight = (
      <TouchableOpacity
        onPress={params.sendDocument}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text style={styles.headerTextStyle}>Send</Text>
      </TouchableOpacity>
    );

    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
      headerRight,
    };
  };

  constructor(props) {
    super(props);
    this.props.navigation.setParams({
      goBack: this.goBack,
      sendDocument: this.sendDocument,
    });
    this.state = {
      creatingMessage: false,
    };
    this.document = this.props.navigation.getParam("document", {});
    this.channelId = this.props.navigation.getParam("channelId", {});
  }

  goBack = () => {
    this.props.navigation.push("ChatScreen", {
      channelId: this.channelId,
    });
  };

  uploadFiles = (fileName) => {
    return new Promise((resolve, reject) => {
      let path = FireBaseStorageService.channels(this.channelId, fileName);
      FireBase.uploadFile(this.document.uri, path).then((uri) => {
        resolve(uri);
      });
    });
  };

  sendDocument = () => {
    if (!this.state.creatingMessage) {
      this.setState(
        {
          creatingMessage: true,
        },
        this.createMessage
      );
    }
  };

  convertMessage = (id, data) => {
    let m = {
      _id: id,
      text: data.message,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
      user: {
        _id: this.props.user.uid,
        name: this.props.user.name,
        avatar: this.props.user.avatar,
      },
      sent: false,
      document: data.document,
      type: DOCUMENT,
    };
    return m;
  };

  createMessage = async () => {
    let collection = FireBaseCollectionService.messages(this.channelId);
    let messageId = FireBase.generateDocumentId(collection);
    let fileName = `${messageId}-document.pdf`;
    let uri = await this.uploadFiles(fileName);
    if (uri) {
      let doc = {
        message: "",
        messageType: DOCUMENT,
        document: {
          uri: uri,
          name: this.document.name,
          fileName: fileName,
          size: this.document.size,
        },
        uid: this.props.user.uid,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      FireBase.createDocument(collection, doc, messageId);
      let pendingMessages = [
        ...this.props.pendingMessagesByChannel[this.channelId],
      ];
      pendingMessages.push(this.convertMessage(messageId, doc));
      this.props.setPendingChatMessages(this.channelId, pendingMessages);
      this.goBack();
    } else {
      alertValue(ERROR_MESSAGE);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <PdfComponent style={styles.container} source={this.document} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  headerTextStyle: {
    paddingHorizontal: 6,
    color: "#FFFFFF",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  pendingMessagesByChannel: state.chatData.pendingMessagesByChannel,
});

export default connect(mapStateToProps, { setPendingChatMessages })(PdfView);
