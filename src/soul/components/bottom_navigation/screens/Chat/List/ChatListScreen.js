import React, { Component, Fragment } from "react";
import { StyleSheet, FlatList } from "react-native";
import TabBarLable from "../../TabBarLable";
import { connect } from "react-redux";
import { setChannels, getChannels } from "../../../../../actions/chatActions";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import { CHANNEL_FIELDS } from "../../../../../assets/FireBaseCollections";
import { isEmptyArray, getKey } from "../../../../../assets/reusableFunctions";
import {
  EmptyComponent,
  LoadingPage,
} from "../../../../../../../lib/ReUsableComponents";
import ChatComponent from "./ChatComponent";
import ModalView from "../../../../../../../lib/ModalView";
import QuickChannelInfo from "../Channel/QuickChannelInfo";
import { CONNECTION_ERROR } from "../../../../../constants/Variables";

class ChatListScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    tabBarLabel: ({ focused }) => {
      return (
        <TabBarLable
          iconName="list-alt"
          iconType="FontAwesome"
          size={15}
          lableText="Channels"
          focused={focused}
        />
      );
    },
  });

  constructor(props) {
    super(props);
    this.isUnmounted = false;
    this.state = {
      loading: true,
      modalVisible: false,
      channelParams: {},
    };
  }

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  openQuickView = (params) => {
    this.setState({
      modalVisible: true,
      channelParams: params,
    });
  };

  openChannel = (params) => {
    this.props.navigation.push("ChatScreen", params);
  };

  openChannelInfo = (params) => {
    this.props.navigation.push("ChannelInfo", params);
  };

  getChatList = () => {
    return (
      <FlatList
        data={this.props.channels.filter((c) => c.active == true)}
        renderItem={({ item, index }) => (
          <ChatComponent
            channel={item}
            onPress={this.openChannel}
            openQuickView={this.openQuickView}
          />
        )}
        keyExtractor={getKey}
        ListEmptyComponent={
          <EmptyComponent style={styles.emptyPage} message={"No Channels"} />
        }
        showsVerticalScrollIndicator={false}
      />
    );
  };

  filters = () => {
    return {
      [CHANNEL_FIELDS.memberIds]: {
        type: "array-contains",
        value: this.props.user.uid,
      },
      [CHANNEL_FIELDS.active]: {
        type: "==",
        value: true,
      },
    };
  };

  channelsUpdate = (channelsRef) => {
    let channelsDiff = channelsRef.docChanges();
    let channels = [...this.props.channels];
    channelsDiff.forEach((channel) => {
      if (channel.type === "added" || channel.type === "modified") {
        let data = channel.doc.data();
        data.uniqueId = channel.doc.id;
        let exists = false;
        for (let i = 0; i < channels.length; i++) {
          if (channels[i].uniqueId == channel.doc.id) {
            channels[i] = channel.doc.data();
            channels[i].uniqueId = channel.doc.id;
            exists = true;
            break;
          }
        }
        if (!exists) channels.push(data);
      } else if (channel.type === "removed") {
        channels = channels.filter((c) => c.uniqueId != channel.doc.id);
      }
    });
    this.props.setChannels(channels);
    this.state.loading &&
      !this.isUnmounted &&
      this.setState({
        loading: false,
      });
  };

  getChannelList = () => {
    let collection = FireBaseCollectionService.channels();
    this.channelsListener = FireBase.getListener(
      collection,
      { [CHANNEL_FIELDS.updatedAt]: "desc" },
      null,
      this.filters(),
      { includeMetadataChanges: false },
      this.channelsUpdate
    );
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.connectionStatus != this.props.connectionStatus) {
      if (!this.channelsListener) this.getChannelList();
    }
  }

  async componentDidMount() {
    await this.props.getChannels();
    if (this.props.connectionStatus) {
      this.getChannelList();
    }
  }

  componentWillUnmount() {
    if (this.channelsListener) this.channelsListener();
    this.isUnmounted = true;
  }

  render() {
    let content;
    if (isEmptyArray(this.props.channels) && !this.props.connectionStatus) {
      content = (
        <EmptyComponent style={styles.emptyPage} message={CONNECTION_ERROR} />
      );
    } else if (isEmptyArray(this.props.channels) && this.state.loading) {
      content = <LoadingPage style={styles.emptyPage} />;
    } else {
      content = this.getChatList();
    }
    let model = (
      <ModalView
        isVisible={this.state.modalVisible}
        onBackButtonPress={this.closeModal}
        onBackdropPress={this.closeModal}
        style={styles.modalStyle(this.props.width)}
      >
        <QuickChannelInfo
          {...this.state.channelParams}
          openChannel={this.openChannel}
          openChannelInfo={this.openChannelInfo}
          closeModal={this.closeModal}
        />
      </ModalView>
    );
    return (
      <Fragment>
        {content}
        {model}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  emptyPage: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  modalStyle: (width) => ({
    justifyContent: "center",
    margin: width * 0.1,
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  channels: state.chatData.channels,
});

export default connect(mapStateToProps, {
  getChannels,
  setChannels,
})(ChatListScreen);
