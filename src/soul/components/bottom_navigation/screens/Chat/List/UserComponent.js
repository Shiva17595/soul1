import React, { PureComponent, Fragment } from "react";
import { StyleSheet, Text, View, TouchableHighlight } from "react-native";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";
import { connect } from "react-redux";
import { CustomizeCheckBox } from "../../../../../../../lib/ReUsableComponents";

class UserComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selected ? true : false,
    };
  }

  onClick = () => {
    let { uid, name, avatar, subTitle } = this.props;
    this.props.onPress({ uid, name, avatar, subTitle }, !this.state.selected);
    this.props.createGroupEnabled &&
      this.setState((oldState) => ({
        selected: !oldState.selected,
      }));
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.createGroupEnabled != this.props.createGroupEnabled) {
      this.setState({
        selected: false,
      });
    }
  }

  onLongPress = () => {
    if (this.props.onLongPress) this.props.onLongPress(this.props.uid);
  };

  render() {
    return (
      <TouchableHighlight
        onPress={this.onClick}
        underlayColor={"#D7D5D3"}
        disabled={this.props.disabled}
        onLongPress={this.onLongPress}
      >
        <Fragment>
          <View
            style={styles.user(this.props.disabled ? "#D7D5D3" : "#FFFFFF")}
          >
            {this.props.createGroupEnabled ? (
              <CustomizeCheckBox
                onUpdate={this.onClick}
                selected={this.state.selected}
                iconSize={25}
                disabled={this.props.disabled}
                color={this.state.selected ? "#7bb956" : ""}
              />
            ) : null}
            <View style={styles.userAvatar}>
              <CustomAvatar
                uid={this.props.uid}
                rounded
                size={40}
                source={{
                  uri: this.props.avatar,
                }}
              />
            </View>
            <View>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={styles.userName(this.props.width - 70)}
              >
                {this.props.name}
              </Text>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={styles.userSubtitle(this.props.width - 70)}
              >
                {this.props.subTitle}
              </Text>
            </View>
          </View>
          <View style={styles.borderDown} />
        </Fragment>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  user: (color) => ({
    flexDirection: "row",
    paddingVertical: 4,
    height: 50,
    alignItems: "center",
    backgroundColor: color,
  }),
  userAvatar: { paddingHorizontal: 8 },
  userName: (maxWidth) => ({
    fontWeight: "bold",
    fontSize: 15,
    maxWidth: maxWidth,
  }),
  userSubtitle: (maxWidth) => ({
    color: "#AFAAAA",
    fontSize: 12,
    maxWidth: maxWidth,
  }),
  borderDown: { borderWidth: 0.5, marginLeft: 57, borderColor: "#CFD1CE" },
});

const mapStateToProps = (state) => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(UserComponent);
