import React, { PureComponent, Fragment } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";

class ChatComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      channelName: this.getChannelName(),
      channelAvatar: this.getChannelAvatar(),
    };
    this.channelId = this.props.channel.uniqueId;
  }

  getChannelName = () => {
    let c = this.props.channel;
    if (c.isGroup) return c.name;
    if (c.memberIds.length == 1) {
      return c.members[this.props.user.uid].name;
    }
    let mId = c.memberIds.find((mId) => mId != this.props.user.uid);
    return c.members[mId].name;
  };

  getChannelAvatar = () => {
    let c = this.props.channel;
    if (c.isGroup) return c.avatar;
    if (c.memberIds.length == 1) {
      return c.members[this.props.user.uid].avatar;
    }
    let mId = c.memberIds.find((mId) => mId != this.props.user.uid);
    return c.members[mId].avatar;
  };

  getChannelSubtitle = () => {
    return "message that to show";
  };

  onClick = () => {
    this.props.onPress({
      channelId: this.channelId,
      name: this.state.channelName,
      avatar: this.state.channelAvatar,
    });
  };

  openQuickView = () => {
    this.props.openQuickView({
      channelId: this.channelId,
      name: this.state.channelName,
      avatar: this.state.channelAvatar,
    });
  };

  componentDidUpdate() {
    if (
      this.props.channel.isGroup &&
      (this.props.channel.name != this.state.channelName ||
        this.props.channel.avatar != this.state.channelAvatar)
    ) {
      this.setState({
        channelName: this.props.channel.name,
        channelAvatar: this.props.channel.avatar,
      });
    }
  }

  render() {
    return (
      <TouchableHighlight onPress={this.onClick} underlayColor={"#D7D5D3"}>
        <View
          style={styles.component(this.props.width, this.props.height * 0.25)}
        >
          <View style={{ flexDirection: "row" }}>
            <View
              style={{
                width: 80,
                heigth: this.props.height * 0.25,
                padding: 10,
              }}
            >
              <TouchableOpacity onPress={this.openQuickView}>
                <CustomAvatar
                  rounded
                  size={60}
                  source={{
                    uri: this.state.channelAvatar,
                  }}
                />
              </TouchableOpacity>
            </View>
            <View style={{ paddingVertical: 10 }}>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={styles.channelName(this.props.width - 100)}
              >
                {this.state.channelName}
              </Text>
              <Text
                ellipsizeMode="tail"
                numberOfLines={2}
                style={styles.channelSubtitle(this.props.width - 100)}
              >
                {this.getChannelSubtitle()}
              </Text>
            </View>
          </View>
          <View style={styles.borderDown} />
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  component: (width, heigth) => ({
    heigth: heigth,
    width: width,
  }),
  borderDown: { borderWidth: 0.5, marginLeft: 80, borderColor: "#CFD1CE" },
  channelName: (maxWidth) => ({
    fontWeight: "bold",
    fontSize: 16,
    maxWidth: maxWidth,
  }),
  channelSubtitle: (maxWidth) => ({
    color: "#AFAAAA",
    fontSize: 15,
    paddingVertical: 5,
    maxWidth: maxWidth,
  }),
});

const mapStateToProps = (state) => ({
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(ChatComponent);
