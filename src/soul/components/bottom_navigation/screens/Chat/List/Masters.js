import React, { Component, Fragment } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  Text,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { MASTER_FIELDS } from "../../../../../assets/FireBaseCollections";
import UserComponent from "./UserComponent";
import {
  LoadingPage,
  EmptyComponent,
} from "../../../../../../../lib/ReUsableComponents";
import FireBase from "../../../../../assets/FireBase";
import CollectionPaths from "../../../../../services/FireBaseCollectionService";
import {
  setMastersData,
  getMastersData,
} from "../../../../../actions/commonActions";
import {
  isEmptyArray,
  filterArray,
} from "../../../../../assets/reusableFunctions";
import TabBarIcon from "../../../../../assets/TabBarIcon";

class Masters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
    this.isUnmounted = false;
  }

  async componentDidMount() {
    await this.props.getMastersData();
    this.getMasters();
  }

  optionsForUserList = () => {
    let options = {
      orders: {},
      filters: {},
    };
    options.orders[MASTER_FIELDS.name] = "asc";
    return options;
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  updateLoadingState = (v) => {
    !this.isUnmounted &&
      this.setState({
        loading: v,
      });
  };

  onRefresh = () => {
    if (this.props.connectionStatus && !this.state.loading) {
      this.updateLoadingState(true);
      this.mastersCall();
    }
  };

  mastersCall = () => {
    let masterCollection = CollectionPaths.masters();
    let options = this.optionsForUserList();
    FireBase.getCollection(masterCollection, options.orders).then(
      async (masters) => {
        if (masters) {
          await this.props.setMastersData(masters);
        }
        this.updateLoadingState(false);
      }
    );
  };

  componentDidUpdate() {
    if (isEmptyArray(this.props.masters)) this.getMasters();
  }

  getMasters = () => {
    if (
      isEmptyArray(this.props.masters) &&
      !this.state.loading &&
      this.props.connectionStatus
    ) {
      this.updateLoadingState(true);
      this.mastersCall();
    }
  };

  disableTouch = (userId) => {
    return this.props.disableList && this.props.disableList.includes(userId);
  };

  getRenderItem = (item) => {
    let disabled = this.disableTouch(item.uniqueId);
    if (this.props.selectedList.find((s) => s.uid == item.uniqueId)) {
      return null;
    } else {
      return (
        <UserComponent
          avatar={item.avatar}
          name={item.name}
          subTitle={
            disabled ? "Already added to the group" : item.classInfo.className
          }
          uid={item.uniqueId}
          onPress={this.props.onPress}
          createGroupEnabled={this.props.createGroupEnabled}
          disabled={disabled}
          selected={disabled}
        />
      );
    }
  };

  header = () => {
    return (
      <View style={styles.list}>
        <Text style={styles.listHeader}>Professors</Text>
        {this.state.loading ? (
          <LoadingPage loadSpinnerSize="small" />
        ) : (
          <TouchableOpacity onPress={this.onRefresh}>
            <TabBarIcon
              name="refresh"
              type="FontAwesome"
              size={20}
              color="#C1BEBB"
            />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  render() {
    let searchString = this.props.search.replace(/\s+/g, " ").trim();
    return (
      <Fragment>
        {this.header()}
        <FlatList
          data={filterArray(this.props.masters || [], searchString, [
            "name",
            ["classInfo", "className"],
          ])}
          renderItem={({ item, index }) => this.getRenderItem(item)}
          keyExtractor={(item, index) => item.uniqueId}
          ListEmptyComponent={
            <EmptyComponent
              style={styles.emptyPage}
              message={this.state.loading ? "loading..." : "No Results"}
            />
          }
          showsVerticalScrollIndicator={false}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  emptyPage: {
    height: 100,
    justifyContent: "center",
    alignItems: "center",
  },
  list: {
    marginTop: 3,
    backgroundColor: "#F4F2F1",
    borderRadius: 3,
    alignItems: "center",
    paddingHorizontal: 6,
    flexDirection: "row",
    justifyContent: "space-between",
    height: 35,
  },
  listHeader: {
    fontWeight: "bold",
    color: "#20368F",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  masters: state.commonInfo.masters,
});

export default connect(mapStateToProps, { setMastersData, getMastersData })(
  Masters
);
