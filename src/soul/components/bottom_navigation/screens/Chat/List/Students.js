import React, { Component, Fragment } from "react";
import {
  StyleSheet,
  FlatList,
  TouchableOpacity,
  View,
  Text,
} from "react-native";
import { connect } from "react-redux";
import { STUDENT_FIELDS } from "../../../../../assets/FireBaseCollections";
import UserComponent from "./UserComponent";
import {
  EmptyComponent,
  LoadingPage,
} from "../../../../../../../lib/ReUsableComponents";
import FireBase from "../../../../../assets/FireBase";
import {
  filterArray,
  checkKeyExistsInHash,
  getKey,
  isEmptyArray,
} from "../../../../../assets/reusableFunctions";
import CollectionPaths from "../../../../../services/FireBaseCollectionService";
import {
  getStudentsData,
  setStudentsData,
} from "../../../../../actions/commonActions";
import TabBarIcon from "../../../../../assets/TabBarIcon";

class Students extends Component {
  constructor(props) {
    super(props);
    this.isUnmounted = false;
    this.state = {
      loading: false,
    };
  }

  async componentDidMount() {
    await this.props.getStudentsData();
    this.getStudents();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.classId != nextProps.classId) return true;
    if (this.props.search != nextProps.search) return true;
    if (this.props.connectionStatus != nextProps.connectionStatus) return true;
    if (this.state.loading != nextState.loading) return true;
    if (this.props.createGroupEnabled != nextProps.createGroupEnabled)
      return true;
    if (this.props.selectedList.length != nextProps.selectedList.length)
      return true;
    if (
      Object.keys(this.props.studentsByClassId).length !=
      Object.keys(nextProps.studentsByClassId).length
    )
      return true;
    return false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.classId != this.props.classId ||
      isEmptyArray(this.props.studentsByClassId[this.props.classId])
    ) {
      this.getStudents();
    }
  }

  optionsForUserList = () => {
    let options = {
      orders: {},
      filters: {},
    };
    options.orders[STUDENT_FIELDS.name] = "asc";
    options.filters[STUDENT_FIELDS.chatVisibility] = {
      type: "==",
      value: true,
    };
    options.filters[`${STUDENT_FIELDS.classInfo}.${STUDENT_FIELDS.classId}`] = {
      type: "==",
      value: this.props.classId,
    };
    return options;
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  updateLoadingState = (v) => {
    !this.isUnmounted &&
      this.setState({
        loading: v,
      });
  };

  onRefresh = () => {
    if (this.props.connectionStatus && !this.state.loading) {
      this.updateLoadingState(true);
      this.studentsCall();
    }
  };

  studentsCall = () => {
    let studentsCollection = CollectionPaths.students();
    let options = this.optionsForUserList();
    FireBase.getCollection(
      studentsCollection,
      options.orders,
      null,
      options.filters
    ).then(async (students) => {
      if (students) {
        let v = {};
        v[this.props.classId] = students;
        await this.props.setStudentsData({
          ...this.props.studentsByClassId,
          ...v,
        });
      }
      this.updateLoadingState(false);
    });
  };

  getStudents = () => {
    if (
      !checkKeyExistsInHash(this.props.studentsByClassId, this.props.classId) &&
      !this.state.loading &&
      this.props.connectionStatus
    ) {
      this.updateLoadingState(true);
      this.studentsCall();
    }
  };

  disableTouch = (userId) => {
    return this.props.disableList && this.props.disableList.includes(userId);
  };

  getRenderItem = ({ item }) => {
    let disabled = this.disableTouch(item.uniqueId);
    if (this.props.selectedList.find((s) => s.uid == item.uniqueId)) {
      return null;
    } else {
      return (
        <UserComponent
          avatar={item.avatar}
          name={item.name}
          subTitle={disabled ? "Already added to the group" : item.rollNo}
          uid={item.uniqueId}
          onPress={this.props.onPress}
          createGroupEnabled={this.props.createGroupEnabled}
          disabled={disabled}
          selected={disabled}
        />
      );
    }
  };

  header = () => {
    return (
      <View style={styles.list}>
        <Text style={styles.listHeader}>Students</Text>
        {this.state.loading ? (
          <LoadingPage loadSpinnerSize="small" />
        ) : (
          <TouchableOpacity onPress={this.onRefresh}>
            <TabBarIcon
              name="refresh"
              type="FontAwesome"
              size={20}
              color="#C1BEBB"
            />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  render() {
    let props = this.props;
    let searchString = props.search.replace(/\s+/g, " ").trim();
    return (
      <Fragment>
        {this.header()}
        <FlatList
          data={filterArray(
            props.studentsByClassId[props.classId] || [],
            searchString,
            ["name", "rollNo"]
          )}
          renderItem={this.getRenderItem}
          keyExtractor={getKey}
          ListEmptyComponent={
            <EmptyComponent
              style={styles.emptyPage}
              message={this.state.loading ? "loading..." : "No Results"}
            />
          }
          showsVerticalScrollIndicator={false}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  emptyPage: {
    height: 100,
    justifyContent: "center",
    alignItems: "center",
  },
  list: {
    marginTop: 3,
    backgroundColor: "#F4F2F1",
    borderRadius: 3,
    alignItems: "center",
    paddingHorizontal: 6,
    flexDirection: "row",
    justifyContent: "space-between",
    height: 35,
  },
  listHeader: {
    fontWeight: "bold",
    color: "#20368F",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  studentsByClassId: state.commonInfo.studentsByClassId,
});

export default connect(mapStateToProps, { getStudentsData, setStudentsData })(
  Students
);
