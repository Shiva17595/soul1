import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Alert } from "react-native";
import TabBarLable from "../../TabBarLable";
import { SearchBar, Button } from "react-native-elements";
import { connect } from "react-redux";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import Students from "./Students";
import Masters from "./Masters";
import { isEmptyArray } from "../../../../../assets/reusableFunctions";
import SelectedList from "./SelectedList";
import { setChannels } from "../../../../../actions/chatActions";
import ClassesDropDown from "../../../../../../../lib/ClassesDropDown";

class UserListScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    tabBarLabel: ({ focused }) => {
      return (
        <TabBarLable
          iconName="add-user"
          iconType="Entypo"
          size={15}
          lableText="New Chat"
          focused={focused}
        />
      );
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      search: "",
      classId: this.props.user.classInfo.classId,
      createGroupEnabled: false,
      selectedList: [],
    };
  }

  clearSearch = () => {
    this.updateSearch();
  };

  updateSearch = (search = "") => {
    if (search != this.state.search) {
      this.setState({
        search: search ? search.toLowerCase() : "",
      });
    }
  };

  cancleGroupCreation = () => {
    if (this.state.selectedList.length > 0) {
      Alert.alert(
        "Cancle group creation",
        "Do you want to discard changes",
        [
          {
            text: "No",
            onPress: () => {
              return;
            },
            style: "cancel",
          },
          {
            text: "Yes",
            onPress: () => {
              this.enableGroupOption();
            },
          },
        ],
        {
          cancelable: false,
        }
      );
    } else {
      this.enableGroupOption();
    }
  };

  enableGroupOption = () => {
    this.setState((oldState) => ({
      createGroupEnabled: !oldState.createGroupEnabled,
      selectedList: [],
    }));
  };

  memberCount = () => {
    let count = 0;
    this.state.selectedList.forEach((m) => {
      if (m.uid != this.props.user.uid) {
        count = count + 1;
      }
    });
    return count;
  };

  createGroup = () => {
    let count = this.memberCount();
    if (count > 0 && count <= 100) {
      this.setState({
        search: "",
        classId: this.props.user.classInfo.classId,
        createGroupEnabled: false,
        selectedList: [],
      });
      this.props.navigation.push("CreateGroup", {
        members: this.state.selectedList,
      });
    } else {
      alertValue(
        count > 100
          ? "Maximum of 100 people allowed"
          : "Atleast two people needed for creating group"
      );
    }
  };

  getGroupButton = () => {
    let content;
    if (this.state.createGroupEnabled) {
      content = (
        <View style={{ flexDirection: "row" }}>
          <Button
            buttonStyle={{
              backgroundColor: "#20368F",
              height: 30,
            }}
            containerStyle={{
              alignItems: "flex-end",
              paddingHorizontal: 5,
            }}
            titleStyle={{
              fontSize: 10,
            }}
            icon={
              <TabBarIcon
                name={"circle-with-cross"}
                type={"Entypo"}
                size={20}
                color="#FFFFFF"
              />
            }
            iconContainerStyle={{
              padding: 10,
            }}
            title="Cancle"
            onPress={this.cancleGroupCreation}
          />
          <Button
            buttonStyle={{
              backgroundColor: "#20368F",
              height: 30,
            }}
            containerStyle={{
              alignItems: "flex-end",
              paddingHorizontal: 5,
            }}
            titleStyle={{
              fontSize: 10,
            }}
            icon={
              <TabBarIcon
                name={"group-add"}
                type={"MaterialIcons"}
                size={20}
                color="#FFFFFF"
              />
            }
            iconContainerStyle={{
              padding: 10,
            }}
            title="Create"
            onPress={this.createGroup}
          />
        </View>
      );
    } else {
      content = (
        <Button
          buttonStyle={{
            backgroundColor: "#20368F",
            height: 30,
          }}
          containerStyle={{
            alignItems: "flex-end",
          }}
          titleStyle={{
            fontSize: 10,
          }}
          icon={
            <TabBarIcon
              name={"group-add"}
              type={"MaterialIcons"}
              size={20}
              color="#FFFFFF"
            />
          }
          iconContainerStyle={{
            padding: 10,
          }}
          title="Create Group"
          onPress={this.enableGroupOption}
        />
      );
    }
    return (
      <View style={styles.list}>
        <View>
          {isEmptyArray(this.state.selectedList) ? null : (
            <Text style={styles.filterText}>
              {this.state.selectedList.length} Selected / 100
            </Text>
          )}
        </View>
        <View>{content}</View>
      </View>
    );
  };

  filterComponent = () => {
    return <ClassesDropDown setClassId={this.setClassId} />;
  };

  addOrRemove = (params, addOrRemove) => {
    if (addOrRemove) {
      this.setState((oldState) => ({
        selectedList: [...oldState.selectedList, params],
      }));
    } else {
      this.setState((oldState) => ({
        selectedList: oldState.selectedList.filter((s) => s.uid != params.uid),
      }));
    }
  };

  setClassId = (classId) => {
    this.setState({
      classId: classId,
    });
  };

  openChannel = (params) => {
    let channelId =
      this.props.user.uid > params.uid
        ? `${params.uid}${this.props.user.uid}`
        : `${this.props.user.uid}${params.uid}`;
    let memberIds = [this.props.user.uid];
    if (params.uid != this.props.user.uid) memberIds.push(params.uid);
    let channelDoc = this.props.channels.find((c) => c.uniqueId == channelId);
    if (!channelDoc) {
      channelDoc = {
        active: false,
        isGroup: false,
        memberIds: memberIds,
        members: {
          [params.uid]: {
            name: params.name,
            avatar: params.avatar,
            subTitle: params.subTitle,
          },
          [this.props.user.uid]: {
            name: this.props.user.name,
            avatar: this.props.user.avatar,
            subTitle: this.props.user.isMaster
              ? this.props.user.classInfo.className
              : this.props.user.rollNo,
          },
        },
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      let channels = [...this.props.channels];
      this.props.setChannels([
        ...channels,
        { ...channelDoc, ...{ uniqueId: channelId } },
      ]);
    }
    this.props.navigation.push("ChatScreen", { channelId });
  };

  getSelectedList = () => {
    return (
      <SelectedList list={this.state.selectedList} onPress={this.addOrRemove} />
    );
  };

  getSearchBar = () => {
    return (
      <View>
        <SearchBar
          placeholder="Search"
          onChangeText={this.updateSearch}
          value={this.state.search}
          platform="default"
          clearIcon={
            this.state.search ? (
              <TouchableOpacity onPress={this.clearSearch}>
                <TabBarIcon
                  name="cancel"
                  type="MaterialIcons"
                  size={20}
                  color={"#A6A6A6"}
                />
              </TouchableOpacity>
            ) : null
          }
          containerStyle={styles.searchBar}
          inputContainerStyle={styles.seacrhBarInput}
          placeholderTextColor={"#AFAAAA"}
          round
        />
        {this.filterComponent()}
        {this.getGroupButton()}
        {this.getSelectedList()}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.getSearchBar()}
        <Students
          classId={this.state.classId}
          search={this.state.search}
          onPress={
            this.state.createGroupEnabled ? this.addOrRemove : this.openChannel
          }
          createGroupEnabled={this.state.createGroupEnabled}
          selectedList={this.state.selectedList}
        />
        <Masters
          search={this.state.search}
          onPress={
            this.state.createGroupEnabled ? this.addOrRemove : this.openChannel
          }
          createGroupEnabled={this.state.createGroupEnabled}
          selectedList={this.state.selectedList}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    padding: 6,
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  filters: {
    backgroundColor: "#F4F2F1",
    minHeight: 50,
    margin: 6,
    padding: 5,
    borderRadius: 4,
  },
  filtersItem: { alignItems: "center" },
  filterText: { fontWeight: "bold", color: "#20368F" },
  searchBar: {
    backgroundColor: "#FFFFFF",
    borderWidth: 0,
    shadowColor: "white",
    borderBottomColor: "#FFFFFF",
    borderTopColor: "#FFFFFF",
  },
  seacrhBarInput: { backgroundColor: "#F4F2F1" },
  list: {
    marginTop: 3,
    backgroundColor: "#F4F2F1",
    borderRadius: 3,
    alignItems: "center",
    paddingHorizontal: 6,
    flexDirection: "row",
    justifyContent: "space-between",
    height: 40,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  channels: state.chatData.channels,
});

export default connect(mapStateToProps, {
  setChannels,
})(UserListScreen);
