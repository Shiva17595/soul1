import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";
import SelectedList from "./SelectedList";
import {
  alertValue,
  LoadingPage,
} from "../../../../../../../lib/ReUsableComponents";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBase from "../../../../../assets/FireBase";
import { setChannels } from "../../../../../actions/chatActions";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import AppPermissions from "../../../../../../../lib/AppPermissions";
import { IMAGE, ERROR_MESSAGE } from "../../../../../constants/Variables";
import { connectActionSheet } from "@expo/react-native-action-sheet";
import FireBaseStorageService from "../../../../../services/FireBaseStorageService";
import { Avatar } from "react-native-elements";

class CreateGroupScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Create Group";
    let headerTitleStyle = {
      color: "white",
    };
    let headerStyle = {
      backgroundColor: "#20368F",
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text
          style={{
            paddingHorizontal: 6,
            color: "#FFFFFF",
          }}
        >
          Back
        </Text>
      </TouchableOpacity>
    );

    let headerRight = (
      <TouchableOpacity
        onPress={params.createGroup}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text
          style={{
            paddingHorizontal: 6,
            color: "#FFFFFF",
          }}
        >
          Submit
        </Text>
      </TouchableOpacity>
    );

    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
      headerRight,
    };
  };

  constructor(props) {
    super(props);
    this.props.navigation.setParams({
      goBack: this.goBack,
      createGroup: this.createGroup,
    });

    this.isUnmounted = false;
    this.state = {
      groupName: "",
      members: this.props.navigation.getParam("members", []),
      groupUri: "",
      creating: false,
    };
  }

  addOrRemove = (params) => {
    this.setState((oldState) => ({
      members: oldState.members.filter((s) => s.uid != params.uid),
    }));
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }
  memberCount = () => {
    let count = 0;
    this.state.members.forEach((m) => {
      if (m.uid != this.props.user.uid) {
        count = count + 1;
      }
    });
    return count;
  };

  updatedCreatingStatus = (val) => {
    this.setState({
      creating: val,
    });
  };

  createGroup = async () => {
    if (this.state.creating) return;
    if (!this.state.groupName.trim()) {
      alertValue("Group name cannot be empty");
      return;
    }
    let count = this.memberCount();
    if (count > 0 && count <= 100) {
      this.updatedCreatingStatus(true);
      let avatar = "";
      let docRef = FireBase.generateDocumentId(
        FireBaseCollectionService.channels()
      );
      if (this.state.groupUri) {
        avatar = await FireBase.uploadFile(
          this.state.groupUri,
          FireBaseStorageService.profilePictures(docRef)
        );
        if (!avatar) {
          this.error();
          return;
        }
      }
      let doc = {
        isGroup: true,
        createdAt: new Date(),
        updatedAt: new Date(),
        active: true,
        name: this.state.groupName.trim(),
        avatar: avatar,
      };
      let memberIds = [];
      let members = {};
      members[this.props.user.uid] = {
        avatar: this.props.user.avatar,
        name: this.props.user.name,
        subTitle: this.props.user.isMaster
          ? this.props.user.classInfo.className
          : this.props.user.rollNo,
      };
      this.state.members.forEach((m) => {
        memberIds.push(m.uid);
        members[m.uid] = {
          avatar: m.avatar,
          name: m.name,
          subTitle: m.subTitle,
        };
      });
      if (!memberIds.includes(this.props.user.uid))
        memberIds.push(this.props.user.uid);
      doc["memberIds"] = memberIds;
      doc["members"] = members;
      let collection = FireBaseCollectionService.channels();
      FireBase.createDocument(collection, doc, docRef)
        .then((document) => {
          this.props.navigation.push("ChatScreen", { channelId: document.id });
        })
        .catch((error) => {
          this.error();
        });
    } else {
      alertValue(
        count > 100
          ? "Maximum of 100 people allowed"
          : "Atleast two people needed for creating group"
      );
    }
  };

  error = () => {
    if (!this.isUnmounted) {
      alertValue(ERROR_MESSAGE);
      this.updatedCreatingStatus(false);
    }
  };

  onTextInputChange = (groupName) => {
    this.setState({
      groupName: groupName,
    });
  };

  selectImage = (buttonIndex) => {
    if (buttonIndex == 1) {
      AppPermissions.checkPermission(Permissions.CAMERA_ROLL)
        .then(async () => {
          const attactment = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 0.1,
          });
          if (!attactment.cancelled) {
            if (attactment.type == IMAGE) {
              this.setState({
                groupUri: attactment.uri,
              });
            } else {
              alertValue("File type is not supported");
            }
          }
        })
        .catch(() => {
          alertValue("Camera Permission is needed");
        });
    } else if (buttonIndex == 0) {
      AppPermissions.checkPermission(Permissions.CAMERA)
        .then(async () => {
          const attactment = await ImagePicker.launchCameraAsync();
          if (!attactment.cancelled) {
            if (attactment.type == IMAGE) {
              this.setState({
                groupUri: attactment.uri,
              });
            } else {
              Alert.alert("File type is not supported");
            }
          }
        })
        .catch(() => {
          Alert.alert("Camera Permission is needed");
        });
    }
  };

  chooseGroupImage = () => {
    const options = ["Take Photo", "Choose Photo", "Cancel"];
    const cancelButtonIndex = 2;
    this.props.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
      },
      this.selectImage
    );
  };

  render() {
    return (
      <View style={styles.container(this.props.width, this.props.height)}>
        <View style={{ height: 150, justifyContent: "center" }}>
          <View
            style={{
              flexDirection: "row",
            }}
          >
            <TouchableOpacity
              style={{ width: this.props.width * 0.3, alignItems: "center" }}
              onPress={this.chooseGroupImage}
            >
              <CustomAvatar
                size={60}
                rounded
                source={{
                  uri: this.state.groupUri,
                }}
              />
            </TouchableOpacity>
            <View
              style={{
                width: this.props.width * 0.7,
              }}
            >
              <View
                style={{
                  borderWidth: 0.5,
                  borderColor: "#CFD1CE",
                }}
              />
              <TextInput
                multiline={true}
                onChangeText={this.onTextInputChange}
                style={{ fontSize: 18, padding: 10 }}
                value={this.state.groupName}
                maxLength={30}
                placeholder="Group Name"
                autoFocus
              />
              <View
                style={{
                  borderWidth: 0.5,
                  borderColor: "#CFD1CE",
                }}
              />
            </View>
          </View>
          <Text
            style={{
              color: "#CFD1CE",
              marginLeft: this.props.width * 0.3,
            }}
          >
            Please provide a group name and group icon
          </Text>
        </View>
        <View style={styles.list}>
          <Text style={styles.listHeader}>Members</Text>
        </View>
        <SelectedList list={this.state.members} onPress={this.addOrRemove} />
        {this.state.creating ? <LoadingPage style={styles.loading} /> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: (width, height) => ({
    flex: 1,
    height: height,
    width: width,
  }),
  headerButton: {
    paddingHorizontal: 8,
    alignItems: "center",
  },
  header: {
    height: 80,
  },
  list: {
    marginTop: 3,
    backgroundColor: "#F4F2F1",
    borderRadius: 3,
    alignItems: "center",
    paddingHorizontal: 6,
    flexDirection: "row",
    height: 35,
  },
  listHeader: {
    fontWeight: "bold",
  },
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  channels: state.chatData.channels,
});

const GroupScreen = connectActionSheet(CreateGroupScreen);

export default connect(mapStateToProps, { setChannels })(GroupScreen);
