import React, { Component } from "react";
import { StyleSheet, FlatList } from "react-native";
import UserComponent from "./UserComponent";

export default class SelectedList extends Component {
  render() {
    return (
      <FlatList
        data={this.props.list}
        renderItem={({ item, index }) => (
          <UserComponent
            avatar={item.avatar}
            name={item.name}
            subTitle={item.subTitle}
            uid={item.uid}
            onPress={this.props.onPress}
            createGroupEnabled={true}
            selected={true}
          />
        )}
        keyExtractor={(item, index) => item.uid}
        showsVerticalScrollIndicator={false}
      />
    );
  }
}
