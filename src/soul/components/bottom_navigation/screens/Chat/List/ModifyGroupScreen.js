import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import SelectedList from "./SelectedList";
import { SearchBar } from "react-native-elements";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import FireBase from "../../../../../assets/FireBase";
import { setChannels } from "../../../../../actions/chatActions";
import Students from "./Students";
import Masters from "./Masters";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import ClassesDropDown from "../../../../../../../lib/ClassesDropDown";
import { ERROR_MESSAGE } from "../../../../../constants/Variables";

class ModifyGroupScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerTitle = "Add Members";
    let headerTitleStyle = {
      color: "white",
    };
    let headerStyle = {
      backgroundColor: "#20368F",
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={params.goBack}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text
          style={{
            paddingHorizontal: 6,
            color: "#FFFFFF",
          }}
        >
          Back
        </Text>
      </TouchableOpacity>
    );

    let headerRight = (
      <TouchableOpacity
        onPress={params.modifyGroup}
        style={styles.headerButton}
        activeOpacity={1}
      >
        <Text
          style={{
            paddingHorizontal: 6,
            color: "#FFFFFF",
          }}
        >
          Submit
        </Text>
      </TouchableOpacity>
    );

    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerLeft,
      headerRight,
    };
  };

  constructor(props) {
    super(props);
    this.props.navigation.setParams({
      goBack: this.goBack,
      modifyGroup: this.modifyGroup,
    });
    this.channelId = this.props.navigation.getParam("channelId");
    this.channel = {
      ...this.props.channels.find((c) => c.uniqueId == this.channelId),
    };
    this.isUnmounted = false;
    this.state = {
      search: "",
      classId: this.props.user.classInfo.classId,
      selectedList: [],
    };
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  clearSearch = () => {
    this.updateSearch();
  };

  updateSearch = (search = "") => {
    if (search != this.state.search) {
      this.setState({
        search: search ? search.toLowerCase() : "",
      });
    }
  };

  memberCount = () => {
    let count = this.channel.memberIds.length;
    this.state.selectedList.forEach((m) => {
      if (m.uid != this.props.user.uid) {
        count = count + 1;
      }
    });
    return count;
  };

  setClassId = (classId) => {
    if (classId != this.state.classId) {
      this.setState({
        classId: classId,
      });
    }
  };

  filterComponent = () => {
    return <ClassesDropDown setClassId={this.setClassId} />;
  };

  addOrRemove = (params, addOrRemove) => {
    if (addOrRemove) {
      this.setState((oldState) => ({
        selectedList: [...oldState.selectedList, params],
      }));
    } else {
      this.setState((oldState) => ({
        selectedList: oldState.selectedList.filter((s) => s.uid != params.uid),
      }));
    }
  };

  getSelectedList = () => {
    return (
      <Fragment>
        <View style={styles.list}>
          <Text style={styles.filterText}>
            {this.state.selectedList.length + this.channel.memberIds.length}{" "}
            Selected / 100
          </Text>
        </View>
        <SelectedList
          list={this.state.selectedList}
          onPress={this.addOrRemove}
        />
      </Fragment>
    );
  };

  getSearchBar = () => {
    return (
      <View>
        <SearchBar
          placeholder="Search"
          onChangeText={this.updateSearch}
          value={this.state.search}
          platform="default"
          clearIcon={
            this.state.search ? (
              <TouchableOpacity onPress={this.clearSearch}>
                <TabBarIcon
                  name="cancel"
                  type="MaterialIcons"
                  size={20}
                  color={"#A6A6A6"}
                />
              </TouchableOpacity>
            ) : null
          }
          containerStyle={styles.searchBar}
          inputContainerStyle={styles.seacrhBarInput}
          placeholderTextColor={"#AFAAAA"}
          round
        />
        {this.filterComponent()}
        {this.getSelectedList()}
      </View>
    );
  };

  modifyGroup = async () => {
    if (this.state.selectedList.length && !this.setState.loading) {
      if (this.memberCount() <= 100) {
        this.updateLoadingState(true);
        let collection = FireBaseCollectionService.channels();
        let updateDoc = {
          memberIds: [...this.channel.memberIds],
          members: this.channel.members,
        };
        this.state.selectedList.forEach((m) => {
          updateDoc.memberIds.push(m.uid);
          updateDoc.members[m.uid] = {
            avatar: m.avatar,
            name: m.name,
            subTitle: m.subTitle,
          };
        });
        try {
          await FireBase.updateDocumentInfo(
            collection,
            this.channel.uniqueId,
            updateDoc
          );
          this.props.navigation.push("ChatScreen", {
            channelId: this.channel.uniqueId,
          });
        } catch {
          alertValue(ERROR_MESSAGE);
          this.updateLoadingState(false);
        }
      } else {
        alertValue("Maximum of 100 people allowed");
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        {this.getSearchBar()}
        <Students
          classId={this.state.classId}
          search={this.state.search}
          onPress={this.addOrRemove}
          createGroupEnabled={true}
          selectedList={this.state.selectedList}
          disableList={this.channel.memberIds}
        />
        <Masters
          search={this.state.search}
          onPress={this.addOrRemove}
          createGroupEnabled={true}
          selectedList={this.state.selectedList}
          disableList={this.channel.memberIds}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    padding: 6,
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  filters: {
    backgroundColor: "#F4F2F1",
    minHeight: 50,
    margin: 6,
    padding: 5,
    borderRadius: 4,
  },
  filtersItem: { alignItems: "center" },
  filterText: { fontWeight: "bold", color: "#20368F" },
  searchBar: {
    backgroundColor: "#FFFFFF",
    borderWidth: 0,
    shadowColor: "white",
    borderBottomColor: "#FFFFFF",
    borderTopColor: "#FFFFFF",
  },
  seacrhBarInput: { backgroundColor: "#F4F2F1" },
  loading: {
    ...StyleSheet.absoluteFill,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.2)",
  },
  list: {
    marginTop: 3,
    backgroundColor: "#F4F2F1",
    borderRadius: 3,
    alignItems: "center",
    paddingHorizontal: 6,
    flexDirection: "row",
    justifyContent: "space-between",
    height: 35,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  channels: state.chatData.channels,
});

export default connect(mapStateToProps, {
  setChannels,
})(ModifyGroupScreen);
