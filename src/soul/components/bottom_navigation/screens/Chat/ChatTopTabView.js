import React from "react";
import { createMaterialTopTabNavigator } from "react-navigation";
import UserListScreen from "./List/UserListScreen";
import ChatListScreen from "./List/ChatListScreen";

export default createMaterialTopTabNavigator(
  {
    UserList: UserListScreen,
    ChatList: ChatListScreen
  },
  {
    initialRouteName: "ChatList",
    activeTintColor: "#FFFFFF",
    inactiveTintColor: "#000000",
    showIcon: false,
    tabBarOptions: {
      style: {
        backgroundColor: "#20368F"
      },
      indicatorStyle: {
        height: 2,
        backgroundColor: "#F6F5F5"
      }
    }
  }
);
