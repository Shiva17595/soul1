import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import TabBarIcon from "../../../assets/TabBarIcon";

export default class TabBarLable extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.iconStyle}>
          <TabBarIcon
            name={this.props.iconName}
            type={this.props.iconType}
            size={this.props.focused ? this.props.size + 3 : this.props.size}
            color="#ffffff"
          />
        </View>
        <View>
          <Text
            style={StyleSheet.flatten([
              styles.textStyle,
              {
                fontSize: this.props.focused ? 15 : 12
              }
            ])}
          >
            {this.props.lableText}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center"
  },
  iconStyle: {
    padding: 4
  },
  textStyle: {
    padding: 4,
    color: "#ffffff"
  }
});
