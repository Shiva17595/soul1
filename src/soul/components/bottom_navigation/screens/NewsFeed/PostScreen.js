import React, { Component } from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  View,
  FlatList,
  ActivityIndicator,
  Alert,
  Text,
  TouchableOpacity
} from "react-native";
import PostItem from "../posts/PostItem";
import {
  NEWS_FEED_FIELDS,
  COMMENT_FIELDS
} from "../../../../assets/FireBaseCollections";
import Comment from "./comments/Comment";
import InputComment from "./comments/InputComment";
import FireBase from "../../../../assets/FireBase";
import { LIMIT } from "../../../../constants/Variables";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import TabBarIcon from "../../../../assets/TabBarIcon";
import { StatusBar } from "../../../../../../lib/ReUsableComponents";
import CollectionPaths from "../../../../services/FireBaseCollectionService";
import { isEmptyArray } from "../../../../assets/reusableFunctions";

class PostScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    let header = (
      <View>
        <StatusBar />
        <View style={styles.navBar}>
          <TouchableOpacity
            onPress={params.goBack}
            style={styles.backButton}
            activeOpacity={1}
          >
            <TabBarIcon
              name="ios-arrow-back"
              type="Ionicons"
              size={30}
              color="#000000"
            />
            <Text
              style={{
                paddingHorizontal: 6
              }}
            >
              Back
            </Text>
          </TouchableOpacity>
          <View style={styles.headerTitle}>
            <Text
              style={{
                fontWeight: "bold"
              }}
            >
              Post
            </Text>
          </View>
        </View>
      </View>
    );
    return {
      header
    };
  };

  constructor(props) {
    super(props);
    this._isMounted = false;
    this.post = this.props.navigation.getParam("post");
    this.state = {
      pendingComments: 0,
      comments: [],
      isFetching: false,
      commentsLoaded: false
    };
    this.props.navigation.setParams({
      goBack: this.goBack,
      width: this.props.width
    });
  }

  getOrders() {
    let orders = {};
    orders[COMMENT_FIELDS.createdAt] = "desc";
    return orders;
  }

  getFilters() {
    return undefined;
  }

  getComments = async offset => {
    let document;
    let commentsCollection = CollectionPaths.comments(this.post.uniqueId);
    if (offset != 0) {
      try {
        document = await FireBase.getDocument(commentsCollection, offset);
      } catch {
        document = undefined;
      }
    }
    FireBase.getCollection(
      commentsCollection,
      this.getOrders(),
      document,
      this.getFilters()
    ).then(comments => {
      if (comments) {
        let diff =
          this.state.comments.length +
          comments.length -
          parseInt(this.post.noOfComments);
        if (diff > 0) {
          let valueToUpdate = {};
          valueToUpdate[NEWS_FEED_FIELDS.noOfComments] =
            diff + parseInt(this.post.noOfComments);
          FireBase.getDocumentReference(
            CollectionPaths.newsFeed(this.post.uniqueId)
          ).update(valueToUpdate);
        }
        this._isMounted &&
          this.setState((state, props) => ({
            comments: state.comments.concat(comments),
            isFetching: false,
            commentsLoaded: comments.length < LIMIT
          }));
        if (comments.length == 0) {
          this._isMounted &&
            this.setState({
              isFetching: false,
              commentsLoaded: true
            });
        }
      } else {
        Alert.alert("please check your internet connection");
        this._isMounted &&
          this.setState({
            isFetching: false
          });
      }
    });
  };

  goBack = () => {
    this.props.navigation.navigate("NewsFeed");
  };

  removeComment = commentId => {
    this.setState((state, props) => ({
      comments: state.comments.filter(item => item.uniqueId != commentId)
    }));
  };

  componentDidMount() {
    this._isMounted = true;
    this._isMounted &&
      this.setState({ isFetching: true }, () => {
        this.getComments(0);
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  createComment = comment => {
    comment.user = {
      name: this.props.user.userName,
      avatar: this.props.user.avatar
    };
    comment.pending = true;

    this.setState((state, props) => ({
      comments: [comment].concat(state.comments),
      pendingComments: state.pendingComments + 1
    }));
  };

  loadMoreComments = () => {
    if (!this.state.isFetching && !this.state.commentsLoaded) {
      let offset = 0;
      if (this.state.comments.length != 0) {
        offset = this.state.comments[this.state.comments.length - 1].uniqueId;
      }
      this.setState({ isFetching: true }, () => {
        this.getComments(offset);
      });
    }
  };

  renderFooter = () => {
    return this.state.isFetching ? (
      <View style={{ marginTop: 10, alignItems: "center" }}>
        <ActivityIndicator size="large" />
      </View>
    ) : (
      <View style={{ height: 50 }} />
    );
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.pendingComments != nextState.pendingComments) {
      return true;
    }
    if (this.state.comments.length != nextState.comments.length) {
      return true;
    }
    if (this.state.isFetching != nextState.isFetching) {
      return true;
    }
    return this.props.commentsLoaded != nextProps.commentsLoaded;
  }

  renderComment = comment => {
    return (
      <Comment
        comment={comment}
        postId={this.post.uniqueId}
        removeComment={this.removeComment}
      />
    );
  };

  render() {
    return (
      <KeyboardAwareScrollView
        enableOnAndroid
        keyboardShouldPersistTaps="always"
        extraScrollHeight={50}
      >
        <View style={styles.container}>
          <View>
            <FlatList
              data={this.state.comments}
              renderItem={this.renderComment}
              keyExtractor={item => item.uniqueId}
              ListHeaderComponent={
                <View>
                  <View>
                    <PostItem
                      post={this.post}
                      newComments={this.state.pendingComments}
                      navigation={this.props.navigation}
                    />
                  </View>
                  <View>
                    <InputComment createComment={this.createComment} />
                  </View>
                </View>
              }
              onEndReachedThreshold={0.1}
              onEndReached={this.loadMoreComments}
              ListFooterComponent={this.renderFooter}
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 40,
    width: "100%",
    flexDirection: "row",
    elevation: 3,
    backgroundColor: "#FFFFFF",
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.2,
    shadowRadius: 0
  },
  backButton: {
    height: 40,
    flexDirection: "row",
    alignSelf: "flex-start",
    paddingHorizontal: 8,
    alignItems: "center"
  },
  headerTitle: {
    justifyContent: "center"
  }
});

const mapStateToProps = state => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height
});

export default connect(mapStateToProps, null)(PostScreen);
