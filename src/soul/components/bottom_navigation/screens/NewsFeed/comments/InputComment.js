import React, { Component } from "react";
import { connect } from "react-redux";
import { StyleSheet, View, TextInput, TouchableOpacity } from "react-native";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import AppPermissions from "../../../../../../../lib/AppPermissions";
import { IMAGE, VIDEO } from "../../../../../constants/Variables";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import { findWidth } from "../../../../../assets/reusableFunctions";

class InputComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentText: "",
      attactmentHeight: 0,
      height: 40,
      attactment: undefined,
      cameraDisabled: false,
    };
  }

  onTextInputChange = (value) => {
    this.setState({
      commentText: value,
      height: value.length > 35 ? 100 : 40,
    });
  };

  removeAttachment = () => {
    this.setState({
      attactment: undefined,
      attactmentHeight: 0,
      cameraDisabled: false,
    });
  };

  createComment = () => {
    let id = new Date().getTime().toString();
    let comment = {
      commentText: this.state.commentText.trim(),
      uniqueId: id,
      uniqueIdForDelete: id,
    };
    if (this.state.attactment) {
      comment.file = {
        type: this.state.attactment.type,
        uri: this.state.attactment.uri,
        width: this.state.attactment.width,
        height: this.state.attactment.height,
      };
      if (this.state.attactment.type == VIDEO)
        comment.file.duriation = this.state.attactment.duriation;
    }
    this.props.createComment(comment);
    this.setState({
      commentText: "",
      attactmentHeight: 0,
      height: 40,
      attactment: undefined,
      cameraDisabled: false,
    });
  };

  _onChoosePic = () => {
    AppPermissions.checkPermission(Permissions.CAMERA_ROLL)
      .then(async () => {
        const attactment = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: "All",
          aspect: [4, 3],
          quality: 0.3,
        });
        if (!attactment.cancelled) {
          if (
            (attactment.type == IMAGE || attactment.type == VIDEO) &&
            attactment.width != null &&
            attactment != null
          ) {
            this.setState({
              attactment: attactment,
              attactmentHeight: 150,
            });
          } else {
            Alert.alert("File type is not supported");
          }
        } else {
          this.setState({
            cameraDisabled: false,
          });
        }
      })
      .catch(() => {
        Alert.alert("Permission is needed");
      });
  };

  selectAttachment = () => {
    this.setState({ cameraDisabled: true }, this._onChoosePic);
  };

  render() {
    return (
      <View
        style={styles.container(
          this.props.width,
          this.state.attactmentHeight + this.state.height
        )}
      >
        <View>
          {this.state.attactment ? (
            <ImageOrVideo
              style={styles.attactmentStyling(
                findWidth(
                  this.state.attactment.height,
                  125,
                  this.state.attactment.width
                )
              )}
              file={this.state.attactment}
              removeAttachment={this.removeAttachment}
              needRemoveButton={true}
            />
          ) : null}
        </View>
        <View style={styles.inputBox}>
          <View style={{ alignSelf: "flex-end", paddingBottom: 3 }}>
            <TouchableOpacity
              onPress={this.selectAttachment}
              disabled={this.state.cameraDisabled}
            >
              <TabBarIcon
                name="camera"
                type="EvilIcons"
                size={35}
                color={this.state.attactment ? "#D2D2D2" : "#595A5A"}
              />
            </TouchableOpacity>
          </View>
          <View
            style={styles.commentInput(
              this.state.height - 8,
              this.props.width * 0.82
            )}
          >
            <View>
              <TextInput
                multiline={true}
                onChangeText={this.onTextInputChange}
                style={{ fontSize: 18, padding: 4 }}
                value={this.state.commentText}
                maxLength={1000}
                spellCheck={true}
                placeholder="Write a comment..."
                autoFocus
              />
            </View>
          </View>
          <View style={{ alignSelf: "flex-end", paddingBottom: 3 }}>
            <TouchableOpacity
              onPress={this.createComment}
              disabled={
                this.state.attactment || this.state.commentText.trim()
                  ? false
                  : true
              }
            >
              <TabBarIcon
                type="MaterialIcons"
                name="send"
                size={30}
                color={
                  this.state.attactment || this.state.commentText.trim()
                    ? "#1B7DE2"
                    : "#D2D2D2"
                }
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: (width, height) => ({
    flex: 1,
    borderWidth: 0.5,
    borderColor: "#E8E7E7",
    width: width,
    height: height,
  }),
  inputBox: {
    flexDirection: "row",
    marginHorizontal: 5,
    marginVertical: 4,
    backgroundColor: "#FFFFFF",
  },
  commentInput: (height, width) => ({
    backgroundColor: "#E8E7E7",
    borderRadius: 15,
    paddingHorizontal: 5,
    width: width,
    textAlign: "center",
    height: height,
  }),
  attactmentStyling: (width) => ({
    borderRadius: 15,
    height: 125,
    width: width,
    alignSelf: "stretch",
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(InputComment);
