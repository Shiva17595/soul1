import React, { Component } from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  Clipboard,
} from "react-native";
import FireBase from "../../../../../assets/FireBase";
import {
  COMMENTS_COLLECTION,
  NEWS_FEED_FIELDS,
} from "../../../../../assets/FireBaseCollections";
import ImageOrVideo from "../../../../../../../lib/ImageOrVideo";
import GeneralOptions from "../../posts/GeneralOptions";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import ModalView from "../../../../../../../lib/ModalView";
import FeedBackScreen from "../../../../feedback/FeedBackScreen";
import InputBox from "./InputBox";
import {
  dateConvertion,
  findHeight,
} from "../../../../../assets/reusableFunctions";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";
import FireBaseCollectionService from "../../../../../services/FireBaseCollectionService";
import FireBaseStorageService from "../../../../../services/FireBaseStorageService";
import { VIDEO, CONNECTION_ERROR } from "../../../../../constants/Variables";
import NavigationService from "../../../../../actions/NavigationService";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";

const optionsArray = [
  ["Give feedback on this post", "", "MaterialIcons", "feedback"],
  ["Copy", "", "MaterialIcons", "content-copy"],
  ["Edit Comment", "", "MaterialIcons", "edit"],
  ["Delete Comment", "", "MaterialCommunityIcons", "delete"],
];

const cancel = ["Cancel", "", "EvilIcons", "close"];

class Comment extends Component {
  constructor(props) {
    super(props);
    this._isMounted = true;
    this.state = {
      showTotalComment: false,
      comment: this.props.comment.item,
      commentCreating: false,
      deleting: false,
      modalVisible: false,
      feedbackModal: false,
      editCommentModal: false,
    };
  }

  deleteComment = () => {
    if (this.props.connectionStatus) {
      FireBase.getDocumentReference(
        FireBaseCollectionService.comments(
          this.props.postId,
          this.state.comment.uniqueId
        )
      )
        .delete()
        .then(() => {
          this.updateCommentCount(-1);
          this.props.removeComment(
            this.state.comment.uniqueIdForDelete || this.state.comment.uniqueId
          );
          this._isMounted &&
            this.setState({
              deleting: false,
            });
        })
        .catch((error) => {
          alertValue("Something Went Wrong");
          this._isMounted &&
            this.setState({
              deleting: false,
            });
        });
    } else {
      alertValue(CONNECTION_ERROR);
    }
  };

  updateCommentCount = (value) => {
    let valueToUpdate = {};
    valueToUpdate[NEWS_FEED_FIELDS.noOfComments] = FireBase.filedValueIncrement(
      value
    );
    FireBase.getDocumentReference(
      FireBaseCollectionService.newsFeed(this.props.postId)
    ).update(valueToUpdate);
  };

  creatingPendingComment = async () => {
    if (this.state.commentCreating) {
      let comment = {
        commentText: this.state.comment.commentText,
        createdAt: new Date(),
        updatedAt: new Date(),
        user: {
          uid: this.props.user.uid,
          name: this.props.user.name,
          avatar: this.props.user.avatar,
        },
      };
      if (this.state.comment.file) {
        let fileName = `${this.state.comment.file.type}-${
          this.props.user.uid
        }-${new Date().getTime()}`;
        let uri = await FireBase.uploadFile(
          this.state.comment.file.uri,
          FireBaseStorageService.comments(fileName)
        );
        if (!uri) {
          this._isMounted &&
            this.setState({
              commentCreating: false,
            });
          return;
        }
        comment.file = {
          type: this.state.comment.file.type,
          uri: uri,
          name: `${fileName}.${this.state.comment.file.uri
            .split(/[\s.]+/)
            .pop()}`,
          width: this.state.comment.file.width,
          height: this.state.comment.file.height,
        };
        if (this.state.comment.file.type == VIDEO)
          comment.file.duration = this.state.comment.file.duration;
      }
      FireBase.createDocument(
        FireBaseCollectionService.comments(this.props.postId),
        comment
      )
        .then((response) => {
          this.updateCommentCount(1);
          comment.uniqueId = response.id;
          comment.pending = false;
          comment.uniqueIdForDelete = this.state.comment.uniqueIdForDelete;
          this._isMounted &&
            this.setState({
              comment: comment,
              commentCreating: false,
            });
        })
        .catch((error) => {
          comment.uniqueId = new Date().getTime().toString();
          comment.pending = true;
          comment.uniqueIdForDelete = this.state.comment.uniqueIdForDelete;
          this._isMounted &&
            this.setState({
              comment: comment,
              commentCreating: false,
            });
        });
    }
  };

  performSelectedAction = async (index) => {
    this.closeModal();
    switch (index) {
      case 1:
        setTimeout(() => {
          this.openFeedBackModal();
        }, 500);
        break;
      case 2:
        await Clipboard.setString(this.state.comment.commentText);
        break;
      case 3:
        setTimeout(() => {
          this.openEditCommentModal();
        }, 500);
        break;
      case 4:
        let self = this;
        setTimeout(() => {
          Alert.alert(
            "Delete Comment",
            "Confirm to delete",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
              },
              {
                text: "Delete",
                onPress: () => {
                  self.setState(
                    {
                      deleting: true,
                    },
                    () => {
                      self.deleteComment();
                    }
                  );
                },
              },
            ],
            {
              cancelable: false,
            }
          );
        }, 1000);
        break;

      default:
        break;
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.comment.uniqueId != nextState.comment.uniqueId) {
      return true;
    }
    if (this.state.showTotalComment != nextState.showTotalComment) {
      return true;
    }
    if (this.state.deleting != nextState.deleting) {
      return true;
    }
    if (this.state.modalVisible != nextState.modalVisible) {
      return true;
    }
    if (this.state.feedbackModal != nextState.feedbackModal) {
      return true;
    }
    if (this.state.editCommentModal != nextState.editCommentModal) {
      return true;
    }
    return this.props.connectionStatus != nextProps.connectionStatus;
  }

  componentDidMount() {
    if (this.props.connectionStatus && this.state.comment.pending) {
      this._isMounted &&
        this.setState({ commentCreating: true }, this.creatingPendingComment);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate() {
    if (
      this.props.connectionStatus &&
      this.state.comment.pending &&
      !this.state.commentCreating
    ) {
      this._isMounted &&
        this.setState({ commentCreating: true }, this.creatingPendingComment);
    }
  }

  getStatusComponent = () => {
    if (!this.props.connectionStatus && this.state.comment.pending) {
      return (
        <View style={{ flexDirection: "row" }}>
          <TabBarIcon type="MaterialIcons" name="error" color="red" size={15} />
          <Text color="red">Please check your network connection</Text>
        </View>
      );
    }
    if (this.state.comment.pending) {
      return (
        <View style={{ flexDirection: "row" }}>
          <ActivityIndicator size="small" />
          <Text style={{ color: "#A19F9F" }}>posting....</Text>
        </View>
      );
    }
    if (this.state.deleting) {
      return (
        <View style={{ flexDirection: "row" }}>
          <ActivityIndicator size="small" />
          <Text style={{ color: "red" }}>deleting...</Text>
        </View>
      );
    }
    return (
      <View>
        <Text style={{ color: "#A19F9F" }}>
          {dateConvertion(
            this.state.comment.pending == false
              ? this.state.comment.createdAt
              : this.state.comment.createdAt.toDate()
          )}
        </Text>
      </View>
    );
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  openModel = () => {
    this.setState({
      modalVisible: true,
    });
  };

  closeFeedBackModal = () => {
    this.setState({
      feedbackModal: false,
    });
  };

  openFeedBackModal = () => {
    this.setState({
      feedbackModal: true,
    });
  };

  closeEditCommentModal = () => {
    this.setState({
      editCommentModal: false,
    });
  };

  openEditCommentModal = () => {
    this.setState({
      editCommentModal: true,
    });
  };

  changeCommentText = (value) => {
    this.setState((state, props) => ({
      comment: { ...state.comment, ...{ commentText: value } },
      editCommentModal: false,
    }));
  };

  showTotal = () => {
    this.setState({
      showTotalComment: !this.state.showTotalComment,
    });
  };

  viewFile = () => {
    NavigationService.navigate("View", { file: this.state.comment.file });
  };

  render() {
    let comment = this.state.comment;
    return (
      <View style={styles.comment(this.props.width * 0.8)}>
        <View style={styles.profilePic}>
          <CustomAvatar
            rounded
            uid={comment.user.uid}
            source={{ uri: comment.user.avatar }}
          />
        </View>
        <View>
          <TouchableOpacity onLongPress={this.openModel} activeOpacity={1}>
            <View style={styles.commentBody}>
              <Text style={styles.userName} onLongPress={this.openModel}>
                {comment.user.name}
              </Text>
              <Text
                style={{ flexWrap: "wrap" }}
                ellipsizeMode="tail"
                numberOfLines={this.state.showTotalComment ? 200 : 2}
                onPress={this.showTotal}
                onLongPress={this.openModel}
              >
                {comment.commentText}
              </Text>
            </View>
            <View style={{ paddingVertical: comment.file ? 5 : 0 }}>
              {comment.file ? (
                <TouchableOpacity onPress={this.viewFile} activeOpacity={1}>
                  <ImageOrVideo
                    style={styles.attachmentStyling(
                      this.props.width * 0.6,
                      findHeight(
                        comment.file.width,
                        this.props.width * 0.6,
                        comment.file.height
                      )
                    )}
                    file={comment.file}
                    resizeMode="cover"
                  />
                </TouchableOpacity>
              ) : null}
            </View>
            {this.getStatusComponent()}
          </TouchableOpacity>
        </View>
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={styles.modalStyle}
        >
          <GeneralOptions
            title={
              <View style={styles.headerComponent}>
                <View style={styles.panelHeader}>
                  <View style={styles.panelHandle} />
                </View>
              </View>
            }
            optionsArray={
              this.props.user.uid == comment.user.uid
                ? optionsArray
                : optionsArray.slice(0, 2)
            }
            cancel={cancel}
            callbackFunction={this.performSelectedAction}
          />
        </ModalView>
        <ModalView
          isVisible={this.state.feedbackModal}
          onBackButtonPress={this.closeFeedBackModal}
          onBackdropPress={this.closeFeedBackModal}
          style={styles.modalStyle}
        >
          <FeedBackScreen
            title={
              <View style={styles.headerComponent}>
                <View style={styles.panelHeader}>
                  <Text style={styles.panelHeaderText}>
                    Please specify the problem you are facing
                  </Text>
                </View>
              </View>
            }
            id={comment.uniqueId}
            type={COMMENTS_COLLECTION}
            close={this.closeFeedBackModal}
          />
        </ModalView>
        <ModalView
          isVisible={this.state.editCommentModal}
          onBackButtonPress={this.closeEditCommentModal}
          onBackdropPress={this.closeEditCommentModal}
          style={styles.modalStyle}
        >
          <InputBox
            title={
              <View style={styles.headerComponent}>
                <View style={styles.panelHeader}>
                  <Text style={styles.panelHeaderText}>Edit Comment</Text>
                </View>
              </View>
            }
            id={comment.uniqueId}
            postId={this.props.postId}
            commentText={comment.commentText}
            changeCommentText={this.changeCommentText}
          />
        </ModalView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  comment: (width) => ({
    flexDirection: "row",
    marginVertical: 10,
    marginHorizontal: 6,
    width: width,
  }),
  profilePic: {
    paddingHorizontal: 6,
  },
  commentBody: {
    padding: 6,
    borderRadius: 4,
    backgroundColor: "#E8E7E7",
    borderRadius: 10,
  },
  userName: {
    fontWeight: "bold",
    fontSize: 13,
    marginVertical: 3,
    color: "#393535",
  },
  image: {
    paddingVertical: 5,
  },
  attachmentStyling: (width, height) => ({
    borderRadius: 10,
    height: height,
    aspectRatio: 1,
    flexWrap: "wrap",
    width: width,
  }),
  headerComponent: {
    paddingTop: 20,
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
  },
  panelHeaderText: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#393535",
    borderBottomWidth: 0.5,
    borderBottomColor: "#000000",
  },
  deletingIndicator: {
    flexDirection: "row",
    height: 25,
    backgroundColor: "#FFFFFF",
  },
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(Comment);
