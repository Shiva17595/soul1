import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  TextInput,
  Text,
} from "react-native";
import { connect } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import FireBase from "../../../../../assets/FireBase";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";
import CollectionPaths from "../../../../../services/FireBaseCollectionService";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import { ERROR_MESSAGE } from "../../../../../constants/Variables";

class InputBox extends Component {
  constructor(props) {
    super(props);
    this._isMounted = true;
    this.state = {
      commentText: this.props.commentText,
      inputHeight: this.props.commentText.length > 35 ? 100 : 40,
      submitting: false,
    };
  }

  onTextInputChange = (value) => {
    this._isMounted &&
      this.setState({
        commentText: value,
        inputHeight: value.length > 35 ? 100 : 40,
      });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  editComment = async () => {
    if (!this.state.submitting) {
      this.setState({
        submitting: true,
      });
      let comment = {
        commentText: this.state.commentText,
        updatedAt: new Date(),
      };
      try {
        await FireBase.updateDocumentInfo(
          CollectionPaths.comments(this.props.postId),
          this.props.id,
          comment
        );
        this.props.changeCommentText(this.state.commentText);
        setTimeout(() => {
          alertValue("Comment edited");
        }, 500);
      } catch (error) {
        alertValue(ERROR_MESSAGE);
      }
      this._isMounted &&
        this.setState({
          submitting: false,
        });
    }
  };

  render() {
    return (
      <View
        style={StyleSheet.flatten([
          styles.container,
          {
            width: isNaN(this.props.width) ? "100%" : this.props.width,
            height: isNaN(this.props.height)
              ? "65%"
              : (this.props.height * 65) / 100,
          },
        ])}
      >
        <KeyboardAwareScrollView
          enableOnAndroid
          keyboardShouldPersistTaps="always"
          extraScrollHeight={50}
        >
          {this.props.title}
          <View
            style={styles.commentInput(this.props.width, this.props.height)}
          >
            <View
              style={{
                alignItems: "flex-start",
                width: this.props.width * 0.1,
              }}
            >
              <CustomAvatar rounded source={{ uri: this.props.user.avatar }} />
            </View>
            <View style={styles.commentInputBox(this.props.width * 0.8)}>
              <TextInput
                multiline={true}
                onChangeText={this.onTextInputChange}
                style={{ fontSize: 18, padding: 10 }}
                value={this.state.commentText}
                maxLength={1000}
                spellCheck={true}
                placeholder=""
                autoFocus
              />
            </View>
            <View
              style={{
                alignItems: "flex-end",
                width: this.props.width * 0.1,
              }}
            >
              <TouchableOpacity
                onPress={this.editComment}
                disabled={
                  this.state.commentText != this.props.commentText
                    ? false
                    : true
                }
              >
                <TabBarIcon
                  type="MaterialIcons"
                  name="send"
                  size={30}
                  color={
                    this.state.commentText != this.props.commentText
                      ? "#1B7DE2"
                      : "#D2D2D2"
                  }
                />
              </TouchableOpacity>
            </View>
          </View>
          {this.state.submitting ? (
            <View
              style={StyleSheet.flatten([
                styles.commentInput,
                {
                  width: isNaN(this.props.width) ? "100%" : this.props.width,
                },
              ])}
            >
              <View style={{ padding: 4 }}>
                <ActivityIndicator size={"large"} />
              </View>
              <Text style={{ padding: 4 }}>editing comment....</Text>
            </View>
          ) : null}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFFFFF",
  },
  commentInput: (width, height) => ({
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 10,
    paddingHorizontal: 3,
  }),
  commentInputBox: (width) => ({
    borderWidth: 1.5,
    borderColor: "#E8E7E7",
    borderRadius: 10,
    width: width,
  }),
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(InputBox);
