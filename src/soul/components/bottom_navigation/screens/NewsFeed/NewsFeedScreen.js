import React, { Component } from "react";
import { connect } from "react-redux";
import { StyleSheet, View, FlatList, ActivityIndicator } from "react-native";
import NewPost from "../posts/NewPost";
import PostItem from "../posts/PostItem";
import PendingPostItem from "../posts/PendingPostItem";
import { setVideoId } from "../../../../actions/commonActions";
import {
  setPosts,
  setPostsAndClassPosts,
  getAllPosts,
  setClassPosts,
} from "../../../../actions/newsFeedActions";
import FireBase from "../../../../assets/FireBase";
import { NEWS_FEED_FIELDS } from "../../../../assets/FireBaseCollections";
import CollectionPaths from "../../../../services/FireBaseCollectionService";
import { VIDEO, componentNames } from "../../../../constants/Variables";
import { isEmptyArray, getKey } from "../../../../assets/reusableFunctions";
import { seperator } from "../../../../../../lib/ReUsableComponents";

class NewsFeedScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    let headerTitle = "News Feed";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      newsFeedData: [],
      classNewsFeedData: [],
      refreshing: true,
      loadingPosts: true,
      loadedAll: false,
    };
    this._isMounted = true;
  }

  async componentDidMount() {
    await this.props.getAllPosts();
    this.getPosts();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getFilters(security, classId) {
    let filters = {};
    filters[NEWS_FEED_FIELDS.security] = {
      type: "==",
      value: security,
    };
    if (classId) {
      filters[`${NEWS_FEED_FIELDS.classInfo}.${NEWS_FEED_FIELDS.classId}`] = {
        type: "==",
        value: classId,
      };
    }
    return filters;
  }

  getOrders() {
    let orders = {};
    orders[NEWS_FEED_FIELDS.uploadedDate] = "desc";
    return orders;
  }

  getPosts = async (postOffset, classPostOffset) => {
    if (this.props.connectionStatus) {
      let collectionName = CollectionPaths.newsFeed();
      let post = await FireBase.getOffsetDoc(collectionName, postOffset);
      let classPost = await FireBase.getOffsetDoc(
        collectionName,
        classPostOffset
      );
      let promises = [];
      promises.push(
        FireBase.getCollection(
          collectionName,
          this.getOrders(),
          post,
          this.getFilters(0)
        )
      );
      promises.push(
        FireBase.getCollection(
          collectionName,
          this.getOrders(),
          classPost,
          this.getFilters(1, this.props.user.classInfo.classId)
        )
      );
      Promise.all(promises).then((results) => {
        let [posts, classPosts] = results;
        if (!posts) posts = [];
        if (!classPosts) classPosts = [];
        if (!postOffset && !classPostOffset) {
          this.props.setPostsAndClassPosts(posts, classPosts);
        }
        this.setState((prev) => ({
          newsFeedData: !post ? posts : prev.newsFeedData.concat(posts),
          classNewsFeedData: !classPost
            ? classPosts
            : prev.classNewsFeedData.concat(classPosts),
          loadingPosts: false,
          refreshing: false,
          loadedAll: isEmptyArray(posts) && isEmptyArray(classPosts),
        }));
      });
    } else {
      this.setState({
        loadingPosts: false,
        refreshing: false,
      });
    }
  };

  navigateToCreatePost = () => {
    this.props.navigation.push("NewPost");
  };

  renderPosts = (post) => {
    if (post.item.localattachments) {
      return (
        <PendingPostItem post={post.item} navigation={this.props.navigation} />
      );
    } else {
      return (
        <PostItem
          post={post.item}
          navigation={this.props.navigation}
          removePost={this.removePost}
          viewName={componentNames.newsFeed}
        />
      );
    }
  };

  removePost = (postId) => {
    this.setState((prev) => ({
      newsFeedData: prev.newsFeedData.filter((item) => item.uniqueId != postId),
      classNewsFeedData: prev.classNewsFeedData.filter(
        (item) => item.uniqueId != postId
      ),
    }));
  };

  componentDidUpdate() {
    let postId = this.props.navigation.getParam("removePostId") || null;
    if (postId) {
      let post = this.state.newsFeedData.find((d) => d.uniqueId == postId);
      if (!post) this.state.classNewsFeedData.find((d) => d.uniqueId == postId);
      if (post) this.removePost(post.uniqueId);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.connectionStatus != nextProps.connectionStatus) return false;
    if (this.props.videoId != nextProps.videoId) return false;
    if (this.props.viewName != nextProps.viewName) return false;
    return true;
  }

  loadMorePosts = () => {
    if (!this.state.loadingPosts && !this.state.loadedAll) {
      let postOffset = this.state.newsFeedData[
        this.state.newsFeedData.length - 1
      ];
      let classPostOffset = this.state.classNewsFeedData[
        this.state.classNewsFeedData.length - 1
      ];
      this._isMounted &&
        this.setState({ loadingPosts: true }, () => {
          this.getPosts(
            postOffset ? postOffset.uniqueId : null,
            classPostOffset ? classPostOffset.uniqueId : null
          );
        });
    }
  };

  renderFooter = () => {
    return this.state.loadingPosts && !this.state.refreshing ? (
      <View style={{ marginTop: 10, alignItems: "center", height: 30 }}>
        <ActivityIndicator size="large" />
      </View>
    ) : null;
  };

  refreshFeeds = () => {
    this._isMounted &&
      this.setState(
        { loadingPosts: true, refreshing: true, loadedAll: false },
        this.getPosts
      );
  };

  getVisiableArray = ({ viewableItems }) => {
    if (this.props.viewName == componentNames.newsFeed && this.props.videoId) {
      let videoItem = viewableItems[0] ? viewableItems[0].item.files[0] : null;
      if (
        !videoItem ||
        this.props.videoId != `${componentNames.newsFeed}-${videoItem.name}`
      ) {
        this.props.setVideoId(null);
      }
    }
  };

  getPostsToRender = () => {
    let posts = [];
    if (
      isEmptyArray(this.state.newsFeedData) &&
      isEmptyArray(this.state.classNewsFeedData)
    ) {
      posts = this.props.newsFeedData.concat(this.props.classNewsFeedData);
    } else {
      posts = this.state.newsFeedData.concat(this.state.classNewsFeedData);
    }
    posts.sort((p1, p2) => (p1.uploadedDate > p2.uploadedDate ? -1 : 1));
    return this.props.pendingPosts.concat(posts);
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.getPostsToRender()}
          renderItem={this.renderPosts}
          keyExtractor={getKey}
          ItemSeparatorComponent={seperator}
          ListHeaderComponent={
            <NewPost navigateToCreatePost={this.navigateToCreatePost} />
          }
          onEndReached={this.loadMorePosts}
          onEndReachedThreshold={0.1}
          ListFooterComponent={this.renderFooter}
          refreshing={this.state.refreshing}
          onRefresh={this.refreshFeeds}
          onViewableItemsChanged={this.getVisiableArray}
          viewabilityConfig={{
            minimumViewTime: 1000,
            itemVisiblePercentThreshold: 70,
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
});

const mapStateToProps = (state) => {
  return {
    connectionStatus: state.commonInfo.connectionStatus,
    user: state.commonInfo.user,
    pendingPosts: state.newsFeeds.pendingPosts,
    newsFeedData: state.newsFeeds.newsFeedData,
    classNewsFeedData: state.newsFeeds.classNewsFeedData,
    videoId: state.commonInfo.videoId,
    viewName: state.commonInfo.viewName,
  };
};

export default connect(mapStateToProps, {
  setPosts,
  setClassPosts,
  getAllPosts,
  setVideoId,
  setPostsAndClassPosts,
})(NewsFeedScreen);
