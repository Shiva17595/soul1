import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { StyleSheet, View } from "react-native";
import { NavigationActions, StackActions } from "react-navigation";
import StudentAttendence from "./StudentAttendence";
import MasterAttendence from "./MasterAttendence";
import User from "../../../../../services/UserService";
import { logout } from "../../../../../actions/authenticateUser";

class AttendenceScreen extends PureComponent {
  getAttendenceScreen = () => {
    if (this.props.user.isStudent) {
      return <StudentAttendence />;
    }
    if (this.props.user.isMaster) {
      return <MasterAttendence />;
    }
    User.logout();
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Login" })],
    });
    this.props.navigation.dispatch(resetAction);
    this.props.logout();
  };

  render() {
    return <View style={styles.container}>{this.getAttendenceScreen()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
});

const mapStateToProps = (state) => {
  return {
    connectionStatus: state.commonInfo.connectionStatus,
    user: state.commonInfo.user,
  };
};

export default connect(mapStateToProps, { logout })(AttendenceScreen);
