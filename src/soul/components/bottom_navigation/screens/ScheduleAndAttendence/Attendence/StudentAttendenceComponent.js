import React, { PureComponent } from "react";
import { StyleSheet, Text, View, Alert } from "react-native";
import { Card } from "react-native-elements";
import { connect } from "react-redux";

class StudentAttendenceComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.noOfClasses = this.props.classInfo.listOfSubjects[
      this.props.subjectId
    ].noOfClasses;
    this.persentPersent =
      (this.props.noOfClassesAttended / this.noOfClasses) * 100;
    this.widthPersent = (this.props.width * 80) / 100;
  }

  render() {
    return (
      <Card containerStyle={{ borderRadius: 5 }}>
        <View style={styles.card}>
          <View style={styles.title}>
            <Text style={{ fontWeight: "bold" }}>Subject Name : </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              onPress={() => Alert.alert(this.props.subjectName)}
            >
              {this.props.subjectName}
            </Text>
          </View>
          <View style={styles.title}>
            <Text style={{ fontWeight: "bold" }}>
              No of Classes Scheduled :{" "}
            </Text>
            <Text>{this.noOfClasses}</Text>
          </View>
          <View style={styles.title}>
            <Text style={{ fontWeight: "bold" }}>
              No of Classes Attended :{" "}
            </Text>
            <Text>{this.props.noOfClassesAttended}</Text>
          </View>
          <View>
            <View style={styles.persent}>
              <Text>{this.persentPersent}%</Text>
            </View>
            <View
              style={StyleSheet.flatten([
                styles.progressBar,
                {
                  width: this.widthPersent
                }
              ])}
            >
              <View
                style={{
                  width: (this.widthPersent * this.persentPersent) / 100 - 4,
                  backgroundColor:
                    this.persentPersent > 50 ? "#7bb956" : "#d6524d"
                }}
              />
              <View
                style={{
                  width:
                    (this.widthPersent * (100 - this.persentPersent)) / 100 - 4,
                  backgroundColor: "#FFFFFF"
                }}
              />
            </View>
          </View>
        </View>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    height: 100,
    justifyContent: "space-between"
  },
  title: {
    flexDirection: "row",
    alignItems: "flex-start"
  },
  persent: {
    alignItems: "center",
    justifyContent: "center"
  },
  progressBar: {
    height: 10,
    borderColor: "#000",
    borderWidth: 2,
    borderRadius: 5,
    flexDirection: "row"
  }
});

const mapStateToProps = state => {
  return {
    width: state.commonInfo.width,
    height: state.commonInfo.height,
    classInfo: state.userData.classInfo
  };
};

export default connect(
  mapStateToProps,
  null
)(StudentAttendenceComponent);
