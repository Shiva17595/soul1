import React, { PureComponent } from "react";
import { StyleSheet, View, FlatList, Text, Alert } from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import StudentAttendenceComponent from "./StudentAttendenceComponent";
import {
  LoadingPage,
  EmptyComponent
} from "../../../../../../../lib/ReUsableComponents";
import { PieChart } from "react-native-chart-kit";
import { isEmptyHash } from "../../../../../assets/reusableFunctions";

class StudentAttendence extends PureComponent {
  attendenceHeaderComponent = () => {
    return (
      <View style={styles.attendenceHeader}>
        <View style={styles.attendenceHeaderLeft}>
          <Text style={styles.contentTitle}>Class : </Text>
          <Text
            style={{ fontWeight: "bold", fontSize: 20 }}
            ellipsizeMode="tail"
            numberOfLines={1}
            style={{
              maxWidth: this.props.width * 0.7
            }}
            onPress={() => Alert.alert(this.props.user.classInfo.className)}
          >
            {this.props.user.classInfo.className}
          </Text>
        </View>
        <Card containerStyle={{ borderRadius: 5 }} title="Overall Attendence">
          <PieChart
            data={[
              {
                name: "Presence",
                persentage: this.props.user.attendenceInfo[
                  this.props.user.classInfo.classId
                ].attendencePersent,
                color: "#7bb956",
                legendFontColor: "#7F7F7F",
                legendFontSize: 15
              },
              {
                name: "Absence",
                persentage:
                  100 -
                  this.props.user.attendenceInfo[
                    this.props.user.classInfo.classId
                  ].attendencePersent,
                color: "#d6524d",
                legendFontColor: "#7F7F7F",
                legendFontSize: 15
              }
            ]}
            width={this.props.width - 60}
            height={70}
            chartConfig={{
              backgroundColor: "#1cc910",
              backgroundGradientFrom: "#eff3ff",
              backgroundGradientTo: "#efefef",
              decimalPlaces: 2,
              color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              style: {
                borderRadius: 16
              }
            }}
            style={{
              marginVertical: 8,
              borderRadius: 16
            }}
            accessor="persentage"
            backgroundColor="transparent"
          />
        </Card>
      </View>
    );
  };

  renderUserAttendence = () => {
    if (this.props.classInfo && !isEmptyHash(this.props.user)) {
      return (
        <FlatList
          data={
            this.props.user.attendenceInfo[this.props.user.classInfo.classId]
              .subjects
          }
          renderItem={subject => (
            <StudentAttendenceComponent {...subject.item} />
          )}
          keyExtractor={(item, index) => item.subjectId}
          ListHeaderComponent={this.attendenceHeaderComponent}
          ListEmptyComponent={
            <EmptyComponent style={styles.emptyComponentStyle} />
          }
          showsVerticalScrollIndicator={false}
        />
      );
    } else {
      return (
        <LoadingPage
          size="large"
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center"
          }}
        />
      );
    }
  };

  render() {
    return <View style={styles.container}>{this.renderUserAttendence()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 8
  },
  attendenceHeader: {
    height: 200
  },
  attendenceHeaderLeft: {
    flexDirection: "row",
    alignItems: "center"
  },
  contentTitle: {
    color: "#AFAAAA",
    fontSize: 20
  },
  emptyComponentStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

const mapStateToProps = state => {
  return {
    connectionStatus: state.commonInfo.connectionStatus,
    user: state.commonInfo.user,
    classInfo: state.userData.classInfo,
    width: state.commonInfo.width,
    height: state.commonInfo.height
  };
};

export default connect(mapStateToProps, null)(StudentAttendence);
