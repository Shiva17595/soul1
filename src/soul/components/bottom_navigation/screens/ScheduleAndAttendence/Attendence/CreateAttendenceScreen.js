import React, { PureComponent } from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity,
  FlatList,
} from "react-native";
import NavigationService from "../../../../../actions/NavigationService";
import User from "../../../../../services/UserService";
import { NavigationActions, StackActions } from "react-navigation";
import {
  LoadingPage,
  alertValue,
  EmptyComponent,
  CustomizeCheckBox,
} from "../../../../../../../lib/ReUsableComponents";
import FireBase from "../../../../../assets/FireBase";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import { DATE_FORMAT, ERROR_MESSAGE } from "../../../../../constants/Variables";
import StudentItem from "./StudentItem";
import {
  getDateWithOutTime,
  nDaysBackDate,
} from "../../../../../assets/reusableFunctions";
import CollectionPaths from "../../../../../services/FireBaseCollectionService";
import { logout } from "../../../../../actions/authenticateUser";

class CreateAttendenceScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let gesturesEnabled = false;
    let headerTitle = params.headerTitle;
    let headerTitleStyle = {
      color: "white",
    };
    let headerStyle = {
      backgroundColor: "#20368F",
    };
    let headerRight = (
      <TouchableOpacity
        style={styles.headerLabel}
        onPress={() => params.onSubmit()}
      >
        <Text
          style={{
            color: "#FFFFFF",
          }}
        >
          Submit
        </Text>
      </TouchableOpacity>
    );
    let headerLeft = (
      <TouchableOpacity
        style={styles.headerLabel}
        onPress={() => params.goBack()}
      >
        <Text
          style={{
            color: "#FFFFFF",
          }}
        >
          Close
        </Text>
      </TouchableOpacity>
    );
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerRight,
      headerLeft,
      gesturesEnabled,
    };
  };

  constructor(props) {
    super(props);
    this.isUnmounted = false;
    this.classId = this.props.navigation.getParam("classId");
    this.subjectId = this.props.navigation.getParam("subjectId");
    this.scheduleFrom = this.props.navigation.getParam("scheduleFrom");
    this.scheduleTo = this.props.navigation.getParam("scheduleTo");
    this.date = this.props.navigation.getParam("date");
    this.state = {
      refreshing: true,
      studentPresence: {},
      selectAll: false,
      creatingAttendence: false,
      notAllowed: !this.permissionAllowed(),
    };
  }

  permissionAllowed = () => {
    return (
      getDateWithOutTime(this.date) <= getDateWithOutTime(new Date()) &&
      getDateWithOutTime(this.date) >= getDateWithOutTime(nDaysBackDate(7))
    );
  };

  getClassInfo = () => {
    let collectionName = CollectionPaths.classes();
    return FireBase.getDocumentInfo(collectionName, this.classId);
  };

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  componentDidMount() {
    try {
      if (this.props.user.isMaster) {
        this.props.navigation.setParams({
          onSubmit: this.onSubmit,
          goBack: this.goBack,
          headerTitle: "Attendence",
        });
        let promises = [this.getClassInfo(), this.getAttendenceObject()];
        Promise.all(promises).then((results) => {
          let [classInfo, attendence] = results;
          let studentPresence = {};
          if (attendence) {
            attendence[0] &&
              attendence[0].presentStudents.forEach(
                (sUid) => (studentPresence[sUid] = true)
              );
            !this.isUnmounted &&
              this.setState({
                refreshing: false,
                classInfo: classInfo,
                attendenceDocument: attendence[0],
                studentPresence: studentPresence,
              });
          } else {
            throw "error";
          }
        });
      } else {
        throw "unauthorized";
      }
    } catch (error) {
      alertValue("Something Went Wrong, Please try again");
      if (error == "unauthorized") {
        User.logout();
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "Login" })],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.logout();
      } else {
        NavigationService.navigate("AttendenceScreen");
      }
    }
  }

  goBack = () => {
    if (this.state.creatingAttendence) {
      return;
    }
    Alert.alert(
      "Confirm",
      "Do you want to discard changes",
      [
        {
          text: "Cancel",
          onPress: () => {},
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => this.props.navigation.goBack(),
        },
      ],
      {
        cancelable: false,
      }
    );
  };

  getAttendenceObject = () => {
    let collection = CollectionPaths.attendence(this.classId);
    let conditions = {
      from: {
        type: "==",
        value: this.scheduleFrom,
      },
      to: {
        type: "==",
        value: this.scheduleTo,
      },
      date: {
        type: "==",
        value: getDateWithOutTime(this.date),
      },
    };
    return FireBase.getCollection(collection, null, null, conditions);
  };

  updateAttendence = () => {
    let classInfo = this.state.classInfo;
    let attendenceSummary = {
      masterInfo: {
        masterName: this.props.user.name,
        masterUid: this.props.user.uid,
      },
      updatedBy: [
        ...this.state.attendenceDocument.updatedBy,
        { masterUid: this.props.user.uid, date: new Date() },
      ],
      updatedAt: new Date(),
      presentStudents: [],
      absentStudents: [],
    };
    classInfo.listOfStudents.forEach((s) => {
      if (this.state.studentPresence[s.studentUid])
        attendenceSummary.presentStudents.push(s.studentUid);
      else attendenceSummary.absentStudents.push(s.studentUid);
    });
    let collection = CollectionPaths.attendence(classInfo.uniqueId);
    FireBase.updateDocumentInfo(
      collection,
      this.state.attendenceDocument.uniqueId,
      attendenceSummary
    )
      .then(() => {
        alertValue("Submitted successfully");
        NavigationService.navigate("AttendenceScreen");
      })
      .catch((error) => {
        alertValue(ERROR_MESSAGE);
        !this.isUnmounted &&
          this.setState({
            creatingAttendence: false,
          });
      });
  };

  createAttendence = () => {
    let classInfo = this.state.classInfo;
    let attendenceSummary = {
      classInfo: {
        className: classInfo.name,
        classId: classInfo.uniqueId,
      },
      date: getDateWithOutTime(this.date),
      from: this.scheduleFrom,
      to: this.scheduleTo,
      masterInfo: {
        masterName: this.props.user.name,
        masterUid: this.props.user.uid,
      },
      subjectInfo: {
        subjectName: classInfo.listOfSubjects[this.subjectId].subjectName,
        subjectId: this.subjectId,
      },
      presentStudents: [],
      absentStudents: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      updatedBy: [
        {
          masterUid: this.props.user.uid,
          date: new Date(),
        },
      ],
    };
    classInfo.listOfStudents.forEach((s) => {
      if (this.state.studentPresence[s.studentUid])
        attendenceSummary.presentStudents.push(s.studentUid);
      else attendenceSummary.absentStudents.push(s.studentUid);
    });
    let collection = CollectionPaths.attendence(classInfo.uniqueId);

    FireBase.createDocument(collection, attendenceSummary)
      .then((doc) => {
        alertValue("Submitted successfully");
        NavigationService.navigate("AttendenceScreen");
      })
      .catch(() => {
        alertValue(ERROR_MESSAGE);
        !this.isUnmounted &&
          this.setState({
            creatingAttendence: false,
          });
      });
  };

  onSubmit = () => {
    !this.isUnmounted &&
      this.setState({
        creatingAttendence: true,
      });
    if (this.state.attendenceDocument) {
      this.updateAttendence();
    } else {
      this.createAttendence();
    }
  };

  toggleTheStatus = (studentUid) => {
    !this.isUnmounted &&
      this.setState((previousState) => {
        if (previousState.studentPresence[studentUid]) {
          previousState.studentPresence[studentUid] = !previousState
            .studentPresence[studentUid];
        } else {
          previousState.studentPresence[studentUid] = true;
        }
        return previousState;
      });
  };

  getHeader = () => {
    let classInfo = this.state.classInfo;
    return (
      <View style={{ height: 130 }}>
        <View style={styles.header}>
          <View style={styles.headerItem}>
            <View style={styles.headerComponent}>
              <Text style={styles.contentLables}>Class :</Text>
              <Text
                style={{
                  fontWeight: "bold",
                  maxWidth: this.props.width * 0.3,
                }}
                ellipsizeMode="tail"
                numberOfLines={1}
                onPress={alertValue.bind(this, classInfo.name)}
              >
                {classInfo.name}
              </Text>
            </View>
            <View style={styles.headerComponent}>
              <Text style={styles.contentLables}>Subject :</Text>
              <Text
                style={{
                  fontWeight: "bold",
                  maxWidth: this.props.width * 0.3,
                }}
                ellipsizeMode="tail"
                numberOfLines={1}
                onPress={alertValue.bind(
                  this,
                  classInfo.listOfSubjects[this.subjectId].subjectName
                )}
              >
                {classInfo.listOfSubjects[this.subjectId].subjectName}
              </Text>
            </View>
          </View>
          <View style={styles.headerItem}>
            <View style={styles.headerComponent}>
              <Text style={styles.contentLables}>Date :</Text>
              <Text>{this.date.toDateString(DATE_FORMAT)}</Text>
            </View>
            <View style={styles.headerComponent}>
              <Text style={styles.contentLables}>From :</Text>
              <Text>{this.scheduleFrom}</Text>
              <TabBarIcon
                name="clock"
                type="EvilIcons"
                size={15}
                color="#AFAAAA"
              />
            </View>
            <View style={styles.headerComponent}>
              <Text style={styles.contentLables}>To :</Text>
              <Text>{this.scheduleTo}</Text>
              <TabBarIcon
                name="clock"
                type="EvilIcons"
                size={15}
                color="#AFAAAA"
              />
            </View>
          </View>
          <View style={{ justifyContent: "center" }}>
            <Text>
              Schedule no :{" "}
              {classInfo.listOfSubjects[this.subjectId].noOfClasses + 1}
            </Text>
          </View>
        </View>
        <View style={styles.selectAll}>
          <View>
            <CustomizeCheckBox
              selected={this.state.selectAll}
              onUpdate={this.selectAllStudents}
              iconSize={25}
              color={this.state.selectAll ? "#7bb956" : ""}
            />
          </View>
          <Text style={styles.selectAllText}>Select ALL</Text>
        </View>
      </View>
    );
  };

  selectAllStudents = () => {
    let newState = { ...this.state };
    newState.classInfo.listOfStudents.forEach((s) => {
      newState.studentPresence[s.studentUid] = !newState.selectAll;
    });
    newState.selectAll = !newState.selectAll;
    !this.isUnmounted && this.setState({ ...newState });
  };

  getStudentList = () => {
    if (this.state.classInfo) {
      return (
        <FlatList
          data={this.state.classInfo.listOfStudents}
          renderItem={(student) => (
            <StudentItem
              {...student.item}
              present={this.state.studentPresence[student.item.studentUid]}
              toggleTheStatus={this.toggleTheStatus}
            />
          )}
          ItemSeparatorComponent={() => (
            <View style={{ height: 2, backgroundColor: "#FFFFFF" }} />
          )}
          keyExtractor={(item, index) => item.studentUid}
          ListHeaderComponent={this.getHeader}
          ListEmptyComponent={
            <EmptyComponent
              style={styles.emptyComponentStyle}
              message={"Students list is empty"}
            />
          }
          showsVerticalScrollIndicator={false}
        />
      );
    } else {
      return (
        <EmptyComponent
          style={styles.emptyComponentStyle}
          message={"Something went wrong"}
        />
      );
    }
  };

  render() {
    if (this.state.notAllowed) {
      return (
        <EmptyComponent
          style={styles.emptyComponentStyle}
          message={"You don't have access"}
        />
      );
    }
    if (this.state.refreshing) {
      return <LoadingPage style={styles.emptyComponentStyle} />;
    } else {
      return (
        <View style={styles.container}>
          <View style={styles.container}>{this.getStudentList()}</View>
          {this.state.creatingAttendence ? (
            <View
              style={{
                position: "absolute",
                width: this.props.width,
                height: this.props.height,
                backgroundColor: "rgba(52, 52, 52, 0.2)",
              }}
            >
              <LoadingPage style={styles.emptyComponentStyle} />
            </View>
          ) : null}
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
    padding: 1.5,
  },
  headerLabel: {
    backgroundColor: "#20368F",
    margin: 5,
    padding: 10,
    borderRadius: 5,
    fontSize: 15,
  },
  header: {
    justifyContent: "space-between",
    height: 90,
    padding: 6,
  },
  headerItem: {
    justifyContent: "space-between",
    flexDirection: "row",
  },
  contentLables: {
    color: "#AFAAAA",
    fontSize: 15,
  },
  headerComponent: {
    flexDirection: "row",
    alignItems: "center",
  },
  emptyComponentStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  selectAll: {
    flexDirection: "row",
    height: 40,
    backgroundColor: "#F4F2F1",
    alignItems: "center",
    padding: 6,
  },
  selectAllText: {
    fontWeight: "bold",
    paddingHorizontal: 6,
  },
});

const mapStateToProps = (state) => {
  return {
    connectionStatus: state.commonInfo.connectionStatus,
    user: state.commonInfo.user,
    width: state.commonInfo.width,
    height: state.commonInfo.height,
  };
};

export default connect(mapStateToProps, { logout })(CreateAttendenceScreen);
