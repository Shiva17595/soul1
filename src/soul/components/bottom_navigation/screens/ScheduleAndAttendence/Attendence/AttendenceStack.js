import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import AttendenceScreen from "./AttendenceScreen";
import CreateAttendenceScreen from "./CreateAttendenceScreen";

export default AttendenceStack = createAppContainer(
  createStackNavigator(
    {
      AttendenceScreen: {
        screen: AttendenceScreen,
        navigationOptions: ({ navigation }) => {
          header = null;
          return { header };
        }
      },
      CreateAttendence: CreateAttendenceScreen
    },
    {
      initialRouteName: "AttendenceScreen"
    }
  )
);
