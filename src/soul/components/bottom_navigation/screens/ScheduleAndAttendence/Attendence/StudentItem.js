import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import {
  CustomizeCheckBox,
  alertValue
} from "../../../../../../../lib/ReUsableComponents";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";

class StudentItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      present: props.present ? props.present : false
    };
  }

  updateStatus = () => {
    this.props.toggleTheStatus(this.props.studentUid);
    this.setState({
      present: !this.state.present
    });
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.present != this.state.present) return true;
    if (nextProps.present != this.props.present) return true;
    return false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.present != this.props.present)
      this.setState({
        present: this.props.present ? this.props.present : false
      });
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={this.updateStatus}
        activeOpacity={1}
      >
        <View>
          <CustomizeCheckBox
            selected={this.state.present}
            onUpdate={this.updateStatus}
            iconSize={25}
            color={this.state.present ? "#7bb956" : ""}
          />
        </View>
        <View style={{ paddingHorizontal: 5 }}>
          <CustomAvatar rounded source={{ uri: this.props.studentAvatar }} />
        </View>
        <View>
          <View style={styles.textValue}>
            <Text style={styles.contentLables}>Name{"   "}:</Text>
            <Text
              style={{
                fontSize: 15,
                maxWidth: this.props.width * 0.6
              }}
              ellipsizeMode="tail"
              numberOfLines={1}
              onPress={alertValue.bind(this, this.props.studentName)}
            >
              {this.props.studentName}
            </Text>
          </View>
          <View style={styles.textValue}>
            <Text style={styles.contentLables}>Roll No :</Text>
            <Text
              style={{
                fontSize: 15
              }}
            >
              {this.props.studentRollNo}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 6,
    backgroundColor: "#F4F2F1",
    height: 60,
    borderRadius: 3
  },
  contentLables: {
    color: "#AFAAAA",
    fontSize: 15
  },
  textValue: {
    flexDirection: "row",
    alignItems: "center"
  }
});

const mapStateToProps = state => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height
});

export default connect(mapStateToProps, null)(StudentItem);
