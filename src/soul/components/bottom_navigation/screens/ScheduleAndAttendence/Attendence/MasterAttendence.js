import React, { PureComponent } from "react";
import {
  StyleSheet,
  Text,
  View,
  DatePickerAndroid,
  TouchableOpacity,
  Platform,
  Alert,
} from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import { DATE_FORMAT, SEPERATOR } from "../../../../../constants/Variables";
import ModalView from "../../../../../../../lib/ModalView";
import { DatePicker } from "../../../../../../../lib/ReUsableComponents";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import NavigationService from "../../../../../actions/NavigationService";
import { nDaysBackDate } from "../../../../../assets/reusableFunctions";
import { Button } from "react-native-elements";

class MasterAttendence extends PureComponent {
  constructor(props) {
    super(props);
    let date = new Date();

    this.state = {
      date: date,
      modalVisible: false,
      classes: this.getClasses(date.getDay()),
      subjects: [],
      classId: "",
      subjectId: "",
      schedules: [],
      schedule: "",
    };
  }

  openDatePicker = async () => {
    if (Platform.OS === "ios") {
      this.setState({
        modalVisible: true,
      });
    } else {
      try {
        const { action, year, month, day } = await DatePickerAndroid.open({
          date: this.state.date,
          maxDate: new Date(),
          minDate: nDaysBackDate(7),
        });
        if (action !== DatePickerAndroid.dismissedAction) {
          this.setCurrentSelectedDate(new Date(year, month, day));
        }
      } catch ({ code, message }) {
        console.log("Cannot open date picker", message);
      }
    }
  };

  attendenceHeader = () => {
    return (
      <View style={{ padding: 8 }}>
        <View style={styles.attendenceHeaderElement}>
          <View style={styles.attendenceHeaderLeft}>
            <Text style={styles.contentTitle}>Date: </Text>
            <Text style={{ fontWeight: "bold" }}>
              {this.state.date.toDateString(DATE_FORMAT)}
            </Text>
          </View>
        </View>
        <Button
          buttonStyle={{
            backgroundColor: "#20368F",
            height: 30,
          }}
          containerStyle={{
            alignItems: "flex-end",
            paddingHorizontal: 5,
          }}
          titleStyle={{
            fontSize: 10,
            color: "#FFFFFF",
          }}
          icon={<TabBarIcon name="clock" type="EvilIcons" size={20} />}
          iconContainerStyle={{
            padding: 5,
          }}
          title={"Select Date"}
          onPress={this.openDatePicker}
        />
      </View>
    );
  };

  setCurrentSelectedDate = (date) => {
    this.setState({
      date: date,
      classes: this.getClasses(date.getDay()),
      classId: "",
      subjects: [],
      subjectId: "",
      schedules: [],
      schedule: "",
    });
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  getClasses = (day) => {
    let classArray = [];
    let classes = [];
    this.props.user.schedule[day].forEach((s) => {
      if (!classArray.includes(s.classId)) {
        classArray.push(s.classId);
        classes.push({ label: s.className, value: s.classId });
      }
    });
    return classes;
  };

  getSubject = (classId) => {
    let subjectIds = [];
    let subjects = [];
    this.props.user.schedule[this.state.date.getDay()].forEach((s) => {
      if (s.classId == classId) {
        if (!subjectIds.includes(s.subjectId)) {
          subjectIds.push(s.subjectId);
          subjects.push({ label: s.subjectName, value: s.subjectId });
        }
      }
    });
    return subjects;
  };

  getSchedules = (subjectId) => {
    let schedules = [];
    this.props.user.schedule[this.state.date.getDay()].forEach((s) => {
      if (s.classId == this.state.classId && s.subjectId == subjectId) {
        schedules.push({
          label: `from: ${s.from} - to: ${s.to}`,
          value: `${s.from}${SEPERATOR}${s.to}`,
        });
      }
    });
    return schedules;
  };

  setClass = (classId) => {
    this.setState({
      classId: classId,
      subjects: this.getSubject(classId),
      subjectId: "",
      schedules: [],
      schedule: "",
    });
  };

  setSchedule = (schedule) => {
    this.setState({ schedule: schedule });
  };

  setSubject = (subjectId) => {
    this.setState({
      subjectId: subjectId,
      schedules: this.getSchedules(subjectId),
      schedule: "",
    });
  };

  markAttendence = () => {
    if (this.state.classId && this.state.subjectId && this.state.schedule) {
      NavigationService.navigate("CreateAttendence", {
        classId: this.state.classId,
        subjectId: this.state.subjectId,
        scheduleFrom: this.state.schedule.split(SEPERATOR)[0],
        scheduleTo: this.state.schedule.split(SEPERATOR)[1],
        date: this.state.date,
      });
    } else {
      Alert.alert("Please select all fields");
    }
  };

  render() {
    return (
      <View style={styles.container}>
        {this.attendenceHeader()}
        <Card
          containerStyle={{
            flex: 0.8,
            borderRadius: 5,
            justifyContent: "space-between",
          }}
          title="Mark Attendence"
        >
          <View>
            <Dropdown
              label={this.state.classes.length > 0 ? "Class" : "No Classes"}
              data={this.state.classes}
              value={this.state.classId}
              onChangeText={this.setClass}
            />
            <Dropdown
              label={this.state.classId ? "Subject" : "No Subjects"}
              data={this.state.subjects}
              value={this.state.subjectId}
              onChangeText={this.setSubject}
            />
            <Dropdown
              label={this.state.subjectId ? "Time" : "No Schedules"}
              data={this.state.schedules}
              value={this.state.schedule}
              onChangeText={this.setSchedule}
            />
            <View style={{ paddingVertical: 20 }}>
              <TouchableOpacity
                style={styles.bottomSheetButton}
                onPress={this.markAttendence}
                activeOpacity={0.5}
              >
                <View>
                  <Text style={{ color: "#FFFFFF" }}>Mark Attendence</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Card>
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={styles.modalStyle}
        >
          <DatePicker
            date={this.state.date}
            mode="date"
            onDateChange={this.setCurrentSelectedDate}
            style={StyleSheet.flatten([
              { heigth: this.props.height / 2 },
              styles.bottomSheetPanelContainer,
            ])}
            maximumDate={new Date()}
            minimumDate={nDaysBackDate(7)}
            header={
              <View style={{ alignItems: "center", paddingBottom: 10 }}>
                <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                  Select Date :
                </Text>
              </View>
            }
          />
        </ModalView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
  attendenceHeaderElement: {
    alignSelf: "flex-end",
  },
  attendenceHeaderLeft: {
    flexDirection: "row",
    alignItems: "center",
  },
  contentTitle: {
    color: "#AFAAAA",
    fontSize: 15,
  },
  dateText: {
    color: "#FFFFFF",
    fontSize: 8,
    paddingTop: 3,
    textAlignVertical: "center",
  },
  bottomSheetButton: {
    flexDirection: "row",
    padding: 5,
    justifyContent: "center",
    backgroundColor: "#20368F",
    borderWidth: 1,
    borderColor: "#919090",
    alignItems: "flex-end",
  },
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
  bottomSheetPanelContainer: {
    margin: 0,
    height: "50%",
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexDirection: "column",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => {
  return {
    connectionStatus: state.commonInfo.connectionStatus,
    user: state.commonInfo.user,
    width: state.commonInfo.width,
    height: state.commonInfo.height,
  };
};

export default connect(mapStateToProps, null)(MasterAttendence);
