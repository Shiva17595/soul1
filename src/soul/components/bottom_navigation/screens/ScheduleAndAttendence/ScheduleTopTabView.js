import React from "react";
import { createMaterialTopTabNavigator } from "react-navigation";
import ScheduleScreen from "./Schedule/ScheduleScreen";
import AttendenceStack from "./Attendence/AttendenceStack";
import TabBarLable from "../TabBarLable";

export default createMaterialTopTabNavigator(
  {
    Schedule: ScheduleScreen,
    Attendence: {
      screen: AttendenceStack,
      navigationOptions: ({ navigation }) => {
        swipeEnabled = false;
        const { routeName } = navigation.state.routes[navigation.state.index];
        if (routeName === "CreateAttendence") {
          tabBarVisible = false;
          tabBarLabel = null;
        } else {
          tabBarVisible = true;
          tabBarLabel = (
            <TabBarLable
              iconName="calendar"
              iconType="AntDesign"
              size={15}
              lableText="Attendence"
              focused={navigation.isFocused()}
            />
          );
        }
        return { tabBarLabel, tabBarVisible, swipeEnabled };
      }
    }
  },
  {
    swipeEnabled: false,
    initialRouteName: "Schedule",
    activeTintColor: "#FFFFFF",
    inactiveTintColor: "#000000",
    showIcon: false,
    tabBarOptions: {
      style: {
        backgroundColor: "#20368F"
      },
      indicatorStyle: {
        height: 2,
        backgroundColor: "#F6F5F5"
      }
    }
  }
);
