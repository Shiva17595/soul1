import React, { PureComponent } from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  View,
  Platform,
  Text,
  DatePickerAndroid,
  FlatList,
} from "react-native";
import {
  LoadingPage,
  DatePicker,
  alertValue,
  EmptyComponent,
} from "../../../../../../../lib/ReUsableComponents";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import ModalView from "../../../../../../../lib/ModalView";
import { DATE_FORMAT } from "../../../../../constants/Variables";
import MasterScheduleComponent from "./MasterScheduleComponent";
import { Button } from "react-native-elements";

class MasterSchedule extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      modalVisible: false,
      refreshing: false,
    };
  }

  setCurrentSelectedDate = (date) => {
    this.setState({
      date: date,
    });
  };

  openDatePicker = async () => {
    if (Platform.OS === "ios") {
      this.setState({
        modalVisible: true,
      });
    } else {
      try {
        const { action, year, month, day } = await DatePickerAndroid.open({
          date: this.state.date,
        });
        if (action !== DatePickerAndroid.dismissedAction) {
          this.setState({
            date: new Date(year, month, day),
          });
        }
      } catch ({ code, message }) {
        console.log("Cannot open date picker", message);
      }
    }
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  scheduleHeaderComponent = () => {
    return (
      <View style={{ padding: 8 }}>
        <View style={styles.scheduleHeaderElement}>
          <View style={styles.scheduleHeaderLeft}>
            <Text style={styles.contentTitle}>Name: </Text>
            <Text
              style={{ fontWeight: "bold" }}
              ellipsizeMode="tail"
              numberOfLines={1}
              style={{
                maxWidth: isNaN(this.props.width)
                  ? "50%"
                  : this.props.width * 0.5,
              }}
              onPress={alertValue.bind(this, this.props.user.name)}
            >
              {this.props.user.name}
            </Text>
          </View>
          <View style={styles.scheduleHeaderLeft}>
            <Text style={styles.contentTitle}>Date: </Text>
            <Text style={{ fontWeight: "bold" }}>
              {this.state.date.toDateString(DATE_FORMAT)}
            </Text>
          </View>
        </View>
        <Button
          buttonStyle={{
            backgroundColor: "#20368F",
            height: 30,
          }}
          containerStyle={{
            alignItems: "flex-end",
            paddingHorizontal: 5,
          }}
          titleStyle={{
            fontSize: 10,
            color: "#FFFFFF",
          }}
          icon={<TabBarIcon name="clock" type="EvilIcons" size={20} />}
          iconContainerStyle={{
            padding: 5,
          }}
          title={"Select Date"}
          onPress={this.openDatePicker}
        />
      </View>
    );
  };

  renderUserSchedule = () => {
    if (this.props.user && this.props.user.schedule) {
      let todaySchedule = this.props.user.schedule[this.state.date.getDay()];
      return (
        <FlatList
          data={todaySchedule}
          renderItem={(schedule) => (
            <MasterScheduleComponent
              schedule={schedule.item}
              date={this.state.date}
            />
          )}
          keyExtractor={(item, index) => `${this.props.user.uid}-${index}`}
          ListHeaderComponent={this.scheduleHeaderComponent}
          refreshing={this.state.refreshing}
          onRefresh={this.getUserSchedule}
          ListEmptyComponent={
            <EmptyComponent
              style={styles.emptyComponentStyle}
              message="No Schedules"
            />
          }
          showsVerticalScrollIndicator={false}
        />
      );
    } else {
      return (
        <LoadingPage
          size="large"
          style={{
            alignItems: "center",
            justifyContent: "center",
          }}
        />
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderUserSchedule()}
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={styles.modalStyle}
        >
          <DatePicker
            date={this.state.date}
            mode="date"
            onDateChange={this.setCurrentSelectedDate}
            style={StyleSheet.flatten([
              { heigth: this.props.height / 2 },
              styles.bottomSheetPanelContainer,
            ])}
            header={
              <View style={{ alignItems: "center", paddingBottom: 10 }}>
                <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                  Select Date :
                </Text>
              </View>
            }
          />
        </ModalView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
  bottomSheetButton: {
    flexDirection: "row",
    padding: 5,
    justifyContent: "center",
    backgroundColor: "#20368F",
    borderWidth: 1,
    borderColor: "#919090",
  },
  dateText: {
    color: "#FFFFFF",
    fontSize: 8,
    paddingTop: 3,
    textAlignVertical: "center",
  },
  bottomSheetPanelContainer: {
    margin: 0,
    height: "50%",
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexDirection: "column",
    justifyContent: "center",
  },
  scheduleHeaderElement: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 8,
  },
  scheduleHeaderLeft: {
    flexDirection: "row",
    alignItems: "center",
  },
  contentTitle: {
    color: "#AFAAAA",
    fontSize: 15,
  },
  emptyComponentStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => {
  return {
    connectionStatus: state.commonInfo.connectionStatus,
    user: state.commonInfo.user,
    width: state.commonInfo.width,
    height: state.commonInfo.height,
  };
};

export default connect(mapStateToProps, null)(MasterSchedule);
