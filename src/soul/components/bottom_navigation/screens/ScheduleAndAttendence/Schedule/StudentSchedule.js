import React, { PureComponent } from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  View,
  Platform,
  Text,
  DatePickerAndroid,
  FlatList,
} from "react-native";
import FireBase from "../../../../../assets/FireBase";
import {
  LoadingPage,
  DatePicker,
  EmptyComponent,
  alertValue,
} from "../../../../../../../lib/ReUsableComponents";
import { setUserClassInfo } from "../../../../../actions/userDataActions";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import ModalView from "../../../../../../../lib/ModalView";
import StudentScheduleComponent from "./StudentScheduleComponent";
import { DATE_FORMAT, ERROR_MESSAGE } from "../../../../../constants/Variables";
import { getDate } from "../../../../../assets/reusableFunctions";
import CollectionPaths from "../../../../../services/FireBaseCollectionService";
import { Button } from "react-native-elements";

class StudentSchedule extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      modalVisible: false,
      refreshing: false,
      loading: true,
    };
  }

  getUserSchedule = () => {
    let collectionName = CollectionPaths.classes();
    FireBase.getDocumentInfo(collectionName, this.props.user.classInfo.classId)
      .then((classInfo) => {
        this.props.setUserClassInfo(classInfo);
        this.setState({
          loading: false,
          refreshing: false,
        });
      })
      .catch((error) => {
        alertValue(ERROR_MESSAGE);
        this.setState({
          loading: false,
          refreshing: false,
        });
      });
  };

  componentDidMount() {
    if (this.props.connectionStatus) this.getUserSchedule();
  }

  setCurrentSelectedDate = (date) => {
    this.setState({
      date: date,
    });
  };

  openDatePicker = async () => {
    if (Platform.OS === "ios") {
      this.setState({
        modalVisible: true,
      });
    } else {
      try {
        const { action, year, month, day } = await DatePickerAndroid.open({
          date: this.state.date,
          maxDate: getDate(this.props.classInfo.endDate),
          minDate: getDate(this.props.classInfo.startDate),
        });
        if (action !== DatePickerAndroid.dismissedAction) {
          this.setState({
            date: new Date(year, month, day),
          });
        }
      } catch ({ code, message }) {
        console.log("Cannot open date picker", message);
      }
    }
  };

  scheduleHeaderComponent = () => {
    return (
      <View style={{ padding: 8 }}>
        <View style={styles.scheduleHeaderElement}>
          <View style={styles.scheduleHeaderLeft}>
            <Text style={styles.contentTitle}>Class: </Text>
            <Text
              style={{ fontWeight: "bold", maxWidth: this.props.width * 0.3 }}
              ellipsizeMode="tail"
              numberOfLines={1}
              onPress={alertValue.bind(this, this.props.classInfo.name)}
            >
              {this.props.classInfo.name}
            </Text>
          </View>
          <View style={styles.scheduleHeaderLeft}>
            <Text style={styles.contentTitle}>Date: </Text>
            <Text style={{ fontWeight: "bold" }}>
              {this.state.date.toDateString(DATE_FORMAT)}
            </Text>
          </View>
        </View>
        <Button
          buttonStyle={{
            backgroundColor: "#20368F",
            height: 30,
          }}
          containerStyle={{
            alignItems: "flex-end",
            paddingHorizontal: 5,
          }}
          titleStyle={{
            fontSize: 10,
            color: "#FFFFFF",
          }}
          icon={<TabBarIcon name="clock" type="EvilIcons" size={20} />}
          iconContainerStyle={{
            padding: 5,
          }}
          title={"Select Date"}
          onPress={this.openDatePicker}
        />
      </View>
    );
  };

  renderUserSchedule = () => {
    if (this.props.classInfo) {
      let todaySchedule = this.props.classInfo.schedule[
        this.state.date.getDay()
      ];
      return (
        <FlatList
          data={todaySchedule}
          renderItem={(schedule) => (
            <StudentScheduleComponent schedule={schedule.item} />
          )}
          keyExtractor={(item, index) =>
            `${this.props.classInfo.uniqueId}-${index}`
          }
          ListHeaderComponent={this.scheduleHeaderComponent}
          refreshing={this.state.refreshing}
          onRefresh={this.getUserSchedule}
          ListEmptyComponent={
            <EmptyComponent
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
              }}
              message={"No Schedules"}
            />
          }
          showsVerticalScrollIndicator={false}
        />
      );
    }
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };

  render() {
    if (this.state.loading) {
      return (
        <LoadingPage
          size="large"
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}
        />
      );
    }
    return (
      <View style={styles.container}>
        {this.renderUserSchedule()}
        <ModalView
          isVisible={this.state.modalVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          style={styles.modalStyle}
        >
          <DatePicker
            date={this.state.date}
            mode="date"
            onDateChange={this.setCurrentSelectedDate}
            style={styles.bottomSheetPanelContainer(this.props.height / 2)}
            maximumDate={getDate(this.props.classInfo.endDate)}
            minimumDate={getDate(this.props.classInfo.startDate)}
            header={
              <View style={{ alignItems: "center", paddingBottom: 10 }}>
                <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                  Select Date :
                </Text>
              </View>
            }
          />
        </ModalView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  modalStyle: {
    justifyContent: "flex-end",
    margin: 0,
  },
  bottomSheetButton: {
    flexDirection: "row",
    padding: 5,
    justifyContent: "center",
    backgroundColor: "#20368F",
    borderWidth: 1,
    borderColor: "#919090",
  },
  dateText: {
    color: "#FFFFFF",
    fontSize: 8,
    paddingTop: 3,
    textAlignVertical: "center",
  },
  bottomSheetPanelContainer: (height) => ({
    margin: 0,
    height: "50%",
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexDirection: "column",
    justifyContent: "center",
    height: height,
  }),
  scheduleHeaderElement: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 8,
  },
  scheduleHeaderLeft: {
    flexDirection: "row",
    alignItems: "center",
  },
  contentTitle: {
    color: "#AFAAAA",
    fontSize: 15,
  },
});

const mapStateToProps = (state) => {
  return {
    connectionStatus: state.commonInfo.connectionStatus,
    user: state.commonInfo.user,
    classInfo: state.userData.classInfo,
    width: state.commonInfo.width,
    height: state.commonInfo.height,
  };
};

export default connect(mapStateToProps, { setUserClassInfo })(StudentSchedule);
