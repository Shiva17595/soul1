import React, { PureComponent } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import NavigationService from "../../../../../actions/NavigationService";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import {
  getDateWithOutTime,
  nDaysBackDate
} from "../../../../../assets/reusableFunctions";

class MasterScheduleComponent extends PureComponent {
  markAttendence = () => {
    NavigationService.navigate("CreateAttendence", {
      classId: this.props.schedule.classId,
      subjectId: this.props.schedule.subjectId,
      scheduleFrom: this.props.schedule.from,
      scheduleTo: this.props.schedule.to,
      date: this.props.date
    });
  };

  showCreateAttendence = () => {
    return (
      getDateWithOutTime(this.props.date) <= getDateWithOutTime(new Date()) &&
      getDateWithOutTime(this.props.date) >=
        getDateWithOutTime(nDaysBackDate(7))
    );
  };
  render() {
    return (
      <Card containerStyle={{ borderRadius: 5 }}>
        <View style={styles.schedule}>
          <View style={styles.scheduleRowHead}>
            <View>
              <Text
                style={styles.scheduleHeadTitle}
                ellipsizeMode="tail"
                numberOfLines={1}
                onPress={alertValue.bind(this, this.props.schedule.className)}
              >
                {this.props.schedule.className}
              </Text>
            </View>
          </View>
          <View style={styles.scheduleMiddleRow}>
            <View style={styles.scheduleRowElement}>
              <Text style={styles.contentTitle}>From: </Text>
              <Text>{this.props.schedule.from}</Text>
              <TabBarIcon
                name="clock"
                type="EvilIcons"
                size={15}
                color="#AFAAAA"
              />
            </View>
            <View style={styles.scheduleRowElement}>
              <Text style={styles.contentTitle}>To: </Text>
              <Text>{this.props.schedule.to}</Text>
              <TabBarIcon
                name="clock"
                type="EvilIcons"
                size={15}
                color="#AFAAAA"
              />
            </View>
          </View>
          <View style={styles.scheduleBottom}>
            <View style={styles.scheduleRowElement}>
              <Text style={styles.contentTitle}>Subject: </Text>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  maxWidth: isNaN(this.props.width)
                    ? "30%"
                    : this.props.width * 0.3
                }}
                onPress={alertValue.bind(this, this.props.schedule.subjectName)}
              >
                {this.props.schedule.subjectName}
              </Text>
            </View>
            <View style={styles.scheduleRowElement}>
              <Text style={styles.contentTitle}>Room: </Text>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  maxWidth: isNaN(this.props.width)
                    ? "20%"
                    : this.props.width * 0.2
                }}
                onPress={alertValue.bind(this, this.props.schedule.roomName)}
              >
                {this.props.schedule.roomName}
              </Text>
            </View>
          </View>
          {this.showCreateAttendence() ? (
            <View style={{ alignItems: "flex-end" }}>
              <TouchableOpacity onPress={this.markAttendence}>
                <View style={styles.attendenceButton}>
                  <TabBarIcon name="list-alt" type="FontAwesome" size={15} />
                  <Text style={styles.attendenceText}> Attendence </Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    width: state.commonInfo.width,
    height: state.commonInfo.height
  };
};

export default connect(mapStateToProps, null)(MasterScheduleComponent);

const styles = StyleSheet.create({
  schedule: {
    height: 100,
    justifyContent: "space-between"
  },
  scheduleRowHead: {
    flexDirection: "row",
    justifyContent: "center"
  },
  scheduleHeadTitle: {
    fontWeight: "bold"
  },
  scheduleMiddleRow: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  scheduleBottom: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  scheduleRowElement: {
    flexDirection: "row",
    alignItems: "center"
  },
  contentTitle: {
    color: "#AFAAAA",
    fontSize: 15
  },
  attendenceButton: {
    flexDirection: "row",
    padding: 5,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#20368F",
    borderWidth: 1,
    borderColor: "#919090"
  },
  attendenceText: {
    color: "#FFFFFF",
    fontSize: 10,
    paddingTop: 3,
    textAlignVertical: "center"
  }
});
