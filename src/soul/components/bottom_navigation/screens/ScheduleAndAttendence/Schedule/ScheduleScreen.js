import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { StyleSheet, View } from "react-native";
import TabBarLable from "../../TabBarLable";
import StudentSchedule from "./StudentSchedule";
import MasterSchedule from "./MasterSchedule";
import { getUserClassInfo } from "../../../../../actions/userDataActions";
import User from "../../../../../services/UserService";
import { NavigationActions, StackActions } from "react-navigation";
import { logout } from "../../../../../actions/authenticateUser";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";

class ScheduleScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    tabBarLabel: ({ focused }) => {
      return (
        <TabBarLable
          iconName="calendar"
          iconType="AntDesign"
          size={15}
          lableText="Schedule"
          focused={focused}
        />
      );
    },
  });

  componentDidMount() {
    if (!this.props.connectionStatus) {
      if (this.props.user.isStudent) {
        this.props.getUserClassInfo();
      }
    }
  }

  getScheduleScreen = () => {
    if (this.props.user.isStudent) {
      return <StudentSchedule />;
    }
    if (this.props.user.isMaster) {
      return <MasterSchedule />;
    }
    User.logout();
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Login" })],
    });
    this.props.navigation.dispatch(resetAction);
    this.props.logout();
  };

  render() {
    return <View style={styles.container}>{this.getScheduleScreen()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
});

const mapStateToProps = (state) => {
  return {
    connectionStatus: state.commonInfo.connectionStatus,
    user: state.commonInfo.user,
  };
};

export default connect(mapStateToProps, { getUserClassInfo, logout })(
  ScheduleScreen
);
