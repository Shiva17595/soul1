import React, { PureComponent } from "react";
import { StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";
import { Card } from "react-native-elements";
import TabBarIcon from "../../../../../assets/TabBarIcon";
import { alertValue } from "../../../../../../../lib/ReUsableComponents";
import CustomAvatar from "../../../../../../../lib/CustomAvatar";

class StudentScheduleComponent extends PureComponent {
  render() {
    return (
      <Card containerStyle={{ borderRadius: 5 }}>
        <View style={styles.schedule}>
          <View style={styles.scheduleRowHead}>
            <View>
              <Text
                style={styles.scheduleHeadTitle}
                ellipsizeMode="tail"
                numberOfLines={1}
                onPress={alertValue.bind(this, this.props.schedule.subjectName)}
              >
                {this.props.schedule.subjectName}
              </Text>
            </View>
          </View>
          <View style={styles.scheduleMiddleRow}>
            <View style={styles.scheduleRowElement}>
              <Text style={styles.contentTitle}>From: </Text>
              <Text>{this.props.schedule.from}</Text>
              <TabBarIcon
                name="clock"
                type="EvilIcons"
                size={15}
                color="#AFAAAA"
              />
            </View>
            <View style={styles.scheduleRowElement}>
              <Text style={styles.contentTitle}>To: </Text>
              <Text>{this.props.schedule.to}</Text>
              <TabBarIcon
                name="clock"
                type="EvilIcons"
                size={15}
                color="#AFAAAA"
              />
            </View>
          </View>
          <View style={styles.scheduleBottom}>
            <View style={styles.scheduleRowElement}>
              <View style={{ paddingHorizontal: 4 }}>
                <CustomAvatar
                  rounded
                  source={{
                    uri: this.props.schedule.masterProfilePicture
                  }}
                  size={23}
                  containerStyle={{
                    borderWidth: 0.3,
                    borderColor: "#AFAAAA"
                  }}
                />
              </View>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  maxWidth: isNaN(this.props.width)
                    ? "30%"
                    : this.props.width * 0.3
                }}
                onPress={alertValue.bind(this, this.props.schedule.masterName)}
              >
                {this.props.schedule.masterName}
              </Text>
            </View>
            <View style={styles.scheduleRowElement}>
              <Text style={styles.contentTitle}>Room: </Text>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  maxWidth: isNaN(this.props.width)
                    ? "20%"
                    : this.props.width * 0.2
                }}
                onPress={alertValue.bind(this, this.props.schedule.roomName)}
              >
                {this.props.schedule.roomName}
              </Text>
            </View>
          </View>
        </View>
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    width: state.commonInfo.width,
    height: state.commonInfo.height
  };
};

export default connect(mapStateToProps, null)(StudentScheduleComponent);

const styles = StyleSheet.create({
  schedule: {
    height: 80,
    justifyContent: "space-between"
  },
  scheduleRowHead: {
    flexDirection: "row",
    justifyContent: "center"
  },
  scheduleHeadTitle: {
    fontWeight: "bold"
  },
  scheduleMiddleRow: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  scheduleBottom: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  scheduleRowElement: {
    flexDirection: "row",
    alignItems: "center"
  },
  contentTitle: {
    color: "#AFAAAA",
    fontSize: 15
  }
});
