import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ActivityIndicator,
  StyleSheet,
  View,
  Alert,
  Image,
  TextInput,
  BackHandler,
  Keyboard,
} from "react-native";
import { userInfo } from "../../../actions/commonActions.js";
import { Card, Button } from "react-native-elements";
import User from "../../../services/UserService";
import TabBarIcon from "../../../assets/TabBarIcon";
import { EMAIL_REGEX } from "../../../constants/Variables";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { StackActions, NavigationActions } from "react-navigation";
import CurrentUser from "../../../services/CurrentUserDataService.js";
import { setVideoId } from "../../../actions/commonActions";

class LoginScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    let headerTitle = "Login Screen";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    let headerRight = (
      <View style={{ paddingHorizontal: 5 }}>
        <TabBarIcon name="md-information-circle" type="Ionicons" />
      </View>
    );
    let headerLeft = null;
    return {
      headerTitle,
      headerTitleStyle,
      headerRight,
      headerLeft,
      headerStyle,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      authenticating: true,
    };
  }

  checkLoginStatus = async () => {
    const user = await CurrentUser.get();
    if (!user) {
      User.loginStatus()
        .then(async (userData) => {
          await this.props.userInfo(userData);
          this.navigateToMainPage();
        })
        .catch((error) => {
          this.setState({ authenticating: false });
        });
    } else {
      await this.props.userInfo(user);
      this.navigateToMainPage();
    }
  };

  navigateToMainPage = () => {
    this.setState({ authenticating: false, password: "" }, () => {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "Main",
            params: { onTabFocus: this.props.setVideoId },
          }),
        ],
      });
      this.props.navigation.dispatch(resetAction);
    });
  };

  componentDidMount() {
    this.checkLoginStatus();
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  _keyboardDidShow(e) {
    console.log(e.endCoordinates.height);
  }

  _keyboardDidHide() {}

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton = () => {
    Alert.alert(
      "Exit App",
      "Do you want exit application?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => BackHandler.exitApp(),
        },
      ],
      {
        cancelable: false,
      }
    );
    return true;
  };

  validateParams(params) {
    let { email, password } = params;
    let meaasge = "";
    if (!email || !EMAIL_REGEX.test(email)) {
      meaasge = "Enter a Valid Email id";
    }
    if (password == "") {
      meaasge = "Invalid Credentials";
    }

    if (meaasge) {
      Alert.alert(meaasge);
      return false;
    } else {
      return true;
    }
  }

  onPressSignIn = () => {
    if (!this.state.authenticating) {
      this.setState({
        authenticating: true,
      });
      if (this.validateParams(this.state)) {
        User.signin(this.state)
          .then(async (userData) => {
            await this.props.userInfo(userData);
            this.navigateToMainPage();
          })
          .catch((error) => {
            Alert.alert("Invalid Credentials");
            this.setState({
              authenticating: false,
            });
          });
      } else {
        this.setState({
          authenticating: false,
        });
      }
    }
  };

  renderCurrentState = () => {
    if (this.state.authenticating) {
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      );
    } else {
      return (
        <View>
          <View style={styles.field}>
            <TabBarIcon
              focused={false}
              name="account-box"
              type="MaterialCommunityIcons"
            />
            <TextInput
              autoCorrect={false}
              onChangeText={(email) => this.setState({ email: email })}
              placeholder="Enter Email"
              style={styles.input}
              value={this.state.email}
            />
          </View>
          <View style={styles.field}>
            <TabBarIcon
              focused={false}
              name="security"
              type="MaterialCommunityIcons"
            />
            <TextInput
              autoCorrect={false}
              onChangeText={(password) => this.setState({ password: password })}
              placeholder="Enter Password"
              style={styles.input}
              secureTextEntry={true}
              value={this.state.password}
            />
          </View>
          <Button title="Login" onPress={this.onPressSignIn} />
        </View>
      );
    }
  };
  render() {
    return (
      <KeyboardAwareScrollView
        enableOnAndroid
        keyboardShouldPersistTaps="always"
        extraScrollHeight={50}
      >
        <View style={styles.container}>
          <Image
            style={{ width: 100, height: 100 }}
            source={require("./sample_testing_image.png")}
          />
          <Card title="Login">
            <View>{this.renderCurrentState()}</View>
          </Card>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    padding: 20,
    flex: 1,
  },
  input: {
    paddingHorizontal: 5,
    marginBottom: 10,
    color: "#333",
    fontSize: 15,
    fontWeight: "600",
    flex: 1,
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },
  field: {
    flexDirection: "row",
    paddingVertical: 5,
  },
});

export default connect(null, { userInfo, setVideoId })(LoginScreen);
