import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";
import { Button } from "react-native-elements";
import TabBarIcon from "../../../../assets/TabBarIcon";

class NotificationScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    let headerTitle = "Notification";
    let headerTitleStyle = { color: "white" };
    let headerStyle = { backgroundColor: "#20368F" };
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
    };
  };

  sendMessageNotification = () => {
    return new Promise(async (resolve, reject) => {
      const message = {
        to: "ExponentPushToken[5kc9geA4CpvSoFzouF4VhU]",
        sound: "default",
        title: "Original Title",
        body: "And here is the body!",
        data: { data: "goes here" },
        _displayInForeground: true,
      };
      const response = await fetch("https://exp.host/--/api/v2/push/send", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Accept-encoding": "gzip, deflate",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(message),
      });
      resolve(response);
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Button
          buttonStyle={{
            backgroundColor: "#20368F",
            height: 40,
          }}
          containerStyle={{
            paddingHorizontal: 5,
          }}
          titleStyle={{
            fontSize: 12,
            color: "#FFFFFF",
          }}
          icon={
            <TabBarIcon
              name={"event"}
              type={"MaterialIcons"}
              size={15}
              color="#FFFFFF"
            />
          }
          iconContainerStyle={{
            padding: 5,
          }}
          title={"Send"}
          onPress={this.sendMessageNotification}
        />
        <Text>Hey</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
  },
});

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user,
  width: state.commonInfo.width,
  height: state.commonInfo.height,
});

export default connect(mapStateToProps, null)(NotificationScreen);
