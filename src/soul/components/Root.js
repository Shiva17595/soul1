import React, { Component } from "react";
import { connect } from "react-redux";
import { NetInfo, Dimensions } from "react-native";
import {
  connectionInfo,
  dimensionsInfo,
  setCurrentView,
  setVideoId,
} from "../actions/commonActions.js";
import BottomNavigationBar from "./InitialNavigation";
import { ActionSheetProvider } from "@expo/react-native-action-sheet";
import { AppLoading } from "expo";
import NavigationService from "../actions/NavigationService.js";
import { getUrlMaps, getDownloadUrlMaps } from "../actions/storageActions.js";
import { switchToPortrait } from "../assets/reusableFunctions.js";
import { alertValue } from "../../../lib/ReUsableComponents.js";

const getCurrentRoute = (state) =>
  state.index !== undefined
    ? getCurrentRoute(state.routes[state.index])
    : state.routeName;

class AppRoot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appIsReady: false,
    };
  }

  _loadFontsAsync = async () => {};

  _handleLoadingError = (error) => {
    alertValue("Something went wrong, Please try again");
  };

  _handleFinishLoading = () => {
    this.setState({ appIsReady: true });
  };

  async componentDidMount() {
    await switchToPortrait();
    const { width, height } = await Dimensions.get("window");
    await this.props.getUrlMaps();
    await this.props.getDownloadUrlMaps();
    this.props.dimensionsInfo(parseInt(width), parseInt(height));
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this._handleConnectivityChange
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    this.props.connectionInfo(isConnected);
    if (!this.props.connectionStatus) {
      alertValue("Device is in Offline");
    }
  };

  setTopLevelNavigator = (navigatorRef) => {
    NavigationService.setTopLevelNavigator(navigatorRef);
  };

  shouldComponentUpdate() {
    return false;
  }

  handleNavigationStateChange = (prevState, newState) => {
    let viewName = getCurrentRoute(newState);
    if (this.props.viewName != viewName) {
      if (this.props.videoId) {
        console.log("setting to null");
        this.props.setVideoId(null);
      }
      this.props.setCurrentView(viewName);
    }
  };

  render() {
    if (!this.state.appIsReady) {
      <AppLoading
        startAsync={this._loadFontsAsync}
        onError={this._handleLoadingError}
        onFinish={this._handleFinishLoading}
      />;
    }
    return (
      <ActionSheetProvider>
        <BottomNavigationBar
          ref={this.setTopLevelNavigator}
          onNavigationStateChange={this.handleNavigationStateChange}
        />
      </ActionSheetProvider>
    );
  }
}

const mapStateToProps = (state) => ({
  connectionStatus: state.commonInfo.connectionStatus,
  viewName: state.commonInfo.viewName,
  videoId: state.commonInfo.videoId,
});

export default connect(mapStateToProps, {
  connectionInfo,
  dimensionsInfo,
  getUrlMaps,
  getDownloadUrlMaps,
  setCurrentView,
  setVideoId,
})(AppRoot);
