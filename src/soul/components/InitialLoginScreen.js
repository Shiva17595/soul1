import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ActivityIndicator,
  StyleSheet,
  View,
  Alert,
  Image,
  TextInput,
  BackHandler
} from "react-native";
import { userInfo } from "../actions/commonActions.js";
import { Card, Button } from "react-native-elements";
import User from "../services/UserService";
import TabBarIcon from "../assets/TabBarIcon";
import { EMAIL_REGEX } from "../constants/Variables";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class InitialLoginScreen extends Component {
  constructor(props) {
    super(props);
    this.renderCurrentState = this.renderCurrentState.bind(this);

    this.state = {
      email: "",
      password: "",
      authenticating: false
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton = () => {
    Alert.alert(
      "Exit App",
      "Do you want exit application?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => BackHandler.exitApp()
        }
      ],
      {
        cancelable: false
      }
    );
    return true;
  };

  validateParams(params) {
    let { email, password } = params;
    let meaasge = "";
    if (email == "") {
      meaasge = "Enter a Valid Email id";
    }
    if (password == "") {
      meaasge = "Invalid Credentials";
    }

    if (!EMAIL_REGEX.test(email)) {
      meaasge = "Enter a Valid Email id";
    }
    if (meaasge) {
      Alert.alert(meaasge);
      return false;
    } else {
      return true;
    }
  }

  onPressSignIn() {
    let self = this;
    self.setState({
      authenticating: true
    });
    if (this.validateParams(self.state)) {
      User.signin(self.state)
        .then(userData => {
          self.setState({
            password: "",
            authenticating: false
          });
          this.props.userInfo(userData);
          this.props.changeAuthenticationStatus(true);
        })
        .catch(error => {
          Alert.alert("Invalid Credentials");
          self.setState({
            authenticating: false
          });
        });
    } else {
      self.setState({
        authenticating: false
      });
    }
  }

  renderCurrentState() {
    if (this.state.authenticating) {
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      );
    } else {
      return (
        <View>
          <View style={styles.field}>
            <TabBarIcon
              focused={false}
              name="account-box"
              type="MaterialCommunityIcons"
            />
            <TextInput
              autoCorrect={false}
              onChangeText={email => this.setState({ email: email })}
              placeholder="Enter Email"
              style={styles.input}
              value={this.state.email}
            />
          </View>
          <View style={styles.field}>
            <TabBarIcon
              focused={false}
              name="security"
              type="MaterialCommunityIcons"
            />
            <TextInput
              autoCorrect={false}
              onChangeText={password => this.setState({ password: password })}
              placeholder="Enter Password"
              style={styles.input}
              secureTextEntry={true}
              value={this.state.password}
            />
          </View>
          <Button
            title="Login"
            onPress={() => {
              this.onPressSignIn();
            }}
          />
        </View>
      );
    }
  }
  render() {
    return (
      <KeyboardAwareScrollView
        enableOnAndroid
        keyboardShouldPersistTaps="always"
        extraScrollHeight={50}
      >
        <View style={styles.container}>
          <Image
            style={{ width: 100, height: 100 }}
            source={require("./bottom_navigation/screens/sample_testing_image.png")}
          />
          <Card title="Login">
            <View>{this.renderCurrentState()}</View>
          </Card>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    padding: 20,
    flex: 1
  },
  input: {
    paddingHorizontal: 5,
    marginBottom: 10,
    color: "#333",
    fontSize: 15,
    fontWeight: "600",
    flex: 1,
    borderBottomColor: "black",
    borderBottomWidth: 1
  },
  field: {
    flexDirection: "row",
    paddingVertical: 5
  }
});

export default connect(
  null,
  { userInfo }
)(InitialLoginScreen);
