import React from "react";
import { createAppContainer, createStackNavigator } from "react-navigation";

import MainTabNavigator from "./bottom_navigation/MainTabNavigator";
import LoginScreen from "./bottom_navigation/screens/LoginScreen";
import CreatePostNavigation from "./create_post_navigation/CreatePostNavigation";
import ViewScreen from "./bottom_navigation/screens/posts/view/ViewScreen";
import ViewPdfScreen from "./bottom_navigation/screens/posts/view/ViewPdfScreen";
import FilesViewScreen from "./bottom_navigation/screens/posts/view/FilesViewScreen";
import ProfileScreen from "./Profile/ProfileScreen";
import EditProfileScreen from "./Profile/EditProfileScreen";

const BottomTransition = (index, position, heigth) => {
  const sceneRange = [index - 1, index, index + 1];
  const outputHeight = [heigth, 0, 0];
  const transition = position.interpolate({
    inputRange: sceneRange,
    outputRange: outputHeight
  });
  return {
    transform: [{ translateY: transition }]
  };
};

const NavigationConfig = () => {
  return {
    screenInterpolator: sceneProps => {
      const position = sceneProps.position;
      const scene = sceneProps.scene;
      const index = scene.index;
      const heigth = sceneProps.layout.initHeight;

      return BottomTransition(index, position, heigth);
    }
  };
};

const ProfileStack = createStackNavigator(
  {
    UserProfile: ProfileScreen,
    EditUserProfile: EditProfileScreen
  },
  {
    initialRouteName: "UserProfile",
    transitionConfig: NavigationConfig,
    navigationOptions: {
      header: null
    }
  }
);

export default createAppContainer(
  createStackNavigator(
    {
      Login: LoginScreen,
      Main: {
        screen: MainTabNavigator,
        navigationOptions: ({ navigation }) => {
          header = null;
          return { header };
        }
      },
      NewPost: {
        screen: CreatePostNavigation,
        navigationOptions: ({ navigation }) => {
          header = null;
          return { header };
        }
      },
      View: {
        screen: ViewScreen,
        navigationOptions: ({ navigation }) => {
          header = null;
          return { header };
        }
      },
      ViewPdf: ViewPdfScreen,
      FilesView: FilesViewScreen,
      Profile: ProfileStack
    },
    {
      initialRouteName: "Login",
      transitionConfig: NavigationConfig
    }
  )
);
