import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  TextInput,
  Text
} from "react-native";
import { connect } from "react-redux";
import TabBarIcon from "../../assets/TabBarIcon";
import FireBase from "../../assets/FireBase";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { FEEDBACK_COLLECTION } from "../../assets/FireBaseCollections";

class FeedBackScreen extends Component {
  constructor(props) {
    super(props);
    this._isMounted = true;
    this.state = {
      feedBackText: "",
      inputHeight: 40,
      submitting: false
    };
  }

  onTextInputChange = value => {
    this._isMounted &&
      this.setState({
        feedBackText: value,
        inputHeight: value.length > 35 ? 100 : 40
      });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  submitFeedBack = async () => {
    if (!this.state.submitting) {
      this.setState({
        submitting: true
      });
      let feedback = {
        collegeCode: this.props.user.collegeCode,
        createdAt: new Date(),
        problem: this.state.feedBackText,
        id: this.props.id,
        type: this.props.type,
        uid: this.props.user.uid
      };
      try {
        await FireBase.createDocument(FEEDBACK_COLLECTION, feedback);
      } catch (error) {}
      this._isMounted &&
        this.setState({
          submitting: false
        });
      this.props.close();
      setTimeout(() => {
        Alert.alert("Thanks for the Reporting");
      }, 500);
    }
  };

  render() {
    return (
      <View
        style={StyleSheet.flatten([
          styles.container,
          {
            width: isNaN(this.props.width) ? "100%" : this.props.width,
            height: isNaN(this.props.height)
              ? "65%"
              : (this.props.height * 65) / 100
          }
        ])}
      >
        <KeyboardAwareScrollView
          enableOnAndroid
          keyboardShouldPersistTaps="always"
          extraScrollHeight={50}
        >
          {this.props.title}
          <View
            style={StyleSheet.flatten([
              styles.feedBackInput,
              {
                width: this.props.width,
                height: this.state.inputHeight
              }
            ])}
          >
            <View
              style={{
                alignItems: "flex-start",
                width: isNaN(this.props.width)
                  ? "10%"
                  : (this.props.width * 10) / 100
              }}
            >
              <TabBarIcon
                name="report-problem"
                type="MaterialIcons"
                size={30}
                color="#AD2B0B"
              />
            </View>
            <View
              style={StyleSheet.flatten([
                styles.feedBackInputBox,
                {
                  width: isNaN(this.props.width)
                    ? "80%"
                    : (this.props.width * 80) / 100
                }
              ])}
            >
              <TextInput
                multiline={true}
                onChangeText={value => this.onTextInputChange(value)}
                style={{ fontSize: 18, padding: 10 }}
                value={this.state.feedBackText}
                maxLength={1000}
                spellCheck={true}
                placeholder="Please specify the problem.."
                autoFocus
              />
            </View>
            <View
              style={{
                alignItems: "flex-end",
                width: isNaN(this.props.width)
                  ? "10%"
                  : (this.props.width * 10) / 100
              }}
            >
              <TouchableOpacity
                onPress={this.submitFeedBack}
                disabled={this.state.feedBackText ? false : true}
              >
                <TabBarIcon
                  type="MaterialIcons"
                  name="send"
                  size={30}
                  color={this.state.feedBackText ? "#1B7DE2" : "#D2D2D2"}
                />
              </TouchableOpacity>
            </View>
          </View>
          {this.state.submitting ? (
            <View
              style={StyleSheet.flatten([
                styles.feedBackInput,
                {
                  width: isNaN(this.props.width) ? "100%" : this.props.width
                }
              ])}
            >
              <View style={{ padding: 4 }}>
                <ActivityIndicator size={"large"} />
              </View>
              <Text style={{ padding: 4 }}>Submitting....</Text>
            </View>
          ) : null}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFFFFF"
  },
  feedBackInput: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 10,
    paddingHorizontal: 3
  },
  feedBackInputBox: {
    borderWidth: 1.5,
    borderColor: "#E8E7E7",
    borderRadius: 10
  }
});

const mapStateToProps = state => ({
  width: state.commonInfo.width,
  height: state.commonInfo.height,
  connectionStatus: state.commonInfo.connectionStatus,
  user: state.commonInfo.user
});

export default connect(
  mapStateToProps,
  null
)(FeedBackScreen);
