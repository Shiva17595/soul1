export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const SECURITY_TEXT = ["Public", "Class", "Only me"];
export const SECURITY_ICON_NAME = ["public", "class", "lock"];
export const SECURITY_ICON_TYPE = ["MaterialIcons", "MaterialIcons", "Entypo"];
export const SECURITY_BOTTOM_TEXT = [
  "Anyone in the Organization",
  "Your class in the Organization",
  "Only me",
];
export const GRANTED = "granted";
export const IMAGE = "image";
export const VIDEO = "video";
export const DOCUMENT = "document";
export const FILE = "file";
export const APP_NAME = "APP_NAME";
export const MEDIA = "Media";
export const IMAGES = "Images";
export const VIDEOS = "Videos";
export const AVATAR = "Avatar";
export const DOCUMENTS = "Documents";

export const CHAT_LIMIT = 10;
export const USER_DATA_KEY = "CurrentUserData";
export const PENDING_POST_ITEMS = "PendingPostItems";
export const CLASS_POSTS_DATA_KEY = "classPostsData";
export const INITIAL_LOAD_POST = "InitialLoadPost";
export const POSTS_DATA_KEY = "postsData";
export const DATE_FORMAT = "dd-MM-yyyy";
export const LIMIT = 50;
export const GB3 = 3000000000;
export const MB1 = 1000000;
export const USER_CLASS_DATA = "UserClassData";
export const CLASSES_DATA = "classesData";
export const COURSES_DATA = "coursesData";
export const STUDENTS_BY_CLASS_ID = "studentsByClassID";
export const MASTERS_DATA = "mastersData";
export const CHANNEL_DATA = "channelData";
export const EVENTS_DATA = "eventsData";
export const ASSIGNMENT_DATA = "assignmentData";
export const MY_ASSIGNMENT_DATA = "myAssignmentData";
export const MY_EVENTS_DATA = "myEventsData";
export const URL_MAPS_DATA = "urlMapsData";
export const DOWNLOAD_URL_MAPS_DATA = "downloadUrlMapsData";
export const CONNECTION_ERROR =
  "Please check your internet connection and try again";
export const ERROR_MESSAGE = "Something went wrong. Please try again";
export const LARGE_FILE =
  "File size is large. Please download and view the file.";

export const COLORS = [
  "#5EB3F0",
  "#5EF0D0",
  "#5EF0A4",
  "#B7F05E",
  "#F0F05E",
  "#F0BF5E",
  "#F07D5E",
  "#F05E5E",
  "#965EF0",
  "#F05E97",
];

export const NEXT = "next";
export const PREVIOUS = "previous";
export const SEPERATOR = "###";

export const componentNames = {
  newsFeed: "NewsFeed",
  filesView: "FilesView",
  userProfile: "UserProfile",
};

export const initialState = {
  commonInfo: {
    connectionStatus: true,
    width: "100%",
    height: "100%",
    user: {},
    classes: [],
    studentsByClassId: {},
    masters: [],
    videoId: null,
    courses: [],
  },
  authenticate: {
    authenticateStatus: false,
  },
  newsFeeds: {
    pendingPosts: [],
    newsFeedData: [],
    pendingComments: [],
    classNewsFeedData: [],
  },
  userData: {
    classInfo: null,
  },
  chatData: {
    channels: [],
    messagesByChannel: {},
    pendingMessagesByChannel: {},
  },
  storageData: {
    urlMaps: {},
    downloadFileName: null,
  },
  eventsData: {
    myEvents: [],
    events: [],
  },
  assignmentsData: {
    myAssignments: [],
    assignments: [],
  },
};
