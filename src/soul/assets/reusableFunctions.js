import {
  DATE_FORMAT,
  IMAGE,
  VIDEO,
  MB1,
  ERROR_MESSAGE,
} from "../constants/Variables";
import * as ImageManipulator from "expo-image-manipulator";
import * as DocumentPicker from "expo-document-picker";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import AppPermissions from "../../../lib/AppPermissions";
import { alertValue } from "../../../lib/ReUsableComponents";
import * as Sharing from "expo-sharing";
import FireBase from "./FireBase";
import { ScreenOrientation } from "expo";

export const dateConvertion = (date) => {
  let dateToConvert = getDate(date);
  let diff = (new Date().getTime() - dateToConvert.getTime()) / 1000;
  let day_diff = Math.floor(diff / 86400);

  if (day_diff >= 31) return dateToConvert.toDateString(DATE_FORMAT);

  return (
    (day_diff == 0 &&
      ((diff < 60 && "Just now") ||
        (diff < 120 && "1 minute ago") ||
        (diff < 3600 && Math.floor(diff / 60) + " minutes ago") ||
        (diff < 7200 && "1 hour ago") ||
        (diff < 86400 && Math.floor(diff / 3600) + " hours ago"))) ||
    (day_diff == 1 && "Yesterday") ||
    (day_diff < 7 && day_diff + " days ago") ||
    (day_diff < 31 && Math.ceil(day_diff / 7) + " weeks ago")
  );
};

export function getDate(date) {
  if (date instanceof Date) {
    return date;
  }
  try {
    return date.toDate();
  } catch (error) {
    try {
      return new Date(date);
    } catch (error) {
      return new Date();
    }
  }
}

export function getKey(item, index) {
  return item.uniqueId || `${index}`;
}

export function isEmptyHash(hash) {
  return !hash || JSON.stringify(hash) === "{}";
}

export function isEmptyArray(array) {
  return !array || !array.length;
}

export function convertArrayTohash(array = [], key = "id") {
  let hash = {};
  array.forEach((a) => (hash[a[key]] = a));
  return hash;
}

export function getDateWithOutTime(dateTime) {
  let date = dateTime ? new Date(dateTime) : new Date();
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

export function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function nDaysBackDate(n) {
  let date = new Date();
  date.setDate(date.getDate() - n);
  return date;
}

export function minutesDiff(date1, date2 = new Date()) {
  date1 = getDate(date1);
  date2 = getDate(date2);
  let diffMs = date2 - date1;
  return Math.round(diffMs / 60000);
}

export function daysDiff(date1, date2 = new Date()) {
  let minDiff = minutesDiff(date1, date2);
  return Math.round(minDiff / 86400);
}

export function checkKeyExistsInHash(hash, key) {
  if (!hash || !hash[key]) return false;
  return true;
}

export function filterArray(array, searchString, fields) {
  if (!searchString) return array;
  return array.filter((a) => {
    return fields.some((field) => {
      if (field instanceof Array) {
        return a[field[0]][field[1]]
          .toLowerCase()
          .replace(/\s+/g, " ")
          .includes(searchString);
      } else {
        return a[field]
          .toLowerCase()
          .replace(/\s+/g, " ")
          .includes(searchString);
      }
    });
  });
}

export function findHeight(actualWidth = 1, width = 0, actualHeight = 0) {
  let persentOfWidth = (width / actualWidth) * 100;
  return (actualHeight * persentOfWidth) / 100;
}

export function findWidth(actualHeigth = 1, height = 1, actualWidth = 0) {
  let persentOfHeight = (height / actualHeigth) * 100;
  return (actualWidth * persentOfHeight) / 100;
}

export function unlockScreenOrientation() {
  return new Promise(async (resolve, reject) => {
    try {
      await ScreenOrientation.unlockAsync();
    } catch {}
    resolve();
  });
}

export function switchToLandscape() {
  return new Promise(async (resolve, reject) => {
    try {
      await ScreenOrientation.lockAsync(
        ScreenOrientation.OrientationLock.LANDSCAPE
      );
    } catch {}
    resolve();
  });
}

export function switchToPortrait() {
  return new Promise(async (resolve, reject) => {
    try {
      await ScreenOrientation.lockAsync(
        ScreenOrientation.OrientationLock.PORTRAIT
      );
    } catch {}
    resolve();
  });
}

export function shareFile(url) {
  return new Promise(async (resolve, reject) => {
    let sharingEnabled = await Sharing.isAvailableAsync();
    if (sharingEnabled) {
      Sharing.shareAsync(url)
        .then((result) => {
          console.log(result);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      console.log("not enabled");
    }
  });
}

export function _onOpenCamera() {
  return new Promise(async (resolve, reject) => {
    AppPermissions.checkPermission(Permissions.CAMERA)
      .then(async () => {
        const attachment = await ImagePicker.launchCameraAsync({
          mediaTypes: "All",
          aspect: [4, 3],
          quality: 0.1,
        });
        if (!attachment.cancelled) {
          if (attachment.type == IMAGE) {
            let manipResult = {};
            try {
              manipResult = await ImageManipulator.manipulateAsync(
                attachment.uri,
                [
                  {
                    resize: {
                      height: 500,
                    },
                  },
                ],
                { compress: 0.1, format: ImageManipulator.SaveFormat.PNG }
              );
            } catch (error) {
              manipResult = {};
            }
            resolve({ ...attachment, ...manipResult });
          } else if (attachment.type == VIDEO) {
            resolve(attachment);
          } else {
            alertValue("File type is not supported");
          }
        }
        resolve();
      })
      .catch((error) => {
        alertValue("Camera Permission is needed");
        resolve();
      });
  });
}

export function _onChoosePic(mediaTypes = "All") {
  return new Promise(async (resolve, reject) => {
    AppPermissions.checkPermission(Permissions.CAMERA_ROLL)
      .then(async () => {
        const attachment = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: mediaTypes,
          aspect: [4, 3],
          quality: 0.1,
        });
        if (
          !attachment.cancelled &&
          attachment.height > 0 &&
          attachment.width > 0
        ) {
          if (attachment.type == IMAGE) {
            let manipResult = {};
            let fileInfo = await FireBase.urlToBlob(attachment.uri);
            if (fileInfo._data.size > MB1) {
              try {
                manipResult = await ImageManipulator.manipulateAsync(
                  attachment.uri,
                  [
                    {
                      resize: {
                        height: 500,
                      },
                    },
                  ],
                  { compress: 0.1, format: ImageManipulator.SaveFormat.PNG }
                );
              } catch (error) {
                console.log(error);
                manipResult = {};
              }
            }
            resolve({ ...attachment, ...manipResult });
          } else if (attachment.type == VIDEO) {
            resolve(attachment);
          } else {
            alertValue("File type is not supported");
          }
        }
        resolve();
      })
      .catch((error) => {
        alertValue("Camera Permission is needed");
        resolve();
      });
  });
}

export function _onChooseDocument() {
  return new Promise(async (resolve, reject) => {
    DocumentPicker.getDocumentAsync({
      type: "application/pdf",
    })
      .then((attactment) => {
        if (attactment.type == "success") resolve(attactment);
      })
      .catch((error) => {
        alertValue(ERROR_MESSAGE);
        resolve();
      });
  });
}

export function findExtention(uri) {
  return uri
    .split(/[\s.]+/)
    .pop()
    .split("?")[0];
}

export function jsonDig(json, keys = []) {
  if (isEmptyHash(json) || isEmptyArray(keys)) return null;
  let val = json || {};
  for (let i = 0; i < keys.length; i++) {
    val = val[keys[i]];
    if (!val) break;
  }
  return val;
}
