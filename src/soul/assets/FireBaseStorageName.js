export const NEWS_FEED_FILES = "newsFeed";
export const EVENTS_FILES = "events";
export const ASSIGNMENT_FILES = "assignments";
export const COMMENTS_FILES = "comments";
export const PROFILE_PICTURES_FILES = "profilePictures";
export const CHANNELS_FILES = "channels";
