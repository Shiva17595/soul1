import React, { Component } from "react";
import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  EvilIcons,
  MaterialIcons,
  Entypo,
  FontAwesome,
  Feather
} from "@expo/vector-icons";
import Colors from "../constants/Colors";

export default class TabBarIcon extends Component {
  setIcon() {
    let color = this.props.focused
      ? Colors.tabIconSelected
      : Colors.tabIconDefault;
    let CustomIcon;
    switch (this.props.type) {
      case "Feather":
        CustomIcon = (
          <Feather
            name={this.props.name}
            size={this.props.size ? this.props.size : 26}
            style={[this.props.styles, { marginBottom: -3 }]}
            color={
              this.props.focused
                ? color
                : this.props.color
                ? this.props.color
                : Colors.tabIconDefault
            }
          />
        );
        break;
      case "Entypo":
        CustomIcon = (
          <Entypo
            name={this.props.name}
            size={this.props.size ? this.props.size : 26}
            style={[this.props.styles, { marginBottom: -3 }]}
            color={
              this.props.focused
                ? color
                : this.props.color
                ? this.props.color
                : Colors.tabIconDefault
            }
          />
        );
        break;
      case "AntDesign":
        CustomIcon = (
          <AntDesign
            name={this.props.name}
            size={this.props.size ? this.props.size : 26}
            style={[this.props.styles, { marginBottom: -3 }]}
            color={
              this.props.focused
                ? color
                : this.props.color
                ? this.props.color
                : Colors.tabIconDefault
            }
          />
        );
        break;
      case "MaterialCommunityIcons":
        CustomIcon = (
          <MaterialCommunityIcons
            name={this.props.name}
            size={this.props.size ? this.props.size : 26}
            style={[this.props.styles, { marginBottom: -3 }]}
            color={
              this.props.focused
                ? color
                : this.props.color
                ? this.props.color
                : Colors.tabIconDefault
            }
          />
        );
        break;
      case "EvilIcons":
        CustomIcon = (
          <EvilIcons
            name={this.props.name}
            size={this.props.size ? this.props.size : 26}
            style={[this.props.styles, { marginBottom: -3 }]}
            color={
              this.props.focused
                ? color
                : this.props.color
                ? this.props.color
                : Colors.tabIconDefault
            }
          />
        );
        break;
      case "MaterialIcons":
        CustomIcon = (
          <MaterialIcons
            name={this.props.name}
            size={this.props.size ? this.props.size : 26}
            style={[this.props.styles, { marginBottom: -3 }]}
            color={
              this.props.focused
                ? color
                : this.props.color
                ? this.props.color
                : Colors.tabIconDefault
            }
          />
        );
        break;
      case "FontAwesome":
        CustomIcon = (
          <FontAwesome
            name={this.props.name}
            size={this.props.size ? this.props.size : 26}
            style={[this.props.styles, { marginBottom: -3 }]}
            color={
              this.props.focused
                ? color
                : this.props.color
                ? this.props.color
                : Colors.tabIconDefault
            }
          />
        );
        break;
      default:
        CustomIcon = (
          <Ionicons
            name={this.props.name}
            size={this.props.size ? this.props.size : 26}
            style={[this.props.styles, { marginBottom: -3 }]}
            color={
              this.props.focused
                ? color
                : this.props.color
                ? this.props.color
                : Colors.tabIconDefault
            }
          />
        );
    }
    return CustomIcon;
  }
  render() {
    return this.setIcon();
  }
}
