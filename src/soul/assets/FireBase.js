import React, { Component } from "react";
import * as firebase from "firebase";
import "@firebase/firestore";
import { LIMIT } from "../constants/Variables";

export default class FireBase extends Component {
  static init = async () => {
    const config = {
      apiKey: "AIzaSyAIuDLi0a0MW7ABr-ZUrSWNI89hzEhtkWA",
      authDomain: "awesomeproject-197a1.firebaseapp.com",
      databaseURL: "https://awesomeproject-197a1.firebaseio.com",
      projectId: "awesomeproject-197a1",
      storageBucket: "awesomeproject-197a1.appspot.com",
      messagingSenderId: "409399296531",
    };

    // const config = {
    //   apiKey: "AIzaSyACl1gLwZLJ5wlhUD8Nxiy1hAC39sF-PYc",
    //   authDomain: "alternate-f0ed6.firebaseapp.com",
    //   databaseURL: "https://alternate-f0ed6.firebaseio.com",
    //   projectId: "alternate-f0ed6",
    //   storageBucket: "alternate-f0ed6.appspot.com",
    //   messagingSenderId: "834085256154",
    //   appId: "1:834085256154:web:977e6727a196a7af"
    // };

    this.app = await firebase.initializeApp(config);
    this.database = firebase.firestore();
  };

  static authentication(email, password) {
    return new Promise(async (resolve, reject) => {
      if (!FireBase._appInitialised()) {
        await FireBase.init();
      }
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(
          (res) => {
            if (res) {
              resolve(res);
            } else {
              reject();
            }
          },
          (error) => {
            reject(error);
          }
        )
        .catch((error) => {
          reject(error);
        });
    });
  }

  static onAuthStateChanged() {
    return new Promise(async (resolve, reject) => {
      if (!FireBase._appInitialised()) {
        await FireBase.init();
      }
      firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
          resolve(user);
        } else {
          reject(false);
        }
      });
    });
  }

  static getCollectionRef(collection) {
    return this.database.collection(collection);
  }

  static applyConditions(collection, orders, document, conditions) {
    let collectionRef = FireBase.getCollectionRef(collection);
    if (orders) {
      for (order in orders) {
        collectionRef = collectionRef.orderBy(order, orders[order]);
      }
    }
    if (document && document.exists) {
      collectionRef = collectionRef.startAfter(document);
    }
    if (conditions) {
      for (condition in conditions) {
        collectionRef = collectionRef.where(
          condition,
          conditions[condition].type,
          conditions[condition].value
        );
      }
    }
    return collectionRef;
  }

  static getListener(
    collection,
    orders,
    document,
    conditions,
    options = {},
    callback
  ) {
    return FireBase.applyConditions(
      collection,
      orders,
      document,
      conditions
    ).onSnapshot(options, callback);
  }

  static getCollection(collection, orders, document, conditions, limit) {
    return new Promise((resolve, reject) => {
      let documentLimit = limit ? limit : LIMIT;
      FireBase.applyConditions(collection, orders, document, conditions)
        .limit(documentLimit)
        .get()
        .then((querySnapshot) => {
          let documents = [];
          documents = querySnapshot.docs.map((document) => {
            let doc = document.data();
            doc.uniqueId = document.id;
            return doc;
          });
          resolve(documents);
        })
        .catch((error) => {
          console.log(collection, orders, conditions, limit);
          console.log(error);
          resolve(null);
        });
    });
  }

  static getDocumentInfo(collection, documentId) {
    return new Promise((resolve, reject) => {
      FireBase.getDocument(collection, documentId)
        .then((document) => {
          let doc = document.data();
          doc.uniqueId = document.id;
          resolve(doc);
        })
        .catch((error) => {
          console.log(collection);
          console.log(documentId);
          console.log(error);
          reject(undefined);
        });
    });
  }

  static updateDocumentInfo(collection, documentId, docObject) {
    return new Promise((resolve, reject) => {
      FireBase.getCollectionRef(collection)
        .doc(documentId)
        .update(docObject)
        .then(function () {
          resolve();
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  }

  static getDocumentReference(document) {
    return this.database.doc(document);
  }

  static filedValueIncrement(value) {
    return firebase.firestore.FieldValue.increment(value);
  }

  static filedValueArrayUnion(values) {
    return firebase.firestore.FieldValue.arrayUnion(values);
  }

  static filedValueArrayRemove(values) {
    return firebase.firestore.FieldValue.arrayRemove(values);
  }

  static getDocument(collection, documentId) {
    return new Promise((resolve, reject) => {
      FireBase.getCollectionRef(collection)
        .doc(documentId)
        .get()
        .then(function (querySnapshot) {
          resolve(querySnapshot);
        })
        .catch((error) => {
          console.log(error);
          reject(undefined);
        });
    });
  }

  static logout() {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signOut()
        .then(
          function () {
            resolve();
          },
          function (error) {
            reject();
          }
        );
    });
  }
  static _appInitialised() {
    return firebase.apps.length;
  }

  static _createUser(params) {
    return new Promise((resolve, reject) => {
      try {
        FireBase.getCollectionRef("users")
          .doc(params.user.uid)
          .set({
            email: params.user.email,
            rollno: params.user.email,
            is_student: true,
          })
          .then(function (docRef) {
            resolve();
          })
          .catch(function (error) {
            console.log(error);
            resolve();
          });
      } catch (error) {}
    });
  }

  static generateDocumentId(collection) {
    return FireBase.getCollectionRef(collection).doc().id;
  }

  static getOffsetDoc(collectionName, docId) {
    return new Promise(async (resolve, reject) => {
      let document;
      try {
        if (docId) document = await this.getDocument(collectionName, docId);
      } catch (error) {
        document = null;
      }
      resolve(document);
    });
  }

  static createDocument(collection, docObject, documentId) {
    return new Promise((resolve, reject) => {
      try {
        let document;
        if (documentId) {
          document = FireBase.getCollectionRef(collection).doc(documentId);
        } else {
          document = FireBase.getCollectionRef(collection).doc();
        }
        document
          .set(docObject)
          .then(() => {
            resolve(document);
          })
          .catch(function (error) {
            console.log(error);
            reject(undefined);
          });
      } catch (error) {
        console.log(error);
        reject(undefined);
      }
    });
  }

  static urlToBlob(url) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function () {
        reject("Network request failed");
      };

      // xhr.onreadystatechange = () => {
      //   if (xhr.readyState === 4) {
      //     resolve(xhr.response);
      //   }
      // };
      xhr.open("GET", url);
      xhr.responseType = "blob"; // convert type
      xhr.send();
    });
  }

  static storageRef() {
    return firebase.storage();
  }

  static getFileMetaData(uri) {
    return new Promise(async (resolve, reject) => {
      FireBase.storageRef()
        .refFromURL(uri)
        .getMetadata()
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          console.log(error);
          resolve();
        });
    });
  }

  static uploadFile(uri, path) {
    return new Promise(async (resolve, reject) => {
      for (let i = 0; i < 3; i++) {
        try {
          const fetchUriResponse = await this.urlToBlob(uri);
          if (!fetchUriResponse._data.type) {
            console.log("blob", uri);
            console.log("blob", path);
            throw "Invalid Blob";
          }
          let ref = FireBase.storageRef().ref().child(path);
          await ref.put(fetchUriResponse);
          let downloadURL = await ref.getDownloadURL();
          resolve(downloadURL);
          break;
        } catch (error) {
          console.log(error);
          if (i == 2) {
            resolve(undefined);
          }
        }
      }
    });
  }
}
