export const COLLEGE_COLLECTION = "colleges";
export const BATCHES_COLLECTION = "batches";
export const BRANCHES_COLLECTION = "branches";
export const SUBJECTS_COLLECTION = "subjects";
export const FEEDBACK_COLLECTION = "feedbacks";
export const NEWS_FEED_COLLECTION = "newsFeed";
export const COMMENTS_COLLECTION = "comments";
export const USERS_COLLECTIONS = "users";
export const CLASSES_COLLECTION = "classes";
export const EVENTS_COLLECTION = "events";
export const ASSIGNMENTS_COLLECTION = "assignments";
export const STUDENTS_COLLECTION = "students";
export const MASTER_COLLECTION = "masters";
export const ATTENDENCE_COLLECTION = "attendance";
export const ACADEMICS_COLLECTION = "academics";
export const CHANNEL_COLLECTION = "channels";
export const DEGREE_CATEGORY_COLLECTION = "degreeCategories";
export const DEGREE_COLLECTION = "degrees";
export const MESSAGE_COLLECTION = "messages";

export const NEWS_FEED_FILES = "newsFeed";
export const COMMENTS_FILES = "comments";

export const NEWS_FEED_FIELDS = {
  noOfComments: "noOfComments",
  noOfLikes: "noOfLikes",
  likedUsers: "likedUsers",
  security: "security",
  uploadedDate: "uploadedDate",
  classInfo: "classInfo",
  classId: "classId",
  className: "className",
  user: "user",
};

export const CHAT_FIELDS = {
  createdAt: "createdAt",
};

export const ACADEMICS_FIELDS = {
  createdAt: "createdAt",
};

export const CHANNEL_FIELDS = {
  createdAt: "createdAt",
  updatedAt: "updatedAt",
  memberIds: "memberIds",
  active: "active",
};

export const COMMENT_FIELDS = {
  createdAt: "createdAt",
};

export const STUDENT_FIELDS = {
  attendenceInfo: "attendenceInfo",
  attendencePersent: "attendencePersent",
  noOfClassesAttended: "noOfClassesAttended",
  noOfClassesScheduled: "noOfClassesScheduled",
  className: "className",
  overallPersent: "overallPersent",
  avatar: "avatar",
  branchInfo: "branchInfo",
  classInfo: "classInfo",
  classId: "classId",
  name: "name",
  presentYear: "presentYear",
  rollNo: "rollNo",
  chatVisibility: "chatVisibility",
};

export const MASTER_FIELDS = {
  className: "className",
  avatar: "avatar",
  classInfo: "classInfo",
  classId: "classId",
  name: "name",
  schedule: "schedule",
  classes: "classes",
};

export const EVENTS_FIELDS = {
  name: "name",
  date: "date",
  classIds: "classIds",
  user: "user",
};

export const ASSIGNMENTS_FIELDS = {
  name: "name",
  createdAt: "createdAt",
  classIds: "classIds",
  user: "user",
};
