import { Audio } from "expo-av";

class AudioSound {
  constructor() {
    this.likeSound = new Audio.Sound();
    this.likeSound.loadAsync(require("./like.mp3"));
  }

  async playLikeSound() {
    // await this.likeSound.playAsync();
    await this.likeSound.replayAsync();
  }
}

// create instance
const SoundService = new AudioSound();

// lock instance
Object.freeze(SoundService);

export default SoundService;
