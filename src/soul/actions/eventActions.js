import CommonStorageObject from "../services/CommonAsyncStorageService";
import { EVENTS_DATA, MY_EVENTS_DATA } from "../constants/Variables";
import { EVENTS_INFO, MY_EVENTS_INFO } from "./types";

export const setEvents = (events) => (dispatch) => {
  CommonStorageObject.set(EVENTS_DATA, events);
  dispatch({
    type: EVENTS_INFO,
    events: events,
  });
};

export const getEvents = () => async (dispatch) => {
  let events = (await CommonStorageObject.get(EVENTS_DATA)) || [];
  dispatch({
    type: EVENTS_INFO,
    events: events,
  });
};

export const setMyEvents = (events) => (dispatch) => {
  CommonStorageObject.set(MY_EVENTS_DATA, events);
  dispatch({
    type: MY_EVENTS_INFO,
    events: events,
  });
};

export const getMyEvents = () => async (dispatch) => {
  let events = (await CommonStorageObject.get(MY_EVENTS_DATA)) || [];
  dispatch({
    type: MY_EVENTS_INFO,
    events: events,
  });
};
