import { AUTHENTICATE_USER, USER_LOGOUT } from "./types";
import FireBase from "../assets/FireBase";

export const authenticateUser = (email, password) => dispatch => {
  let response = FireBase.authentication(email, password);
  dispatch({
    type: AUTHENTICATE_USER,
    authenticateStatus: response
  });
};

export const logout = () => dispatch => {
  dispatch({
    type: USER_LOGOUT
  });
};
