import { URL_MAPS, DOWNLOAD_VIDEO, DOWNLOAD_URL_MAPS } from "./types";
import { URL_MAPS_DATA, DOWNLOAD_URL_MAPS_DATA } from "../constants/Variables";
import CommonStorageObject from "../services/CommonAsyncStorageService";
import LocalStorageService from "../services/LocalStorageService";
import { daysDiff } from "../assets/reusableFunctions";

export const setUrlMaps = (urlMaps) => (dispatch) => {
  CommonStorageObject.set(URL_MAPS_DATA, urlMaps);
  dispatch({
    type: URL_MAPS,
    urlMaps: urlMaps,
  });
};

export const setDownloadUrlMaps = (urlMaps) => (dispatch) => {
  CommonStorageObject.set(DOWNLOAD_URL_MAPS_DATA, urlMaps);
  dispatch({
    type: DOWNLOAD_URL_MAPS,
    downloadUrlMaps: urlMaps,
  });
};

function filterUrlMaps(urlMaps) {
  let newUrlMaps = {};
  Object.keys(urlMaps).forEach((name) => {
    let urlMap = urlMaps[name];
    if (daysDiff(urlMap.downloadStartedAt) < 7) {
      newUrlMaps[name] = { ...urlMap };
    } else {
      LocalStorageService.deleteFile(urlMap.localUri);
    }
  });
  return newUrlMaps;
}

export const getDownloadUrlMaps = () => async (dispatch) => {
  let urlMaps = (await CommonStorageObject.get(DOWNLOAD_URL_MAPS_DATA)) || {};
  urlMaps = filterUrlMaps(urlMaps);
  dispatch({
    type: DOWNLOAD_URL_MAPS,
    downloadUrlMaps: urlMaps,
  });
};

export const getUrlMaps = () => async (dispatch) => {
  let urlMaps = (await CommonStorageObject.get(URL_MAPS_DATA)) || {};
  urlMaps = filterUrlMaps(urlMaps);
  dispatch({
    type: URL_MAPS,
    urlMaps: urlMaps,
  });
};

export const setDownloadFileName = (fileName) => async (dispatch) => {
  dispatch({
    type: DOWNLOAD_VIDEO,
    downloadFileName: fileName,
  });
};
