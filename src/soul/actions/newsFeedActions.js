import {
  POSTS_DATA,
  POSTS_AND_PENDING_POSTS,
  PENDING_POSTS,
  POSTS,
  CLASS_POSTS,
  POSTS_AND_CLASS_POSTS
} from "./types";
import {
  PENDING_POST_ITEMS,
  POSTS_DATA_KEY,
  CLASS_POSTS_DATA_KEY
} from "../constants/Variables";
import CommonStorageObject from "../services/CommonAsyncStorageService";

export const setPendingPostItems = pendingPosts => dispatch => {
  CommonStorageObject.set(PENDING_POST_ITEMS, pendingPosts);
  dispatch({
    type: PENDING_POSTS,
    pendingPosts: pendingPosts
  });
};

export const getPendingPostItems = () => async dispatch => {
  let pendingPosts = (await CommonStorageObject.get(PENDING_POST_ITEMS)) || [];
  dispatch({
    type: PENDING_POSTS,
    pendingPosts: pendingPosts
  });
};

export const setClassPosts = classNewsFeedData => dispatch => {
  CommonStorageObject.set(CLASS_POSTS_DATA_KEY, classNewsFeedData);
  dispatch({
    type: CLASS_POSTS,
    classNewsFeedData: classNewsFeedData
  });
};

export const setPosts = newsFeedData => dispatch => {
  CommonStorageObject.set(POSTS_DATA_KEY, newsFeedData);
  dispatch({
    type: POSTS,
    newsFeedData: newsFeedData
  });
};

export const setPostsAndClassPosts = (
  newsFeedData,
  classNewsFeedData
) => dispatch => {
  CommonStorageObject.set(POSTS_DATA_KEY, newsFeedData);
  CommonStorageObject.set(CLASS_POSTS_DATA_KEY, classNewsFeedData);
  dispatch({
    type: POSTS_AND_CLASS_POSTS,
    newsFeedData: newsFeedData,
    classNewsFeedData: classNewsFeedData
  });
};

export const getPosts = () => async dispatch => {
  let newsFeedData = (await CommonStorageObject.get(POSTS_DATA_KEY)) || [];
  dispatch({
    type: POSTS,
    newsFeedData: newsFeedData
  });
};

export const getAllPosts = () => async dispatch => {
  let newsFeedData = (await CommonStorageObject.get(POSTS_DATA_KEY)) || [];
  let pendingPosts = (await CommonStorageObject.get(PENDING_POST_ITEMS)) || [];
  let classNewsFeedData =
    (await CommonStorageObject.get(CLASS_POSTS_DATA_KEY)) || [];
  dispatch({
    type: POSTS_DATA,
    newsFeedData: newsFeedData,
    pendingPosts: pendingPosts,
    classNewsFeedData: classNewsFeedData
  });
};

export const setPostsAndPendingPost = (
  pendingPosts,
  newsFeedData
) => dispatch => {
  CommonStorageObject.set(PENDING_POST_ITEMS, pendingPosts);
  dispatch({
    type: POSTS_AND_PENDING_POSTS,
    pendingPosts: pendingPosts,
    newsFeedData: newsFeedData
  });
};
