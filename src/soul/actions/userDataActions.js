import { USER_CLASS_INFO } from "./types";
import { USER_CLASS_DATA } from "../constants/Variables";
import CommonStorageObject from "../services/CommonAsyncStorageService";

export const setUserClassInfo = classInfo => dispatch => {
  CommonStorageObject.set(USER_CLASS_DATA, classInfo);
  dispatch({
    type: USER_CLASS_INFO,
    classInfo: classInfo
  });
};

export const getUserClassInfo = () => async dispatch => {
  let classInfo = (await CommonStorageObject.get(USER_CLASS_DATA)) || {};
  dispatch({
    type: USER_CLASS_INFO,
    classInfo: classInfo
  });
};
