import CommonStorageObject from "../services/CommonAsyncStorageService";
import { ASSIGNMENT_DATA, MY_ASSIGNMENT_DATA } from "../constants/Variables";
import { ASSIGNMENT_INFO, MY_ASSIGNMENT_INFO } from "./types";

export const setAssignments = (assignments) => (dispatch) => {
  CommonStorageObject.set(ASSIGNMENT_DATA, assignments);
  dispatch({
    type: ASSIGNMENT_INFO,
    assignments: assignments,
  });
};

export const getAssignments = () => async (dispatch) => {
  let assignments = (await CommonStorageObject.get(ASSIGNMENT_DATA)) || [];
  dispatch({
    type: ASSIGNMENT_INFO,
    assignments: assignments,
  });
};

export const setMyAssignments = (assignments) => (dispatch) => {
  CommonStorageObject.set(MY_ASSIGNMENT_DATA, assignments);
  dispatch({
    type: MY_ASSIGNMENT_INFO,
    assignments: assignments,
  });
};

export const getMyAssignments = () => async (dispatch) => {
  let assignments = (await CommonStorageObject.get(MY_ASSIGNMENT_DATA)) || [];
  dispatch({
    type: MY_ASSIGNMENT_INFO,
    assignments: assignments,
  });
};
