import { CHANNEL_INFO, MESSAGE_INFO, PENDING_MESSAGE_INFO } from "./types";
import { CHANNEL_DATA } from "../constants/Variables";
import CommonStorageObject from "../services/CommonAsyncStorageService";

export const setChannels = channels => dispatch => {
  channels.sort((c1, c2) => (c1.updatedAt > c2.updatedAt ? -1 : 1));
  CommonStorageObject.set(CHANNEL_DATA, channels);
  dispatch({
    type: CHANNEL_INFO,
    channels: channels
  });
};

export const getChannels = () => async dispatch => {
  let channels = (await CommonStorageObject.get(CHANNEL_DATA)) || [];
  dispatch({
    type: CHANNEL_INFO,
    channels: channels
  });
};

export const setChatMessages = (channelId, messages) => dispatch => {
  messages.sort((m1, m2) => (m1.createdAt > m2.createdAt ? -1 : 1));
  CommonStorageObject.set(channelId, messages);
  dispatch({
    type: MESSAGE_INFO,
    messages: messages,
    channelId: channelId
  });
};

export const getChatMessages = channelId => async dispatch => {
  let messages = (await CommonStorageObject.get(channelId)) || [];
  dispatch({
    type: MESSAGE_INFO,
    messages: messages,
    channelId: channelId
  });
};

export const setPendingChatMessages = (channelId, messages) => dispatch => {
  CommonStorageObject.set(`${channelId}-PENDING`, messages);
  dispatch({
    type: PENDING_MESSAGE_INFO,
    messages: messages,
    channelId: channelId
  });
};

export const getPendingChatMessages = channelId => async dispatch => {
  let messages = (await CommonStorageObject.get(`${channelId}-PENDING`)) || [];
  dispatch({
    type: PENDING_MESSAGE_INFO,
    messages: messages,
    channelId: channelId
  });
};
