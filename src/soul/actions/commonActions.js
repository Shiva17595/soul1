import {
  CONNECTION_INFO,
  DIMENSIONS_INFO,
  USER_INFO,
  CLASSES_INFO,
  STUDENTS_INFO,
  MASTERS_INFO,
  VIDEO_PLAY,
  COURSES_INFO,
  VIEW_NAME,
} from "./types";
import {
  CLASSES_DATA,
  STUDENTS_BY_CLASS_ID,
  MASTERS_DATA,
  COURSES_DATA,
} from "../constants/Variables";
import CommonStorageObject from "../services/CommonAsyncStorageService";
import CurrentUser from "../services/CurrentUserDataService";

export const connectionInfo = (connectionStatus) => (dispatch) => {
  dispatch({
    type: CONNECTION_INFO,
    connectionStatus: connectionStatus,
  });
};

export const setVideoId = (videoId) => (dispatch) => {
  dispatch({
    type: VIDEO_PLAY,
    videoId: videoId,
  });
};

export const setCurrentView = (viewName) => (dispatch) => {
  dispatch({
    type: VIEW_NAME,
    viewName: viewName,
  });
};

export const dimensionsInfo = (width, height) => (dispatch) => {
  dispatch({
    type: DIMENSIONS_INFO,
    width: width,
    height: height,
  });
};

export const userInfo = (user) => (dispatch) => {
  CurrentUser.set(user);
  dispatch({
    type: USER_INFO,
    user: user,
  });
};

export const setCourses = (courses) => (dispatch) => {
  CommonStorageObject.set(COURSES_DATA, courses);
  dispatch({
    type: COURSES_INFO,
    courses: courses,
  });
};

export const getCourses = () => async (dispatch) => {
  let courses = (await CommonStorageObject.get(COURSES_DATA)) || [];
  dispatch({
    type: COURSES_INFO,
    courses: courses,
  });
};

export const setClassesInfo = (classes) => (dispatch) => {
  CommonStorageObject.set(CLASSES_DATA, classes);
  dispatch({
    type: CLASSES_INFO,
    classes: classes,
  });
};

export const getClassesInfo = () => async (dispatch) => {
  let classes = (await CommonStorageObject.get(CLASSES_DATA)) || [];
  dispatch({
    type: CLASSES_INFO,
    classes: classes,
  });
};

export const setStudentsData = (studentsByClassId) => (dispatch) => {
  CommonStorageObject.set(STUDENTS_BY_CLASS_ID, studentsByClassId);
  dispatch({
    type: STUDENTS_INFO,
    studentsByClassId: studentsByClassId,
  });
};

export const getStudentsData = () => async (dispatch) => {
  let studentsByClassId =
    (await CommonStorageObject.get(STUDENTS_BY_CLASS_ID)) || {};
  dispatch({
    type: STUDENTS_INFO,
    studentsByClassId: studentsByClassId,
  });
};

export const setMastersData = (masters) => (dispatch) => {
  CommonStorageObject.set(MASTERS_DATA, masters);
  dispatch({
    type: MASTERS_INFO,
    masters: masters,
  });
};

export const getMastersData = () => async (dispatch) => {
  let masters = (await CommonStorageObject.get(MASTERS_DATA)) || [];
  dispatch({
    type: MASTERS_INFO,
    masters: masters,
  });
};
