import { AUTHENTICATE_USER } from "../actions/types";
import { initialState } from "../constants/Variables";

const initialProps = initialState.authenticate;

export default function(state = initialProps, action) {
  switch (action.type) {
    case AUTHENTICATE_USER:
      return {
        authenticateStatus: true
      };
    default:
      return false;
  }
}
