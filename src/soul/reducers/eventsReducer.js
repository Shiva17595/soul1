import { EVENTS_INFO, MY_EVENTS_INFO } from "../actions/types";
import { initialState } from "../constants/Variables";

const initialProps = initialState.eventsData;
export default function (state = initialProps, action) {
  switch (action.type) {
    case EVENTS_INFO:
      return {
        ...state,
        events: action.events,
      };
    case MY_EVENTS_INFO:
      return {
        ...state,
        myEvents: action.events,
      };
    default:
      return state;
  }
}
