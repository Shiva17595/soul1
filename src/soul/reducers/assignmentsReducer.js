import { ASSIGNMENT_INFO, MY_ASSIGNMENT_INFO } from "../actions/types";
import { initialState } from "../constants/Variables";

const initialProps = initialState.assignmentsData;
export default function (state = initialProps, action) {
  switch (action.type) {
    case ASSIGNMENT_INFO:
      return {
        ...state,
        assignments: action.assignments,
      };
    case MY_ASSIGNMENT_INFO:
      return {
        ...state,
        myAssignments: action.assignments,
      };
    default:
      return state;
  }
}
