import {
  CHANNEL_INFO,
  MESSAGE_INFO,
  PENDING_MESSAGE_INFO
} from "../actions/types";
import { initialState } from "../constants/Variables";

const initialProps = initialState.chatData;

export default function(state = initialProps, action) {
  switch (action.type) {
    case CHANNEL_INFO:
      return {
        ...state,
        channels: action.channels
      };
    case MESSAGE_INFO:
      return {
        ...state,
        messagesByChannel: {
          ...state.messagesByChannel,
          ...{ [action.channelId]: action.messages }
        }
      };
    case PENDING_MESSAGE_INFO:
      return {
        ...state,
        pendingMessagesByChannel: {
          ...state.pendingMessagesByChannel,
          ...{ [action.channelId]: action.messages }
        }
      };
    default:
      return state;
  }
}
