import {
  PENDING_POSTS,
  PENDING_COMMENTS,
  POSTS,
  POSTS_AND_PENDING_POSTS,
  POSTS_DATA,
  CLASS_POSTS,
  POSTS_AND_CLASS_POSTS
} from "../actions/types";
import { initialState } from "../constants/Variables";

const initialProps = initialState.newsFeeds;

export default function(state = initialProps, action) {
  switch (action.type) {
    case POSTS:
      return {
        ...state,
        newsFeedData: action.newsFeedData
      };
    case CLASS_POSTS:
      return {
        ...state,
        classNewsFeedData: action.classNewsFeedData
      };
    case POSTS_AND_CLASS_POSTS:
      return {
        ...state,
        newsFeedData: action.newsFeedData,
        classNewsFeedData: action.classNewsFeedData
      };
    case POSTS_DATA:
      return {
        ...state,
        newsFeedData: action.newsFeedData,
        pendingPosts: action.pendingPosts,
        classNewsFeedData: action.classNewsFeedData
      };
    case PENDING_POSTS:
      return {
        ...state,
        pendingPosts: action.pendingPosts
      };
    case PENDING_COMMENTS:
      return {
        ...state,
        pendingComments: action.pendingComments
      };
    case POSTS_AND_PENDING_POSTS:
      return {
        ...state,
        newsFeedData: action.newsFeedData,
        pendingPosts: action.pendingPosts
      };
    default:
      return state;
  }
}
