import { USER_CLASS_INFO } from "../actions/types";
import { initialState } from "../constants/Variables";

const initialProps = initialState.userData;

export default function(state = initialProps, action) {
  switch (action.type) {
    case USER_CLASS_INFO:
      return {
        ...state,
        classInfo: action.classInfo
      };

    default:
      return state;
  }
}
