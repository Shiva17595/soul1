import { combineReducers } from "redux";
import authenticateReducer from "./authenticateReducer";
import commonReducers from "./commonReducers";
import newsFeedReducer from "./newsFeedReducer";
import { USER_LOGOUT } from "../actions/types";
import userDataReducer from "./userDataReducer";
import chatDataReducer from "./chatReducer";
import { initialState } from "../constants/Variables";
import storageReducer from "./storageReducer";
import eventsReducer from "./eventsReducer";
import assignmentsReducer from "./assignmentsReducer";

const appReducer = combineReducers({
  authenticate: authenticateReducer,
  commonInfo: commonReducers,
  newsFeeds: newsFeedReducer,
  userData: userDataReducer,
  chatData: chatDataReducer,
  storageData: storageReducer,
  eventsData: eventsReducer,
  assignmentsData: assignmentsReducer,
});

const rootReducer = (state, action) => {
  if (action.type === USER_LOGOUT) {
    state = {
      ...initialState,
      ...{
        commonInfo: {
          connectionStatus: state.commonInfo.connectionStatus,
          width: state.commonInfo.width,
          height: state.commonInfo.height,
          user: {},
          classes: [],
          studentsByClassId: {},
          masters: [],
        },
      },
    };
  }
  return appReducer(state, action);
};
export default rootReducer;
