import {
  CONNECTION_INFO,
  DIMENSIONS_INFO,
  USER_INFO,
  CLASSES_INFO,
  STUDENTS_INFO,
  MASTERS_INFO,
  VIDEO_PLAY,
  COURSES_INFO,
  VIEW_NAME,
} from "../actions/types";
import { initialState } from "../constants/Variables";

const initialProps = initialState.commonInfo;

export default function (state = initialProps, action) {
  switch (action.type) {
    case CONNECTION_INFO:
      return {
        ...state,
        connectionStatus: action.connectionStatus,
      };
    case DIMENSIONS_INFO:
      return {
        ...state,
        width: action.width,
        height: action.height,
      };
    case USER_INFO:
      return {
        ...state,
        user: action.user,
      };
    case CLASSES_INFO:
      return {
        ...state,
        classes: action.classes,
      };
    case STUDENTS_INFO:
      return {
        ...state,
        studentsByClassId: action.studentsByClassId,
      };
    case MASTERS_INFO:
      return {
        ...state,
        masters: action.masters,
      };
    case VIDEO_PLAY:
      return {
        ...state,
        videoId: action.videoId,
      };
    case VIEW_NAME:
      return {
        ...state,
        viewName: action.viewName,
      };
    case COURSES_INFO:
      return {
        ...state,
        courses: action.courses,
      };
    default:
      return state;
  }
}
