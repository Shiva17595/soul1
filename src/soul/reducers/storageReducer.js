import { URL_MAPS, DOWNLOAD_VIDEO, DOWNLOAD_URL_MAPS } from "../actions/types";
import { initialState } from "../constants/Variables";

const initialProps = initialState.storageData;

export default function (state = initialProps, action) {
  switch (action.type) {
    case URL_MAPS:
      return {
        ...state,
        urlMaps: action.urlMaps,
      };
    case DOWNLOAD_VIDEO:
      return {
        ...state,
        downloadFileName: action.downloadFileName,
      };
    case DOWNLOAD_URL_MAPS:
      return {
        ...state,
        downloadUrlMaps: action.downloadUrlMaps,
      };
    default:
      return state;
  }
}
